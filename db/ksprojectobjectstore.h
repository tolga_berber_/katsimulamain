#ifndef KSPROJECTOBJECTSTORE_H
#define KSPROJECTOBJECTSTORE_H

#include "ksobjectstore.h"

class KsLaneObject;
class KsRoadModelObject;
class KsNodeObject;

class KsProjectObjectStore : public KsObjectStore
{
    Q_OBJECT
public:
    explicit KsProjectObjectStore(QString name, QString filePath, QObject *parent=0);
    ~KsProjectObjectStore();

    QList<KsLaneObject*> lanes();
    KsLaneObject *newLane();
    void removeLane(KsLaneObject *lane);

    QList<KsRoadModelObject*> roads();
    KsRoadModelObject *newRoad();
    void removeRoad(KsRoadModelObject *road);

    QList<KsNodeObject*> nodes();
    KsNodeObject *newNode();
    void removeNode(KsNodeObject *node);

protected:
    virtual bool check();
    virtual void createTable(QString table);
    virtual void createFromScratch();

    virtual void insertObject(QObject *q);
    virtual void relationInsert(QObject *q);
    virtual void updateObject(QObject *q);
    virtual void deleteObject(QObject *q);

    virtual void sortNewObjects();

private:
    QList<KsLaneObject*> findRoadLanes(KsRoadModelObject *obj);
    QList<KsLaneObject*> findInputLanes(KsLaneObject *obj);
    QList<KsRoadModelObject*> findRoadInputs(KsNodeObject *node);
    QList<KsRoadModelObject*> findRoadOutputs(KsNodeObject *node);
    KsLaneObject *laneWithId(qint64 id);
    KsRoadModelObject *roadWithId(qint64 id);
};

#endif // KSPROJECTOBJECTSTORE_H
