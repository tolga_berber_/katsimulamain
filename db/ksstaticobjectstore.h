#ifndef KSSTATICOBJECTSTORE_H
#define KSSTATICOBJECTSTORE_H

#include "ksobjectstore.h"
#include <QList>
#include "model/ksvehicle.h"

class KsStaticObjectStore : public KsObjectStore
{
    Q_OBJECT
public:
    KsStaticObjectStore(QString name, QString filePath, QObject *parent=NULL);
    ~KsStaticObjectStore();

    QList<KsVehicle *> vehicles();
    KsVehicle *newVehicle();
    void removeVehicle(KsVehicle *v);

protected:
    virtual bool check();
    virtual void createTable(QString table);
    virtual void createFromScratch();

    virtual void insertObject(QObject *q);
    virtual void relationInsert(QObject *q);
    virtual void updateObject(QObject *q);
    virtual void deleteObject(QObject *q);
};

#endif // KSSTATICOBJECTSTORE_H
