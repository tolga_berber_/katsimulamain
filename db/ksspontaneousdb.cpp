#include "ksspontaneousdb.h"

#include "db/model/simulation/kssimulationinfo.h"
#include "db/model/simulation/kssimulationvehicle.h"
#include "db/model/simulation/kssimulationlane.h"
#include "db/model/simulation/kssimulationlocation.h"
#include "db/model/simulation/kssimulationspeed.h"
#include "db/model/simulation/kssimulationcarbonemissions.h"
#include "db/model/project/ksnodeobject.h"
#include "db/model/ksvehicle.h"
#include "db/model/project/kslaneobject.h"


#include <QSqlError>

KsSpontaneousDB::KsSpontaneousDB(QString name, QString filePath, QObject *parent) : QObject(parent),
    m_strName(name),
    m_strFilePath(filePath)
{
    QSqlDatabase::addDatabase("QSQLITE", m_strName);
    db().setDatabaseName(filePath);
    if(!db().open()) {
    } else {
        prepareDB();
        prepareSqls();
    }
}

KsSpontaneousDB::~KsSpontaneousDB()
{
    db().close();
    QSqlDatabase::removeDatabase(m_strName);
}

void KsSpontaneousDB::beginTransaction()
{
    db().transaction();
}

void KsSpontaneousDB::endTransaction()
{
    db().commit();
}

void KsSpontaneousDB::insertInfo(KsSimulationInfo *inf)
{
    m_qryInfo->bindValue(":duration", inf->duration());
    m_qryInfo->bindValue(":starttime", inf->startTime());
    if(!m_qryInfo->exec()) {
    } else {
        inf->setId(m_qryInfo->lastInsertId().toLongLong());
    }
}

void KsSpontaneousDB::insertVehicle(KsSimulationVehicle *vehicle)
{
    if(vehicle->id()!=-1)
        return;
    m_qryVehicle->bindValue(":infoid", vehicle->info()->id());
    m_qryVehicle->bindValue(":vehicleid", vehicle->vehicle()->id());
    m_qryVehicle->bindValue(":startnode", vehicle->start()->id());
    m_qryVehicle->bindValue(":endnode", vehicle->end()->id());
    if(!m_qryVehicle->exec()) {
    } else {
        vehicle->setId(m_qryVehicle->lastInsertId().toLongLong());
    }
}

void KsSpontaneousDB::insertLane(KsSimulationLane *lane)
{
    m_qryLane->bindValue(":infoid", lane->info()->id());
    m_qryLane->bindValue(":second", lane->second());
    m_qryLane->bindValue(":laneid", lane->lane()->id());
    m_qryLane->bindValue(":vehicleCount", lane->vehicleCount());
    if(!m_qryLane->exec()) {
    }
}

void KsSpontaneousDB::insertLocation(KsSimulationLocation *location)
{
    m_qryLocation->bindValue(":infoid", location->info()->id());
    m_qryLocation->bindValue(":second", location->second());
    m_qryLocation->bindValue(":laneid", location->lane()?location->lane()->id():-1);
    m_qryLocation->bindValue(":vehicle", location->vehicle()->id());
    m_qryLocation->bindValue(":location", location->location());
    if(!m_qryLocation->exec()) {
    }
}

void KsSpontaneousDB::insertSpeed(KsSimulationSpeed *speed)
{
    m_qrySpeed->bindValue(":infoid",   speed->info()->id());
    m_qrySpeed->bindValue(":second",   speed->second());
    m_qrySpeed->bindValue(":vehicle",  speed->vehicle()->id());
    m_qrySpeed->bindValue(":speed",    speed->speed());
    m_qrySpeed->bindValue(":acceleration", speed->acceleration());
    if(!m_qrySpeed->exec()) {
    }
}

void KsSpontaneousDB::insertCarbonEmissions(KsSimulationCarbonEmissions *emission)
{
    m_qryCarbonEmission->bindValue(":infoid",   emission->info()->id());
    m_qryCarbonEmission->bindValue(":second",   emission->second());
    m_qryCarbonEmission->bindValue(":vehicle",  emission->vehicle()->id());
    m_qryCarbonEmission->bindValue(":emission",    emission->carbonEmission());
    if(!m_qryCarbonEmission->exec()) {
    }
}

void KsSpontaneousDB::prepareSqls()
{
    m_qryInfo = new QSqlQuery(db());
    m_qryVehicle = new QSqlQuery(db());
    m_qryLane = new QSqlQuery(db());
    m_qryLocation = new QSqlQuery(db());
    m_qrySpeed = new QSqlQuery(db());
    m_qryCarbonEmission = new QSqlQuery(db());

    m_qryInfo->prepare("insert into info (duration, starttime) "
                       "values (:duration, :starttime)");

    m_qryVehicle->prepare("insert into vehicle (infoid, vehicleid, startnode, endnode) "
                          "values (:infoid, :vehicleid, :startnode, :endnode)");

    m_qryLane->prepare("insert into lane (infoid, second, laneid, vehicleCount) "
                       "values (:infoid, :second, :laneid, :vehicleCount)");

    m_qryLocation->prepare("insert into location (infoid, second, laneid, vehicle, location) "
                           "values (:infoid, :second, :laneid, :vehicle, :location)");

    m_qrySpeed->prepare("insert into speed (infoid, second, vehicle, speed, acceleration) "
                        "values (:infoid, :second, :vehicle, :speed, :acceleration)");

    m_qryCarbonEmission->prepare("insert into carbonEmission (infoid, second, vehicle, emission) "
                                 "values (:infoid, :second, :vehicle, :emission)");

}

void KsSpontaneousDB::prepareDB()
{
    QSqlQuery q(db());
    q.exec("PRAGMA synchronous = OFF");
    q.exec("PRAGMA journal_mode = MEMORY");

    if(db().tables().count()!=6) {
        foreach (QString tbl, db().tables()) {
            q.exec(tr("drop table %1").arg(tbl));
        }

        q.exec("create table info (id integer primary key, duration integer, starttime text)");

        q.exec("create table vehicle (id integer primary key, infoid integer, vehicleid integer, startnode integer, "
               "endnode integer)");

        q.exec("create table lane (id integer primary key, infoid integer, second integer, laneid integer, "
               "vehicleCount integer)");

        q.exec("create table location (id integer primary key, infoid integer, second integer, "
               "laneid integer, vehicle integer, location real)");

        q.exec("create table speed (id integer primary key, infoid integer, second integer, "
               "vehicle integer, speed real, acceleration)");

        q.exec("create table carbonEmission (id integer primary key, infoid integer, second integer, "
               "vehicle integer, emission qreal)");
    }
}

