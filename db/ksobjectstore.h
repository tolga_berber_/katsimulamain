#ifndef KSOBJECTSTORE_H
#define KSOBJECTSTORE_H

#include <QObject>
#include <QSqlDatabase>
#include <QMap>
#include <QVariant>

typedef QMap<QString, QObjectList> CreatedObjectMap;

class KsObjectStore : public QObject
{
    Q_OBJECT
public:
    explicit KsObjectStore(QString name, QString filePath, QObject *parent = 0);
    ~KsObjectStore();

    virtual void saveAll();
    virtual void createAllTables();

    virtual bool hasObjectToSave();

signals:

public slots:
    void open(bool createIfNotValid=true);
    void close();
    void setPath(QString path);
    void setName(QString newName);

private slots:
    void handleChange();

protected:
    QString m_strFilePath;
    QString m_strName;

    virtual QSqlDatabase db();
    virtual bool check()=0;
    virtual void createFromScratch()=0;
    virtual void createTable(QString tableName)=0;
    virtual void dropTable(QString tableName);
    virtual void dropAllTables();
    virtual QStringList tableFields(QString tablename);
    virtual void mapObject(QObject *q);
    virtual void unMapObject(QObject *q);
    virtual void insertObject(QObject *obj)=0;
    virtual void relationInsert(QObject *obj)=0;
    virtual void updateObject(QObject *obj)=0;
    virtual void deleteObject(QObject *obj)=0;

    virtual QString VariantToString(QVariant v);
    virtual QVariant StringToVariant(QString);
    virtual QString PolygonToString(QPolygonF p);
    virtual QPolygonF StringToPolygon(QString str);
    virtual QString PointToString(QPointF pnt);
    virtual QPointF StringToPoint(QString str);

    virtual void sortNewObjects();

    QObjectList m_lstNewObjects;
    QObjectList m_lstDirtyObjects;
    QObjectList m_lstDeletedObjects;
    QObjectList m_lstRelationUpdateObjects;
    CreatedObjectMap m_mapCreatedObjects;
};

#endif // KSOBJECTSTORE_H
