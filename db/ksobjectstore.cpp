#include "ksobjectstore.h"

#include <QStringList>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>
#include <QMetaProperty>
#include <QByteArray>
#include <QDataStream>
#include <QPolygonF>

KsObjectStore::KsObjectStore(QString name, QString filePath, QObject *parent) : QObject(parent),
    m_strFilePath(filePath),
    m_strName(name)
{
    QSqlDatabase::addDatabase("QSQLITE", m_strName);
    db().setDatabaseName(m_strFilePath);
}

KsObjectStore::~KsObjectStore()
{
    foreach (QString classNames, m_mapCreatedObjects.keys()) {
        foreach (QObject *obj, m_mapCreatedObjects[classNames]) {
            delete obj;
        }
    }
}

void KsObjectStore::open(bool createIfNotValid)
{
    if(!db().open()) {
        return;
    } else if (!check()) {
        if(createIfNotValid)
            createFromScratch();
    }
}

void KsObjectStore::close()
{
    QSqlDatabase db = QSqlDatabase::database(m_strName, false);
    if(db.isOpen()) {
        db.close();
    }
}

void KsObjectStore::setPath(QString path)
{
    if(m_strFilePath!=path) {
        m_strFilePath = path;

        db().setDatabaseName(path);
    }
}

void KsObjectStore::setName(QString newName)
{
    if(m_strName!=newName) {
        QSqlDatabase::removeDatabase(m_strName);
        m_strName = newName;
        QSqlDatabase::addDatabase("QSQLITE", m_strName);
    }
}

void KsObjectStore::handleChange()
{
    if(m_lstDeletedObjects.contains(sender())) {
        return;
    }
    if(!m_lstNewObjects.contains(sender())) {
        if(!m_lstDirtyObjects.contains(sender())) {
            m_lstDirtyObjects.append(sender());
        }
    }

}

QSqlDatabase KsObjectStore::db()
{
    return QSqlDatabase::database(m_strName);
}

void KsObjectStore::dropTable(QString tableName)
{
    QSqlQuery q(db());
    q.exec(tr("drop table %1;").arg(tableName));
    q.finish();
}

void KsObjectStore::dropAllTables()
{
    QStringList tables = db().tables();

    foreach (QString tbl, tables) {
        dropTable(tbl);
    }
}

QStringList KsObjectStore::tableFields(QString tablename)
{
    QSqlQuery q(db());
    QStringList result;
    q.exec(tr("PRAGMA table_info(%1);").arg(tablename));
    while(q.next()) {
        result.append(q.value(1).toString());
    }
    q.finish();

    return result;
}

void KsObjectStore::mapObject(QObject *q)
{
    if(m_mapCreatedObjects[q->metaObject()->className()].contains(q)) {
       return;
    }
    const QMetaObject *mObj = q->metaObject();

    for(int i=0;i<mObj->propertyCount();i++) {
        QMetaProperty p = mObj->property(i);
        int idx = metaObject()->indexOfSlot("handleChange()");
        QMetaMethod m = metaObject()->method(idx);
        connect(q, p.notifySignal(),
                this, m);
    }

    m_mapCreatedObjects[q->metaObject()->className()].append(q);
}

void KsObjectStore::unMapObject(QObject *q)
{
    if(!m_mapCreatedObjects[q->metaObject()->className()].contains(q)) {
        return;
    }

    m_mapCreatedObjects[q->metaObject()->className()].removeAll(q);

    if(m_lstNewObjects.contains(q)) {
        m_lstNewObjects.removeAll(q);
        delete q;
    }

    if(m_lstDirtyObjects.contains(q)) {
        m_lstDirtyObjects.removeAll(q);
        m_lstDeletedObjects.append(q);
    }
}

QString KsObjectStore::VariantToString(QVariant v)
{
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::WriteOnly);
    ds<<v;
    QString result(ba.toBase64());
    return result;
}

QVariant KsObjectStore::StringToVariant(QString s)
{
    QByteArray ba = s.toLatin1();
    QDataStream ds(&ba, QIODevice::ReadOnly);
    QVariant v;
    ds>>v;
    return v;
}

QString KsObjectStore::PolygonToString(QPolygonF p)
{
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::WriteOnly);
    ds<<p;
    QByteArray cba = qCompress(ba);
    QString result(cba.toBase64());
    return result;
}

QPolygonF KsObjectStore::StringToPolygon(QString str)
{
    QByteArray cba = str.toLatin1();
    cba = cba.fromBase64(cba);
    QByteArray ba = qUncompress(cba);
    QDataStream ds(&ba, QIODevice::ReadOnly);
    QPolygonF p;
    ds>>p;
    return p;
}

QString KsObjectStore::PointToString(QPointF pnt)
{
    QByteArray ba;
    QDataStream ds(&ba, QIODevice::WriteOnly);
    ds<<pnt;
    QByteArray cba = qCompress(ba);
    QString result(cba.toBase64());
    return result;
}

QPointF KsObjectStore::StringToPoint(QString str)
{
    QByteArray cba = str.toLatin1();
    cba = cba.fromBase64(cba);
    QByteArray ba = qUncompress(cba);
    QDataStream ds(&ba, QIODevice::ReadOnly);
    QPointF pnt;
    ds>>pnt;
    return pnt;
}

void KsObjectStore::sortNewObjects()
{
}

void KsObjectStore::saveAll()
{
    sortNewObjects();
    while(m_lstNewObjects.size()) {
        QObject *obj = m_lstNewObjects.takeFirst();
        insertObject(obj);
    }

    while(m_lstRelationUpdateObjects.size()) {
        QObject *obj = m_lstRelationUpdateObjects.takeFirst();
        relationInsert(obj);
    }

    while(m_lstDirtyObjects.size()) {
        QObject *obj = m_lstDirtyObjects.takeFirst();
        updateObject(obj);
    }

    while(m_lstDeletedObjects.size()) {
        QObject *obj = m_lstDeletedObjects.takeFirst();
        deleteObject(obj);
        delete obj;
    }
}

void KsObjectStore::createAllTables()
{
    createFromScratch();
}

bool KsObjectStore::hasObjectToSave()
{
    return m_lstNewObjects.size()>0 || m_lstDirtyObjects.size()>0 || m_lstDeletedObjects.size()>0;
}

