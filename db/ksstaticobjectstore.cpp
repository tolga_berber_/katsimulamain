#include "ksstaticobjectstore.h"
#include <QSqlQuery>
#include <QStringList>
#include <QVariant>
#include <QSqlError>

KsStaticObjectStore::KsStaticObjectStore(QString name, QString filePath, QObject *parent):KsObjectStore(name,filePath,parent)
{
}

KsStaticObjectStore::~KsStaticObjectStore()
{
}

QList<KsVehicle *> KsStaticObjectStore::vehicles()
{
    QList<KsVehicle*> result;
    if(m_mapCreatedObjects.contains("KsVehicle")) {
        QObjectList lst = m_mapCreatedObjects["KsVehicle"];
        foreach (QObject *obj, lst) {
            result.append(qobject_cast<KsVehicle*>(obj));
        }
    } else {
        QSqlQuery q("select * from vehicles",db());
        q.exec();
        while(q.next()) {
            KsVehicle *v = new KsVehicle;
            v->setID(q.value(0).toLongLong());
            v->setName(q.value(1).toString());
            v->setDescription(q.value(2).toString());
            v->setMaxSpeed(q.value(3).toInt());
            v->setNumberOfSeats(q.value(4).toInt());
            v->setVehicleType((KsVehicle::VehicleType)q.value(5).toInt());
            v->setWeight(q.value(6).toReal());
            v->setHeight(q.value(7).toReal());
            v->setWidth(q.value(8).toReal());
            v->setAxleLoad(q.value(9).toReal());
            v->setCarbonEmission(q.value(10).toReal());
            v->setLength(q.value(11).toReal());
            mapObject(v);
            result.append(v);
        }
    }

    return result;
}

KsVehicle *KsStaticObjectStore::newVehicle()
{
    KsVehicle *v = new KsVehicle;
    m_lstNewObjects.append(v);
    mapObject(v);
    return v;
}

void KsStaticObjectStore::removeVehicle(KsVehicle *v)
{
    unMapObject(v);
}

void KsStaticObjectStore::createTable(QString table)
{
    if(table=="vehicles") {
        QSqlQuery q(db());
        q.prepare("create table vehicles (id integer primary key,"
                  "name text,"
                  "description text,"
                  "maxSpeed integer,"
                  "numberOfSeats integer,"
                  "vehicleType integer,"
                  "weight real,"
                  "height real,"
                  "width real,"
                  "axleLoad real,"
                  "carbonEmission real,"
                  "length real);");
        q.exec();
    }
}

void KsStaticObjectStore::createFromScratch()
{
    dropAllTables();
    createTable("vehicles");
}

void KsStaticObjectStore::insertObject(QObject *q)
{
    if(!qstricmp(q->metaObject()->className(), "KsVehicle")) {
        KsVehicle *v = qobject_cast<KsVehicle*>(q);
        QSqlQuery query(db());
        query.prepare("insert into vehicles (name,description,maxSpeed,numberOfSeats,vehicleType,weight,height,width,axleLoad,carbonEmission,length)"
                      "values (:name, :description, :maxSpeed, :numberOfSeats, :vehicleType, :weight, :height, :width, :axleLoad, :carbonEmission,:length)");
        query.bindValue(":name", v->name());
        query.bindValue(":description", v->description());
        query.bindValue(":maxSpeed", v->maxSpeed());
        query.bindValue(":numberOfSeats", v->numberOfSeats());
        query.bindValue(":vehicleType", v->vehicleType());
        query.bindValue(":weight", v->weight());
        query.bindValue(":height", v->height());
        query.bindValue(":width", v->width());
        query.bindValue(":axleLoad", v->axleLoad());
        query.bindValue(":carbonEmission", v->carbonEmission());
        query.bindValue(":length", v->length());
        if(!query.exec()) {
        } else {
        }
        v->setID(query.lastInsertId().toLongLong());
    }
}

void KsStaticObjectStore::relationInsert(QObject *q)
{
    Q_UNUSED(q);
}

void KsStaticObjectStore::updateObject(QObject *q)
{
    if(!qstricmp(q->metaObject()->className(), "KsVehicle")) {
        KsVehicle *v = qobject_cast<KsVehicle*>(q);
        QSqlQuery query(db());
        query.prepare("update vehicles set name=:name,"
                      "description=:description,"
                      "maxSpeed=:maxSpeed,"
                      "numberOfSeats=:numberOfSeats,"
                      "vehicleType=:vehicleType,"
                      "weight=:weight,"
                      "height=:height,"
                      "width=:width,"
                      "axleLoad=:axleLoad,"
                      "carbonEmission=:carbonEmission,"
                      "length=:length"
                      " where id=:id");
        query.bindValue(":name", v->name());
        query.bindValue(":description", v->description());
        query.bindValue(":maxSpeed", v->maxSpeed());
        query.bindValue(":numberOfSeats", v->numberOfSeats());
        query.bindValue(":vehicleType", v->vehicleType());
        query.bindValue(":weight", v->weight());
        query.bindValue(":height", v->height());
        query.bindValue(":width", v->width());
        query.bindValue(":axleLoad", v->axleLoad());
        query.bindValue(":carbonEmission", v->carbonEmission());
        query.bindValue(":length", v->length());
        query.bindValue(":id",v->id());
        if(!query.exec()) {
        } else {
        }
    }
}

void KsStaticObjectStore::deleteObject(QObject *q)
{
    if(!qstricmp(q->metaObject()->className(), "KsVehicle")) {
        KsVehicle *v = qobject_cast<KsVehicle*>(q);
        QSqlQuery query(db());
        query.prepare("delete from vehicle where id=:id");
        query.bindValue(":id",v->id());
        query.exec();
    }
}


bool KsStaticObjectStore::check()
{
    QStringList tables = db().tables();
    if(tables.size()==1) {
        QStringList fields = tableFields("vehicles");
        if(fields.size()==12) {
            if(fields.contains("id") &&
                    fields.contains("name") &&
                    fields.contains("description") &&
                    fields.contains("maxSpeed") &&
                    fields.contains("numberOfSeats") &&
                    fields.contains("vehicleType") &&
                    fields.contains("weight") &&
                    fields.contains("height") &&
                    fields.contains("width") &&
                    fields.contains("axleLoad") &&
                    fields.contains("carbonEmission")&&
                    fields.contains("length")) {
                return true;
            }
        }
    }
    return false;
}

