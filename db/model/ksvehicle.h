#ifndef KSVEHICLE_H
#define KSVEHICLE_H

#include <QObject>

class KsVehicle : public QObject
{
    Q_OBJECT
    Q_ENUMS(VehicleType)

    Q_PROPERTY(quint64 id READ id WRITE setID NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(quint32 maxSpeed READ maxSpeed WRITE setMaxSpeed NOTIFY maxSpeedChanged)
    Q_PROPERTY(quint32 numberOfSeats READ numberOfSeats WRITE setNumberOfSeats NOTIFY numberOfSeatsChanged)
    Q_PROPERTY(VehicleType vehicleType READ vehicleType WRITE setVehicleType NOTIFY vehicleTypeChanged)
    Q_PROPERTY(qreal weight READ weight WRITE setWeight NOTIFY weightChanged)
    Q_PROPERTY(qreal height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(qreal width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal axleLoad READ axleLoad WRITE setAxleLoad NOTIFY axleLoadChanged)
    Q_PROPERTY(qreal carbonEmission READ carbonEmission WRITE setCarbonEmission NOTIFY carbonEmissionChanged)
    Q_PROPERTY(qreal length READ length WRITE setLength NOTIFY lengthChanged)
public:

    static QString vehicleTypeString(QString in);

    enum VehicleType {
        Family=0,
        Business,
        Individual
    };

    explicit KsVehicle(QObject *parent = 0);
    ~KsVehicle();

    inline quint64 id() {
        return m_liID;
    }

    inline const QString name() {
        return m_strName;
    }

    inline const QString description() {
        return m_strDescription;
    }

    inline qint32 maxSpeed() {
        return m_iMaxSpeed;
    }

    inline qint32 numberOfSeats() {
        return m_iNumberOfSeats;
    }

    inline VehicleType vehicleType() {
        return m_eVehicleType;
    }

    inline qreal weight() {
        return m_fWeight;
    }

    inline qreal height() {
        return m_fHeight;
    }

    inline qreal width() {
        return m_fWidth;
    }

    inline qreal axleLoad() {
        return m_fAxleLoad;
    }

    inline qreal carbonEmission() {
        return m_fCarbonEmission;
    }

    inline qreal length() {
        return m_fLength;
    }

    void storeValues();
    void applyValues();
    void restoreValues();


signals:
    void idChanged(quint64 newId);
    void nameChanged(QString newName);
    void descriptionChanged(QString newDescription);
    void maxSpeedChanged(qint32 newMaxSpeed);
    void numberOfSeatsChanged(qint32 numberOfSeats);
    void vehicleTypeChanged(VehicleType newType);
    void carbonEmissionChanged(qreal newLevel);
    void weightChanged(qreal newWeight);
    void heightChanged(qreal newHeight);
    void widthChanged(qreal newWidth);
    void axleLoadChanged(qreal newLoad);
    void lengthChanged(qreal newLength);

public slots:
    void setID(quint64 id);
    void setName(QString name);
    void setDescription(QString description);
    void setMaxSpeed(qint32 maxSpeed);
    void setNumberOfSeats(qint32 maxNumberOfSeats);
    void setVehicleType(VehicleType vType);
    void setCarbonEmission(qreal newLevel);
    void setWeight(qreal newWeight);
    void setHeight(qreal newHeight);
    void setWidth(qreal newWidth);
    void setAxleLoad(qreal newLoad);
    void setLength(qreal length);

private:
    quint64 m_liID;
    QString m_strName;
    QString m_strDescription;
    qint32 m_iMaxSpeed;
    qint32 m_iNumberOfSeats;
    VehicleType m_eVehicleType;
    qreal m_fCarbonEmission;
    qreal m_fWeight;
    qreal m_fHeight;
    qreal m_fWidth;
    qreal m_fAxleLoad;
    qreal m_fLength;

    bool m_bBackupModeActive;
};

#endif // KSVEHICLE_H
