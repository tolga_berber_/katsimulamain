#ifndef KSSIMULATIONLOCATION_H
#define KSSIMULATIONLOCATION_H

#include "kssimulationcommondata.h"

#include <QGraphicsEllipseItem>

class KsSimulationVehicle;
class KsSimulationInfo;
class KsLaneObject;

class KsSimulationLocation : public KsSimulationCommonData
{
    Q_OBJECT
public:
    explicit KsSimulationLocation(KsLaneObject *l, KsSimulationInfo *i, KsSimulationVehicle *v, QObject *parent = 0);
    ~KsSimulationLocation();

    qreal location();
    KsLaneObject *lane();
    KsSimulationVehicle *vehicle();

signals:

public slots:
    void setLocation(qreal newLocation);
    void setLane(KsLaneObject *newLane);

    void increaseLocation(qreal amount);

private:
    qreal m_fLocation;
    KsLaneObject *m_ptrLane;
    KsSimulationVehicle *m_ptrVehicle;
    QGraphicsEllipseItem *m_ptrItem;
    qreal m_fSize;
};

#endif // KSSIMULATIONLOCATION_H
