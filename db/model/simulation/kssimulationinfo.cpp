#include "kssimulationinfo.h"

KsSimulationInfo::KsSimulationInfo(QObject *parent) : QObject(parent)
{

}

KsSimulationInfo::~KsSimulationInfo()
{

}

qint64 KsSimulationInfo::id()
{
    return m_liId;
}

qint64 KsSimulationInfo::duration()
{
    return m_liSimDur;
}

QTime KsSimulationInfo::startTime()
{
    return m_tmStartTime;
}

void KsSimulationInfo::setId(qint64 id)
{
    if(m_liId != id) {
        m_liId = id;

        emit idChanged(m_liId);
    }
}

void KsSimulationInfo::setDuration(qint64 dur)
{
    if(m_liSimDur!=dur) {
        m_liSimDur = dur;

        emit durationChanged(m_liSimDur);
    }
}

void KsSimulationInfo::setStartTime(QTime time)
{
    if(m_tmStartTime!=time) {
        m_tmStartTime = time;

        emit startTimeChanged(m_tmStartTime);
    }
}

