#ifndef KSSIMULATIONINFO_H
#define KSSIMULATIONINFO_H

#include <QObject>
#include <QTime>

class KsSimulationInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(qint64 duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(QTime startTime READ startTime WRITE setStartTime NOTIFY startTimeChanged)
public:
    explicit KsSimulationInfo(QObject *parent = 0);
    ~KsSimulationInfo();

    qint64 id();
    qint64 duration();
    QTime startTime();

signals:
    void idChanged(qint64 id);
    void durationChanged(qint64 dur);
    void startTimeChanged(QTime time);

public slots:
    void setId(qint64 id);
    void setDuration(qint64 dur);
    void setStartTime(QTime time);

private:
    qint64 m_liId;
    qint64 m_liSimDur;
    QTime m_tmStartTime;
};

#endif // KSSIMULATIONINFO_H
