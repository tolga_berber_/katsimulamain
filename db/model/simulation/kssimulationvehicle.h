#ifndef KSSIMULATIONVEHICLE_H
#define KSSIMULATIONVEHICLE_H

#include <QObject>

class KsVehicle;
class KsSimulationInfo;
class KsSimulationLocation;
class KsSimulationSpeed;
class KsSimulationCarbonEmissions;
class KsNodeObject;
class KsRoadModelObject;

class KsSimulationVehicle : public QObject
{
    Q_OBJECT
public:
    explicit KsSimulationVehicle(KsNodeObject *start, KsNodeObject *end, KsVehicle *v, KsSimulationInfo *i, QObject *parent = 0);

    qint64 id();
    KsVehicle *vehicle();
    KsSimulationInfo *info();
    KsSimulationLocation *location();
    KsSimulationSpeed *speed();
    KsSimulationCarbonEmissions *emissions();
    KsNodeObject *start();
    KsNodeObject *end();

    void setSecond(qint64 sec);

    void advance();

    void selectLane();

    bool hasValidRoute();

    bool isDone();

    qreal vehicleLengthInDegrees();
    qreal vehicleWidthInDegrees();

signals:

public slots:
    void setId(qint64 id);

    void setRoute(QList<KsRoadModelObject*> route);

    void setEnd(KsNodeObject *node);

private:
    bool m_bDone;
    qint64 m_liId;
    qint32 m_iRIdx;
    KsVehicle *m_ptrVehicle;
    KsSimulationInfo *m_ptrInfo;
    KsSimulationLocation *m_ptrLocation;
    KsSimulationSpeed *m_ptrSpeed;
    KsSimulationCarbonEmissions *m_ptrEmissions;

    KsNodeObject *m_ptrStart;
    KsNodeObject *m_ptrEnd;

    QList<KsRoadModelObject*> m_lstRoute;
};

#endif // KSSIMULATIONVEHICLE_H
