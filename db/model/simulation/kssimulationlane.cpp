#include "kssimulationlane.h"

KsSimulationLane::KsSimulationLane(KsSimulationInfo *inf, KsLaneObject *lane, QObject *parent):KsSimulationCommonData(inf,parent),
    m_ptrLane(lane)
{

}

KsSimulationLane::~KsSimulationLane()
{

}

qint64 KsSimulationLane::vehicleCount()
{
    return m_liVehicleCount;
}


KsLaneObject *KsSimulationLane::lane()
{
    return m_ptrLane;
}

void KsSimulationLane::increaseVehicleCount(qint64 number)
{
    m_liVehicleCount += number;
}

void KsSimulationLane::decreaseVehicleCount(qint64 number)
{
    m_liVehicleCount -= number;
}

