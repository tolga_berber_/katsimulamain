#ifndef KSSIMULATIONLANE_H
#define KSSIMULATIONLANE_H

#include "kssimulationcommondata.h"

class KsLaneObject;
class KsSimulationInfo;

class KsSimulationLane : public KsSimulationCommonData
{
    Q_OBJECT
public:
    explicit KsSimulationLane(KsSimulationInfo *inf, KsLaneObject *lane, QObject *parent=0);
    ~KsSimulationLane();

    qint64 vehicleCount();
    KsLaneObject *lane();

signals:

public slots:
    void increaseVehicleCount(qint64 number);
    void decreaseVehicleCount(qint64 number);

private:
    qint64 m_liVehicleCount;
    KsLaneObject *m_ptrLane;
};

#endif // KSSIMULATIONLANE_H
