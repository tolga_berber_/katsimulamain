#include "kssimulationcarbonemissions.h"

KsSimulationCarbonEmissions::KsSimulationCarbonEmissions(KsSimulationVehicle *v, KsSimulationInfo *i, QObject *parent) : KsSimulationCommonData(i,parent),
    m_ptrVehicle(v),
    m_fCarbonEmissionRate(0)
{

}

KsSimulationCarbonEmissions::~KsSimulationCarbonEmissions()
{

}

qreal KsSimulationCarbonEmissions::carbonEmission()
{
    return m_fCarbonEmissionRate;
}

KsSimulationVehicle *KsSimulationCarbonEmissions::vehicle()
{
    return m_ptrVehicle;
}

void KsSimulationCarbonEmissions::setEmissionRate(qreal newEmission)
{
    m_fCarbonEmissionRate = newEmission;
}

