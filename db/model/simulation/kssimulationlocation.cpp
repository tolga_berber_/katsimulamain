#include "kssimulationlocation.h"

#include "kssimulationvehicle.h"
#include "db/model/project/ksnodeobject.h"
#include "db/model/project/kslaneobject.h"

#include "workspace/ksworkspace.h"

#include <QPen>
#include <QBrush>

KsSimulationLocation::KsSimulationLocation(KsLaneObject *l, KsSimulationInfo *i, KsSimulationVehicle *v, QObject *parent) : KsSimulationCommonData(i,parent),
    m_ptrLane(l),
    m_fLocation(0),
    m_ptrVehicle(v)
{
    m_ptrItem = new QGraphicsEllipseItem();
    m_ptrItem->setPen(QPen(Qt::black));
    m_ptrItem->setBrush(QBrush(Qt::red));
    m_fSize = qMin(m_ptrVehicle->vehicleLengthInDegrees(), m_ptrVehicle->vehicleWidthInDegrees());
    m_ptrItem->setPos(v->start()->location()-QPointF(m_fSize/2, m_fSize/2));
    m_ptrItem->setRect(QRectF(QPointF(0,0),QSizeF(m_fSize,m_fSize)));
    m_ptrItem->setZValue(5000);
    m_ptrItem->setVisible(true);

    KsWorkspace::SharedInstance()->mapScene()->addItem(m_ptrItem);
    KsWorkspace::SharedInstance()->mapScene()->update();
}

KsSimulationLocation::~KsSimulationLocation()
{
    delete m_ptrItem;
}

qreal KsSimulationLocation::location()
{
    return m_fLocation;
}

KsLaneObject *KsSimulationLocation::lane()
{
    return m_ptrLane;
}

KsSimulationVehicle *KsSimulationLocation::vehicle()
{
    return m_ptrVehicle;
}

void KsSimulationLocation::setLocation(qreal newLocation)
{
    m_fLocation = newLocation;
}

void KsSimulationLocation::setLane(KsLaneObject *newLane)
{
    m_ptrLane = newLane;
    QPointF p = m_ptrLane->pointAtLength(m_fLocation);

    m_ptrItem->setPos(p);
    m_ptrItem->update();

    KsWorkspace::SharedInstance()->mapScene()->update();
}

void KsSimulationLocation::increaseLocation(qreal amount)
{
    m_fLocation += amount;

    if(m_fLocation>m_ptrLane->laneLength()) {
        return;
    }


    QPointF p = m_ptrLane->pointAtLength(m_fLocation);

    QPointF dP = p-QPointF(m_fSize/2, m_fSize/2);

    m_ptrItem->setPos(dP);
    m_ptrItem->update();

    KsWorkspace::SharedInstance()->mapScene()->update();
}

