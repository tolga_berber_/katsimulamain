#include "kssimulationcommondata.h"

KsSimulationCommonData::KsSimulationCommonData(KsSimulationInfo *i, QObject *parent) : QObject(parent),
    m_ptrInfo(i),
    m_liSecond(0)
{

}

KsSimulationCommonData::~KsSimulationCommonData()
{

}

qint64 KsSimulationCommonData::second()
{
    return m_liSecond;
}

KsSimulationInfo *KsSimulationCommonData::info()
{
    return m_ptrInfo;
}

void KsSimulationCommonData::setSecond(qint64 second)
{
    if(m_liSecond!=second) {
        m_liSecond = second;
    }
}

