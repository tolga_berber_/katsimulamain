#ifndef KSSIMULATIONCARBONEMISSIONS_H
#define KSSIMULATIONCARBONEMISSIONS_H

#include "kssimulationcommondata.h"

class KsSimulationVehicle;

class KsSimulationCarbonEmissions : public KsSimulationCommonData
{
    Q_OBJECT
public:
    explicit KsSimulationCarbonEmissions(KsSimulationVehicle *v, KsSimulationInfo *i, QObject *parent = 0);
    ~KsSimulationCarbonEmissions();

    qreal carbonEmission();
    KsSimulationVehicle *vehicle();

signals:

public slots:

    void setEmissionRate(qreal newEmission);

private:
    qreal m_fCarbonEmissionRate;
    KsSimulationVehicle *m_ptrVehicle;
};

#endif // KSSIMULATIONCARBONEMISSIONS_H
