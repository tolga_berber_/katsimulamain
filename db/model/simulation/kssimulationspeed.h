#ifndef KSSIMULATIONSPEED_H
#define KSSIMULATIONSPEED_H

#include "kssimulationcommondata.h"

class KsSimulationVehicle;

class KsSimulationSpeed : public KsSimulationCommonData
{
    Q_OBJECT
public:
    explicit KsSimulationSpeed(KsSimulationInfo *i, KsSimulationVehicle *v, QObject *parent = 0);
    ~KsSimulationSpeed();

    qreal speed();
    qreal acceleration();
    KsSimulationVehicle *vehicle();

    void updateAcceleration();
    void updateSpeed();

signals:

public slots:
    void setSpeed(qreal newSpeed);
    void setAcceleration(qreal newAcc);

private:
    qreal m_fCurrentSpeed;
    qreal m_fAcceleration;
    KsSimulationVehicle *m_ptrVehicle;

    qreal friction();
};

#endif // KSSIMULATIONSPEED_H
