#include "kssimulationvehicle.h"

#include "kssimulationspeed.h"
#include "kssimulationcarbonemissions.h"
#include "kssimulationlocation.h"

#include "db/model/project/kslaneobject.h"
#include "db/model/project/ksroadmodelobject.h"

#include "../ksvehicle.h"
#include "workspace/ksworkspace.h"

KsSimulationVehicle::KsSimulationVehicle(KsNodeObject *start, KsNodeObject *end, KsVehicle *v, KsSimulationInfo *i, QObject *parent) : QObject(parent),
    m_bDone(false),
    m_liId(-1),
    m_ptrVehicle(v),
    m_ptrInfo(i),
    m_ptrStart(start),
    m_ptrEnd(end)
{
    m_ptrSpeed = new KsSimulationSpeed(i,this,this);
    m_ptrLocation = new KsSimulationLocation(NULL,i,this,this);
    m_ptrEmissions = new KsSimulationCarbonEmissions(this,i,this);

    m_iRIdx = 0;
}

qint64 KsSimulationVehicle::id()
{
    return m_liId;
}

KsVehicle *KsSimulationVehicle::vehicle()
{
    return m_ptrVehicle;
}

KsSimulationInfo *KsSimulationVehicle::info()
{
    return m_ptrInfo;
}

KsSimulationLocation *KsSimulationVehicle::location()
{
    return m_ptrLocation;
}

KsSimulationSpeed *KsSimulationVehicle::speed()
{
    return m_ptrSpeed;
}

KsSimulationCarbonEmissions *KsSimulationVehicle::emissions()
{
    return m_ptrEmissions;
}

KsNodeObject *KsSimulationVehicle::start()
{
    return m_ptrStart;
}

KsNodeObject *KsSimulationVehicle::end()
{
    return m_ptrEnd;
}

void KsSimulationVehicle::setSecond(qint64 sec)
{
    m_ptrEmissions->setSecond(sec);
    m_ptrLocation->setSecond(sec);
    m_ptrSpeed->setSecond(sec);
}

void KsSimulationVehicle::advance()
{
    m_ptrSpeed->updateAcceleration();
    m_ptrSpeed->updateSpeed();
    m_ptrLocation->increaseLocation(m_ptrSpeed->speed());
    if(m_ptrLocation->location()>m_ptrLocation->lane()->laneLength()) {
        qreal tLocation = m_ptrLocation->location();
        tLocation -= m_ptrLocation->lane()->laneLength();
        m_ptrLocation->setLocation(tLocation);
        m_iRIdx++;
        if(m_iRIdx>=m_lstRoute.size()) {
            m_bDone = true;
        } else {
            selectLane();
        }
    }
}

void KsSimulationVehicle::setId(qint64 id)
{
    m_liId = id;
}

void KsSimulationVehicle::selectLane()
{
    int r = qrand()%m_lstRoute[m_iRIdx]->laneCount();

    KsRoadModelObject &p = *m_lstRoute[m_iRIdx];

    m_ptrLocation->setLane(p[r]);
}

bool KsSimulationVehicle::hasValidRoute()
{
    return m_lstRoute.count()>1;
}

bool KsSimulationVehicle::isDone()
{
    return m_bDone;
}

qreal KsSimulationVehicle::vehicleLengthInDegrees()
{
    return m_ptrVehicle->length()*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees();
}

qreal KsSimulationVehicle::vehicleWidthInDegrees()
{
    return m_ptrVehicle->width()*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees();
}

void KsSimulationVehicle::setRoute(QList<KsRoadModelObject *> route)
{
    m_lstRoute = route;

    selectLane();
}

void KsSimulationVehicle::setEnd(KsNodeObject *node)
{
    m_ptrEnd = node;
}


