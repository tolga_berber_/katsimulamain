#ifndef KSSIMULATIONCOMMONDATA_H
#define KSSIMULATIONCOMMONDATA_H

#include <QObject>

class KsSimulationInfo;

class KsSimulationCommonData : public QObject
{
    Q_OBJECT
public:
    explicit KsSimulationCommonData(KsSimulationInfo *i, QObject *parent = 0);
    ~KsSimulationCommonData();

    qint64 second();
    KsSimulationInfo *info();

signals:

public slots:
    void setSecond(qint64 second);

private:
    qint64 m_liSecond;
    KsSimulationInfo *m_ptrInfo;
};

#endif // KSSIMULATIONCOMMONDATA_H
