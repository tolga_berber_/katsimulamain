#include "kssimulationspeed.h"

#include <QtMath>

KsSimulationSpeed::KsSimulationSpeed(KsSimulationInfo *i, KsSimulationVehicle *v, QObject *parent) : KsSimulationCommonData(i,parent),
    m_ptrVehicle(v),
    m_fCurrentSpeed(0),
    m_fAcceleration(0)
{

}

KsSimulationSpeed::~KsSimulationSpeed()
{

}

qreal KsSimulationSpeed::speed()
{
    return m_fCurrentSpeed;
}

qreal KsSimulationSpeed::acceleration()
{
    return m_fAcceleration;
}

KsSimulationVehicle *KsSimulationSpeed::vehicle()
{
    return m_ptrVehicle;
}

void KsSimulationSpeed::updateAcceleration()
{
    int rMod = 274-183;
    int rNumber = qrand()%rMod;
    setAcceleration(rNumber/100.0);
}

void KsSimulationSpeed::updateSpeed()
{
    m_fCurrentSpeed += m_fAcceleration;
}

void KsSimulationSpeed::setSpeed(qreal newSpeed)
{
    m_fCurrentSpeed = newSpeed;
}

void KsSimulationSpeed::setAcceleration(qreal newAcc)
{
    m_fAcceleration = newAcc;
}

qreal KsSimulationSpeed::friction()
{
    if(qFuzzyIsNull(m_fCurrentSpeed)) {
        return 1.0;
    } else {
        return qPow(1.0208*m_fCurrentSpeed, -0.274);
    }
}

