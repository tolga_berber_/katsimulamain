#include "ksvehicle.h"
#include <QMetaProperty>

QString KsVehicle::vehicleTypeString(QString in)
{
    if(in=="Individual") {
        return tr("Kişisel Araç");
    } else if(in=="Family") {
        return tr("Aile Aracı");
    } else if(in=="Business") {
        return tr("İş Aracı");
    }
    return "";
}

KsVehicle::KsVehicle(QObject *parent) : QObject(parent),
    m_liID(0),
    m_strName(""),
    m_strDescription(""),
    m_iMaxSpeed(0),
    m_iNumberOfSeats(0),
    m_eVehicleType(Individual),
    m_fCarbonEmission(0),
    m_fWeight(0),
    m_fHeight(0),
    m_fWidth(0),
    m_fAxleLoad(0)
{

}

KsVehicle::~KsVehicle()
{

}

void KsVehicle::storeValues()
{
    m_bBackupModeActive = true;
    for(int i=0;i<metaObject()->propertyCount();i++) {
        QString dpName = tr("b_%1").arg(metaObject()->property(i).name());
        setProperty(dpName.toStdString().c_str(), property(metaObject()->property(i).name()));
    }
}

void KsVehicle::applyValues()
{
    m_bBackupModeActive = false;
    emit idChanged(m_liID);
}

void KsVehicle::restoreValues()
{
    for(int i=0;i<metaObject()->propertyCount();i++) {
        QString dpName = tr("b_%1").arg(metaObject()->property(i).name());
        setProperty(metaObject()->property(i).name(),
                    property(dpName.toStdString().c_str()));
    }
    m_bBackupModeActive = false;
}

void KsVehicle::setID(quint64 id)
{
    if(m_liID!=id) {
        m_liID = id;

        if(!m_bBackupModeActive)
            emit idChanged(m_liID);
    }
}

void KsVehicle::setName(QString name)
{
    if(m_strName!=name) {
        m_strName = name;

        if(!m_bBackupModeActive)
            emit nameChanged(m_strName);
    }
}

void KsVehicle::setDescription(QString description)
{
    if(m_strDescription!=description) {
        m_strDescription = description;

        if(!m_bBackupModeActive)
            emit descriptionChanged(m_strDescription);
    }
}

void KsVehicle::setMaxSpeed(qint32 maxSpeed)
{
    if(m_iMaxSpeed!=maxSpeed) {
        m_iMaxSpeed = maxSpeed;

        if(!m_bBackupModeActive)
            emit maxSpeedChanged(m_iMaxSpeed);
    }
}

void KsVehicle::setNumberOfSeats(qint32 maxNumberOfSeats)
{
    if(m_iNumberOfSeats != maxNumberOfSeats) {
        m_iNumberOfSeats = maxNumberOfSeats;

        if(!m_bBackupModeActive)
            emit numberOfSeatsChanged(m_iNumberOfSeats);
    }
}

void KsVehicle::setVehicleType(KsVehicle::VehicleType vType)
{
    if(m_eVehicleType!=vType) {
        m_eVehicleType = vType;

        if(!m_bBackupModeActive)
            emit vehicleTypeChanged(m_eVehicleType);
    }
}

void KsVehicle::setCarbonEmission(qreal newLevel)
{
    if(m_fCarbonEmission!=newLevel) {
        m_fCarbonEmission = newLevel;

        if(!m_bBackupModeActive)
            emit carbonEmissionChanged(m_fCarbonEmission);
    }
}

void KsVehicle::setWeight(qreal newWeight)
{
    if(!qFuzzyCompare(m_fWeight+1.0, newWeight+1.0)) {
        m_fWeight = newWeight;

        if(!m_bBackupModeActive)
            emit weightChanged(m_fWeight);
    }
}

void KsVehicle::setHeight(qreal newHeight)
{
    if(!qFuzzyCompare(m_fHeight+1.0, newHeight+1.0)) {
        m_fHeight = newHeight;

        if(!m_bBackupModeActive)
            emit heightChanged(m_fHeight);
    }
}

void KsVehicle::setWidth(qreal newWidth)
{
    if(!qFuzzyCompare(m_fWidth+1.0, newWidth+1.0)) {
        m_fWidth = newWidth;

        if(!m_bBackupModeActive)
            emit widthChanged(m_fWidth);
    }
}

void KsVehicle::setAxleLoad(qreal newLoad)
{
    if(!qFuzzyCompare(m_fAxleLoad+1.0, newLoad+1.0)) {
        m_fAxleLoad = newLoad;

        if(!m_bBackupModeActive)
            emit axleLoadChanged(m_fAxleLoad);
    }
}

void KsVehicle::setLength(qreal length)
{
    if(!qFuzzyCompare(m_fLength+1.0, length+1.0)) {
        m_fLength = length;

        if(!m_bBackupModeActive)
            emit lengthChanged(m_fLength);
    }
}

