#include "ksnodeobject.h"
#include "ksroadmodelobject.h"

KsNodeObject::KsNodeObject(QObject *parent) : QObject(parent)
{
    setName("Trafik Nesnesi");
}

KsNodeObject::~KsNodeObject()
{

}

qint64 KsNodeObject::id()
{
    return m_liId;
}

KsNodeObject::NodeType KsNodeObject::nodeType()
{
    return m_eNodeType;
}

QPointF KsNodeObject::location()
{
    return m_pntLocation;
}

QString KsNodeObject::name()
{
    return m_strName;
}

RoadListIterator KsNodeObject::inputRoadsBegin()
{
    return m_lstInputRoads.begin();
}

RoadListIterator KsNodeObject::inputRoadsEnd()
{
    return m_lstInputRoads.end();
}

RoadListIterator KsNodeObject::outputRoadBegin()
{
    return m_lstOutputRoads.begin();
}

RoadListIterator KsNodeObject::outputRoadEnd()
{
    return m_lstOutputRoads.end();
}

void KsNodeObject::setId(qint64 id)
{
    if(m_liId!=id) {
        m_liId = id;

        emit idChanged(m_liId);
    }
}

void KsNodeObject::setNodeType(KsNodeObject::NodeType nType)
{
    if(m_eNodeType!=nType) {
        m_eNodeType = nType;

        emit nodeTypeChanged(m_eNodeType);
    }
}

void KsNodeObject::setLocation(QPointF pnt)
{
    if(m_pntLocation!=pnt) {
        m_pntLocation =  pnt;

        emit locationChanged(pnt);
    }
}

void KsNodeObject::setName(QString name)
{
    if(m_strName!=name) {
        m_strName = name;
    }
}

void KsNodeObject::addInputRoad(KsRoadModelObject *inputRoad)
{
    if(!m_lstInputRoads.contains(inputRoad)) {
        m_lstInputRoads.append(inputRoad);

        inputRoad->setDestination(this);

        emit inputRoadConnected(inputRoad);
    }
}

void KsNodeObject::addOutputRoad(KsRoadModelObject *outputRoad)
{
    if(!m_lstOutputRoads.contains(outputRoad)) {
        m_lstOutputRoads.append(outputRoad);

        outputRoad->setSource(this);

        emit outputRoadConnected(outputRoad);
    }
}

void KsNodeObject::removeInputRoad(KsRoadModelObject *r)
{
    if(m_lstInputRoads.contains(r)) {
        m_lstInputRoads.removeAll(r);

        r->setDestination(NULL);

        emit inputRoadDisconnected(r);
    }
}

void KsNodeObject::removeOutputRoad(KsRoadModelObject *r)
{
    if(m_lstOutputRoads.contains(r)) {
        m_lstOutputRoads.removeAll(r);

        r->setSource(NULL);

        emit outputRoadDisconnected(r);
    }
}

