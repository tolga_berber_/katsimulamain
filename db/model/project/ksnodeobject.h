#ifndef KSNODEOBJECT_H
#define KSNODEOBJECT_H

#include <QObject>
#include <QList>
#include <QPointF>

class KsRoadModelObject;

typedef QList<KsRoadModelObject*> RoadList;
typedef QList<KsRoadModelObject*>::iterator RoadListIterator;

class KsNodeObject : public QObject
{
    Q_OBJECT
    Q_ENUMS(NodeType)

    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(NodeType nodeType READ nodeType WRITE setNodeType NOTIFY nodeTypeChanged)
    Q_PROPERTY(QPointF location READ location WRITE setLocation NOTIFY locationChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
public:
    explicit KsNodeObject(QObject *parent = 0);
    ~KsNodeObject();

    enum NodeType {
        NTSource=0,
        NTIntersection
    };

    qint64 id();
    NodeType nodeType();
    QPointF location();
    QString name();

    RoadListIterator inputRoadsBegin();
    RoadListIterator inputRoadsEnd();

    RoadListIterator outputRoadBegin();
    RoadListIterator outputRoadEnd();

signals:
    void idChanged(qint64 id);
    void nodeTypeChanged(NodeType nType);
    void locationChanged(QPointF pnt);
    void nameChanged(QString newName);

    void inputRoadConnected(KsRoadModelObject *inputRoad);
    void outputRoadConnected(KsRoadModelObject *outputRoad);

    void inputRoadDisconnected(KsRoadModelObject *r);
    void outputRoadDisconnected(KsRoadModelObject *r);

public slots:
    void setId(qint64 id);
    void setNodeType(NodeType nType);
    void setLocation(QPointF pnt);
    void setName(QString name);

    void addInputRoad(KsRoadModelObject *inputRoad);
    void addOutputRoad(KsRoadModelObject *outputRoad);

    void removeInputRoad(KsRoadModelObject *r);
    void removeOutputRoad(KsRoadModelObject *r);

private:
    QString m_strName;
    qint64 m_liId;
    NodeType m_eNodeType;
    QPointF m_pntLocation;
    RoadList m_lstInputRoads;
    RoadList m_lstOutputRoads;

};

#endif // KSNODEOBJECT_H
