#include "ksroadmodelobject.h"

#include "kslaneobject.h"
#include "db/ksprojectobjectstore.h"
#include "ksnodeobject.h"
#include "utilities/ksgeometryhelper.h"

KsRoadModelObject::KsRoadModelObject(QObject *parent) : QObject(parent),
  m_bCurve(false)
{

}

KsRoadModelObject::~KsRoadModelObject()
{

}

qint64 KsRoadModelObject::id()
{
    return m_liId;
}

qint32 KsRoadModelObject::laneCount()
{
    return m_lstLanes.count();
}

QString KsRoadModelObject::name()
{
    return m_strName;
}

QPolygonF KsRoadModelObject::roadNormal()
{
    return m_polRoadNormal;
}

bool KsRoadModelObject::isCurve()
{
    return m_bCurve;
}

qreal KsRoadModelObject::designSpeed()
{
    return m_fDesignSpeed;
}

qreal KsRoadModelObject::slope()
{
    return m_fSlope;
}

qreal KsRoadModelObject::roadWidth()
{
    float result = 0;
    foreach (KsLaneObject *l, m_lstLanes) {
        result += l->laneWidth();
    }
    return result;
}

qreal KsRoadModelObject::roadWidthBeforeLane(int idx)
{
    float result = 0;
    for(int i=0;i<idx;i++) {
        result += m_lstLanes[i]->laneWidth();
    }
    return result;
}

qreal KsRoadModelObject::roadWidthBeforeLane(KsLaneObject *l)
{
    float result = 0;
    for(int i=0;i<m_lstLanes.count();i++) {
        if(m_lstLanes[i]==l)
            return result;
        result += m_lstLanes[i]->laneWidth();
    }
    return -1;
}

KsLaneObject *&KsRoadModelObject::operator[](int idx)
{
    Q_ASSERT(idx<m_lstLanes.count());
    return m_lstLanes[idx];
}

void KsRoadModelObject::addLaneObject(KsLaneObject *l)
{
    if(!m_lstLanes.contains(l)) {
        m_lstLanes.append(l);
        l->setRoad(this);
    }
}

KsNodeObject *KsRoadModelObject::source()
{
    return m_ptrSource;
}

KsNodeObject *KsRoadModelObject::destination()
{
    return m_ptrDestination;
}

qreal KsRoadModelObject::roadLength()
{
    return KsGeometryHelper::sharedInstance()->length(m_polRoadNormal);
}

void KsRoadModelObject::setId(qint64 id)
{
    if(m_liId!=id) {
        m_liId = id;

        emit idChanged(m_liId);
    }
}

void KsRoadModelObject::setLaneCount(qint32 laneCount)
{
    if(m_lstLanes.count()!=laneCount) {
        while(m_lstLanes.count()!=laneCount) {
            if(m_lstLanes.count()>laneCount) {
                KsLaneObject *lane = m_lstLanes.takeLast();
                m_ptrStore->removeLane(lane);
            } else {
                KsLaneObject *lane = m_ptrStore->newLane();
                m_lstLanes.append(lane);
                lane->setRoad(this);
            }
        }
        emit laneCountChanged(m_lstLanes.count());
    }
}

void KsRoadModelObject::setName(QString name)
{
    if(m_strName!=name) {
        m_strName = name;

        emit nameChanged(m_strName);
    }
}

void KsRoadModelObject::setRoadNormal(QPolygonF normal)
{
    if(m_polRoadNormal!=normal) {
        m_polRoadNormal = normal;

        emit roadNormalChanged(m_polRoadNormal);
    }
}

void KsRoadModelObject::setCurve(bool curve)
{
    if(m_bCurve!=curve) {
        m_bCurve = curve;

        emit curveChanged(curve);
    }
}

void KsRoadModelObject::setDesignSpeed(qreal designSpeed)
{
    if(m_fDesignSpeed!=designSpeed) {
        m_fDesignSpeed = designSpeed;

        emit designSpeedChanged(m_fDesignSpeed);
    }
}

void KsRoadModelObject::setSlope(qreal slope)
{
    if(m_fSlope!=slope) {
        m_fSlope = slope;

        emit slopeChanged(m_fSlope);
    }
}

void KsRoadModelObject::setSource(KsNodeObject *sNode)
{
    if(m_ptrSource!=sNode) {
        m_ptrSource = sNode;

        connect(sNode, SIGNAL(locationChanged(QPointF)), this, SLOT(startPointChanged(QPointF)));

        sNode->addOutputRoad(this);

        emit sourceNodeChanged(m_ptrSource);
    }
}

void KsRoadModelObject::setDestination(KsNodeObject *dNode)
{
    if(m_ptrDestination!=dNode) {
        m_ptrDestination = dNode;

        connect(dNode, SIGNAL(locationChanged(QPointF)), this, SLOT(endPointChanged(QPointF)));

        dNode->addInputRoad(this);

        emit destinationNodeChanged(m_ptrDestination);
    }
}

void KsRoadModelObject::setStore(KsProjectObjectStore *store)
{
    if(m_ptrStore!=store) {
        m_ptrStore = store;
    }
}

void KsRoadModelObject::startPointChanged(QPointF sPoint)
{

    m_polRoadNormal.first() = sPoint;

    emit roadNormalChanged(m_polRoadNormal);
}

void KsRoadModelObject::endPointChanged(QPointF ePoint)
{
    m_polRoadNormal.last() = ePoint;

    emit roadNormalChanged(m_polRoadNormal);
}

