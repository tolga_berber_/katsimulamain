#include "kslaneobject.h"

#include "ksroadmodelobject.h"

#include "utilities/ksgeometryhelper.h"

#include "utilities/polygonhandlers.h"

#include "workspace/ksworkspace.h"

KsLaneObject::KsLaneObject(QObject *parent) : QObject(parent),
    m_fLaneWidth(2.5),
    m_bLaneNormalFetched(false)
{

}

KsLaneObject::~KsLaneObject()
{

}

qint64 KsLaneObject::id()
{
    return m_liId;
}

KsRoadModelObject *KsLaneObject::road()
{
    return m_ptrRoad;
}

qint64 KsLaneObject::roadId()
{
    return m_liRoadId;
}

qreal KsLaneObject::laneWidth()
{
    return m_fLaneWidth;
}

qreal KsLaneObject::laneLength()
{
    return KsGeometryHelper::sharedInstance()->length(laneNormal());
}

QPolygonF KsLaneObject::laneNormal()
{
    if(!m_bLaneNormalFetched) {
        qreal widthBeforeMe = m_ptrRoad->roadWidthBeforeLane(this);
        widthBeforeMe += m_fLaneWidth/2;
        qreal halfRoadLength = m_ptrRoad->roadWidth()/2;
        widthBeforeMe -= halfRoadLength;
        widthBeforeMe *= KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees();

        if(m_ptrRoad->isCurve()) {
            QPolygonF curvePoly = PolygonHandlers::calculateBezier(m_ptrRoad->roadNormal());
            m_polLaneNormal = PolygonHandlers::calculateParalelPolygon(curvePoly, widthBeforeMe);
        } else {
            m_polLaneNormal = PolygonHandlers::calculateParalelPolygon(m_ptrRoad->roadNormal(), widthBeforeMe);
        }
        m_bLaneNormalFetched = true;
    }

    return m_polLaneNormal;
}

QPointF KsLaneObject::pointAtLength(qreal length)
{
    return KsGeometryHelper::sharedInstance()->lengthToPolygonLocation(laneNormal(), length);
}

RelatedLanesIterator KsLaneObject::inputLaneIterator()
{
    return m_lstInputLanes.begin();
}

RelatedLanesIterator KsLaneObject::inputLastIterator()
{
    return m_lstInputLanes.end();
}

RelatedLanesIterator KsLaneObject::outputLanesIterator()
{
    return m_lstOutputLanes.begin();
}

RelatedLanesIterator KsLaneObject::outputLaneLastIterator()
{
    return m_lstOutputLanes.end();
}

void KsLaneObject::setId(qint64 id)
{
    if(m_liId!=id) {
        m_liId = id;

        emit idChanged(m_liId);
    }
}

void KsLaneObject::setRoad(KsRoadModelObject *road)
{
    if(m_ptrRoad!=road) {
        m_ptrRoad = road;

        connect(m_ptrRoad, SIGNAL(idChanged(qint64)), this, SLOT(setRoadId(qint64)));
        setRoadId(road->id());
        emit roadChanged(m_ptrRoad);
    }
}

void KsLaneObject::setRoadId(qint64 rId)
{
    if(m_liRoadId!=rId) {
        m_liRoadId = rId;

        emit roadIdChanged(m_liRoadId);
    }
}

void KsLaneObject::setLaneWidth(qreal laneWidth)
{
    if(m_fLaneWidth!=laneWidth) {
        m_fLaneWidth = laneWidth;
    }
}

void KsLaneObject::connectToInput(KsLaneObject *obj)
{
    if(!m_lstInputLanes.contains(obj)) {
        m_lstInputLanes.append(obj);
        obj->connectToOutput(this);
    }
}

void KsLaneObject::connectToOutput(KsLaneObject *obj)
{
    if(!m_lstOutputLanes.contains(obj)) {
        m_lstOutputLanes.append(obj);
        obj->connectToInput(this);
    }
}

void KsLaneObject::disconnectFromInput(KsLaneObject *obj)
{
    if(m_lstInputLanes.contains(obj)) {
        m_lstInputLanes.removeAll(obj);
        obj->disconnectFromOutput(this);
    }
}

void KsLaneObject::disconnectFromOutput(KsLaneObject *obj)
{
    if(m_lstOutputLanes.contains(obj)) {
        m_lstOutputLanes.removeAll(obj);
        obj->disconnectFromInput(this);
    }
}

