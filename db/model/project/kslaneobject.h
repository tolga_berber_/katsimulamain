#ifndef KSLANEOBJECT_H
#define KSLANEOBJECT_H

#include <QObject>
#include <QList>
#include <QPolygonF>

class KsLaneObject;
class KsRoadModelObject;

typedef QList<KsLaneObject*> RelatedLanes;
typedef QList<KsLaneObject*>::iterator RelatedLanesIterator;

class KsLaneObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(KsRoadModelObject *road READ road WRITE setRoad NOTIFY roadChanged)
    Q_PROPERTY(qreal laneWidth READ laneWidth WRITE setLaneWidth NOTIFY laneWidthChanged)
public:
    explicit KsLaneObject(QObject *parent = 0);
    ~KsLaneObject();

    qint64 id();
    KsRoadModelObject *road();
    qint64 roadId();
    qreal laneWidth();

    qreal laneLength();

    QPolygonF laneNormal();

    QPointF pointAtLength(qreal length);

    RelatedLanesIterator inputLaneIterator();
    RelatedLanesIterator inputLastIterator();

    RelatedLanesIterator outputLanesIterator();
    RelatedLanesIterator outputLaneLastIterator();

signals:
    void idChanged(qint64 id);
    void roadChanged(KsRoadModelObject *road);
    void roadIdChanged(qint64 roadId);
    void laneWidthChanged(qreal laneWidth);

public slots:
    void setId(qint64 id);
    void setRoad(KsRoadModelObject *road);
    void setRoadId(qint64 rId);
    void setLaneWidth(qreal laneWidth);

    void connectToInput(KsLaneObject *obj);
    void connectToOutput(KsLaneObject *obj);

    void disconnectFromInput(KsLaneObject *obj);
    void disconnectFromOutput(KsLaneObject *obj);

private:
    qint64 m_liId;
    KsRoadModelObject *m_ptrRoad;
    qint64 m_liRoadId;
    qreal m_fLaneWidth;

    bool m_bLaneNormalFetched;
    QPolygonF m_polLaneNormal;

    RelatedLanes m_lstInputLanes;
    RelatedLanes m_lstOutputLanes;
};

#endif // KSLANEOBJECT_H
