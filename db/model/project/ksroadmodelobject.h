#ifndef KSROADMODELOBJECT_H
#define KSROADMODELOBJECT_H

#include <QObject>
#include <QList>
#include <QPolygonF>

class KsLaneObject;

typedef QList<KsLaneObject*> Lanes;

class KsProjectObjectStore;
class KsNodeObject;

class KsRoadModelObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint64 id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(qint32 laneCount READ laneCount WRITE setLaneCount NOTIFY laneCountChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QPolygonF roadNormal READ roadNormal WRITE setRoadNormal NOTIFY roadNormalChanged)
    Q_PROPERTY(bool curve READ isCurve WRITE setCurve NOTIFY curveChanged)
    Q_PROPERTY(qreal designSpeed READ designSpeed WRITE setDesignSpeed NOTIFY designSpeedChanged)
    Q_PROPERTY(qreal slope READ slope WRITE setSlope NOTIFY slopeChanged)
public:
    explicit KsRoadModelObject(QObject *parent = 0);
    ~KsRoadModelObject();

    qint64 id();

    qint32 laneCount();
    QString name();
    QPolygonF roadNormal();
    bool isCurve();
    qreal designSpeed();
    qreal slope();

    qreal roadWidth();
    qreal roadWidthBeforeLane(int idx);
    qreal roadWidthBeforeLane(KsLaneObject *l);

    KsLaneObject *&operator[] (int idx);

    void addLaneObject(KsLaneObject *l);

    KsNodeObject *source();
    KsNodeObject *destination();

    qreal roadLength();

signals:
    void idChanged(qint64 id);
    void laneCountChanged(qint32 newLaneCount);
    void nameChanged(QString name);
    void roadNormalChanged(QPolygonF newNormal);
    void curveChanged(bool curve);
    void designSpeedChanged(qreal ds);
    void slopeChanged(qreal slope);

    void sourceNodeChanged(KsNodeObject *s);
    void destinationNodeChanged(KsNodeObject *s);

public slots:
    void setId(qint64 id);
    void setLaneCount(qint32 laneCount);
    void setName(QString name);
    void setRoadNormal(QPolygonF normal);
    void setCurve(bool curve);
    void setDesignSpeed(qreal designSpeed);
    void setSlope(qreal slope);
    void setSource(KsNodeObject *sNode);
    void setDestination(KsNodeObject *dNode);

    void setStore(KsProjectObjectStore *store);


    void startPointChanged(QPointF sPoint);
    void endPointChanged(QPointF ePoint);

private:
    qint64 m_liId;
    QString m_strName;
    QPolygonF m_polRoadNormal;
    bool m_bCurve;
    qreal m_fDesignSpeed;
    qreal m_fSlope;
    KsProjectObjectStore *m_ptrStore;
    KsNodeObject *m_ptrSource;
    KsNodeObject *m_ptrDestination;

    Lanes m_lstLanes;
};

#endif // KSROADOBJECT_H
