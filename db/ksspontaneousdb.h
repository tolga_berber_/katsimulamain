#ifndef KSSPONTANEOUSDB_H
#define KSSPONTANEOUSDB_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>

class KsSimulationInfo;
class KsSimulationVehicle;
class KsSimulationLane;
class KsSimulationLocation;
class KsSimulationSpeed;
class KsSimulationCarbonEmissions;

class KsSpontaneousDB : public QObject
{
    Q_OBJECT
public:
    explicit KsSpontaneousDB(QString name, QString filePath, QObject *parent = 0);
    ~KsSpontaneousDB();

    void beginTransaction();
    void endTransaction();

    void insertInfo(KsSimulationInfo *inf);
    void insertVehicle(KsSimulationVehicle *vehicle);
    void insertLane(KsSimulationLane *lane);
    void insertLocation(KsSimulationLocation *location);
    void insertSpeed(KsSimulationSpeed *speed);
    void insertCarbonEmissions(KsSimulationCarbonEmissions *emission);

signals:

public slots:

private:
    QString m_strName;
    QString m_strFilePath;

    QSqlQuery *m_qryInfo;
    QSqlQuery *m_qryVehicle;
    QSqlQuery *m_qryLane;
    QSqlQuery *m_qryLocation;
    QSqlQuery *m_qrySpeed;
    QSqlQuery *m_qryCarbonEmission;

    inline QSqlDatabase db() {
        return QSqlDatabase::database(m_strName);
    }

    void prepareSqls();
    void prepareDB();
};

#endif // KSSPONTANEOUSDB_H
