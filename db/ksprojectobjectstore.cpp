#include "ksprojectobjectstore.h"
#include "model/project/kslaneobject.h"
#include "model/project/ksroadmodelobject.h"
#include "model/project/ksnodeobject.h"
#include <QSqlQuery>
#include <QSqlError>

KsProjectObjectStore::KsProjectObjectStore(QString name, QString filePath, QObject *parent):
    KsObjectStore(name,filePath,parent)
{
}

KsProjectObjectStore::~KsProjectObjectStore()
{
}

QList<KsLaneObject *> KsProjectObjectStore::lanes()
{
    QList<KsLaneObject*> result;
    if(m_mapCreatedObjects.contains("KsLaneObject")) {
        QObjectList &lst = m_mapCreatedObjects["KsLaneObject"];
        foreach (QObject *obj, lst) {
            result.append(qobject_cast<KsLaneObject*>(obj));
        }
    } else {
        QSqlQuery q("select * from lanes",db());
        q.exec();
        while(q.next()) {
            KsLaneObject *v = new KsLaneObject;
            v->setId(q.value(0).toLongLong());
            v->setRoadId(q.value(1).toLongLong());
            v->setLaneWidth(q.value(2).toReal());
            mapObject(v);
            result.append(v);
        }
        q.finish();

        foreach (KsLaneObject *l, result) {
            QList<KsLaneObject*> ls = findInputLanes(l);
            foreach (KsLaneObject *ll, ls) {
                l->connectToInput(ll);
            }
        }
    }

    return result;
}

KsLaneObject *KsProjectObjectStore::newLane()
{
    KsLaneObject *l = new KsLaneObject;
    m_lstNewObjects.append(l);
    mapObject(l);
    return l;
}

void KsProjectObjectStore::removeLane(KsLaneObject *lane)
{
    unMapObject(lane);
}

QList<KsRoadModelObject *> KsProjectObjectStore::roads()
{
    QList<KsRoadModelObject*> result;
    if(m_mapCreatedObjects.contains("KsRoadModelObject")) {
        QObjectList &lst = m_mapCreatedObjects["KsLaneObject"];
        foreach (QObject *obj, lst) {
            result.append(qobject_cast<KsRoadModelObject*>(obj));
        }
    } else {
        QSqlQuery q("select * from roads", db());
        q.exec();
        while(q.next()) {
            KsRoadModelObject *r = new KsRoadModelObject;
            r->setId(q.value(0).toLongLong());
            r->setName(q.value(1).toString());
            r->setRoadNormal(StringToPolygon(q.value(3).toString()));
            r->setCurve(q.value(2).toBool());
            r->setDesignSpeed(q.value(4).toReal());
            r->setSlope(q.value(5).toReal());
            r->setStore(this);
            mapObject(r);
            QList<KsLaneObject*> lanes = findRoadLanes(r);
            foreach (KsLaneObject *lane, lanes) {
                r->addLaneObject(lane);
            }
            result.append(r);
        }
        q.finish();
    }
    return result;
}

KsRoadModelObject *KsProjectObjectStore::newRoad()
{
    KsRoadModelObject *m = new KsRoadModelObject;
    m_lstNewObjects.append(m);
    m->setStore(this);
    mapObject(m);
    return m;
}

void KsProjectObjectStore::removeRoad(KsRoadModelObject *road)
{
    int laneCount = road->laneCount();
    for(int i=0;i<laneCount;i++) {
        removeLane((*road)[i]);
    }
    unMapObject(road);
}

QList<KsNodeObject *> KsProjectObjectStore::nodes()
{
    QList<KsNodeObject *> result;
    if(m_mapCreatedObjects.contains("KsNodeObject")) {
        QObjectList &lst = m_mapCreatedObjects["KsNodeObject"];
        foreach (QObject *o, lst) {
            result.append(qobject_cast<KsNodeObject*>(o));
        }
    } else {
        QSqlQuery q("select * from nodes", db());
        q.exec();
        while(q.next()) {
            KsNodeObject *n = new KsNodeObject;
            n->setId(q.value(0).toLongLong());
            n->setName(q.value(1).toString());
            n->setNodeType((KsNodeObject::NodeType)q.value(2).toInt());
            n->setLocation(StringToPoint(q.value(3).toString()));
            mapObject(n);

            QList<KsRoadModelObject*> input = findRoadInputs(n);
            QList<KsRoadModelObject*> output = findRoadOutputs(n);

            foreach (KsRoadModelObject *r, input) {
                n->addInputRoad(r);
            }

            foreach (KsRoadModelObject *r, output) {
                n->addOutputRoad(r);
            }

            result.append(n);
        }
        q.finish();
    }

    return result;
}

KsNodeObject *KsProjectObjectStore::newNode()
{
    KsNodeObject *n = new KsNodeObject;
    m_lstNewObjects.append(n);
    mapObject(n);
    return n;
}

void KsProjectObjectStore::removeNode(KsNodeObject *node)
{
    unMapObject(node);
}

bool KsProjectObjectStore::check()
{
    QStringList tables = db().tables();

    if(tables.count()==5) {
        if(tables.contains("lanes") &&
                tables.contains("roads") &&
                tables.contains("rellanes") &&
                tables.contains("nodes") &&
                tables.contains("nodetoroad")) {
            QStringList fields = tableFields("lanes");
            if(fields.count()==3 &&
                    fields.contains("id") &&
                    fields.contains("roadId") &&
                    fields.contains("laneWidth")) {
                fields = tableFields("roads");
                if(fields.count()==6 &&
                        fields.contains("id") &&
                        fields.contains("name") &&
                        fields.contains("curve") &&
                        fields.contains("roadNormal") &&
                        fields.contains("designSpeed") &&
                        fields.contains("slope")) {
                    fields = tableFields("rellanes");
                    if(fields.count()==2 &&
                            fields.contains("sid") &&
                            fields.contains("did")) {
                        fields=tableFields("nodes");
                        if(fields.count()==4 &&
                                fields.contains("id") &&
                                fields.contains("name") &&
                                fields.contains("nodeType") &&
                                fields.contains("location")) {
                            fields=tableFields("nodetoroad");
                            if(fields.count()==3 &&
                                    fields.contains("snodeid") &&
                                    fields.contains("dnodeid") &&
                                    fields.contains("roadid")) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}

void KsProjectObjectStore::createTable(QString table)
{
    if(table=="lanes") {
        QSqlQuery q(db());
        q.exec("create table lanes ("
               "id integer primary key,"
               "roadId integer,"
               "laneWidth real);");
        q.finish();
    } else if(table=="roads") {
        QSqlQuery q(db());
        q.exec("create table roads ("
               "id integer primary key,"
               "name text,"
               "curve integer,"
               "roadNormal text,"
               "designSpeed real,"
               "slope real);");
        q.finish();
    } else if(table=="rellanes") {
        QSqlQuery q(db());
        q.exec("create table rellanes ("
               "sid integer not null,"
               "did integer not null,"
               "primary key(sid,did));");
        q.finish();
    } else if(table=="nodes") {
        QSqlQuery q(db());
        q.exec("create table nodes ("
               "id integer primary key,"
               "name text,"
               "nodeType integer,"
               "location text);");
        q.finish();
    } else if(table=="nodetoroad") {
        QSqlQuery q(db());
        q.exec("create table nodetoroad ("
               "snodeid integer,"
               "dnodeid integer,"
               "roadid integer,"
               "primary key (snodeid,dnodeid,roadid))");
        q.finish();
    }
}

void KsProjectObjectStore::createFromScratch()
{
    dropAllTables();
    createTable("lanes");
    createTable("roads");
    createTable("rellanes");
    createTable("nodes");
    createTable("nodetoroad");
}

void KsProjectObjectStore::insertObject(QObject *q)
{
    if(!qstrcmp(q->metaObject()->className(), "KsLaneObject")) {
        QSqlQuery query(db());
        KsLaneObject *l = qobject_cast<KsLaneObject*>(q);
        query.prepare("insert into lanes (roadId, laneWidth) "
                  "values (:roadId, :laneWidth);");
        query.bindValue(":roadId", l->roadId());
        query.bindValue(":laneWidth", l->laneWidth());
        query.exec();
        l->setId(query.lastInsertId().toLongLong());
        query.finish();
        m_lstRelationUpdateObjects.append(q);
    } else if(!qstrcmp(q->metaObject()->className(), "KsRoadModelObject")) {
        QSqlQuery query(db());
        KsRoadModelObject *r = qobject_cast<KsRoadModelObject*>(q);
        query.prepare("insert into roads (name, curve, roadNormal, designSpeed, slope) "
                      "values (:name, :curve, :roadNormal, :designSpeed, :slope)");
        query.bindValue(":name", r->name());
        query.bindValue(":curve", r->isCurve());
        query.bindValue(":roadNormal", PolygonToString(r->roadNormal()));
        query.bindValue(":designSpeed", r->designSpeed());
        query.bindValue(":slope", r->slope());
        query.exec();
        r->setId(query.lastInsertId().toLongLong());
        query.finish();
    } else if(!qstrcmp(q->metaObject()->className(), "KsNodeObject")) {
        QSqlQuery query(db());
        KsNodeObject *n = qobject_cast<KsNodeObject*>(q);
        query.prepare("insert into nodes (nodeType, location, name) "
                      "values (:nodeType, :location, :name)");
        query.bindValue(":nodeType", n->nodeType());
        query.bindValue(":location", PointToString(n->location()));
        query.bindValue(":name", n->name());
        if(!query.exec()) {
        }
        n->setId(query.lastInsertId().toLongLong());
        query.finish();
        m_lstRelationUpdateObjects.append(q);
    }
}

void KsProjectObjectStore::relationInsert(QObject *q)
{
    if(!qstrcmp(q->metaObject()->className(), "KsLaneObject")) {
        KsLaneObject *l = qobject_cast<KsLaneObject*>(q);

        for(RelatedLanesIterator i=l->inputLaneIterator();i!=l->inputLastIterator();i++) {
            QSqlQuery rQ(db());
            KsLaneObject *d = *i;
            rQ.prepare("insert into rellanes (sid,did) "
                       "values (:sid, :did)");
            rQ.bindValue(":sid", l->id());
            rQ.bindValue(":did", d->id());
            rQ.exec();
            rQ.finish();
        }
    } else if(!qstrcmp(q->metaObject()->className(), "KsNodeObject")) {
        KsNodeObject *n = qobject_cast<KsNodeObject*>(q);


        QSqlQuery rQ(db());
        rQ.prepare("insert into nodetoroad (snodeid, dnodeid, roadid) "
                   "values (:snodeid, :dnodeid, :roadid)");

        for(RoadListIterator i=n->inputRoadsBegin();i!=n->inputRoadsEnd();i++) {
            KsRoadModelObject *rmo = *i;
            rQ.bindValue(":snodeid", rmo->source()->id());
            rQ.bindValue(":dnodeid", rmo->destination()->id());
            rQ.bindValue(":roadid", rmo->id());
            rQ.exec();
            rQ.finish();
        }

        for(RoadListIterator i=n->outputRoadBegin();i!=n->outputRoadEnd();i++) {
            KsRoadModelObject *rmo = *i;
            rQ.bindValue(":snodeid", rmo->source()->id());
            rQ.bindValue(":dnodeid", rmo->destination()->id());
            rQ.bindValue(":roadid", rmo->id());
            rQ.exec();
            rQ.finish();
        }
    }
}

void KsProjectObjectStore::updateObject(QObject *q)
{
    if(!qstrcmp(q->metaObject()->className(), "KsLaneObject")) {
        QSqlQuery query(db());
        KsLaneObject *l = qobject_cast<KsLaneObject*>(q);
        query.prepare("update lanes set roadId = :roadId, "
                      "laneWidth = :laneWidth "
                  "where id = :id");
        query.bindValue(":roadId", l->roadId());
        query.bindValue(":laneWidth", l->laneWidth());
        query.bindValue(":id", l->id());
        query.exec();
        query.finish();
        query.prepare("delete from rellanes where sid = :sid");
        query.bindValue(":sid", l->id());
        query.exec();
        query.finish();
        for(RelatedLanesIterator i=l->inputLaneIterator();i!=l->inputLastIterator();i++) {
            QSqlQuery rQ(db());
            KsLaneObject *d = *i;
            rQ.prepare("insert into rellanes (sid,did) "
                       "values (:sid, :did)");
            rQ.bindValue(":sid", l->id());
            rQ.bindValue(":did", d->id());
            rQ.exec();
            rQ.finish();
        }
    } else if(!qstrcmp(q->metaObject()->className(), "KsRoadModelObject")) {
        QSqlQuery query(db());
        KsRoadModelObject *r = qobject_cast<KsRoadModelObject*>(q);
        query.prepare("update roads set name = :name, "
                      "curve = :curve, "
                      "roadNormal = :roadNormal, "
                      "designSpeed = :designSpeed, "
                      "slope = :slope "
                      "where id = :id");
        query.bindValue(":name", r->name());
        query.bindValue(":curve", r->isCurve());
        query.bindValue(":roadNormal", PolygonToString(r->roadNormal()));
        query.bindValue(":designSpeed", r->designSpeed());
        query.bindValue(":slope", r->slope());
        query.bindValue(":id", r->id());
        query.exec();
        query.finish();
    } else if(!qstrcmp(q->metaObject()->className(), "KsNodeObject")) {
        QSqlQuery query(db());
        KsNodeObject *n = qobject_cast<KsNodeObject*>(q);
        query.prepare("update nodes set "
                      "nodeType = :nodeType, "
                      "location = :location, "
                      "name = :name "
                      "where id = :id");
        query.bindValue(":nodeType", n->nodeType());
        query.bindValue(":location", PointToString(n->location()));
        query.bindValue(":name", n->name());
        query.bindValue(":id", n->id());
        query.exec();
        query.finish();

        query.prepare("delete from nodetoroad where snodeid = :sid and dnodeid = :did");
        query.bindValue(":sid", n->id());
        query.bindValue(":did", n->id());
        query.exec();
        query.finish();
        QSqlQuery rQ(db());
        rQ.prepare("insert into nodetoroad (snodeid, dnodeid, roadid) "
                   "values (:snodeid, :dnodeid, :roadid)");

        for(RoadListIterator i=n->inputRoadsBegin();i!=n->inputRoadsEnd();i++) {
            KsRoadModelObject *rmo = *i;
            rQ.bindValue(":snodeid", rmo->source()->id());
            rQ.bindValue(":dnodeid", rmo->destination()->id());
            rQ.bindValue(":roadid", rmo->id());
            rQ.exec();
            rQ.finish();
        }

        for(RoadListIterator i=n->outputRoadBegin();i!=n->outputRoadEnd();i++) {
            KsRoadModelObject *rmo = *i;
            rQ.bindValue(":snodeid", rmo->source()->id());
            rQ.bindValue(":dnodeid", rmo->destination()->id());
            rQ.bindValue(":roadid", rmo->id());
            rQ.exec();
            rQ.finish();
        }

    }
}

void KsProjectObjectStore::deleteObject(QObject *q)
{
    if(!qstrcmp(q->metaObject()->className(), "KsLaneObject")) {
        QSqlQuery query(db());
        KsLaneObject *l = qobject_cast<KsLaneObject*>(q);
        query.prepare("delete from lanes where id = :id");
        query.bindValue(":id", l->id());
        query.exec();
        query.finish();

        query.prepare("delete from rellanes where sid = :sid or did = :did");
        query.bindValue(":sid", l->id());
        query.bindValue(":did", l->id());
        query.exec();
        query.finish();

    } else if(!qstrcmp(q->metaObject()->className(), "KsRoadModelObject")) {
        QSqlQuery query(db());
        KsRoadModelObject *r = qobject_cast<KsRoadModelObject*>(q);
        query.prepare("delete from roads where id = :id");
        query.bindValue(":id", r->id());
        query.exec();
        query.finish();

    } if(!qstrcmp(q->metaObject()->className(), "KsNodeObject")) {
        QSqlQuery query(db());

        KsNodeObject *n = qobject_cast<KsNodeObject*>(q);
        query.prepare("delete from nodes where id = :id");
        query.bindValue(":id", n->id());
        query.exec();
        query.finish();

        query.prepare("delete from nodetoroad where snodeid=:sid and dnodeid=:did");
        query.bindValue("sid", n->id());
        query.bindValue(":did", n->id());
        query.exec();
        query.finish();
    }
}

void KsProjectObjectStore::sortNewObjects()
{
    QObjectList newList;

    foreach (QObject *obj, m_mapCreatedObjects["KsNodeObject"]) {
        if(m_lstNewObjects.contains(obj))
            newList.append(obj);
    }

    foreach (QObject *obj, m_mapCreatedObjects["KsRoadModelObject"]) {
        if(m_lstNewObjects.contains(obj))
            newList.append(obj);
    }

    foreach (QObject *obj, m_mapCreatedObjects["KsLaneObject"]) {
        if(m_lstNewObjects.contains(obj))
            newList.append(obj);
    }



    m_lstNewObjects.clear();
    foreach (QObject *obj, newList) {
        m_lstNewObjects.append(obj);
    }
}

QList<KsLaneObject *> KsProjectObjectStore::findRoadLanes(KsRoadModelObject *obj)
{
    QList<KsLaneObject*> result;

    foreach (QObject *o, m_mapCreatedObjects["KsLaneObject"]) {
        KsLaneObject *l = qobject_cast<KsLaneObject*>(o);
        if(l->roadId() == obj->id()) {
            result.append(l);
        }
    }
    return result;
}

QList<KsLaneObject *> KsProjectObjectStore::findInputLanes(KsLaneObject *obj)
{
    QList<KsLaneObject*> result;

    QSqlQuery q(db());
    q.prepare("select did from rellanes where sid=:sid");
    q.bindValue(":sid", obj->id());
    q.exec();
    while(q.next()) {
        qint64 lId = q.value(0).toLongLong();
        result.append(laneWithId(lId));
    }

    q.finish();

    return result;
}

QList<KsRoadModelObject *> KsProjectObjectStore::findRoadInputs(KsNodeObject *node)
{
    QList<KsRoadModelObject*> result;

    QSqlQuery q(db());
    q.prepare("select roadid from nodetoroad where dnodeid = :nodeid");
    q.bindValue(":nodeid", node->id());
    q.exec();
    while(q.next()) {
        qint64 rid = q.value(0).toLongLong();
        KsRoadModelObject *rmo = roadWithId(rid);
        result.append(rmo);
    }
    q.finish();

    return result;
}

QList<KsRoadModelObject *> KsProjectObjectStore::findRoadOutputs(KsNodeObject *node)
{
    QList<KsRoadModelObject*> result;

    QSqlQuery q(db());
    q.prepare("select roadid from nodetoroad where snodeid = :nodeid");
    q.bindValue(":nodeid", node->id());
    q.exec();
    while(q.next()) {
        qint64 rid = q.value(0).toLongLong();
        KsRoadModelObject *rmo = roadWithId(rid);
        result.append(rmo);
    }
    q.finish();

    return result;
}

KsLaneObject *KsProjectObjectStore::laneWithId(qint64 id)
{
    foreach (QObject *o, m_mapCreatedObjects["KsLaneObject"]) {
        KsLaneObject *l = qobject_cast<KsLaneObject*>(o);
        if(l->id()==id)
            return l;
    }
    return NULL;
}

KsRoadModelObject *KsProjectObjectStore::roadWithId(qint64 id)
{
    foreach (QObject *o, m_mapCreatedObjects["KsRoadModelObject"]) {
        KsRoadModelObject *r = qobject_cast<KsRoadModelObject*>(o);
        if(r->id()==id)
            return r;
    }
    return NULL;
}

