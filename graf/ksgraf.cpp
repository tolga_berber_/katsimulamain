#include "ksgraf.h"

#include "db/model/project/ksnodeobject.h"
#include "db/model/project/ksroadmodelobject.h"
#include <limits>

KsGraf::KsGraf(QObject *parent) : QObject(parent)
{

}

KsGraf::~KsGraf()
{

}

KsNodeObject *KsGraf::selectRandomDestNode(KsNodeObject *source)
{
    int idx;
    do {
        idx = qrand()%m_lstSources.size();
    } while(m_lstSources[idx]==source);
    return m_lstSources[idx];
}

NodeListIterator KsGraf::nodeBegin()
{
    return m_lstVertices.begin();
}

NodeListIterator KsGraf::nodeEnd()
{
    return m_lstVertices.end();
}

EdgeListIterator KsGraf::edgeBegin()
{
    return m_lstEdges.begin();
}

EdgeListIterator KsGraf::edgeEnd()
{
    return m_lstEdges.end();
}

int KsGraf::sourceCount()
{
    return m_lstSources.count();
}

KsNodeObject *KsGraf::source(int idx)
{
    return m_lstSources[idx];
}

RoadList KsGraf::findShortestPathBetweenTwoNodes(KsNodeObject *src, KsNodeObject *dst)
{
    RoadList result;
    bool *visited = new bool[m_lstVertices.length()];
    int sIdx = m_lstVertices.indexOf(src);
    int dIdx = m_lstVertices.indexOf(dst);

    qreal *lengths = new qreal[m_lstVertices.length()];
    RoadList *roads = new RoadList[m_lstVertices.length()];

    for(int i=0;i<m_lstVertices.length();i++) {
        visited[i] = 0;
        lengths[i] = std::numeric_limits<qreal>::max();
        roads[i] = RoadList();
    }

    visited[sIdx] = true;
    lengths[sIdx] = 0;

    while(true) {
        for(RoadListIterator i=src->outputRoadBegin();
            i!=src->outputRoadEnd();
            i++) {
            KsRoadModelObject *obj = *i;
            int rIndex = m_lstVertices.indexOf(obj->destination());
            qreal newLength = lengths[sIdx] +obj->roadLength();
            if(newLength<lengths[rIndex]) {
                lengths[rIndex] = newLength;
                roads[rIndex].clear();
                roads[rIndex]<<roads[sIdx];
                roads[rIndex].append(obj);
            }
        }

        int tIdx;
        qreal minLength = std::numeric_limits<qreal>::max();
        bool minLengthFound = false;

        for(int i=0;i<m_lstVertices.count();i++) {
            if(!visited[i]) {
                if(lengths[i]<minLength) {
                    tIdx = i;
                    minLength = lengths[i];
                    minLengthFound = true;
                }
            }
        }


        visited[tIdx]=true;

        if(!minLengthFound) {
            result = RoadList();
            break;
        }

        if(tIdx==dIdx) {
            result = roads[tIdx];
            break;
        }

        sIdx = tIdx;
        src = m_lstVertices[sIdx];
    }

    delete[] visited;
    delete[] lengths;
    delete[] roads;

    return result;
}

void KsGraf::addVertex(KsNodeObject *v)
{
    if(!m_lstVertices.contains(v)) {
        m_lstVertices.append(v);

        if(v->nodeType()==KsNodeObject::NTSource) {
            m_lstSources.append(v);
        }

        emit vertexAdded(v);
    }
}

void KsGraf::addEdge(KsRoadModelObject *e)
{
    if(!m_lstEdges.contains(e)) {
        m_lstEdges.append(e);

        emit edgeAdded(e);
    }
}

