#ifndef KSGRAF_H
#define KSGRAF_H

#include <QObject>
#include <QList>

class KsRoadModelObject;
class KsNodeObject;

typedef QList<KsNodeObject*> NodeList;
typedef QList<KsRoadModelObject*> EdgeList, RoadList;

typedef NodeList::iterator NodeListIterator;
typedef EdgeList::iterator EdgeListIterator;

class KsGraf : public QObject
{
    Q_OBJECT
public:
    explicit KsGraf(QObject *parent = 0);
    ~KsGraf();

    KsNodeObject *selectRandomDestNode(KsNodeObject *source);

    NodeListIterator nodeBegin();
    NodeListIterator nodeEnd();

    EdgeListIterator edgeBegin();
    EdgeListIterator edgeEnd();

    int sourceCount();
    KsNodeObject *source(int idx);

    RoadList findShortestPathBetweenTwoNodes(KsNodeObject *src, KsNodeObject *dst);

signals:
    void vertexAdded(KsNodeObject *v);
    void edgeAdded(KsRoadModelObject *e);

public slots:
    void addVertex(KsNodeObject *v);
    void addEdge(KsRoadModelObject *e);

private:
    NodeList m_lstVertices;
    NodeList m_lstSources;
    EdgeList m_lstEdges;

};

#endif // KSGRAF_H
