#include "kssettingsstorage.h"
#include <QMutex>
#include "workspace/ksdirectorymanager.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDriver>

KsSettingsStorage *KsSettingsStorage::s_pcSharedInstance = NULL;

KsSettingsStorage *KsSettingsStorage::sharedInstance()
{
    static QMutex mutex;

    if(s_pcSharedInstance==NULL) {
        mutex.lock();

        if(s_pcSharedInstance==NULL) {
            s_pcSharedInstance = new KsSettingsStorage();
        }
        mutex.unlock();
    }

    return s_pcSharedInstance;
}

QVariant KsSettingsStorage::readSetting(QString name, QVariant defaultValue)
{
    QSqlQuery q(m_cDB);
    q.prepare("select value from settings where key = :key");
    q.bindValue(":key",name);
    if(!q.exec()) {
        return defaultValue;
    } else {
        q.next();
        QByteArray arr = q.value(0).toByteArray();
        QDataStream str(arr);
        QVariant result;
        str>>result;
        return result;
    }
}

void KsSettingsStorage::saveSetting(QString name, QVariant value)
{
    QByteArray arr;
    QDataStream str(&arr, QIODevice::ReadWrite);
    str<<value;
    QSqlQuery q(m_cDB);
    q.prepare("insert or replace into settings (key, value) values (:key, :value)");
    q.bindValue(":key",name);
    q.bindValue(":value",arr);
    if(!q.exec()){
    }
}

KsSettingsStorage::KsSettingsStorage(QObject *parent) :
    QObject(parent)
{
    KsDirectoryManager::SharedInstance()->registerDirectory("ayar", "settings");
    m_cDB = KsDirectoryManager::SharedInstance()->openSqliteDatabase("ayar","settings.katsimula","Ayar");
    checkDB();
}

void KsSettingsStorage::checkDB()
{
    if(!m_cDB.tables().contains("settings")) {
        QSqlQuery q(m_cDB);
        q.prepare("create table settings (key varchar(256) UNIQUE NOT NULL, value BLOB NULL);");
        if(!q.exec()) {
        }
    }
}

