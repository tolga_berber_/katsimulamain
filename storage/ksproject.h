#ifndef KSPROJECT_H
#define KSPROJECT_H

#include <QObject>

class KsProjectObjectStore;
class KsGraf;
class KsRoadModelObject;
class KsNodeObject;
class KsLaneObject;
class KsSimulationEngine;

class KsProject : public QObject
{
    Q_OBJECT
public:
    explicit KsProject(QObject *parent = 0);
    ~KsProject();

    inline QString projectPath() {
        return m_strProjectPath;
    }

    QString projectName();

    KsGraf *graf();

    bool needSave();
    bool isNew();

    bool saveTo();
    bool save();

    void createProject();
    void openProject();

    KsRoadModelObject *newRoad();
    KsNodeObject *newNode();
    KsLaneObject *newLane();

    KsSimulationEngine *simEngine();

signals:
    void projectPathChanged(QString newPath);
    void projectNameChanged(QString newName);

public slots:
    void setProjectPath(QString newPath);
    void setProjectName(QString name);

private:
    QString m_strProjectPath;
    QString m_strProjectName;

    bool m_bNew;

    KsProjectObjectStore *m_ptrStore;
    KsGraf *m_ptrGraf;

    KsSimulationEngine *m_ptrEngine;

    void loadObject();

    bool moveFilesToLocation(QString oldPath, QString oldName);
    bool movefile(QString src, QString dst);
};

#endif // KSPROJECT_H
