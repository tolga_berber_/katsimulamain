#ifndef KSSETTINGSSTORAGE_H
#define KSSETTINGSSTORAGE_H

#include <QObject>
#include <QSqlDatabase>
#include <QVariant>

class KsSettingsStorage : public QObject
{
    Q_OBJECT
public:
    static KsSettingsStorage *sharedInstance();

    QVariant readSetting(QString name, QVariant defaultValue=QVariant());
signals:

public slots:
    void saveSetting(QString name, QVariant value);

private:
    static KsSettingsStorage *s_pcSharedInstance;

    QSqlDatabase m_cDB;

    explicit KsSettingsStorage(QObject *parent = 0);

    void checkDB();
};

#endif // KSSETTINGSSTORAGE_H
