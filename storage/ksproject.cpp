#include "ksproject.h"

#include "workspace/ksdirectorymanager.h"
#include "db/ksprojectobjectstore.h"
#include "db/model/project/ksroadmodelobject.h"
#include "db/model/project/kslaneobject.h"
#include "graf/ksgraf.h"
#include <QFile>
#include <QFileDialog>
#include "graphicObjects/ksgraphicnode.h"
#include "workspace/ksworkspace.h"

#include "graphicObjects/ksroadobject.h"

#include "graphicObjects/ksmovehandler.h"

#include "Simulation/kssimulationengine.h"


KsProject::KsProject(QObject *parent) :
    QObject(parent),
    m_ptrEngine(NULL)
{
    KsDirectoryManager::SharedInstance()->registerDirectory("temp", "temporary");
    setProjectPath(KsDirectoryManager::SharedInstance()->getPathForRelPath("temp", "unnamedproject"));
    setProjectName("unnamedproject");
    m_ptrGraf = new KsGraf;
}

KsProject::~KsProject()
{
    delete m_ptrGraf;
    delete m_ptrStore;
}

QString KsProject::projectName()
{
    return m_strProjectName;
}

KsGraf *KsProject::graf()
{
    return m_ptrGraf;
}

bool KsProject::needSave()
{
    return m_ptrStore->hasObjectToSave();
}

bool KsProject::isNew()
{
    return m_bNew;
}

bool KsProject::saveTo()
{
    QString newPath = QFileDialog::getSaveFileName(qobject_cast<QWidget*>(parent()), tr("Yeni Proje Klasörü Seçin"));
    QStringList pathElements = newPath.split('/');
    QString projectName = pathElements.last();
    QString oldPath = m_strProjectPath;
    QString oldName = m_strProjectName;
    m_ptrStore->close();
    setProjectName(projectName);
    setProjectPath(newPath);
    m_ptrStore->setName(projectName);
    m_ptrStore->setPath(tr("%1/%2_design.ksdf").arg(m_strProjectPath).arg(m_strProjectName));
    m_ptrStore->open();
    m_ptrStore->saveAll();
    m_bNew = false;
    return moveFilesToLocation(oldPath, oldName);;
}

bool KsProject::save()
{
    if(m_bNew)
        saveTo();
    else
        m_ptrStore->saveAll();

    m_bNew = false;

    return true;
}

void KsProject::createProject()
{
    m_ptrStore = new KsProjectObjectStore(m_strProjectName, tr("%1/%2_design.ksdf").arg(m_strProjectPath).arg(m_strProjectName));
    m_ptrStore->open(true);
    m_bNew = true;
    KsMoveHandler::hideAllMoveHandlers();
}

void KsProject::openProject()
{
    m_ptrStore = new KsProjectObjectStore(m_strProjectName, tr("%1/%2_design.ksdf").arg(m_strProjectPath).arg(m_strProjectName));
    m_ptrStore->open(false);
    m_bNew = false;
    loadObject();
    KsMoveHandler::hideAllMoveHandlers();
}

KsRoadModelObject *KsProject::newRoad()
{
    KsRoadModelObject *result = m_ptrStore->newRoad();
    m_ptrGraf->addEdge(result);
    return result;
}

KsNodeObject *KsProject::newNode()
{
    KsNodeObject *result = m_ptrStore->newNode();
    m_ptrGraf->addVertex(result);
    return result;
}

KsLaneObject *KsProject::newLane()
{
    KsLaneObject *result = m_ptrStore->newLane();
    return result;
}

KsSimulationEngine *KsProject::simEngine()
{
    if(!m_ptrEngine) {
        m_ptrEngine = new KsSimulationEngine(this);
    }
    return m_ptrEngine;
}

void KsProject::setProjectPath(QString newPath)
{
    if(m_strProjectPath!=newPath) {
        m_strProjectPath = newPath;
        QDir d;
        d.mkpath(m_strProjectPath);
        emit projectPathChanged(m_strProjectPath);
    }
}

void KsProject::setProjectName(QString name)
{
    if(m_strProjectName != name) {
        m_strProjectName = name;

        emit projectNameChanged(name);
    }
}


void KsProject::loadObject()
{
    m_ptrStore->lanes();
    QList<KsRoadModelObject*> rl = m_ptrStore->roads();
    QList<KsNodeObject*> nl = m_ptrStore->nodes();

    foreach (KsNodeObject *n, nl) {
        m_ptrGraf->addVertex(n);

        KsGraphicNode *nd = new KsGraphicNode(n);

        KsWorkspace::SharedInstance()->mapScene()->addItem(nd);
        KsWorkspace::SharedInstance()->mapScene()->currentTrafficObjectLayer()->addObject(nd);
    }

    foreach (KsRoadModelObject *e, rl) {
        m_ptrGraf->addEdge(e);

        KsRoadObject *rd = new KsRoadObject(e);

        KsWorkspace::SharedInstance()->mapScene()->addItem(rd);
        KsWorkspace::SharedInstance()->mapScene()->currentLinkLayer()->addObject(rd);
    }

    foreach (QGraphicsMapLayer *l, KsWorkspace::SharedInstance()->mapScene()->layers()) {
        l->unlockObjects();
    }
}

bool KsProject::moveFilesToLocation(QString oldPath, QString oldName)
{
    return movefile(tr("%1/%2_design.ksdf").arg(oldPath).arg(oldName),
             tr("%1/%2_design.ksdf").arg(m_strProjectPath).arg(m_strProjectName));
}

bool KsProject::movefile(QString src, QString dst)
{
    QFile f(src);
    f.copy(dst);
    return f.remove();
}
