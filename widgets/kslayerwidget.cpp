#include "kslayerwidget.h"
#include "ui_kslayerwidget.h"

#include "workspace/ksworkspace.h"

KsLayerWidget::KsLayerWidget(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::KsLayerWidget)
{
    ui->setupUi(this);

    ui->tbLayerCreate->setDefaultAction(ui->actionKatman_Ekle);
    ui->tbLayerDelete->setDefaultAction(ui->actionKatmanSil);
    ui->tbLayerRename->setDefaultAction(ui->actionKatmanYenidenAdlandir);
}

KsLayerWidget::~KsLayerWidget()
{
    delete ui;
}

void KsLayerWidget::on_tvLayers_activated(const QModelIndex &index)
{
    Q_UNUSED(index);
}

void KsLayerWidget::on_tvLayers_clicked(const QModelIndex &index)
{
    if(index.isValid()) {
        if(index.parent().isValid()) {
            KsGraphicObject *obj = qobject_cast<KsGraphicObject *>((QObject*)index.internalPointer());
            QGraphicsWorldItem *itm = qobject_cast<QGraphicsWorldItem*>(obj);
            if(itm)
                return;
            KsWorkspace::SharedInstance()->mapView()->centerOn(obj);
        }
    }
}
