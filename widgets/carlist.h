#ifndef CARLIST_H
#define CARLIST_H

#include <QDialog>
#include <QList>
#include <QTableWidgetItem>

class KsVehicle;

namespace Ui {
class CarList;
}

class CarList : public QDialog
{
    Q_OBJECT

public:
    explicit CarList(QWidget *parent = 0);
    ~CarList();

private slots:
    void on_btnYeniArac_clicked();

    void on_btnAracSil_clicked();

    void on_tblAraclar_itemDoubleClicked(QTableWidgetItem *item);

private:
    Ui::CarList *ui;

    QList<KsVehicle*> m_lstVehicles;

    void init();
    void updateList();
};

#endif // CARLIST_H
