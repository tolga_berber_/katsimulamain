#include "carlist.h"
#include "ui_carlist.h"
#include "workspace/ksworkspace.h"

#include "db/model/ksvehicle.h"
#include "yeniaractanimlama.h"

#include <QMessageBox>

#include <QTableWidgetItem>

CarList::CarList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CarList)
{
    ui->setupUi(this);
    init();
    ui->tblAraclar->setColumnWidth(0, 150);
    ui->tblAraclar->setColumnWidth(1,250);
}

CarList::~CarList()
{
}

void CarList::init()
{
    m_lstVehicles = KsWorkspace::SharedInstance()->staticDatabase()->vehicles();
    ui->tblAraclar->setRowCount(m_lstVehicles.count());

    updateList();
}

void CarList::updateList()
{
    ui->tblAraclar->clear();

    QStringList columnHeaders;
    columnHeaders<<"Araç Adı"<<"Araç Türü";
    ui->tblAraclar->setHorizontalHeaderLabels(columnHeaders);
    ui->tblAraclar->setRowCount(m_lstVehicles.count());

    for(int i=0;i<m_lstVehicles.count();i++) {
        QTableWidgetItem *itm = new QTableWidgetItem(m_lstVehicles.at(i)->name());
        ui->tblAraclar->setItem(i,0,itm);
        QTableWidgetItem *sym = new QTableWidgetItem;
        switch (m_lstVehicles[i]->vehicleType()) {
        case KsVehicle::Individual:
            sym->setIcon(QPixmap(":/img/vehicles/Individual.png"));
            sym->setText(KsVehicle::vehicleTypeString("Individual"));
            break;
        case KsVehicle::Family:
            sym->setIcon(QPixmap(":/img/vehicles/Family.png"));
            sym->setText(KsVehicle::vehicleTypeString("Family"));
            break;
        case KsVehicle::Business:
            sym->setIcon(QPixmap(":/img/vehicles/Business.png"));
            sym->setText(KsVehicle::vehicleTypeString("Business"));
            break;
        default:
            break;
        }
        ui->tblAraclar->setItem(i,1,sym);
    }
}

void CarList::on_btnYeniArac_clicked()
{
    YeniAracTanimlama *aracTanimi;
    KsVehicle *newVehicle = KsWorkspace::SharedInstance()->staticDatabase()->newVehicle();
    aracTanimi = new YeniAracTanimlama(newVehicle);
    if(aracTanimi->exec()==QDialog::Accepted) {
        m_lstVehicles.append(newVehicle);
        updateList();
    } else {
        KsWorkspace::SharedInstance()->staticDatabase()->removeVehicle(newVehicle);
    }
    delete aracTanimi;
}

void CarList::on_btnAracSil_clicked()
{
    QMessageBox mb;
    mb.setText(tr("Seçili aracı silmek istediğinize emin misiniz?"));
    mb.setInformativeText(tr("Bu işlem geri alınamaz!"));
    mb.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
    mb.setButtonText(QMessageBox::Yes, tr("Evet"));
    mb.setButtonText(QMessageBox::No, tr("Hayır"));

    if(mb.exec()==QMessageBox::Yes) {
        int idx = ui->tblAraclar->selectedItems()[0]->row();
        KsVehicle *obj = m_lstVehicles.takeAt(idx);
        KsWorkspace::SharedInstance()->staticDatabase()->removeVehicle(obj);
        updateList();
    }
}

void CarList::on_tblAraclar_itemDoubleClicked(QTableWidgetItem *item)
{
    YeniAracTanimlama *arac ;
    KsVehicle *v = m_lstVehicles.at(item->row());
    v->storeValues();

    arac = new YeniAracTanimlama(v);
    if(arac->exec()) {
        v->applyValues();
        updateList();
    } else {
        v->restoreValues();
    }
}
