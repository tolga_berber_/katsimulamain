#include "yeniaractanimlama.h"
#include "ui_yeniaractanimlama.h"

#include "db/model/ksvehicle.h"
#include <QMetaEnum>

YeniAracTanimlama::YeniAracTanimlama(KsVehicle *veh, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::YeniAracTanimlama),
    m_ptrVehicle(veh)
{
    ui->setupUi(this);

    QMetaEnum e = KsVehicle::staticMetaObject.enumerator(0);

    for(int i=0;i<e.keyCount();i++) {
        ui->cmbAracTuru->addItem(QPixmap(tr(":/img/vehicles/%1.png").arg(e.key(i)))
                                 , KsVehicle::vehicleTypeString(e.key(i)));
    }

    connectObject();
    setInitValues();
}

YeniAracTanimlama::~YeniAracTanimlama()
{
    delete ui;
}

void YeniAracTanimlama::handleDescriptionChange()
{
    m_ptrVehicle->setDescription(ui->edtAciklama->toPlainText());
}

void YeniAracTanimlama::handleTypeChange(int idx)
{
    m_ptrVehicle->setVehicleType((KsVehicle::VehicleType)idx);
}

void YeniAracTanimlama::connectObject()
{
    connect(ui->edtAd, SIGNAL(textChanged(QString)), m_ptrVehicle, SLOT(setName(QString)));
    connect(ui->edtAciklama, SIGNAL(textChanged()), this, SLOT(handleDescriptionChange()));
    connect(ui->spnYuksekHiz, SIGNAL(valueChanged(int)), m_ptrVehicle, SLOT(setMaxSpeed(qint32)));
    connect(ui->spnKoltukSayisi, SIGNAL(valueChanged(int)), m_ptrVehicle, SLOT(setNumberOfSeats(qint32)));
    connect(ui->cmbAracTuru, SIGNAL(currentIndexChanged(int)), this, SLOT(handleTypeChange(int)));
    connect(ui->dspGenislik, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setWidth(qreal)));
    connect(ui->dspYukseklik, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setHeight(qreal)));
    connect(ui->dspAgirlik, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setWeight(qreal)));
    connect(ui->dspAksYuku, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setAxleLoad(qreal)));
    connect(ui->dspKarbon, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setCarbonEmission(qreal)));
    connect(ui->dpsUzunluk, SIGNAL(valueChanged(double)), m_ptrVehicle, SLOT(setLength(qreal)));
}

void YeniAracTanimlama::setInitValues()
{
    ui->edtAd->setText(m_ptrVehicle->name());
    ui->edtAciklama->setPlainText(m_ptrVehicle->description());
    ui->spnYuksekHiz->setValue(m_ptrVehicle->maxSpeed());
    ui->spnKoltukSayisi->setValue(m_ptrVehicle->numberOfSeats());
    ui->cmbAracTuru->setCurrentIndex((int)m_ptrVehicle->vehicleType());
    ui->dspGenislik->setValue(m_ptrVehicle->width());
    ui->dspYukseklik->setValue(m_ptrVehicle->height());
    ui->dspAgirlik->setValue(m_ptrVehicle->weight());
    ui->dspAksYuku->setValue(m_ptrVehicle->axleLoad());
    ui->dspKarbon->setValue(m_ptrVehicle->carbonEmission());
    ui->dpsUzunluk->setValue(m_ptrVehicle->length());
}
