#include "ksroadpropertieswidget.h"
#include "ui_ksroadpropertieswidget.h"

#include "db/model/project/ksroadmodelobject.h"
#include "db/model/project/kslaneobject.h"

#include "utilities/ksgeometryhelper.h"
#include "utilities/polygonhandlers.h"

KsRoadPropertiesWidget::KsRoadPropertiesWidget(KsRoadModelObject *obj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KsRoadPropertiesWidget),
    m_ptrRoad(obj)
{
    ui->setupUi(this);

    updateUI();
}

KsRoadPropertiesWidget::~KsRoadPropertiesWidget()
{
    delete ui;
}

void KsRoadPropertiesWidget::updateUI()
{
    ui->edtName->setText(m_ptrRoad->name());
    ui->spnDesignSpeed->setValue(m_ptrRoad->designSpeed());
    ui->spnLaneCount->setValue(m_ptrRoad->laneCount());
    ui->spnSlope->setValue(m_ptrRoad->slope());
    QPolygonF poly = KsGeometryHelper::sharedInstance()->convertFromQt(m_ptrRoad->roadNormal());
    if(m_ptrRoad->isCurve()) {
        poly = PolygonHandlers::calculateBezier(poly);
    }
    qreal length = KsGeometryHelper::sharedInstance()->length(poly);
    ui->edtRoadLength->setText(QString::number(length));
    setWindowTitle(tr("%1 Yoluna Ait Özellikler").arg(m_ptrRoad->name()));
}
