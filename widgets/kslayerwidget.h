#ifndef KSLAYERWIDGET_H
#define KSLAYERWIDGET_H

#include <QDockWidget>

#include "graphicObjects/qmaplayermodel.h"

namespace Ui {
class KsLayerWidget;
}

/**
 * @brief Katman yönetici dock widget
 *
 */
class KsLayerWidget : public QDockWidget
{
    Q_OBJECT

public:
    /**
     * @brief Burada eylemlerin buttonlar ile ilişkilendirmesi sağlanmaktadır.
     *
     * @param parent Göz ardı edilebilir, Qt Uyumluluğu için konulmuştur.
     */
    explicit KsLayerWidget(QWidget *parent = 0);

    /**
     * @brief Oluşturulan nesnelerin silinmesini sağlar.
     *
     */
    ~KsLayerWidget();

private slots:
    void on_tvLayers_activated(const QModelIndex &index);

    void on_tvLayers_clicked(const QModelIndex &index);

private:
    Ui::KsLayerWidget *ui; /**< Grafiksel Arabirim sınıfı */

    QMapLayerModel *m_pcModel; /**< Katman bilgilerini gösteren model **/
};

#endif // KSLAYERWIDGET_H
