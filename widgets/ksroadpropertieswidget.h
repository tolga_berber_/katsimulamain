#ifndef KSROADPROPERTIESWIDGET_H
#define KSROADPROPERTIESWIDGET_H

#include <QDialog>

class KsRoadModelObject;

namespace Ui {
    class KsRoadPropertiesWidget;
}

class KsRoadPropertiesWidget : public QDialog
{
    Q_OBJECT

public:
    explicit KsRoadPropertiesWidget(KsRoadModelObject *obj, QWidget *parent = 0);
    ~KsRoadPropertiesWidget();

private:
    Ui::KsRoadPropertiesWidget *ui;

    KsRoadModelObject *m_ptrRoad;

    void updateUI();
};

#endif // KSROADPROPERTIESWIDGET_H
