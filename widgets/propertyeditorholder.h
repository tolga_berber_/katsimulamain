#ifndef PROPERTYEDITORHOLDER_H
#define PROPERTYEDITORHOLDER_H

#include <QDockWidget>
#include <QLabel>

namespace Ui {
class PropertyEditorHolder;
}

class QtTreePropertyBrowser;

class PropertyEditorHolder : public QDockWidget
{
    Q_OBJECT

public:
    explicit PropertyEditorHolder(QWidget *parent = 0);
    ~PropertyEditorHolder();

public slots:
    void setPropertyBrowser(QtTreePropertyBrowser *browser);

private:
    Ui::PropertyEditorHolder *ui;
    QLabel *m_ptrNoEditorLabel;
    QtTreePropertyBrowser *m_ptrBrowser;
};

#endif // PROPERTYEDITORHOLDER_H
