#ifndef YENIARACTANIMLAMA_H
#define YENIARACTANIMLAMA_H

#include <QDialog>

class KsVehicle;

namespace Ui {
class YeniAracTanimlama;
}

class YeniAracTanimlama : public QDialog
{
    Q_OBJECT

public:
    explicit YeniAracTanimlama(KsVehicle *veh, QWidget *parent = 0);
    ~YeniAracTanimlama();

private slots:
    void handleDescriptionChange();
    void handleTypeChange(int idx);

private:
    Ui::YeniAracTanimlama *ui;

    KsVehicle *m_ptrVehicle;

    void connectObject();
    void setInitValues();
};

#endif // YENIARACTANIMLAMA_H
