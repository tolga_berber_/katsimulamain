#include "propertyeditorholder.h"
#include "ui_propertyeditorholder.h"

#include "qttreepropertybrowser.h"

PropertyEditorHolder::PropertyEditorHolder(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::PropertyEditorHolder),
    m_ptrBrowser(NULL)
{
    ui->setupUi(this);
    m_ptrNoEditorLabel = ui->label;

}

PropertyEditorHolder::~PropertyEditorHolder()
{
    delete ui;
}

void PropertyEditorHolder::setPropertyBrowser(QtTreePropertyBrowser *browser)
{
    if(m_ptrBrowser!=browser) {
        if(m_ptrBrowser) {
            ui->verticalLayout->removeWidget(m_ptrBrowser);
            m_ptrBrowser->hide();
            ui->verticalLayout->update();
        } else {
            ui->verticalLayout->removeWidget(m_ptrNoEditorLabel);
            m_ptrNoEditorLabel->hide();
            ui->verticalLayout->update();
        }
        if(browser) {
            m_ptrBrowser = browser;
            ui->verticalLayout->addWidget(browser);
            m_ptrBrowser->show();
            ui->verticalLayout->update();
        } else {
            m_ptrBrowser = NULL;
            ui->verticalLayout->addWidget(m_ptrNoEditorLabel);
            m_ptrNoEditorLabel->show();
            ui->verticalLayout->update();
        }
    }
}
