#include "ksroadpropertyeditor.h"

#include "db/model/project/ksroadmodelobject.h"
#include "graphicObjects/ksroadobject.h"

#include "propertyEditors/kspropertyeditorstore.h"
#include "propertyEditors/propertyeditorconstants.h"
#include "workspace/ksworkspace.h"

#include "qtpropertymanager.h"

#include "qteditorfactory.h"

#include "qttreepropertybrowser.h"

KsRoadPropertyEditor::KsRoadPropertyEditor(QObject *parent):QObject(parent),
    m_ptrBrowser(NULL)
{
    m_bUpdateRequestedFromModel = false;
}

KsRoadPropertyEditor::~KsRoadPropertyEditor()
{

}

QtTreePropertyBrowser *KsRoadPropertyEditor::getBrowser()
{
    if(!m_ptrBrowser) {
        prepareEditor();
    }

    return m_ptrBrowser;
}

void KsRoadPropertyEditor::setQObject(QObject *target)
{
    if(!target) {
        unsetQObject();
        return;
    }
    if(!m_ptrBrowser)
        prepareEditor();
    KsRoadObject *nodeG = qobject_cast<KsRoadObject*>(target);
    if(m_ptrTarget!=nodeG->model()) {
        m_ptrTarget = nodeG->model();
        updateEditorValues();

        connect(m_ptrTarget, SIGNAL(idChanged(qint64)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(nameChanged(QString)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(curveChanged(bool)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(laneCountChanged(qint32)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(designSpeedChanged(qreal)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(slopeChanged(qreal)), this, SLOT(updateEditorValues()));

        connect(m_ptrIntManager, SIGNAL(valueChanged(QtProperty*,int)),
                this, SLOT(handleLaneCountChange(QtProperty*,int)));
        connect(m_ptrStringManager, SIGNAL(valueChanged(QtProperty*,QString)),
                this, SLOT(handleNameChanged(QtProperty*,QString)));
        connect(m_ptrBoolManager, SIGNAL(valueChanged(QtProperty*,bool)),
                this, SLOT(handleCurveChange(QtProperty*,bool)));
        connect(m_ptrDoubleManager, SIGNAL(valueChanged(QtProperty*,double)),
                this, SLOT(handleDoubleChange(QtProperty*,qreal)));
    }
}

void KsRoadPropertyEditor::unsetQObject()
{
    disconnect(m_ptrTarget, SIGNAL(idChanged(qint64)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(nameChanged(QString)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(curveChanged(bool)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(laneCountChanged(qint32)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(designSpeedChanged(qreal)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(slopeChanged(qreal)), this, SLOT(updateEditorValues()));
    m_ptrTarget = NULL;
}

void KsRoadPropertyEditor::updateEditorValues()
{
    m_bUpdateRequestedFromModel = true;
    m_ptrIntManager->setValue(m_ptrIdProperty, m_ptrTarget->id());
    m_ptrStringManager->setValue(m_ptrNameProperty, m_ptrTarget->name());
    m_ptrBoolManager->setValue(m_ptrCurveProperty, m_ptrTarget->isCurve());
    m_ptrIntManager->setValue(m_ptrLaneCountProperty, m_ptrTarget->laneCount());
    m_ptrDoubleManager->setValue(m_ptrDesignSpeedProperty, m_ptrTarget->designSpeed());
    m_ptrDoubleManager->setValue(m_ptrSlopeProperty, m_ptrTarget->slope());
    m_bUpdateRequestedFromModel = false;
}

void KsRoadPropertyEditor::handleNameChanged(QtProperty *p, QString newVal)
{
    if(m_bUpdateRequestedFromModel)
        return;
    if(p==m_ptrNameProperty) {
        m_ptrTarget->setName(newVal);
    }
}

void KsRoadPropertyEditor::handleLaneCountChange(QtProperty *p, int newVal)
{
    if(m_bUpdateRequestedFromModel)
        return;
    if(p==m_ptrLaneCountProperty) {
        m_ptrTarget->setLaneCount(newVal);
    }
}

void KsRoadPropertyEditor::handleCurveChange(QtProperty *p, bool newVal)
{
    if(m_bUpdateRequestedFromModel)
        return;
    if(p==m_ptrCurveProperty) {
        m_ptrTarget->setCurve(newVal);
    }
}

void KsRoadPropertyEditor::handleDoubleChange(QtProperty *p, qreal newVal)
{
    if(m_bUpdateRequestedFromModel)
        return;
    if(p==m_ptrDesignSpeedProperty) {
        m_ptrTarget->setDesignSpeed(newVal);
    } else if(p==m_ptrSlopeProperty) {
        m_ptrTarget->setSlope(newVal);
    }
}

void KsRoadPropertyEditor::prepareEditor()
{
    KsPropertyEditorStore *store = KsWorkspace::SharedInstance()->propertyEditorStore();

    m_ptrIntManager = new QtIntPropertyManager(this);
    m_ptrStringManager = new QtStringPropertyManager(this);
    m_ptrDoubleManager = new QtDoublePropertyManager(this);
    m_ptrBoolManager = new QtBoolPropertyManager(this);

    m_ptrIdProperty = m_ptrIntManager->addProperty(tr("Kaynak Id"));
    m_ptrNameProperty = m_ptrStringManager->addProperty(tr("Kaynak Adı"));
    m_ptrLaneCountProperty = m_ptrIntManager->addProperty(tr("Şerit Sayısı"));
    m_ptrCurveProperty = m_ptrBoolManager->addProperty(tr("Eğrileştirme Aktif"));
    m_ptrDesignSpeedProperty = m_ptrDoubleManager->addProperty(tr("Tasarım Hızı"));
    m_ptrSlopeProperty = m_ptrDoubleManager->addProperty(tr("Eğim"));

    m_ptrIdProperty->setEnabled(false);
    m_ptrIdProperty->setToolTip(tr("Kaynağın programdaki ID si."));

    m_ptrSpinBoxFactory = qobject_cast<QtSpinBoxFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_SPINBOX, QVariant::Int, KsPropertyEditorStore::PET_SpinBox));
    m_ptrTextEditorFactory = qobject_cast<QtLineEditFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_TEXTEDIT, QVariant::String, KsPropertyEditorStore::PET_LineEdit));
    m_ptrCheckBoxFactory = qobject_cast<QtCheckBoxFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_CHECKBOX, QVariant::Bool, KsPropertyEditorStore::PET_CheckBox));
    m_ptrDoubleSpinboxFactory = qobject_cast<QtDoubleSpinBoxFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_DOUBLESPINBOX, QVariant::Double, KsPropertyEditorStore::PET_DoubleSpinBox));

    m_ptrBrowser = new QtTreePropertyBrowser;

    m_ptrBrowser->addProperty(m_ptrIdProperty);
    m_ptrBrowser->addProperty(m_ptrNameProperty);
    m_ptrBrowser->addProperty(m_ptrLaneCountProperty);
    m_ptrBrowser->addProperty(m_ptrCurveProperty);
    m_ptrBrowser->addProperty(m_ptrDesignSpeedProperty);
    m_ptrBrowser->addProperty(m_ptrSlopeProperty);

    m_ptrBrowser->setFactoryForManager(m_ptrIntManager, m_ptrSpinBoxFactory);
    m_ptrBrowser->setFactoryForManager(m_ptrStringManager, m_ptrTextEditorFactory);
    m_ptrBrowser->setFactoryForManager(m_ptrBoolManager, m_ptrCheckBoxFactory);
    m_ptrBrowser->setFactoryForManager(m_ptrDoubleManager, m_ptrDoubleSpinboxFactory);
}

