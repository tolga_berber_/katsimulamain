#ifndef KSWORLDPROPERTYEDITOR_H
#define KSWORLDPROPERTYEDITOR_H

#include <QObject>
#include "propertyEditors/kspropertyeditorbridge.h"

class QtPointFPropertyManager;
class QtSizeFPropertyManager;
class QtProperty;
class QtDoubleSpinBoxFactory;
class QtTreePropertyBrowser;
class QGraphicsWorldItem;

class KsWorldPropertyEditor : public QObject, public KsPropertyEditorBridge
{
    Q_OBJECT
public:
    explicit KsWorldPropertyEditor(QObject *parent = 0);
    ~KsWorldPropertyEditor();

    virtual QtTreePropertyBrowser *getBrowser();
    virtual void setQObject(QObject *target);
    virtual void unsetQObject();

signals:

private slots:
    void visibleChanged();

private:
    QGraphicsWorldItem *m_ptrTarget;
    QtPointFPropertyManager *m_ptrPointFManager;
    QtSizeFPropertyManager *m_ptrSizeFManager;

    QtProperty *m_ptrTopLeftProperty;
    QtProperty *m_ptrWorldSizeProperty;

    QtDoubleSpinBoxFactory *m_ptrDoubleSpinBoxFactory;

    QtTreePropertyBrowser *m_ptrBrowser;

    void prepareEditor();

    void updateEditorValues();
};

#endif // KSWORLDPROPERTYEDITOR_H
