#include "ksworldpropertyeditor.h"

#include "workspace/ksworkspace.h"

#include "../propertyeditorconstants.h"

#include "qtpropertymanager.h"

#include "qteditorfactory.h"

#include "qttreepropertybrowser.h"

#include "graphicObjects/qgraphicsworlditem.h"

KsWorldPropertyEditor::KsWorldPropertyEditor(QObject *parent) : QObject(parent),
    m_ptrTarget(NULL),
    m_ptrPointFManager(NULL),
    m_ptrSizeFManager(NULL),
    m_ptrTopLeftProperty(NULL),
    m_ptrWorldSizeProperty(NULL),
    m_ptrDoubleSpinBoxFactory(NULL),
    m_ptrBrowser(NULL)
{

}

KsWorldPropertyEditor::~KsWorldPropertyEditor()
{

}

QtTreePropertyBrowser *KsWorldPropertyEditor::getBrowser()
{
    if(!m_ptrBrowser) {
        prepareEditor();
    }
    return m_ptrBrowser;
}

void KsWorldPropertyEditor::setQObject(QObject *target)
{
    if(m_ptrTarget!=target) {
        m_ptrTarget = qobject_cast<QGraphicsWorldItem*>(target);
        updateEditorValues();
        connect(KsWorkspace::SharedInstance()->mapScene(),SIGNAL(visiblePortionChanged()),
                this, SLOT(visibleChanged()));
    }
}

void KsWorldPropertyEditor::unsetQObject()
{
    m_ptrTarget = NULL;
    disconnect(KsWorkspace::SharedInstance()->mapScene(),SIGNAL(visiblePortionChanged()),
               this, SLOT(visibleChanged()));
}

void KsWorldPropertyEditor::visibleChanged()
{
    updateEditorValues();
}

void KsWorldPropertyEditor::prepareEditor()
{
    KsPropertyEditorStore *store = KsWorkspace::SharedInstance()->propertyEditorStore();

    m_ptrPointFManager = qobject_cast<QtPointFPropertyManager*>(store->getManagerForNameAndType(SIMPLE_MANAGER_POINTF, QVariant::PointF));
    m_ptrSizeFManager = qobject_cast<QtSizeFPropertyManager*>(store->getManagerForNameAndType(SIMPLE_MANAGER_SIZEF, QVariant::SizeF));

    m_ptrTopLeftProperty = m_ptrPointFManager->addProperty(tr("Sol Üst Nokta"));
    m_ptrWorldSizeProperty = m_ptrSizeFManager->addProperty(tr("Görünen Alan Büyüklüğü"));

    m_ptrTopLeftProperty->setEnabled(false);
    m_ptrTopLeftProperty->setToolTip(tr("Ekranda Görünen Harita Bölümünün Sol Üst Köşe Koordinatı"));

    m_ptrWorldSizeProperty->setEnabled(false);
    m_ptrWorldSizeProperty->setToolTip(tr("Ekranda Görünen Harita Bölümünen Büyüklüğü"));

    m_ptrDoubleSpinBoxFactory = qobject_cast<QtDoubleSpinBoxFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_DOUBLESPINBOX, QVariant::Double, KsPropertyEditorStore::PET_DoubleSpinBox));

    m_ptrBrowser = new QtTreePropertyBrowser;

    m_ptrBrowser->addProperty(m_ptrTopLeftProperty);
    m_ptrBrowser->addProperty(m_ptrWorldSizeProperty);

    m_ptrBrowser->setFactoryForManager(m_ptrPointFManager->subDoublePropertyManager(), m_ptrDoubleSpinBoxFactory);
    m_ptrBrowser->setFactoryForManager(m_ptrSizeFManager->subDoublePropertyManager(), m_ptrDoubleSpinBoxFactory);
}

void KsWorldPropertyEditor::updateEditorValues()
{
    QRectF r = m_ptrTarget->visibleWorldPart(0);
    m_ptrPointFManager->setValue(m_ptrTopLeftProperty, r.topLeft());
    m_ptrSizeFManager->setValue(m_ptrWorldSizeProperty, r.size());
}

