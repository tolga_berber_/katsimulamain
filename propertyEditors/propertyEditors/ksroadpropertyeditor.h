#ifndef KSROADPROPERTYEDITOR_H
#define KSROADPROPERTYEDITOR_H

#include <QObject>

#include "propertyEditors/kspropertyeditorbridge.h"

class QtProperty;
class QtPointFPropertyManager;
class QtIntPropertyManager;
class QtStringPropertyManager;
class QtDoublePropertyManager;
class QtBoolPropertyManager;

class QtSpinBoxFactory;
class QtLineEditFactory;
class QtDoubleSpinBoxFactory;
class QtCheckBoxFactory;

class QtTreePropertyBrowser;
class KsRoadModelObject;


class KsRoadPropertyEditor : public QObject, public KsPropertyEditorBridge
{
    Q_OBJECT
public:
    explicit KsRoadPropertyEditor(QObject *parent=0);
    ~KsRoadPropertyEditor();

    virtual QtTreePropertyBrowser *getBrowser();
    virtual void setQObject(QObject *target);
    virtual void unsetQObject();

signals:

public slots:    
    void updateEditorValues();

    void handleNameChanged(QtProperty *p, QString newVal);
    void handleLaneCountChange(QtProperty *p, int newVal);
    void handleCurveChange(QtProperty *p, bool newVal);
    void handleDoubleChange(QtProperty *p, qreal newVal);


private:
    bool m_bUpdateRequestedFromModel;
    QtTreePropertyBrowser *m_ptrBrowser;
    KsRoadModelObject *m_ptrTarget;

    QtProperty *m_ptrIdProperty;
    QtProperty *m_ptrNameProperty;
    QtProperty *m_ptrLaneCountProperty;
    QtProperty *m_ptrCurveProperty;
    QtProperty *m_ptrDesignSpeedProperty;
    QtProperty *m_ptrSlopeProperty;

    QtIntPropertyManager *m_ptrIntManager;
    QtStringPropertyManager *m_ptrStringManager;
    QtBoolPropertyManager *m_ptrBoolManager;
    QtDoublePropertyManager *m_ptrDoubleManager;

    QtSpinBoxFactory *m_ptrSpinBoxFactory;
    QtLineEditFactory *m_ptrTextEditorFactory;
    QtCheckBoxFactory *m_ptrCheckBoxFactory;
    QtDoubleSpinBoxFactory *m_ptrDoubleSpinboxFactory;

    void prepareEditor();
};

#endif // KSROADPROPERTYEDITOR_H
