#include "ksnodepropertyeditor.h"

#include "db/model/project/ksnodeobject.h"
#include "graphicObjects/ksgraphicnode.h"

#include "propertyEditors/kspropertyeditorstore.h"
#include "propertyEditors/propertyeditorconstants.h"
#include "workspace/ksworkspace.h"

#include "qtpropertymanager.h"

#include "qteditorfactory.h"

#include "qttreepropertybrowser.h"

KsNodePropertyEditor::KsNodePropertyEditor(QObject *parent) : QObject(parent),
    m_ptrBrowser(NULL)
{

}

KsNodePropertyEditor::~KsNodePropertyEditor()
{

}

QtTreePropertyBrowser *KsNodePropertyEditor::getBrowser()
{
    if(!m_ptrBrowser) {
        prepareEditor();
    }

    return m_ptrBrowser;
}

void KsNodePropertyEditor::setQObject(QObject *target)
{
    if(!target) {
        unsetQObject();
        return;
    }
    if(!m_ptrBrowser)
        prepareEditor();
    KsGraphicNode *nodeG = qobject_cast<KsGraphicNode*>(target);
    if(m_ptrTarget!=nodeG->model()) {
        m_ptrTarget = nodeG->model();
        updateEditorValues();

        connect(m_ptrTarget, SIGNAL(idChanged(qint64)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(nameChanged(QString)), this, SLOT(updateEditorValues()));
        connect(m_ptrTarget, SIGNAL(locationChanged(QPointF)), this, SLOT(updateEditorValues()));

        connect(m_ptrNameManager, SIGNAL(valueChanged(QtProperty*,QString)), this, SLOT(handleNameChanged(QtProperty*,QString)));
    }
}

void KsNodePropertyEditor::unsetQObject()
{
    disconnect(m_ptrTarget, SIGNAL(idChanged(qint64)), this, SLOT(updateEditorValues()));
    disconnect(m_ptrTarget, SIGNAL(nameChanged(QString)), this, SLOT(updateEditorValues()));
    m_ptrTarget = NULL;
}

void KsNodePropertyEditor::updateEditorValues()
{
    m_ptrIdManager->setValue(m_ptrIdProperty, m_ptrTarget->id());
    m_ptrNameManager->setValue(m_ptrNameProperty, m_ptrTarget->name());
}

void KsNodePropertyEditor::handleNameChanged(QtProperty *p, QString newVal)
{
    if(p==m_ptrNameProperty) {
        m_ptrTarget->setName(newVal);
    }
}

void KsNodePropertyEditor::prepareEditor()
{
    KsPropertyEditorStore *store = KsWorkspace::SharedInstance()->propertyEditorStore();

    m_ptrIdManager = qobject_cast<QtIntPropertyManager*>(store->getManagerForNameAndType(SIMPLE_MANAGER_INT, QVariant::Int));
    m_ptrNameManager = qobject_cast<QtStringPropertyManager*>(store->getManagerForNameAndType(SIMPLE_MANAGER_STRING, QVariant::String));

    m_ptrIdProperty = m_ptrIdManager->addProperty(tr("Kaynak Id"));
    m_ptrNameProperty = m_ptrNameManager->addProperty(tr("Kaynak Adı"));

    m_ptrIdProperty->setEnabled(false);
    m_ptrIdProperty->setToolTip(tr("Kaynağın programdaki ID si."));

    m_ptrSpinBoxFactory = qobject_cast<QtSpinBoxFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_SPINBOX, QVariant::Int, KsPropertyEditorStore::PET_SpinBox));
    m_ptrTextEditorFactory = qobject_cast<QtLineEditFactory*>(store->getEditorFactoryForNameAndType(SIMPLE_FACTORY_TEXTEDIT, QVariant::String, KsPropertyEditorStore::PET_LineEdit));

    m_ptrBrowser = new QtTreePropertyBrowser;

    m_ptrBrowser->addProperty(m_ptrIdProperty);
    m_ptrBrowser->addProperty(m_ptrNameProperty);


    m_ptrBrowser->setFactoryForManager(m_ptrIdManager, m_ptrSpinBoxFactory);
    m_ptrBrowser->setFactoryForManager(m_ptrNameManager, m_ptrTextEditorFactory);
}

