#ifndef KSNODEPROPERTYEDITOR_H
#define KSNODEPROPERTYEDITOR_H

#include <QObject>

#include "propertyEditors/kspropertyeditorbridge.h"

class QtProperty;
class QtPointFPropertyManager;
class QtIntPropertyManager;
class QtStringPropertyManager;

class QtSpinBoxFactory;
class QtLineEditFactory;
class QtDoubleSpinBoxFactory;

class QtTreePropertyBrowser;
class KsNodeObject;

class KsNodePropertyEditor : public QObject, public KsPropertyEditorBridge
{
    Q_OBJECT
public:
    explicit KsNodePropertyEditor(QObject *parent = 0);
    ~KsNodePropertyEditor();

    virtual QtTreePropertyBrowser *getBrowser();
    virtual void setQObject(QObject *target);
    virtual void unsetQObject();

signals:

public slots:

private slots:
    void updateEditorValues();

    void handleNameChanged(QtProperty *p, QString newVal);

private:
    QtTreePropertyBrowser *m_ptrBrowser;
    KsNodeObject *m_ptrTarget;

    QtProperty *m_ptrIdProperty;
    QtProperty *m_ptrNameProperty;

    QtIntPropertyManager *m_ptrIdManager;
    QtStringPropertyManager *m_ptrNameManager;

    QtSpinBoxFactory *m_ptrSpinBoxFactory;
    QtLineEditFactory *m_ptrTextEditorFactory;

    void prepareEditor();
};

#endif // KSNODEPROPERTYEDITOR_H
