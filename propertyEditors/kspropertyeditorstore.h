#ifndef KSPROPERTYEDITORSTORE_H
#define KSPROPERTYEDITORSTORE_H

#include <QObject>
#include <QString>
#include <QHash>

#include <QVariant>

#include "widgets/propertyeditorholder.h"

class QtTreePropertyBrowser;
class KsPropertyEditorBridge;
class QGraphicsMapLayer;
class KsGraphicObject;
class QtAbstractPropertyManager;
class QtAbstractEditorFactoryBase;

typedef QHash<QString, KsPropertyEditorBridge*> PropertyEditorMap;
typedef QHash<QString, QtAbstractPropertyManager*> PropertyManagerMap;
typedef QHash<QString, QtAbstractEditorFactoryBase*> PropertEditorFactoryMap;

class KsPropertyEditorStore : public QObject
{
    Q_OBJECT
    Q_ENUMS(PropertyEditorType)
public:

    enum PropertyEditorType {
        PET_SpinBox = 0,
        PET_Slider,
        PET_ScrollBar,
        PET_CheckBox,
        PET_DoubleSpinBox,
        PET_LineEdit,
        PET_DateEdit,
        PET_TimeEdit,
        PET_DateTimeEdit,
        PET_KeySequenceEdit,
        PET_CharEdit,
        PET_EnumEditor,
        PET_CursorEditor,
        PET_ColorEditor,
        PET_FontEditor,
        PET_TotalNumberOfEditors
    };


    static KsPropertyEditorStore *sharedInstance();

    QtTreePropertyBrowser *getPropertyBrowserForActiveObject();

    QtAbstractPropertyManager *getManagerForNameAndType(QString name, QVariant::Type dataType);

    QtAbstractEditorFactoryBase *getEditorFactoryForNameAndType(QString name, QVariant::Type dataType, PropertyEditorType editorType);

    PropertyEditorHolder *getUI();


private:
    explicit KsPropertyEditorStore(QObject *parent = 0);

    static KsPropertyEditorStore *s_ptrSharedInstance;

signals:
    void editorChanged(QtTreePropertyBrowser *newEditor);

public slots:
    void layerObjectSelected(QGraphicsMapLayer *layer, KsGraphicObject *newObj);
    void layerObjectDeselected(QGraphicsMapLayer *layer, KsGraphicObject *newObj);

private:
    PropertyEditorMap m_mapPropertyEditors;
    PropertyManagerMap m_mapPropertyManagers;
    PropertEditorFactoryMap m_mapPropertyEditorFactories;
    QObject *m_ptrSelectedObject;
    PropertyEditorHolder *m_ptrUI;

    void initializePropertyEditorDatabase();
    void setSelectedObject(QObject *obj);
};

#endif // KSPROPERTYEDITORSTORE_H
