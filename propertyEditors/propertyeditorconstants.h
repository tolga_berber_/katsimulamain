#ifndef PROPERTYEDITORCONSTANTS
#define PROPERTYEDITORCONSTANTS

#define SIMPLE_MANAGER_POINTF                 "_PointFManager"
#define SIMPLE_MANAGER_SIZEF                  "_SizeFManager"
#define SIMPLE_MANAGER_INT                    "_IntManager"
#define SIMPLE_MANAGER_STRING                 "_StringManager"
#define SIMPLE_MANAGER_DOUBLE                 "_DoubleManager"
#define SIMPLE_MANAGER_BOOL                   "_BoolManager"

#define SIMPLE_FACTORY_DOUBLESPINBOX          "_DoubleSpinBox"
#define SIMPLE_FACTORY_SPINBOX                "_IntSpinBox"
#define SIMPLE_FACTORY_TEXTEDIT               "_TextEdit"
#define SIMPLE_FACTORY_CHECKBOX               "_CheckBox"

#endif // PROPERTYEDITORCONSTANTS

