#ifndef KSPROPERTYEDITORBRIDGE
#define KSPROPERTYEDITORBRIDGE

class QtTreePropertyBrowser;

class KsPropertyEditorBridge {
public:
    virtual QtTreePropertyBrowser *getBrowser()=0;
    virtual void setQObject(QObject *target)=0;
    virtual void unsetQObject()=0;
};

#endif // KSPROPERTYEDITORBRIDGE

