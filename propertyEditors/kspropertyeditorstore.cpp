#include "kspropertyeditorstore.h"

#include "qttreepropertybrowser.h"

#include "graphicObjects/ksgraphicobject.h"

#include "graphicObjects/qgraphicsmaplayer.h"

#include "qtpropertymanager.h"

#include "qteditorfactory.h"

#include "propertyEditors/propertyEditors/ksnodepropertyeditor.h"

#include "propertyEditors/propertyEditors/ksroadpropertyeditor.h"

#include <QMutex>

KsPropertyEditorStore *KsPropertyEditorStore::s_ptrSharedInstance = NULL;

KsPropertyEditorStore *KsPropertyEditorStore::sharedInstance()
{
    static QMutex lock;

    if(s_ptrSharedInstance==NULL) {
        lock.lock();

        if(s_ptrSharedInstance==NULL) {
            s_ptrSharedInstance = new KsPropertyEditorStore();
        }

        lock.unlock();
    }

    return s_ptrSharedInstance;
}

/*void KsPropertyEditorStore::setSelectedObject(QGraphicsMapLayer *layer, QObject *newObj)
{

    if(m_ptrSelectedObject!=newObj) {
        KsPropertyEditable *oldObject = dynamic_cast<KsPropertyEditable*>(m_ptrSelectedObject);
        KsPropertyEditable *newObject = dynamic_cast<KsPropertyEditable*>(newObj);
        QtTreePropertyBrowser *oldBrowser = NULL;
        if(!newObj) {
            m_ptrPropertyEditor->setPropertyBrowser(NULL);
            if(oldObject) {
                oldBrowser = getPropertyBrowserForClass(m_ptrSelectedObject->metaObject()->className());
                oldObject->disconnectBrowser(oldBrowser);
                m_ptrSelectedObject = NULL;
            }
            return;
        }
        QtTreePropertyBrowser *browser = getPropertyBrowserForClass(newObj->metaObject()->className());
        if(!browser) {
            registerPropertyBrowser(newObj);
            browser = getPropertyBrowserForClass(newObj->metaObject()->className());
        }
        if(oldObject) {
            oldBrowser = getPropertyBrowserForClass(m_ptrSelectedObject->metaObject()->className());
            oldObject->disconnectBrowser(oldBrowser);
        }
        newObject->connectBrowser(browser);
        m_ptrSelectedObject = newObj;

        emit selectedObjectChanged(newObject);

        if(oldBrowser) {
            emit editorChanged(browser);
        }

        m_ptrPropertyEditor->setPropertyBrowser(browser);
    }
}*/

QtAbstractPropertyManager *KsPropertyEditorStore::getManagerForNameAndType(QString name, QVariant::Type dataType)
{
    if(m_mapPropertyManagers.keys().contains(name)) {
        return m_mapPropertyManagers.value(name);
    } else {
        QtAbstractPropertyManager *result = NULL;
        switch (dataType) {
        case QVariant::Bool:
            result = new QtBoolPropertyManager(this);
            break;
        case QVariant::Char:
            result = new QtCharPropertyManager(this);
            break;
        case QVariant::Color:
            result = new QtColorPropertyManager(this);
            break;
        case QVariant::Date:
            result = new QtDatePropertyManager(this);
            break;
        case QVariant::DateTime:
            result = new QtDateTimePropertyManager(this);
            break;
        case QVariant::Double:
            result = new QtDoublePropertyManager(this);
            break;
        case QVariant::Font:
            result = new QtFontPropertyManager(this);
            break;
        case QVariant::Int:
            result = new QtIntPropertyManager(this);
            break;
        case QVariant::KeySequence:
            result = new QtKeySequencePropertyManager(this);
            break;
        case QVariant::Point:
            result = new QtPointPropertyManager(this);
            break;
        case QVariant::PointF:
            result = new QtPointFPropertyManager(this);
            break;
        case QVariant::Rect:
            result = new QtRectPropertyManager(this);
            break;
        case QVariant::RectF:
            result = new QtRectFPropertyManager(this);
            break;
        case QVariant::Size:
            result = new QtSizePropertyManager(this);
            break;
        case QVariant::SizeF:
            result = new QtSizeFPropertyManager(this);
            break;
        case QVariant::String:
            result = new QtStringPropertyManager(this);
            break;
        case QVariant::Time:
            result = new QtTimePropertyManager(this);
            break;
        default:
            break;
        }
        if(result)
            m_mapPropertyManagers.insert(name,result);
        return result;
    }
}

QtAbstractEditorFactoryBase *KsPropertyEditorStore::getEditorFactoryForNameAndType(QString name, QVariant::Type dataType, KsPropertyEditorStore::PropertyEditorType editorType)
{
    QtAbstractEditorFactoryBase *result = NULL;

    if(m_mapPropertyEditorFactories.keys().contains(name)) {
        result = m_mapPropertyEditorFactories[name];
    } else {
        switch(dataType) {
        case QVariant::Int:
            switch(editorType) {
            case PET_SpinBox:
                result = new QtSpinBoxFactory(this);
                break;
            case PET_Slider:
                result = new QtSliderFactory(this);
                break;
            case PET_ScrollBar:
                result = new QtScrollBarFactory(this);
                break;
            default:
                break;
            }
            break;
        case QVariant::Double:
            if(editorType==PET_DoubleSpinBox) {
                return new QtDoubleSpinBoxFactory(this);
            }
        case QVariant::Bool:
            if(editorType==PET_CheckBox) {
                result = new QtCheckBoxFactory(this);
            }
            break;
        case QVariant::String:
            if(editorType==PET_LineEdit) {
                result = new QtLineEditFactory(this);
            }
            break;
        case QVariant::Date:
            if(editorType==PET_DateEdit) {
                result = new QtDateEditFactory(this);
            }
            break;
        case QVariant::Time:
            if(editorType==PET_TimeEdit) {
                result = new  QtTimeEditFactory(this);
            }
            break;
        case QVariant::DateTime:
            if(editorType==PET_DateTimeEdit) {
                result = new QtDateTimeEditFactory(this);
            }
            break;
        case QVariant::KeySequence:
            if(editorType==PET_KeySequenceEdit) {
                result = new QtKeySequenceEditorFactory(this);
            }
            break;
        case QVariant::Char:
            if(editorType==PET_CharEdit) {
                result = new QtCharEditorFactory(this);
            }
            break;
        case QVariant::Cursor:
            if(editorType==PET_CursorEditor) {
                result = new QtCursorEditorFactory(this);
            }
            break;
        case QVariant::Color:
            if(editorType==PET_ColorEditor) {
                result = new QtColorEditorFactory(this);
            }
            break;
        case QVariant::Font:
            if(editorType==PET_FontEditor) {
                result = new QtFontEditorFactory(this);
            }
            break;
        default:
            break;
        }

        if(editorType==PET_EnumEditor) {
            result = new QtEnumEditorFactory(this);
        }
    }

    if(result) {
        m_mapPropertyEditorFactories.insert(name, result);
    }

    return result;
}

PropertyEditorHolder *KsPropertyEditorStore::getUI()
{
    return m_ptrUI;
}

KsPropertyEditorStore::KsPropertyEditorStore(QObject *parent) :
    QObject(parent),
    m_ptrSelectedObject(NULL)
{
    m_ptrUI = new PropertyEditorHolder;
    initializePropertyEditorDatabase();

    connect(this, SIGNAL(editorChanged(QtTreePropertyBrowser*)), m_ptrUI, SLOT(setPropertyBrowser(QtTreePropertyBrowser*)));
}

void KsPropertyEditorStore::layerObjectSelected(QGraphicsMapLayer *layer, KsGraphicObject *newObj)
{
    Q_UNUSED(layer);
    setSelectedObject(newObj);
}

void KsPropertyEditorStore::layerObjectDeselected(QGraphicsMapLayer *layer, KsGraphicObject *newObj)
{
    Q_UNUSED(layer);
    Q_UNUSED(newObj)
    setSelectedObject(NULL);
}

void KsPropertyEditorStore::initializePropertyEditorDatabase()
{
    //m_mapPropertyEditors.insert("QGraphicsWorldItem", new KsWorldPropertyEditor(this));
    m_mapPropertyEditors.insert("KsGraphicNode", new KsNodePropertyEditor(this));
    m_mapPropertyEditors.insert("KsRoadObject", new KsRoadPropertyEditor(this));
}

void KsPropertyEditorStore::setSelectedObject(QObject *obj)
{
    if(!obj) {
        if(m_ptrSelectedObject) {
            KsPropertyEditorBridge *oldBridge = m_mapPropertyEditors.value(m_ptrSelectedObject->metaObject()->className());
            oldBridge->unsetQObject();
            m_ptrSelectedObject = NULL;

            emit editorChanged(NULL);
        }
        return;
    }
    if(obj!=m_ptrSelectedObject) {
        QString className = obj->metaObject()->className();
        if(m_mapPropertyEditors.keys().contains(className)) {
            KsPropertyEditorBridge *newBridge = m_mapPropertyEditors.value(className);
            if(m_ptrSelectedObject) {
                KsPropertyEditorBridge *oldBridge = m_mapPropertyEditors.value(m_ptrSelectedObject->metaObject()->className());
                oldBridge->unsetQObject();
            }
            newBridge->setQObject(obj);
            m_ptrSelectedObject = obj;

            emit editorChanged(newBridge->getBrowser());
        } else {
            if(m_ptrSelectedObject) {
                KsPropertyEditorBridge *oldBridge = m_mapPropertyEditors.value(m_ptrSelectedObject->metaObject()->className());
                oldBridge->unsetQObject();
                m_ptrSelectedObject = NULL;

                emit editorChanged(NULL);
            }
        }
    }
}
