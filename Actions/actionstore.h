#ifndef ACTIONSTORE_H
#define ACTIONSTORE_H

#include <QHash>
#include <QWidget>
#include <QString>
#include <QStringList>
#include <QActionGroup>
#include <QMainWindow>

class QActionStore;

/**
 * @brief
 *
 */
typedef QStringList TabOrder;
/**
 * @brief
 *
 */
typedef QHash<QString, QStringList> GroupOrder;
/**
 * @brief
 *
 */
typedef QHash<QString, QActionGroup *> Actions;
/**
 * @brief
 *
 */
typedef QHash<QString, QWidget *> WidgetStore;

/**
 * @brief
 *
 */
typedef QHash<QMainWindow *, QActionStore *> ActionStoreHash;


#endif // ACTIONSTORE_H
