#ifndef KSACTION_H
#define KSACTION_H

#include <QAction>
#include <QString>

/**
 * @brief
 *
 */
class KsAction {
public:
    /**
     * @brief
     *
     * @return QAction
     */
    virtual QAction *action()=0;
    /**
     * @brief
     *
     * @return const QString
     */
    virtual const QString groupName()=0;
    /**
     * @brief
     *
     * @return const QString
     */
    virtual const QString tabName()=0;
    /**
     * @brief
     *
     * @return const QString
     */
    virtual const QString path()=0;
    /**
     * @brief
     *
     */
    virtual void init()=0;
    /**
     * @brief
     *
     */
    virtual void afterInit()=0;
};

Q_DECLARE_INTERFACE(KsAction, "Katsimula.Action/1.0")

#endif // KSACTION_H
