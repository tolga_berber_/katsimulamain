#ifndef KSABSTRACTACTION_H
#define KSABSTRACTACTION_H

#include <QAction>
#include <QShortcut>
#include "KsAction.h"

/**
 * @brief Uygulamada kullanılacak bütün eylemler için ana sınıf.
 *
 * Uygulama kapsamında kullanılacak bütün eylemler bu sınıftan türetilmelidir. Bütün eylemler için tab konumu, grup adı ve yol değişkenleri atanmalıdır. QAction sınıfının ilgili
 * sinyalleri protected fonksiyonlara eşleştirilmiştir.
 *
 * @see execute(bool)
 * @see toggled(bool)
 * @see hovered()
 *
 * NOT: Bu sınıf eylemler için uygulama kapsamında geçerli bir kısayol nesnesi içerir.
 * QAction sınıfının kısayol eylemi sadece eyleme bağlı nesnelerden biri görüntülendiğinde
 * aktif olduğundan istenildiğinde tüm uygulama kapsamında kullanılabilcek bir kısayol gerekmektedir.
 *
 * @see getAppShortCut()
 * @see shortcutExecuted()
 *
 */
class KsAbstractAction : public QAction, public KsAction
{
    Q_OBJECT
    Q_INTERFACES(KsAction)
public:
    /**
     * @brief Soyut Eylem sınıfı ilklendiricisi.
     *        İlklendiricide QAction sınıfına ait SIGNAL'lerin yerel SLOT'lara bağlanması ve uygulama kısayolunun ilk değerinin atanması işlemleri gerçekleştirilmiştir.
     *        Bütün Qt sınıflarında olduğu gibi burada sınıfın ait olduğu üst nesne bağlantısı kurulmaktadır.
     *
     * @param parent Bağlanılacak üst nesne.
     */
    explicit KsAbstractAction(QObject *parent = 0);

    /**
     * @brief KsAction sınıfı için gerekli olan QAction nesnesini döndürür. Aksi gerekmediği sürece QAction sınıfının kendisi döndürülmektedir.
     *
     * @return QAction İlgili QAction nesnesi.
     */
    virtual QAction *action();

    /**
     * @brief KsAction sınıfı için gerekli grup adını döndürür. Bu grup adı tab içerisindeki gruplandırma için kullanılmaktadır.
     *
     * @return const QString QAction'ın grup adı.
     */
    virtual const QString groupName();

    /**
     * @brief KsAction sınıfı içian gerekli tab adını döndürür. Bu tab adı bu eylemin bulunması gereken tabbar ın adı için kullanılır.
     *
     * @return const QString QAction'ın bulunması gereken tab'ın adı.
     */
    virtual const QString tabName();

    /**
     * @brief Bu eylemin görsel olarak bulunduğu konumun yoludur. Program dahilinde tab/grup şeklinde yollar kullanılmaktadır. Aksi durumda program ilgili eylemin bulunamaması gibi
     * sorunlar olabilir. Bu fonksiyonun alt sınıflar tarafından ezilmemesi gereklidir.
     *
     * @return const QString Eylemin yolu.
     */
    virtual const QString path();

    /**
     * @brief Bu fonksiyon eylem, bu eylem bir action group'a eklenmeden hemen önce çalıştırılır. Bu aşamada actiongroup fonksiyonu NULL döndürecektir.
     * @see afterInit()
     *
     */
    virtual void init();

    /**
     * @brief Bu fonksiyon eylem, bu eylem bir action group'a eklendikten hemen sonra çalıştırılır. Bu aşamada actiongroup fonksiyonu ait olunan action grup nesnesini döndürecektir.
     * @see init()
     *
     */
    virtual void afterInit();

signals:

public slots:

protected:
    QString m_strGroupName; /**< Bu eylemin ait olduğu grup adı. */
    QString m_strTabName; /**< Bu Eylemin ait olduğu tab adı. */

    /**
     * @brief Eylem kullanıcı tarafından çalıştırıldığında çağırılan fonksiyon. Varsayılan hali bu sınıfta tanımlıdır ve eylemin hazırlanmakta olduğuna dair bir mesaj gösterir.
     *
     * @param checked Eylemin çalıştığı halinde eylemin seçili olup olmadığını belirtir. true ise eylem seçili, false ile eylem seçili değildir.
     */
    virtual void execute(bool checked=false);

    /**
     * @brief Eylem'in seçili olma durumu değiştiğinde çalışacak fonksiyondur. Varsayılan hali bu sınıfta tanımlıdır ve herhangi bir fonksiyon çalıştırmaz.
     *
     * @param checked Eylemin çalıştığı halinde eylemin seçili olup olmadığını belirtir. true ise eylem seçili, false ile eylem seçili değildir.
     */
    virtual void toggled(bool checked=false);

    /**
     * @brief Kullanıcı fare ile eylemin bağlandığı bir nesne üzerinden geçtiğinde çalışacak fonksiyondur. Varsayılan hali bu sınıfta tanımlıdır ve herhangi bir fonksiyon çalıştırmaz.
     *
     */
    virtual void hovered();

    /**
     * @brief Kullanıcı bu eylem ile ilişkili klavye kısayoluna bastığında çalıştırılacak fonksiyondur. Varsayılan hali bu sınıfta tanımlıdır ve @see execute(bool) fonksiyonunu çağırır.
     * Bu fonksiyonun çalışması için @see getAppShortCut() fonksiyonu çalıştırılması gerekmektedir.
     *
     */
    virtual void shortcutExecuted();

    /**
     * @brief Eylemin uygulama dahilinde bir klavye kısayolu aracılığı ile çalışması istenirse, bu fonksiyon aracılığıyla uygulama kapsamında bir QShortcut nesnesi oluşturulması
     * gerekmektedir.
     *
     * @return QShortcut Uygulama dahilinde kullanılabilecek QShortcut nesnesini gerekirse oluşturup geri döndürür.
     */
    QShortcut *getAppShortCut();

private slots:
    /**
     * @brief QAction sınıfının triggered sinyali için tanımlanan slot. @see execute(bool) fonksiyonunu çağırır.
     *
     * @param checked QAction'ın seçili olup olmadığını belirtir.
     */
    void actionTriggerred(bool checked);

    /**
     * @brief QAction sınıfının triggered sinyali için tanımlanan slot. @see execute(bool) fonksiyonunu false parametresi ile çağırır.
     *
     */
    void actionTriggerred();

    /**
     * @brief Kullanıcı bu eylem ile ilişkili klavye kısayoluna bastığında çalıştırılacak fonksiyondur. Varsayılan hali bu sınıfta tanımlıdır ve @see shortcutExecuted()
     * fonksiyonunu çağırır.
     *
     */
    void shortcutTriggerred();

    /**
     * @brief QAction sınıfının toggled sinyali için tanımlanan slot. @see toggled(bool) fonksiyonunu çağırır.
     *
     * @param checked QAction'ın seçili olup olmadığını belirtir.
     */
    void actionToggled(bool checked);

    /**
     * @brief QAction sınıfının hovered sinyali için tanımlanan slot. @see hovered() fonksiyonunu çağırır.
     *
     */
    void actionHovered();

private:
    QShortcut *m_pcAppShortcut; /**< Uygulama kapsamında kullanılabilecek kısayol nesnesi. */

};

#endif // KSABSTRACTACTION_H
