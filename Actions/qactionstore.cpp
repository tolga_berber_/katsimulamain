#include "qactionstore.h"
#include <QMutex>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QToolButton>
#include <QSpacerItem>

#include "allactions.h"

#include "workspace/ksworkspace.h"
#include "interactions/ksinteractionstore.h"

ActionStoreHash *QActionStore::s_pcActionStores = NULL;

QActionStore *QActionStore::SharedInstanceForMainWindow(QMainWindow *mainWin)
{
    static QMutex mutex;

     if(s_pcActionStores==NULL) {
         mutex.lock();
         if(s_pcActionStores==NULL) {
             s_pcActionStores = new ActionStoreHash();
             s_pcActionStores->insert(mainWin, new QActionStore(mainWin));
         } else {
             if(!s_pcActionStores->contains(mainWin)) {
                 s_pcActionStores->insert(mainWin, new QActionStore(mainWin));
             }
         }
         mutex.unlock();
     }

     return s_pcActionStores->value(mainWin);
}

QWidget *QActionStore::getTabBarWidget(QString &tabName)
{
    if(m_cWidget.contains(tabName)) {
        return m_cWidget.value(tabName);
    } else {
        QWidget *wgt = new QWidget();
        QHBoxLayout *wLayout = new QHBoxLayout(wgt);
        wLayout->setSpacing(0);
        wLayout->setContentsMargins(0,1,0,2);
        foreach (QString str, m_cGroupOrder[tabName]) {
            QString path = tabName+"/"+str;
            QGroupBox *grpBox = new QGroupBox(wgt);
            grpBox->setTitle(str);
            QHBoxLayout *layout = new QHBoxLayout(grpBox);
            layout->setContentsMargins(1,0,1,15);
            QActionGroup *grp = m_cActions[path];
            foreach (QAction *act, grp->actions()) {
                QToolButton *button = new QToolButton(grpBox);
                button->setIconSize(QSize(32,32));
                button->setDefaultAction(act);
                button->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
                layout->addWidget(button);
            }
            wLayout->addWidget(grpBox);
        }
        QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
        wLayout->addItem(spacer);
        m_cWidget.insert(tabName, wgt);
        return wgt;
    }
}

QTabWidget *QActionStore::getTabWidget(QWidget *parent)
{
    if(!m_pcTabWidget) {
        m_pcTabWidget = new QTabWidget(parent);
        m_pcTabWidget->setObjectName("menu");

        foreach (QString str, m_cTabOrder) {
            m_pcTabWidget->addTab(getTabBarWidget(str), str);
        }
        m_pcTabWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    }

    return m_pcTabWidget;
}

void QActionStore::registerAction(KsAction *act)
{
    if(!m_cTabOrder.contains(act->tabName())) {
        m_cTabOrder.append(act->tabName());
        m_cGroupOrder.insert(act->tabName(), QStringList());
    }
    if(!m_cGroupOrder[act->tabName()].contains(act->groupName())) {
        m_cGroupOrder[act->tabName()].append(act->groupName());
    }
    if(!m_cActions.contains(act->path())) {
        m_cActions.insert(act->path(), new QActionGroup(this));
        m_cActions[act->path()]->setExclusive(false);
    }
    act->init();
    m_cActions[act->path()]->addAction(act->action());
    act->afterInit();
}

void QActionStore::registerActionGroup(QActionGroup *grp)
{
    KsAction *act = qobject_cast<KsAction*>(grp->actions().at(0));
    if(!m_cTabOrder.contains(act->tabName())) {
        m_cTabOrder.append(act->tabName());
        m_cGroupOrder.insert(act->tabName(), QStringList());
    }
    if(!m_cGroupOrder[act->tabName()].contains(act->groupName())) {
        m_cGroupOrder[act->tabName()].append(act->groupName());
    }
    m_cActions.insert(act->path(), grp);
}

QActionStore::QActionStore(QMainWindow *mainWin, QObject *parent) :
    QObject(parent),
    m_pcTabWidget(NULL),
    m_pcMainWindow(mainWin)
{
    m_cTabOrder = TabOrder();
    m_cGroupOrder = GroupOrder();
    m_cActions = Actions();
    m_cWidget = WidgetStore();
    initializeActions();
}

void QActionStore::initializeActions()
{
    // File Actions
    registerAction(new KsNewProjectAction(this->m_pcMainWindow));
    registerAction(new KsOpenProjectAction(this->m_pcMainWindow));
    registerAction(new KsSaveAction(this->m_pcMainWindow));
    registerAction(new KsScenarios(this->m_pcMainWindow));
    registerAction(new KsAddScenario(this->m_pcMainWindow));
    registerAction(new KsRemoveScenario(this->m_pcMainWindow));
    registerAction(new KsClearCacheAction(this->m_pcMainWindow));

    // Edit Actions
    registerAction(new KsCut(this->m_pcMainWindow));
    registerAction(new KsCopy(this->m_pcMainWindow));
    registerAction(new KsPaste(this->m_pcMainWindow));
    registerAction(new KsUndo(this->m_pcMainWindow));
    registerAction(new KsRedo(this->m_pcMainWindow));

    //Road Net Actions
    registerAction(new KsNewAction(this->m_pcMainWindow));
    registerAction(new KsCheckRoadNetwork(this->m_pcMainWindow));

    //View Actions
    registerAction(new KsFullScreen(this->m_pcMainWindow));
    registerAction(new KsZoomIn(this->m_pcMainWindow));
    registerAction(new KsZoomOut(this->m_pcMainWindow));

    //DB Actions
    registerAction(new KsAracEditorAction(this->m_pcMainWindow));
    registerAction(new KsSimulationResultReport(this->m_pcMainWindow));

    //Simulation Actions
    registerAction(new KsStartSimulation(this->m_pcMainWindow));
    registerAction(new KsPauseSimulation(this->m_pcMainWindow));
    registerAction(new KsStopSimulation(this->m_pcMainWindow));

    registerActionGroup(KsWorkspace::SharedInstance()->mapProviders()->mapActionGroup());
    registerActionGroup(KsWorkspace::SharedInstance()->interactionStore()->actions());
    /*registerAction(new KsOpenStreetMap(this->m_pcMainWindow));
    registerAction(new KsOpenCycleMap(this->m_pcMainWindow));
    registerAction(new KsGoogleMaps(this->m_pcMainWindow));*/

}


