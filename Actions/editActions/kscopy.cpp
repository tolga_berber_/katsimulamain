#include "kscopy.h"

KsCopy::KsCopy(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Pano");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/edit/copy.png"));
    setText(tr("Kopyala"));
}
