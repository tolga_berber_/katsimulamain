#include "ksundo.h"

KsUndo::KsUndo(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Düzen");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/edit/undo.png"));
    setText(tr("Geri Al"));
}
