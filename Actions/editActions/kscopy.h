#ifndef KSCOPY_H
#define KSCOPY_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsCopy : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsCopy(QObject *parent = 0);

signals:

public slots:

};

#endif // KSCOPY_H
