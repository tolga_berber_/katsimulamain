#ifndef KSCUT_H
#define KSCUT_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsCut : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsCut(QObject *parent = 0);

signals:

public slots:

};

#endif // KSCUT_H
