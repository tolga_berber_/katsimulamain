#include "ksredo.h"

KsRedo::KsRedo(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Düzen");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/edit/redo.png"));
    setText(tr("Yinele"));
}
