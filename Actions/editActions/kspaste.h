#ifndef KSPASTE_H
#define KSPASTE_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsPaste : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsPaste(QObject *parent = 0);

signals:

public slots:

};

#endif // KSPASTE_H
