#include "kscut.h"

KsCut::KsCut(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Pano");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/edit/cut.png"));
    setText(tr("Kes"));
}
