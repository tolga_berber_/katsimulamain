#ifndef KSREDO_H
#define KSREDO_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsRedo : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsRedo(QObject *parent = 0);

signals:

public slots:

};

#endif // KSREDO_H
