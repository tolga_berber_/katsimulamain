#ifndef KSUNDO_H
#define KSUNDO_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsUndo : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsUndo(QObject *parent = 0);

signals:

public slots:

};

#endif // KSUNDO_H
