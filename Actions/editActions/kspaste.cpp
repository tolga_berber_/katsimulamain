#include "kspaste.h"

KsPaste::KsPaste(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Pano");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/edit/paste.png"));
    setText(tr("Yapıştır"));
}
