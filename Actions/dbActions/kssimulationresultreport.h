#ifndef KSSIMULATIONRESULTREPORT_H
#define KSSIMULATIONRESULTREPORT_H


#include "../ksabstractaction.h"

class KsSimulationResultReport : public KsAbstractAction
{
    Q_OBJECT
public:
    KsSimulationResultReport(QObject *parent=0);
    ~KsSimulationResultReport();
};

#endif // KSSIMULATIONRESULTREPORT_H
