#include "kssimulationresultreport.h"

KsSimulationResultReport::KsSimulationResultReport(QObject *parent):KsAbstractAction(parent)
{
    m_strGroupName = tr("Raporlar");
    m_strTabName = tr("VERİTABANI");
    setIcon(QPixmap(":/img/action/DB/report.png"));
    setText(tr("Simülasyon\nSonuçları"));
}

KsSimulationResultReport::~KsSimulationResultReport()
{

}

