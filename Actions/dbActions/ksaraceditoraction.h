#ifndef KSARACEDITORACTION_H
#define KSARACEDITORACTION_H

#include "../ksabstractaction.h"

class KsAracEditorAction : public KsAbstractAction
{
    Q_OBJECT
public:
    KsAracEditorAction(QObject *parent=0);

protected:

    virtual void execute(bool checked=false);
};

#endif // KSARACEDITORACTION_H
