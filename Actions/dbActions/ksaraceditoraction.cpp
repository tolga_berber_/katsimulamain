#include "ksaraceditoraction.h"

#include "widgets/carlist.h"
#include "workspace/ksworkspace.h"

KsAracEditorAction::KsAracEditorAction(QObject *parent):
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Sabit Bilgiler");
    m_strTabName = tr("VERİTABANI");
    setIcon(QPixmap(":/img/action/DB/car.png"));
    setText(tr("Tanımlı\nAraçlar"));
}

void KsAracEditorAction::execute(bool checked)
{
    Q_UNUSED(checked);
    CarList lst;
    lst.exec();
    KsWorkspace::SharedInstance()->staticDatabase()->saveAll();
}


