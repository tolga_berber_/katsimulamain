#ifndef ALLACTIONS_H
#define ALLACTIONS_H

// File actions
#include "fileActions/ksnewprojectaction.h"
#include "fileActions/ksopenprojectaction.h"
#include "fileActions/ksscenarios.h"
#include "fileActions/ksaddscenario.h"
#include "fileActions/ksremovescenario.h"
#include "fileActions/ksclearcacheaction.h"
#include "fileActions/kssaveaction.h"

// Edit Actions
#include "editActions/kscopy.h"
#include "editActions/kscut.h"
#include "editActions/kspaste.h"
#include "editActions/ksundo.h"
#include "editActions/ksredo.h"

// Road Actions
#include "RoadNetActions/ksnewaction.h"
#include "RoadNetActions/kscheckroadnetwork.h"

// View Actions
#include "viewActions/ksfullscreen.h"
#include "viewActions/kszoomin.h"
#include "viewActions/kszoomout.h"

// DB Actions
#include "dbActions/ksaraceditoraction.h"
#include "dbActions/kssimulationresultreport.h"

// Simulation Actions
#include "simulation/kspausesimulation.h"
#include "simulation/ksstartsimulation.h"
#include "simulation/ksstopsimulation.h"


#endif // ALLACTIONS_H
