#include "kspausesimulation.h"


#include "workspace/ksworkspace.h"
#include "Simulation/kssimulationengine.h"

KsPauseSimulation::KsPauseSimulation(QObject *parent) : KsAbstractAction(parent)
{
    m_strGroupName = tr("Simülasyon Kontrolü");
    m_strTabName = tr("SİMÜLASYON");
    setIcon(QPixmap(":/img/action/simulation/pause.png"));
    setText(tr("Duraklat"));
}

KsPauseSimulation::~KsPauseSimulation()
{

}

void KsPauseSimulation::execute(bool checked)
{
    KsWorkspace::SharedInstance()->currentProject()->simEngine()->pauseSimulation();
}

