#include "ksstartsimulation.h"

#include "workspace/ksworkspace.h"
#include "Simulation/kssimulationengine.h"

KsStartSimulation::KsStartSimulation(QObject *parent) : KsAbstractAction(parent)
{
    m_strGroupName = tr("Simülasyon Kontrolü");
    m_strTabName = tr("SİMÜLASYON");
    setIcon(QPixmap(":/img/action/simulation/play.png"));
    setText(tr("Başlat"));
}

KsStartSimulation::~KsStartSimulation()
{

}

void KsStartSimulation::execute(bool checked)
{
    KsWorkspace::SharedInstance()->currentProject()->simEngine()->startSimulation();
}

