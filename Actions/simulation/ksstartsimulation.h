#ifndef KSSTARTSIMULATION_H
#define KSSTARTSIMULATION_H

#include "../ksabstractaction.h"

class KsStartSimulation : public KsAbstractAction
{
    Q_OBJECT
public:
    explicit KsStartSimulation(QObject *parent = 0);
    ~KsStartSimulation();

signals:

public slots:

protected:
    virtual void execute(bool checked=false);
};

#endif // KSSTARTSIMULATION_H
