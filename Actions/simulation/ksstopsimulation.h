#ifndef KSSTOPSIMULATION_H
#define KSSTOPSIMULATION_H

#include "../ksabstractaction.h"

class KsStopSimulation : public KsAbstractAction
{
    Q_OBJECT
public:
    KsStopSimulation(QObject *parent=0);
    ~KsStopSimulation();
};

#endif // KSSTOPSIMULATION_H
