#include "ksstopsimulation.h"

KsStopSimulation::KsStopSimulation(QObject *parent) : KsAbstractAction(parent)
{
    m_strGroupName = tr("Simülasyon Kontrolü");
    m_strTabName = tr("SİMÜLASYON");
    setIcon(QPixmap(":/img/action/simulation/stop.png"));
    setText(tr("Durdur"));
}

KsStopSimulation::~KsStopSimulation()
{

}

