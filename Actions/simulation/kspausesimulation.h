#ifndef KSPAUSESIMULATION_H
#define KSPAUSESIMULATION_H

#include "../ksabstractaction.h"

class KsPauseSimulation : public KsAbstractAction
{
    Q_OBJECT
public:
    KsPauseSimulation(QObject *parent=0);
    ~KsPauseSimulation();

protected:
    virtual void execute(bool checked=false);
};

#endif // KSPAUSESIMULATION_H
