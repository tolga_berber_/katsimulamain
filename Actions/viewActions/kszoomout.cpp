#include "kszoomout.h"

#include "workspace/ksworkspace.h"

KsZoomOut::KsZoomOut(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Çalışma Alanı");
    m_strTabName = tr("GÖRÜNÜM");
    setIcon(QPixmap(":/img/action/view/zoomout.png"));
    setText(tr("Uzaklaş"));
}

void KsZoomOut::execute(bool checked)
{
    Q_UNUSED(checked);
    KsWorkspace::SharedInstance()->mapView()->zoomOut();
}
