#ifndef KSZOOMIN_H
#define KSZOOMIN_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsZoomIn : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsZoomIn(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);

};

#endif // KSZOOMIN_H
