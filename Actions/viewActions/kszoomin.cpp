#include "kszoomin.h"

#include "workspace/ksworkspace.h"

KsZoomIn::KsZoomIn(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Çalışma Alanı");
    m_strTabName = tr("GÖRÜNÜM");
    setIcon(QPixmap(":/img/action/view/zoomin.png"));
    setText(tr("Yakınlaş"));
}

void KsZoomIn::execute(bool checked)
{
    Q_UNUSED(checked);
    KsWorkspace::SharedInstance()->mapView()->zoomIn();
}
