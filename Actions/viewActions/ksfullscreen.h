#ifndef KSFULLSCREEN_H
#define KSFULLSCREEN_H

#include "../ksabstractaction.h"
#include <QShortcut>

/**
 * @brief
 *
 */
class KsFullScreen : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsFullScreen(QObject *parent = 0);

signals:

public slots:

protected:
    /**
     * @brief
     *
     * @param checked
     */
    virtual void execute(bool checked=false);


};

#endif // KSFULLSCREEN_H
