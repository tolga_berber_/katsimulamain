#ifndef KSZOOMOUT_H
#define KSZOOMOUT_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsZoomOut : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsZoomOut(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);

};

#endif // KSZOOMOUT_H
