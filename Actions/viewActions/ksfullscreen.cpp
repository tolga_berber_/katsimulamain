#include "ksfullscreen.h"
#include <QMainWindow>

KsFullScreen::KsFullScreen(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Çalışma Alanı");
    m_strTabName = tr("GÖRÜNÜM");
    setIcon(QPixmap(":/img/action/view/fullscreen.png"));
    setText(tr("Tam Ekran"));
    setCheckable(true);
    getAppShortCut()->setKey(tr("F11"));
}

void KsFullScreen::execute(bool checked)
{
    QMainWindow *mainWin = qobject_cast<QMainWindow*>(parent());
    if(!checked) {
        mainWin->showMaximized();
    } else {
        mainWin->showFullScreen();
    }
}
