#include "ksnewaction.h"

KsNewAction::KsNewAction(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Ağ İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/roadnet/newroadnet.png"));
    setText(tr("Yeni Yol Ağı"));
}
