#ifndef KSNEWACTION_H
#define KSNEWACTION_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsNewAction : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsNewAction(QObject *parent = 0);

signals:

public slots:

private:

};

#endif // KSNEWACTION_H
