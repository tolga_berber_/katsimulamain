#include "kscheckroadnetwork.h"

KsCheckRoadNetwork::KsCheckRoadNetwork(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Ağ İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/roadnet/checkroadnet.png"));
    setText(tr("Ağı\nKontrol Et"));
}
