#ifndef KSCHECKROADNETWORK_H
#define KSCHECKROADNETWORK_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsCheckRoadNetwork : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsCheckRoadNetwork(QObject *parent = 0);

signals:

public slots:

};

#endif // KSCHECKROADNETWORK_H
