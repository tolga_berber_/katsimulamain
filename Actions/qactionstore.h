#ifndef QACTIONSTORE_H
#define QACTIONSTORE_H

#include <QObject>
#include <QWidget>
#include <QTabWidget>
#include "actionstore.h"
#include "KsAction.h"

/**
 * @brief
 *
 */
class QActionStore : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param mainWin
     * @return QActionStore
     */
    static QActionStore *SharedInstanceForMainWindow(QMainWindow *mainWin=NULL);
    /**
     * @brief
     *
     * @param tabName
     * @return QWidget
     */
    QWidget *getTabBarWidget(QString &tabName);
    /**
     * @brief
     *
     * @param parent
     * @return QTabWidget
     */
    QTabWidget *getTabWidget(QWidget *parent=0);

signals:

public slots:
    /**
     * @brief
     *
     * @param act
     */
    void registerAction(KsAction *act);

    void registerActionGroup(QActionGroup *grp);

private:
    /**
     * @brief
     *
     * @param mainWin
     * @param parent
     */
    explicit QActionStore(QMainWindow *mainWin=0, QObject *parent = 0);
    static ActionStoreHash *s_pcActionStores; /**< TODO */

    TabOrder m_cTabOrder; /**< TODO */
    GroupOrder m_cGroupOrder; /**< TODO */
    Actions m_cActions; /**< TODO */
    WidgetStore m_cWidget; /**< TODO */
    QTabWidget *m_pcTabWidget; /**< TODO */
    QMainWindow *m_pcMainWindow; /**< TODO */

    /**
     * @brief
     *
     */
    void initializeActions();
};

#endif // QACTIONSTORE_H
