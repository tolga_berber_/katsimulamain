#include "ksopenprojectaction.h"

#include "workspace/ksworkspace.h"

#include <QFileDialog>

KsOpenProjectAction::KsOpenProjectAction(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Proje İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/openproject.png"));
    setText(tr("Proje Aç"));
    getAppShortCut()->setKey(tr("Ctrl+O"));
}

void KsOpenProjectAction::execute(bool checked)
{
    Q_UNUSED(checked);

    QString ProjectPath = QFileDialog::getExistingDirectory(NULL, tr("Proje Klasörünü Seçin"));

    if(ProjectPath.isEmpty()||ProjectPath.isNull())
        return;

    KsWorkspace::SharedInstance()->openProject(ProjectPath);
}
