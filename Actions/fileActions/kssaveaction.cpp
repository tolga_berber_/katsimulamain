#include "kssaveaction.h"
#include "workspace/ksworkspace.h"

#include <QFileDialog>

KsSaveAction::KsSaveAction(QObject *parent) : KsAbstractAction(parent)
{
    m_strGroupName = tr("Proje İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/save.png"));
    setText(tr("Kaydet"));
}

void KsSaveAction::execute(bool checked)
{
    Q_UNUSED(checked);
    KsProject *proj = KsWorkspace::SharedInstance()->currentProject();
    if(proj->needSave()) {
        proj->save();
    }
}


