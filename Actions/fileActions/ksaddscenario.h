#ifndef KSADDSCENARIO_H
#define KSADDSCENARIO_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsAddScenario : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsAddScenario(QObject *parent = 0);

signals:

public slots:

};

#endif // KSADDSCENARIO_H
