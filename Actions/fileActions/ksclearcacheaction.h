#ifndef KSCLEARCACHEACTION_H
#define KSCLEARCACHEACTION_H

#include "../ksabstractaction.h"

class KsClearCacheAction : public KsAbstractAction
{
    Q_OBJECT
public:
    explicit KsClearCacheAction(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);
};

#endif // KSCLEARCACHEACTION_H
