#include "ksscenarios.h"

KsScenarios::KsScenarios(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Senaryo İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/scenarios.png"));
    setText(tr("Tanımlı\nSenaryolar"));
}
