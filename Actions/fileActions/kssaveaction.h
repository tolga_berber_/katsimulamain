#ifndef KSSAVEACTION_H
#define KSSAVEACTION_H

#include "../ksabstractaction.h"

class KsSaveAction : public KsAbstractAction
{
    Q_OBJECT
public:
    explicit KsSaveAction(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);
};

#endif // KSSAVEACTION_H
