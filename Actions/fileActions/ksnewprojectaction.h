#ifndef KSNEWPROJECTACTION_H
#define KSNEWPROJECTACTION_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsNewProjectAction : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsNewProjectAction(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);

private:

};

#endif // KSNEWPROJECTACTION_H
