#ifndef KSREMOVESCENARIO_H
#define KSREMOVESCENARIO_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsRemoveScenario : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsRemoveScenario(QObject *parent = 0);

signals:

public slots:

};

#endif // KSREMOVESCENARIO_H
