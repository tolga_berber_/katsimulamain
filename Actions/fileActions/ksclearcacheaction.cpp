#include "ksclearcacheaction.h"

#include "workspace/ksworkspace.h"
#include "tileSource/kstilesource.h"

KsClearCacheAction::KsClearCacheAction(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Diğer İşlemler");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/clearCache.png"));
    setText(tr("Önbelleği\nTemizle"));
}

void KsClearCacheAction::execute(bool checked)
{
    Q_UNUSED(checked);
    KsWorkspace::SharedInstance()->mapProviders()->activeProvider()->tileSource()->clearCache();
}
