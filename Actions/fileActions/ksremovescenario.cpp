#include "ksremovescenario.h"

KsRemoveScenario::KsRemoveScenario(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Senaryo İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/scenario_delete.png"));
    setText(tr("Senaryo\nSil"));
}
