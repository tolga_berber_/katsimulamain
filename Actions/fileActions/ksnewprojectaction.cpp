#include "ksnewprojectaction.h"
#include "workspace/ksworkspace.h"

KsNewProjectAction::KsNewProjectAction(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Proje İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/newproject.png"));
    setText(tr("Yeni Proje"));
    getAppShortCut()->setKey(tr("Ctrl+N"));
}

void KsNewProjectAction::execute(bool checked)
{
    Q_UNUSED(checked);
    KsWorkspace::SharedInstance()->newProject();
}
