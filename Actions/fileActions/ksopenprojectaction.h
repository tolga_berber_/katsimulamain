#ifndef KSOPENPROJECTACTION_H
#define KSOPENPROJECTACTION_H

#include "../ksabstractaction.h"

/**
 * @brief
 *
 */
class KsOpenProjectAction : public KsAbstractAction
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit KsOpenProjectAction(QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);

};

#endif // KSOPENPROJECTACTION_H
