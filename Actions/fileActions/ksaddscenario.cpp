#include "ksaddscenario.h"

KsAddScenario::KsAddScenario(QObject *parent) :
    KsAbstractAction(parent)
{
    m_strGroupName = tr("Senaryo İşlemleri");
    m_strTabName = tr("GİRİŞ");
    setIcon(QPixmap(":/img/action/file/scenario_add.png"));
    setText(tr("Senaryo\nEkle"));
}
