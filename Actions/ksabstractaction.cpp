#include "ksabstractaction.h"
#include <QMessageBox>

KsAbstractAction::KsAbstractAction(QObject *parent) :
    QAction(parent)
{
    m_pcAppShortcut = NULL;
    connect(this,SIGNAL(triggered(bool)), this, SLOT(actionTriggerred(bool)));
    connect(this, SIGNAL(hovered()), this, SLOT(actionHovered()));
    connect(this, SIGNAL(toggled(bool)), this, SLOT(actionToggled(bool)));
}

QAction *KsAbstractAction::action()
{
    return this;
}

const QString KsAbstractAction::groupName()
{
    return m_strGroupName;
}

const QString KsAbstractAction::tabName()
{
    return m_strTabName;
}

const QString KsAbstractAction::path()
{
    return m_strTabName+"/"+m_strGroupName;
}

void KsAbstractAction::execute(bool checked)
{
    Q_UNUSED(checked);
    QMessageBox mBox;
    mBox.setText(tr("Henüz Hazır Değil!"));
    mBox.setInformativeText(tr("Yazılımcılarımız bu fonksiyon üzerinde çalışmaktadırlar! Sabrınız için teşekkür ederiz."));
    mBox.setIcon(QMessageBox::Information);
    mBox.exec();
}

void KsAbstractAction::toggled(bool checked)
{
    Q_UNUSED(checked);
}

void KsAbstractAction::hovered()
{
}

void KsAbstractAction::shortcutExecuted()
{
    execute(!isChecked());
    setChecked(!isChecked());
}

void KsAbstractAction::init()
{

}

void KsAbstractAction::afterInit()
{

}

QShortcut *KsAbstractAction::getAppShortCut()
{
    if(m_pcAppShortcut==NULL) {
        m_pcAppShortcut = new QShortcut(qobject_cast<QWidget*>(parent()));
        m_pcAppShortcut->setContext(Qt::ApplicationShortcut);
        connect(m_pcAppShortcut, SIGNAL(activated()), this, SLOT(shortcutTriggerred()));
    }
    return m_pcAppShortcut;
}

void KsAbstractAction::actionTriggerred(bool checked)
{
    execute(checked);
}

void KsAbstractAction::actionTriggerred()
{
    execute();
}

void KsAbstractAction::shortcutTriggerred()
{
    shortcutExecuted();
}

void KsAbstractAction::actionToggled(bool checked)
{
    toggled(checked);
}

void KsAbstractAction::actionHovered()
{
    hovered();
}
