#include "mainwindow.h"

#include <QFile>
#include <QTextStream>

//#include "mapProviders/qmapproviderstore.h"
#include "Actions/qactionstore.h"
//#include "utilities/ksgeometryhelper.h"
#include "workspace/ksworkspace.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    //setMenuWidget(QActionStore::SharedInstanceForMainWindow(this)->getTabWidget(this));

    m_pcStatus = new QStatusBar(this);
    setStatusBar(m_pcStatus);

    m_pcWorldPoint = new QLabel(m_pcStatus);
    m_pcWorldPoint->setFixedWidth(110);
    m_pcWorldPoint->setLineWidth(0);
    m_pcStatus->addWidget(m_pcWorldPoint);

    m_pcWorldArea = new QLabel(m_pcStatus);
    m_pcWorldArea->setFixedWidth(450);
    m_pcStatus->addWidget(m_pcWorldArea);

    QFile stylesheet(":/stylesheet/design.qss");

    if(stylesheet.open(QIODevice::ReadOnly)) {
        QTextStream str(&stylesheet);
        QString qss = str.readAll();
        setStyleSheet(qss);
        stylesheet.close();
    }

}

MainWindow::~MainWindow()
{
}

void MainWindow::setWorldPoint(const QPointF &point)
{
    m_pcWorldPoint->setText(point2String(point));
}

void MainWindow::setWorldRect(const QRectF &rect)
{
    m_pcWorldArea->setText(rect2String(rect));
}

void MainWindow::closeEvent(QCloseEvent *evt)
{
    Q_UNUSED(evt);
    KsWorkspace::SharedInstance()->saveWorkspace();
}

QString MainWindow::point2String(QPointF point)
{
    return tr("%1, %2").arg(point.x(), 9, 'f', 4, ' ').arg(point.y()*-1.0, 9, 'f', 4, ' ');
}

QString MainWindow::rect2String(QRectF rect)
{
    return tr("Görünen Bölge: (%1),(%2) Alan: %3").arg(point2String(rect.topLeft())).arg(point2String(rect.bottomRight())).arg(friendlyAreaDescription(areaOfRect(rect)));
}

double MainWindow::areaOfRect(QRectF rect)
{
/*
    Polygon poly;
    Point p1,p2,p3,p4;

    p1.lat(-1.0*rect.top());
    p1.lon(rect.left());
    bg::append(poly, p1);

    p2.lat(-1.0*rect.top());
    p2.lon(rect.right());
    bg::append(poly, p2);

    p3.lat(-1.0*rect.bottom());
    p3.lon(rect.right());
    bg::append(poly, p3);

    p4.lat(-1.0*rect.bottom());
    p4.lon(rect.left());
    bg::append(poly, p4);
    bg::append(poly, p1);

    return bg::area(poly);*/
    KsGeometryHelper *geom = KsWorkspace::SharedInstance()->geometryHelper();
    QPolygonF poly(rect);

    return geom->area(geom->convertFromQt(poly));
}

QString MainWindow::friendlyAreaDescription(double area)
{
    qreal divisor;
    QString metric;
    if(qAbs(area)>1000000) {
        divisor = 1000000;
        metric = "km";
    } else if(qAbs(area)>10000) {
        divisor = 10000;
        metric = "hm";
    } else if(qAbs(area)>100) {
        divisor = 100;
        metric = "Dm";
    } else {
        divisor = 1;
        metric = "m";
    }

    return tr("%1 %2<sup>2</sup>").arg(area/divisor, 9, 'f', 4, ' ').arg(metric);
}
