#include "kstilesource.h"

KsTileSource::KsTileSource(MapProvider *provider, QObject *parent) :
    QThread(parent),
    m_pcMapProvider(provider)
{
}

bool KsTileSource::isReady()
{
    if(m_pcDatabase)
        return m_pcDatabase->ready();
    else
        return false;
}

void KsTileSource::requestTile(int x, int y, int z)
{
    emit internalTileRequest(x,y,z);
}

void KsTileSource::internalDownloadComplete(int x, int y, int z, const QByteArray &arr)
{
    QImage image;
    image.loadFromData(arr);
    emit tileImage(x,y,z,image);
}

void KsTileSource::run()
{
    m_pcDownloader = new QTileDownloader((MapProvider*)m_pcMapProvider);
    m_pcDatabase = new QTileDatabase((MapProvider*)m_pcMapProvider);

    connect(this, SIGNAL(internalTileRequest(int,int,int)), m_pcDatabase, SLOT(getTile(int,int,int)));
    connect(this, SIGNAL(clearCache()), m_pcDatabase, SLOT(clearCache()));
    connect(m_pcDatabase, SIGNAL(cacheImage(int,int,int,const QImage&)), this, SIGNAL(tileImage(int,int,int,const QImage&)));

    connect(m_pcDatabase, SIGNAL(imageDoesNotExists(int,int,int)), m_pcDownloader, SLOT(downloadTile(int,int,int)));
    connect(m_pcDownloader, SIGNAL(dataReady(int,int,int,const QByteArray&)), m_pcDatabase, SLOT(saveTile(int,int,int,const QByteArray&)));
    connect(m_pcDownloader, SIGNAL(dataReady(int,int,int,const QByteArray&)), this, SLOT(internalDownloadComplete(int,int,int,const QByteArray&)));

    exec();
}
