#ifndef KSTILESOURCE_H
#define KSTILESOURCE_H

#include <QThread>

#include "mapProviders/providers/mapprovider.h"
#include "network/qtiledownloader.h"
#include "cache/qtiledatabase.h"

class KsTileSource : public QThread
{
    Q_OBJECT
public:
    explicit KsTileSource(MapProvider *provider, QObject *parent = 0);

    bool isReady();

signals:
    void internalTileRequest(int x, int y, int z);
    void clearCache();
    void tileImage(int x, int y, int z, const QImage &image);

public slots:
    void requestTile(int x, int y,int z);

private slots:
    void internalDownloadComplete(int x, int y, int z, const QByteArray &arr);

protected:
    virtual void run();

private:
    const MapProvider *m_pcMapProvider;
    QTileDownloader *m_pcDownloader;
    QTileDatabase *m_pcDatabase;
};

#endif // KSTILESOURCE_H
