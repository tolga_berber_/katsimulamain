#ifndef KSDEFAULTINTERACTION_H
#define KSDEFAULTINTERACTION_H

#include "ksabstractinteraction.h"

class KsDefaultInteraction : public KsAbstractInteraction
{
    Q_OBJECT
public:
    explicit KsDefaultInteraction(QObject *parent = 0);

    virtual void wheelEvent(QGraphicsSceneWheelEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);

    virtual void afterActivate();

    QString interactionName();

    QString interactionID();

signals:

public slots:

};

#endif // KSDEFAULTINTERACTION_H
