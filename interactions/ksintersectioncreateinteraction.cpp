#include "ksintersectioncreateinteraction.h"

#include "graphicObjects/qgraphicsmapview.h"
#include "graphicObjects/ksgraphicnode.h"
#include "workspace/ksworkspace.h"
#include "db/model/project/ksnodeobject.h"

KsIntersectionCreateInterAction::KsIntersectionCreateInterAction(QObject *parent) : KsAbstractInteraction(parent)
{

}

void KsIntersectionCreateInterAction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    KsNodeObject *node = KsWorkspace::SharedInstance()->currentProject()->newNode();
    node->setName(tr("İsimsiz Kavşak"));
    node->setNodeType(KsNodeObject::NTIntersection);
    node->setLocation(event->scenePos());
    KsGraphicNode *gNode = new KsGraphicNode(event->scenePos());
    gNode->setModel(node);
    scene()->addItem(gNode);
    scene()->currentTrafficObjectLayer()->addObject(gNode);
}

void KsIntersectionCreateInterAction::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Escape) {
        KsAbstractInteraction *interaction = KsWorkspace::SharedInstance()->interactionStore()->interaction(KsInteractionStore::ITDefaultInteraction);
        scene()->setActiveInteractionObject(interaction);
    }
}

void KsIntersectionCreateInterAction::afterActivate()
{
    KsAbstractInteraction::afterActivate();
    view()->setDragMode(QGraphicsView::NoDrag);
    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->lockObjects();
    }
}

void KsIntersectionCreateInterAction::afterDeactivate()
{
    KsAbstractInteraction::afterDeactivate();
    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->unlockObjects();
    }
}

QString KsIntersectionCreateInterAction::interactionID()
{
    return tr("INTERSECTION");
}

QString KsIntersectionCreateInterAction::interactionName()
{
    return tr("Kavşak\nEkleme");
}


