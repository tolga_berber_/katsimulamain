#ifndef KSLINKCREATEINTERACTION_H
#define KSLINKCREATEINTERACTION_H

#include "ksabstractinteraction.h"
#include "graphicObjects/ksroadobject.h"

class KsLinkCreateInteraction : public KsAbstractInteraction
{
    Q_OBJECT
public:
    explicit KsLinkCreateInteraction(QObject *parent = 0);

    void keyReleaseEvent(QKeyEvent *event);

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *evt);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *evt);

    void afterActivate();

    void beforeDeactivate(bool &cancel);

    QString interactionID();
    QString interactionName();

signals:

public slots:

private:
    bool m_bDrawing;
    bool m_bAppendOnNextMove;
    KsRoadObject *m_ptrTempRoad;
};

#endif // KSLINKCREATEINTERACTION_H
