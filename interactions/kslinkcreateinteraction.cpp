#include "kslinkcreateinteraction.h"

#include "ksinteractionstore.h"
#include "workspace/ksworkspace.h"
#include "db/model/project/ksroadmodelobject.h"
#include "db/model/project/kslaneobject.h"
#include "db/model/project/ksnodeobject.h"
#include "graphicObjects/ksgraphicnode.h"

KsLinkCreateInteraction::KsLinkCreateInteraction(QObject *parent) :
    KsAbstractInteraction(parent),
    m_bDrawing(false),
    m_ptrTempRoad(NULL)
{
}

void KsLinkCreateInteraction::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Escape) {
        KsAbstractInteraction *interaction = KsWorkspace::SharedInstance()->interactionStore()->interaction(KsInteractionStore::ITDefaultInteraction);
        scene()->setActiveInteractionObject(interaction);
    }
}

void KsLinkCreateInteraction::mouseReleaseEvent(QGraphicsSceneMouseEvent *evt)
{
    if(evt->button()==Qt::LeftButton) {
        if(m_bDrawing) {
            m_bAppendOnNextMove = true;
        } else {
            m_bDrawing = true;
            m_ptrTempRoad = new KsRoadObject;
            m_ptrTempRoad->setInitialMode(true);
            m_ptrTempRoad->addPointToRoadNormal(evt->scenePos());
            m_ptrTempRoad->addPointToRoadNormal(evt->scenePos());
            scene()->addItem(m_ptrTempRoad);
        }
    } else if(evt->button()==Qt::RightButton) {
        if(m_bAppendOnNextMove) {
            m_ptrTempRoad->addPointToRoadNormal(evt->scenePos());
        }

        if(m_ptrTempRoad) {
            KsRoadModelObject *obj = KsWorkspace::SharedInstance()->currentProject()->newRoad();

            obj->setDesignSpeed(30);
            obj->setName(tr("İsimsiz Yol"));
            obj->setSlope(0);
            obj->addLaneObject(KsWorkspace::SharedInstance()->currentProject()->newLane());
            obj->addLaneObject(KsWorkspace::SharedInstance()->currentProject()->newLane());

            m_ptrTempRoad->setModel(obj);
            m_ptrTempRoad->setInitialMode(false);
            m_ptrTempRoad->updateRoadGeometry();

            KsGraphicNode *startObject = dynamic_cast<KsGraphicNode*>(KsWorkspace::SharedInstance()->mapScene()->itemAt(obj->roadNormal().first(), QTransform()));
            KsGraphicNode *endObject = dynamic_cast<KsGraphicNode*>(KsWorkspace::SharedInstance()->mapScene()->itemAt(obj->roadNormal().last(), QTransform()));

            if(startObject) {
                obj->setSource(startObject->model());
            }

            if(endObject) {
                obj->setDestination(endObject->model());
            }

            KsWorkspace::SharedInstance()->mapScene()->currentLinkLayer()->addObject(m_ptrTempRoad);
        }

        m_bAppendOnNextMove = false;
        m_bDrawing = false;

        m_ptrTempRoad = NULL;
    }
}

void KsLinkCreateInteraction::mouseMoveEvent(QGraphicsSceneMouseEvent *evt)
{
    if(m_ptrTempRoad==NULL)
        return;
    if(m_bDrawing) {
        if(m_bAppendOnNextMove) {
            m_ptrTempRoad->addPointToRoadNormal(evt->scenePos());
            m_bAppendOnNextMove = false;
        }
        m_ptrTempRoad->setNormalPoint(m_ptrTempRoad->numberOfControlPoints()-1, evt->scenePos());
    }
}

void KsLinkCreateInteraction::afterActivate()
{
    m_bAppendOnNextMove = false;
    m_bDrawing = false;

    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->lockObjects();
    }
}

void KsLinkCreateInteraction::beforeDeactivate(bool &cancel)
{
    Q_UNUSED(cancel);
    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->unlockObjects();
    }
}

QString KsLinkCreateInteraction::interactionID()
{
    return tr("LINKDRAW");
}

QString KsLinkCreateInteraction::interactionName()
{
    return tr("Yol Çiz");
}
