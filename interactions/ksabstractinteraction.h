#ifndef KSABSTRACTINTERACTION_H
#define KSABSTRACTINTERACTION_H

#include <QObject>
#include "ksmouseinteraction.h"
#include "kskeyboardinteraction.h"

class QGraphicsMapScene;
class QGraphicsMapView;

/**
 * @brief Uygulamada kullanılacak klavye ve fare etkileşimi nesneleri için kullanılacak ana sınıftır.
 *
 * Tüm fonksiyonların iskeletini içerir, istenilen fonksiyon ezilip fonksiyonellik eklenebilir.
 */
class KsAbstractInteraction : public QObject, public KsMouseInteraction, public KsKeyboardInteraction
{
    Q_OBJECT
    Q_INTERFACES(KsKeyboardInteraction)
    Q_INTERFACES(KsMouseInteraction)
public:
    /**
     * @brief İlklendirici fonksiyon, herhangibir işlem yapılmamaktadır. Qt uyumluluğu için konulmuştur.
     *
     * @param parent Evebeyn nesne, göz ardı edilebilir. (Qt uyumluluğu için konulmuştur).
     */
    explicit KsAbstractInteraction(QObject *parent = 0);

    /**
     * @see KsKeyboardInteraction::keyPressEvent(QKeyEvent*)
     */
    virtual void keyPressEvent(QKeyEvent *event);

    /**
     * @see KsKeyboardInteraction::keyReleaseEvent(QKeyEvent*)
     */
    virtual void keyReleaseEvent(QKeyEvent *event);

    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

    /**
     * @see KsMouseInteraction::mouseMoveEvent(QGraphicsSceneEvent*)
     */
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    /**
     * @see KsMouseInteraction::mousePressEvent(QGraphicsSceneMouseEvent*)
     */
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);

    /**
     * @see KsMouseInteraction::mouseReleaseEvent(QGraphicsSceneMouseEvent*)
     */
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    /**
     * @see KsMouseInteraction::wheelEvent(QGraphicsSceneWheelEvent*)
     */
    virtual void wheelEvent(QGraphicsSceneWheelEvent *event);

    /**
     * @brief Etkileşim nesnesi etkinleştirmeden önce çalışacak eylem. Gerektiğinde işlem kesilebilir.
     *
     * @param cancel Eğer true atanırsa işlem durdurulur.
     */
    virtual void beforeActivate(bool &cancel);

    /**
     * @brief Etkileşim nesnesi etkinleştirildikten sonra çalışacak eylem.
     *
     */
    virtual void afterActivate();

    /**
     * @brief Etkileşim nesnesi pasifleştirilmeden önce çalışacak eylem. Gerektiğinde işlem kesilebilir.
     *
     * @param cancel Eğer true atanırsa işlem durdurulur.
     */
    virtual void beforeDeactivate(bool &cancel);

    /**
     * @brief Etkileşim nesnesi pasifleştirildikten sonra çalışacak eylem.
     *
     */
    virtual void afterDeactivate();

    virtual QString interactionName();

    virtual QString interactionID();


signals:
    void interactionActivated(bool activated);

public slots:

protected:
    /**
     * @brief Sahne nesnesine ulaşmak için kullanılacak fonksiyon.
     *
     * @return QGraphicsMapScene Sahne nesnesi.
     */
    QGraphicsMapScene *scene();

    QGraphicsMapView *view();

};

#endif // KSABSTRACTINTERACTION_H
