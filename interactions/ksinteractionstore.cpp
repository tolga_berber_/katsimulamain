#include "ksinteractionstore.h"

#include <QMutex>

#include "ksdefaultinteraction.h"
#include "ksnodecreateinteraction.h"
#include "kslinkcreateinteraction.h"
#include "ksinteractionaction.h"
#include "ksintersectioncreateinteraction.h"

KsInteractionStore *KsInteractionStore::s_pcSharedInstance = NULL;

KsInteractionStore *KsInteractionStore::sharedInstance()
{
    static QMutex lock;

    if(s_pcSharedInstance==NULL) {
        lock.lock();

        if(s_pcSharedInstance==NULL) {
            s_pcSharedInstance = new KsInteractionStore();
        }

        lock.unlock();
    }

    return s_pcSharedInstance;
}

KsAbstractInteraction *KsInteractionStore::interaction(KsInteractionStore::InteractionType type)
{
    if(type<ITTotalNumberOfInteractions) {
        return m_cInteractions.at(type);
    }
    return NULL;
}

QActionGroup *KsInteractionStore::actions()
{
    return m_pcActions;
}

KsInteractionStore::KsInteractionStore(QObject *parent) :
    QObject(parent)
{
    m_cInteractions<<new KsDefaultInteraction();
    m_cInteractions<<new KsNodeCreateInteraction();
    m_cInteractions<<new KsLinkCreateInteraction();
    m_cInteractions<<new KsIntersectionCreateInterAction();

    m_pcActions = new QActionGroup(this);

    foreach (KsAbstractInteraction *interaction, m_cInteractions) {
        m_pcActions->addAction(new KsInteractionAction(interaction, m_pcActions));
    }
    m_pcActions->setExclusive(true);

}
