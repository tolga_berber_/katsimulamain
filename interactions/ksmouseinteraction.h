#ifndef KSMOUSEINTERACTION_H
#define KSMOUSEINTERACTION_H

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsSceneWheelEvent>

/**
 * @brief Sahne üzerinde fare etkileşimi yapacak sınıfların gerçekleştirmesi gereken arabirim.
 *
 */
class KsMouseInteraction
{
public:
    /**
     * @brief Fare hareket ettirildiğinde çalışacak olay.
     *
     * @param mouseEvent Qt olay parametreleri.
     */
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)=0;

    /**
     * @brief Fare tuşuna basıldığında çalışacak olay.
     *
     * @param mouseEvent Qt olay parametreleri.
     */
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)=0;

    /**
     * @brief Fare tuşu bırakıldığında çalışacak olay.
     *
     * @param mouseEvent Qt olay parametreleri.
     */
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)=0;

    /**
     * @brief Fare tuşuna çift tıklatıldığında Çalışacak Olay.
     *
     * @param mouseEvent Qt olay parametreleri
     */
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent)=0;

    /**
     * @brief Fare tekeri döndürüldüğünde çalışacak olay.
     *
     * @param wheelEvent Qt olay parametreleri.
     */
    virtual void wheelEvent(QGraphicsSceneWheelEvent *wheelEvent)=0;
};


Q_DECLARE_INTERFACE(KsMouseInteraction, "Katsimula.MouseInteraction/1.0")

#endif // KSMOUSEINTERACTION_H
