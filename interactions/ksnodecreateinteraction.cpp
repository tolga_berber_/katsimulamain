#include "ksnodecreateinteraction.h"
#include "graphicObjects/qgraphicsmapview.h"
#include "graphicObjects/ksgraphicnode.h"
#include "workspace/ksworkspace.h"
#include "db/model/project/ksnodeobject.h"

KsNodeCreateInteraction::KsNodeCreateInteraction(QObject *parent) :
    KsAbstractInteraction(parent)
{
}

void KsNodeCreateInteraction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    KsNodeObject *node = KsWorkspace::SharedInstance()->currentProject()->newNode();
    node->setName(tr("Trafik Kaynağı"));
    node->setNodeType(KsNodeObject::NTSource);
    node->setLocation(event->scenePos());
    KsGraphicNode *gNode = new KsGraphicNode(event->scenePos());
    gNode->setModel(node);
    scene()->addItem(gNode);
    scene()->currentTrafficObjectLayer()->addObject(gNode);
}

void KsNodeCreateInteraction::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Escape) {
        KsAbstractInteraction *interaction = KsWorkspace::SharedInstance()->interactionStore()->interaction(KsInteractionStore::ITDefaultInteraction);
        scene()->setActiveInteractionObject(interaction);
    }
}

void KsNodeCreateInteraction::afterActivate()
{
    KsAbstractInteraction::afterActivate();
    view()->setDragMode(QGraphicsView::NoDrag);
    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->lockObjects();
    }
}

void KsNodeCreateInteraction::afterDeactivate()
{
    KsAbstractInteraction::afterDeactivate();
    QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();

    foreach (QGraphicsMapLayer *layer, scene->layers()) {
        layer->unlockObjects();
    }
}

QString KsNodeCreateInteraction::interactionID()
{
    return tr("ADDNODE");
}

QString KsNodeCreateInteraction::interactionName()
{
    return tr("Trafik Kaynağı\nEkleme");
}
