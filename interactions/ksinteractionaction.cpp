#include "ksinteractionaction.h"

#include "workspace/ksworkspace.h"

KsInteractionAction::KsInteractionAction(KsAbstractInteraction *interaction, QObject *parent) :
    KsAbstractAction(parent),
    m_pcInteractionObject(interaction)
{
    m_strGroupName = tr("Araçlar");
    m_strTabName = tr("ÇİZİM ARAÇLARI");
    setIcon(QPixmap(tr(":/img/action/draw/%1.png").arg(interaction->interactionID())));
    setText(interaction->interactionName());
    setCheckable(true);

    connect(m_pcInteractionObject, SIGNAL(interactionActivated(bool)), this, SLOT(setChecked(bool)));
}

void KsInteractionAction::setInteractionObject(KsAbstractInteraction *interactionObject)
{
    if(m_pcInteractionObject!=interactionObject) {
        m_pcInteractionObject = interactionObject;
    }
}

void KsInteractionAction::execute(bool checked)
{
    Q_UNUSED(checked);
    if(m_pcInteractionObject) {
        KsWorkspace::SharedInstance()->mapScene()->setActiveInteractionObject(this->m_pcInteractionObject);
    }
}
