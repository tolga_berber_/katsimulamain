#ifndef KSINTERSECTIONCREATEINTERACTION_H
#define KSINTERSECTIONCREATEINTERACTION_H

#include "ksabstractinteraction.h"

class KsIntersectionCreateInterAction : public KsAbstractInteraction
{
    Q_OBJECT
public:
    explicit KsIntersectionCreateInterAction(QObject *parent = 0);

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void afterActivate();
    void afterDeactivate();

    QString interactionID();
    QString interactionName();

signals:

public slots:
};

#endif // KSINTERSECTIONCREATEINTERACTION_H
