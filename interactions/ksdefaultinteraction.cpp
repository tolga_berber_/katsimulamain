#include "ksdefaultinteraction.h"

#include "graphicObjects/qgraphicsmapscene.h"
#include "workspace/ksworkspace.h"

KsDefaultInteraction::KsDefaultInteraction(QObject *parent) :
    KsAbstractInteraction(parent)
{
}

void KsDefaultInteraction::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    if(event->modifiers()==Qt::ControlModifier) {
        event->delta()>0?view()->zoomIn():view()->zoomOut();
        event->accept();
    }
}

void KsDefaultInteraction::keyPressEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Control) {
        view()->setDragMode(QGraphicsView::ScrollHandDrag);
        foreach (QGraphicsMapLayer *l, scene()->layers()) {
            l->lockObjects();
        }
    }
}

void KsDefaultInteraction::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Control) {
        view()->setDragMode(QGraphicsView::NoDrag);
        foreach (QGraphicsMapLayer *l, scene()->layers()) {
            l->unlockObjects();
        }
    }
}

void KsDefaultInteraction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    KsAbstractInteraction::mouseReleaseEvent(event);
    QList<QGraphicsItem *> itemsBelowMouse = scene()->items(event->scenePos());
    foreach (QGraphicsItem *itm, itemsBelowMouse) {
        if(itm->isSelected())
            return;
        if(itm->flags() & QGraphicsItem::ItemIsSelectable) {
            itm->setSelected(true);
            break;
        }
    }
}

void KsDefaultInteraction::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    KsAbstractInteraction::mousePressEvent(event);
    QList<QGraphicsItem *> itemsBelowMouse = scene()->items(event->scenePos());
    foreach (QGraphicsItem *itm, itemsBelowMouse) {
        if(itm->isSelected()) {
            event->accept();
            return;
        }
        if(itm->flags() & QGraphicsItem::ItemIsSelectable) {
            itm->setSelected(true);
            break;
        }
    }
}

void KsDefaultInteraction::afterActivate()
{
    KsAbstractInteraction::afterActivate();
    view()->setDragMode(QGraphicsView::NoDrag);
    // view()->setMouseTracking(true);
}

QString KsDefaultInteraction::interactionName()
{
    return tr("Seçim Aracı");
}

QString KsDefaultInteraction::interactionID()
{
    return tr("DEFAULT");
}

