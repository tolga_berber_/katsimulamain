#ifndef KSNODECREATEINTERACTION_H
#define KSNODECREATEINTERACTION_H

#include "ksabstractinteraction.h"

class KsNodeCreateInteraction : public KsAbstractInteraction
{
    Q_OBJECT
public:
    explicit KsNodeCreateInteraction(QObject *parent = 0);

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void afterActivate();
    void afterDeactivate();

    QString interactionID();
    QString interactionName();

signals:

public slots:

};

#endif // KSNODECREATEINTERACTION_H
