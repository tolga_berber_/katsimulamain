#ifndef KSKEYBOARDINTERACTION_H
#define KSKEYBOARDINTERACTION_H

#include <QKeyEvent>

/**
 * @brief Sahne üzerinde klavye etkileşimi yapacak olan sınıfların gerçekleştirmesi gereken arabirim.
 *
 */
class KsKeyboardInteraction
{
public:
    /**
     * @brief Klavyede bir tuşa basıldığında çalışacak olay.
     *
     * @param keyEvent Qt olay parametreleri.
     */
    virtual void keyPressEvent(QKeyEvent *keyEvent)=0;

    /**
     * @brief Klavyede basılan tuş bırakıldığında çalışacak olay.
     *
     * @param keyEvent Qt Olay parametreleri.
     */
    virtual void keyReleaseEvent(QKeyEvent *keyEvent)=0;
};

Q_DECLARE_INTERFACE(KsKeyboardInteraction, "Katsimula.KeyboardInteraction/1.0")

#endif // KSKEYBOARDINTERACTION_H
