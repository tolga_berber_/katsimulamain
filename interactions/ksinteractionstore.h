#ifndef KSINTERACTIONSTORE_H
#define KSINTERACTIONSTORE_H

#include <QObject>

#include "ksabstractinteraction.h"
#include <QList>
#include <QActionGroup>

typedef QList<KsAbstractInteraction*> KsInteractionList;

class KsInteractionStore : public QObject
{
    Q_OBJECT
    Q_ENUMS(InteractionType)

public:
    static KsInteractionStore *sharedInstance();

    enum InteractionType {
        ITDefaultInteraction = 0,
        ITNodeCreateInteraction,
        ITLinkCreateInteraction,
        ITTotalNumberOfInteractions
    };

    KsAbstractInteraction *interaction(InteractionType type);

    QActionGroup *actions();

private:
    KsInteractionList m_cInteractions;
    QActionGroup *m_pcActions;
    explicit KsInteractionStore(QObject *parent = 0);

    static KsInteractionStore *s_pcSharedInstance;

signals:

public slots:

};

#endif // KSINTERACTIONSTORE_H
