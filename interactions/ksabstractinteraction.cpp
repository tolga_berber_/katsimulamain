#include "ksabstractinteraction.h"

#include "workspace/ksworkspace.h"

KsAbstractInteraction::KsAbstractInteraction(QObject *parent) :
    QObject(parent)
{
}

void KsAbstractInteraction::keyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
}

void KsAbstractInteraction::keyReleaseEvent(QKeyEvent *event)
{
    Q_UNUSED(event)
}

void KsAbstractInteraction::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

void KsAbstractInteraction::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF scenePos = event->scenePos();
    QPointF worldPoint = scene()->worldItem()->mapFromScene(scenePos);
    if(scene()->worldItem()->visibleWorldPart(0).contains(worldPoint)) {
        KsWorkspace::SharedInstance()->mainWindow()->setWorldPoint(worldPoint);
    }
}

void KsAbstractInteraction::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
}

void KsAbstractInteraction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event)
}

void KsAbstractInteraction::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    Q_UNUSED(event)
}

void KsAbstractInteraction::beforeActivate(bool &cancel)
{
    Q_UNUSED(cancel)
}

void KsAbstractInteraction::afterActivate()
{
    emit interactionActivated(true);
}

void KsAbstractInteraction::beforeDeactivate(bool &cancel)
{
    Q_UNUSED(cancel)
}

void KsAbstractInteraction::afterDeactivate()
{
    emit interactionActivated(false);
}

QString KsAbstractInteraction::interactionName()
{
    return tr("Belirlenmedi");
}

QString KsAbstractInteraction::interactionID()
{
    return tr("NONE");
}

QGraphicsMapScene *KsAbstractInteraction::scene()
{
    return KsWorkspace::SharedInstance()->mapScene();
}

QGraphicsMapView *KsAbstractInteraction::view()
{
    return KsWorkspace::SharedInstance()->mapView();
}
