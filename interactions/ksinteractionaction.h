#ifndef KSINTERACTIONACTION_H
#define KSINTERACTIONACTION_H

#include "../Actions/ksabstractaction.h"
#include "ksabstractinteraction.h"

class KsInteractionAction : public KsAbstractAction
{
    Q_OBJECT
public:
    explicit KsInteractionAction(KsAbstractInteraction *interaction, QObject *parent = 0);

signals:

public slots:
    void setInteractionObject(KsAbstractInteraction *interactionObject);

protected:
    void execute(bool checked=false);

private:
    KsAbstractInteraction *m_pcInteractionObject;

};

#endif // KSINTERACTIONACTION_H
