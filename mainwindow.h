#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QStatusBar>
#include <QLabel>

/**
 * @brief
 *
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit MainWindow(QWidget *parent = 0);
    /**
     * @brief
     *
     */
    ~MainWindow();


public slots:
    void setWorldPoint(const QPointF &point);
    void setWorldRect(const QRectF &rect);

private slots:

protected:
    virtual void closeEvent(QCloseEvent *evt);

private:
    QVBoxLayout *m_pcLayout; /**< TODO */
    QStatusBar *m_pcStatus; /**< TODO */
    QLabel *m_pcWorldPoint; /**< TODO */
    QLabel *m_pcWorldArea; /**< TODO */

    inline QString point2String(QPointF point);
    inline QString rect2String(QRectF rect);
    inline double areaOfRect(QRectF rect);
    inline QString friendlyAreaDescription(double area);
};

#endif // MAINWINDOW_H
