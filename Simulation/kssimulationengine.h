#ifndef KSSIMULATIONENGINE_H
#define KSSIMULATIONENGINE_H

#include <QObject>

#include <QList>
#include <QHash>

class KsSimulationInfo;
class KsSimulationVehicle;
class KsGraf;
class KsVehicle;
class KsSpontaneousDB;
class KsNodeObject;
class KsRoadModelObject;
class KsLaneObject;

typedef QList<KsSimulationVehicle*> VehicleList;
typedef QList<KsVehicle*> AvailVehicleList;
typedef QHash<int, qreal> IntToIntHash;
typedef QHash<KsLaneObject*, VehicleList > LaneLoads;

class KsSimulationEngine : public QObject
{
    Q_OBJECT
public:
    explicit KsSimulationEngine(QObject *parent = 0);

    KsSimulationInfo *info();

    void writeVehicle(KsSimulationVehicle *v);
signals:

public slots:
    void setSimulationInfo(KsSimulationInfo *inf);

    void startSimulation();
    void pauseSimulation();
    void stopSimulation();

    void calculateSecond();
    void increaseSecond();

private:
    qint64 m_liSimTime;
    KsSimulationInfo *m_ptrInfo;

    VehicleList m_lstVehicles;
    AvailVehicleList m_lstAvailVehicles;

    KsGraf *m_ptrGraf;

    KsSpontaneousDB *m_ptrDb;

    IntToIntHash m_hshLaneCapacities;

    LaneLoads m_hshLoads;

    KsVehicle *selectRandomVehicle();
    KsSimulationVehicle *generateVehicleForSource(KsNodeObject *src);
    void calculateLaneCapacities();
    qreal maxVehicleLength();
    void produceVehicles();

    bool m_bPaused;
};

#endif // KSSIMULATIONENGINE_H
