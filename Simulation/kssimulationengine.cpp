#include "kssimulationengine.h"

#include "db/model/simulation/kssimulationinfo.h"
#include "db/model/simulation/kssimulationvehicle.h"
#include "db/model/project/ksnodeobject.h"
#include "db/model/project/ksroadmodelobject.h"
#include "db/model/project/kslaneobject.h"

#include "db/ksspontaneousdb.h"

#include "workspace/ksworkspace.h"

#include "graf/ksgraf.h"

#include <cstdlib>

#include <QApplication>

KsSimulationEngine::KsSimulationEngine(QObject *parent) : QObject(parent),
    m_ptrInfo(NULL),
    m_bPaused(false)
{
    m_ptrGraf = KsWorkspace::SharedInstance()->currentProject()->graf();
    m_lstAvailVehicles = KsWorkspace::SharedInstance()->staticDatabase()->vehicles();
}

KsSimulationInfo *KsSimulationEngine::info()
{
    return m_ptrInfo;
}

void KsSimulationEngine::setSimulationInfo(KsSimulationInfo *inf)
{
    if(m_ptrInfo!=inf) {
        m_ptrInfo = inf;
    }
}

void KsSimulationEngine::startSimulation()
{
    if(!m_bPaused) {
        if(!m_ptrInfo) {
            m_ptrInfo = new KsSimulationInfo(this);
            m_ptrInfo->setDuration(500);
            m_ptrInfo->setStartTime(QTime::fromString("08:00:00"));
        }
        KsProject *p = KsWorkspace::SharedInstance()->currentProject();
        m_ptrDb = new KsSpontaneousDB(tr("%1_simrun").arg(p->projectName()),
                                      tr("%1/%2_simrun.ksdf").arg(p->projectPath()).arg(p->projectName()));
        calculateLaneCapacities();
        m_ptrDb->beginTransaction();
        m_liSimTime = 0;
        m_ptrDb->insertInfo(m_ptrInfo);
    } else {
        m_bPaused = false;
    }

    while(m_liSimTime<m_ptrInfo->duration()) {
        increaseSecond();
        calculateSecond();
        produceVehicles();
        qApp->processEvents();
        if(m_bPaused) {
            return;
        }
        if(m_liSimTime%100) {
            m_ptrDb->endTransaction();
            m_ptrDb->beginTransaction();
        }
    }
    m_ptrDb->endTransaction();

    m_bPaused = false;

    delete m_ptrInfo;
    delete m_ptrDb;
}

void KsSimulationEngine::pauseSimulation()
{
    m_bPaused = true;
}

void KsSimulationEngine::stopSimulation()
{
    m_ptrDb->endTransaction();
}

void KsSimulationEngine::calculateSecond()
{
    VehicleList vList;
    foreach (KsSimulationVehicle *v, m_lstVehicles) {
        v->setSecond(m_liSimTime);
        v->advance();
        writeVehicle(v);
        if(v->isDone()) {
            vList.append(v);
        }
    }

    while(vList.size()>0) {
        KsSimulationVehicle *v = vList.takeFirst();
        m_lstVehicles.removeAll(v);
        delete v;
    }

}

void KsSimulationEngine::increaseSecond()
{
    m_liSimTime++;
}

KsVehicle *KsSimulationEngine::selectRandomVehicle()
{
    return m_lstAvailVehicles[qrand()%m_lstAvailVehicles.count()];
}

KsSimulationVehicle *KsSimulationEngine::generateVehicleForSource(KsNodeObject *src)
{
    KsSimulationVehicle *result = new KsSimulationVehicle(src, m_ptrGraf->selectRandomDestNode(src),selectRandomVehicle(),m_ptrInfo,m_ptrInfo);
    RoadList route;
    int tryOuts = 0;
    do {
        route = m_ptrGraf->findShortestPathBetweenTwoNodes(src, result->end());
        if(route.count()!=0) {
            result->setRoute(route);
            break;
        }
        result->setEnd(m_ptrGraf->selectRandomDestNode(src));
        tryOuts++;
        if(tryOuts>5) {
            delete result;
            return NULL;
        }
    } while (!result->hasValidRoute());
    return result;
}

void KsSimulationEngine::calculateLaneCapacities()
{
    m_hshLaneCapacities.clear();
    qreal maxLength = maxVehicleLength();

    for(EdgeListIterator i=m_ptrGraf->edgeBegin();
        i!=m_ptrGraf->edgeEnd();
        i++) {
        KsRoadModelObject *obj = *i;
        for(int i=0;i<obj->laneCount();i++) {
            m_hshLaneCapacities[(*obj)[i]->id()] = (*obj)[i]->laneLength()/(maxLength+2);
            m_hshLoads[(*obj)[i]] = VehicleList();
        }
    }
}

qreal KsSimulationEngine::maxVehicleLength()
{
    qreal result=0;
    foreach (KsVehicle *v, m_lstAvailVehicles) {
        if(result<v->length()) {
            result = v->length();
        }
    }
    return result;
}

void KsSimulationEngine::writeVehicle(KsSimulationVehicle *v)
{
    m_ptrDb->insertVehicle(v);
    m_ptrDb->insertCarbonEmissions(v->emissions());
    m_ptrDb->insertLocation(v->location());
    m_ptrDb->insertSpeed(v->speed());
}

void KsSimulationEngine::produceVehicles()
{
    double probabilities = 1.0/m_ptrGraf->sourceCount();

    for(int i=0;i<m_ptrGraf->sourceCount();i++) {
        qreal chance = (double)qrand()/RAND_MAX;
        if(chance>probabilities) {
            KsSimulationVehicle *v = generateVehicleForSource(m_ptrGraf->source(i));
            if(!v) {
                continue;
            }
            v->setSecond(m_liSimTime);
            writeVehicle(v);
            m_lstVehicles.append(v);
        }
    }
}

