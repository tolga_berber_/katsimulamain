#include "qtilemapprovider.h"

QTileMapProvider::QTileMapProvider(QObject *parent) :
    QObject(parent)
{
    m_pcSource = new KsTileSource(this);
    m_pcSource->start();
}

QTileMapProvider::~QTileMapProvider()
{
    m_pcSource->quit();
    m_pcSource->deleteLater();
}

KsTileSource *QTileMapProvider::tileSource()
{
    return this->m_pcSource;
}

bool QTileMapProvider::isProviderReady()
{
    return m_bDBReady && m_bNetReady;
}

void QTileMapProvider::setNetworkReady(bool ready)
{
    m_bDBReady = ready;
}

void QTileMapProvider::setDBReady(bool ready)
{
    m_bNetReady = ready;
}

QRect QTileMapProvider::tileRectForWorldRect(QRectF rect, int z)
{
    int sX = qMax(long2tilex(rect.left(), z)-1, 0);
    int eX = qMin((double)long2tilex(rect.right(), z)+1, pow(2.0,z)-1);

    int sY = qMax(lat2tiley(-1.0*rect.top(), z)-1, 0);
    int eY = qMin((double)lat2tiley(-1.0*rect.bottom(), z)+1, pow(2.0,z)-1);

    return QRect(sX,sY,eX-sX+1,eY-sY+1);
}

QRectF QTileMapProvider::tileWorldRect(int x, int y, int z)
{
    qreal left = tilex2long(x, z);
    qreal right = tilex2long(x+1, z);

    qreal top = -1.0*tiley2lat(y, z);
    qreal bottom = -1.0*tiley2lat(y+1, z);

    QPointF topLeft(left, top);
    QPointF bottomRight(right, bottom);

    return QRectF(topLeft, bottomRight);
}

QRectF QTileMapProvider::tileWorldRect(QPoint p, int z)
{
    return tileWorldRect(p.x(), p.y(), z);
}

QUrl QTileMapProvider::urlForTile(int x, int y, int z)
{
    return QUrl(urlStringForTile(x,y,z));
}

bool QTileMapProvider::isValidZoomLevel(int zoomLevel)
{
    return zoomLevel<=maxZoomLevel() && zoomLevel>=minZoomLevel();
}
