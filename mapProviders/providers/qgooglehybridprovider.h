#ifndef QGOOGLEHYBRIDPROVIDER_H
#define QGOOGLEHYBRIDPROVIDER_H

#include "qtilemapprovider.h"

/**
 * @brief Google Haritalar için kullanılacak harita sağlayıcı sınıf.
 *
 */
class QGoogleHybridProvider : public QTileMapProvider
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici,Qt uyumluluğu için kullanılmaktadır.
     *
     */
    explicit QGoogleHybridProvider(QObject *parent = 0);

    /**
     * @brief @see MapProvider::urlForTile(int,int,int)
     *
     */
    virtual QString urlStringForTile(int x, int y, int z);

    /**
     * @brief @see MapProvider::providerName()
     *
     */
    virtual QString providerName();

    /**
     * @brief @see MapProvider::providerID()
     *
     */
    virtual QString providerID();

    /**
     * @brief @see MapProvider::minZoomLevel()
     *
     */
    virtual int minZoomLevel();

    /**
     * @brief @see MapProvider::maxZoomLevel()
     *
     */
    virtual int maxZoomLevel();

signals:

public slots:

};

#endif // QGOOGLEHYBRIDPROVIDER_H
