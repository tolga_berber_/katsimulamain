#ifndef KSMAPPROVIDERACTION_H
#define KSMAPPROVIDERACTION_H

#include "Actions/ksabstractaction.h"

#include "mapprovider.h"

class KsMapProviderAction : public KsAbstractAction
{
    Q_OBJECT
public:
    explicit KsMapProviderAction(MapProvider *provider, QObject *parent = 0);

signals:

public slots:

protected:
    virtual void execute(bool checked=false);

    virtual void shortcutExecuted();

private:
    MapProvider *m_pcMapProvider;

};

#endif // KSMAPPROVIDERACTION_H
