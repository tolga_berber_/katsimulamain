#ifndef QTILEMAPPROVIDER_H
#define QTILEMAPPROVIDER_H

#include <QObject>
#define _USE_MATH_DEFINES 1
#include <math.h>
#include "mapprovider.h"

#include "tileSource/kstilesource.h"

/**
 * @brief MapProvider sınıfındaki bazı fonksiyonların gerçekleştirimini içeren sınıf.
 *
 */
class QTileMapProvider : public QObject, public MapProvider
{
    Q_OBJECT
    Q_INTERFACES(MapProvider)
public:
    /**
     * @brief İlklendirici, herhangi bir işlevi bulunmamaktadır,Qt uyumluluğu için konulmuştur.
     *
     * @param parent Göz ardı edilebilir, Qt uyumluluğu için konulmuştur.
     */
    explicit QTileMapProvider(QObject *parent = 0);

    virtual ~QTileMapProvider();

    virtual KsTileSource *tileSource();

    virtual bool isProviderReady();

    virtual void setNetworkReady(bool ready);

    virtual void setDBReady(bool ready);

    /**
     * @brief MapProvider::tileRectForWorldRect(QRectF,int) fonksiyonunun gerçekleştirimini içerir.
     *
     */
    virtual QRect tileRectForWorldRect(QRectF rect, int z);

    /**
     * @brief MapProvider::tileWorldRect(int,int,int) fonksiyonunun gerçekleştirimini içerir.
     *
     */
    virtual QRectF tileWorldRect(int x, int y, int z);

    /**
     * @brief MapProvider::tileWorldRect(QPoint,int) fonksiyonunun gerçekleştirimini içerir.
     *
     */
    virtual QRectF tileWorldRect(QPoint p, int z);

    /**
     * @brief MapProvider::urlForTile(int,int,int)  fonksiyonunun gerçekleştirimini içerir.
     *
     */
    virtual QUrl urlForTile(int x, int y, int z);

    /**
     * @brief MapProvider::isValidZoomLevel(int)  fonksiyonunun gerçekleştirimini içerir.
     *
     */
    virtual bool isValidZoomLevel(int zoomLevel);

protected:
    /**
     * @brief Longitude değerlerini karelerin X koordinatlarına dönüştüren fonksiyon.
     *
     * @param lon Dönüşüm yapılacak longitude değeri
     * @param z Yakınlaşma seviyesi
     * @return int Karenin X koordinatı
     */
    inline int long2tilex(double lon, int z)
    {
        return qMin((floor((lon + 180.0) / 360.0 * pow(2.0,z))), pow(2.0,z)-1);
    }

    /**
     * @brief Latitude değerlerini karelerin Y koordinatlarına dönüştüren fonksiyon.
     *
     * @param lat Dönüşüm yapılacak latitude değeri
     * @param z Yakınlaşma seviyesi
     * @return int Karenin Y koordinatı
     */
    inline int lat2tiley(double lat, int z)
    {
        return qMin((floor((1.0 - log( tan(lat * M_PI/180.0) + 1.0 / cos(lat * M_PI/180.0)) / M_PI) / 2.0 * pow(2.0,z))), pow(2.0,z)-1);
    }

    /**
     * @brief Karenin X koordinatını longitude değerine dönüştüren fonksiyon.
     *
     * @param x Dönüşüm yapılacak x değeri
     * @param z Yakınlaşma seviyesi
     * @return double Longitude değeri
     */
    inline double tilex2long(int x, int z)
    {
        return x / pow(2.0,z) * 360.0 - 180;
    }

    /**
     * @brief Karenin Y koordinatını latitude değerine dönüştüren fonksiyon.
     *
     * @param y Dönüşüm yapılacak y değeri
     * @param z Yakınlaşma seviyesi
     * @return double Latitude değeri
     */
    inline double tiley2lat(int y, int z)
    {
        double n = M_PI - 2.0 * M_PI * y / pow(2.0,z);
        return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
    }

private:
    KsTileSource *m_pcSource;
    bool m_bNetReady;
    bool m_bDBReady;
};

#endif // QTILEMAPPROVIDER_H
