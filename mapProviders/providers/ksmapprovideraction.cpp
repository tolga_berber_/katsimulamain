#include "ksmapprovideraction.h"
#include "workspace/ksworkspace.h"

KsMapProviderAction::KsMapProviderAction(MapProvider *provider, QObject *parent) :
    KsAbstractAction(parent),
    m_pcMapProvider(provider)
{
    m_strGroupName = tr("Harita");
    m_strTabName = tr("GÖRÜNÜM");
    setIcon(QPixmap(tr(":/img/action/view/%1.png").arg(provider->providerID())));
    setText(provider->providerName());
    setCheckable(true);
}

void KsMapProviderAction::execute(bool checked)
{
    Q_UNUSED(checked);
    KsWorkspace::SharedInstance()->mapProviders()->setActiveProvider(this->m_pcMapProvider);
}

void KsMapProviderAction::shortcutExecuted()
{
    execute();
    setChecked(true);
}
