#include "qosmstandardprovider.h"

QOSMStandardProvider::QOSMStandardProvider(QObject *parent) :
    QTileMapProvider(parent)
{
}

QString QOSMStandardProvider::urlStringForTile(int x, int y, int z)
{
    QString format ="http://a.tile.openstreetmap.org/%1/%2/%3.png";
    return format.arg(z).arg(x).arg(y);
}

QString QOSMStandardProvider::providerName()
{
    return tr("OpenStreetMap");
}

QString QOSMStandardProvider::providerID()
{
    return "OSMSTD";
}

int QOSMStandardProvider::maxZoomLevel()
{
    return 19;
}

int QOSMStandardProvider::minZoomLevel()
{
    return 0;
}
