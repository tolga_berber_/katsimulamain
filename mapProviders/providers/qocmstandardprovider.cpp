#include "qocmstandardprovider.h"

QOCMStandardProvider::QOCMStandardProvider(QObject *parent) :
    QTileMapProvider(parent)
{
}

QString QOCMStandardProvider::urlStringForTile(int x, int y, int z)
{
    QString format ="http://a.tile.opencyclemap.org/cycle/%1/%2/%3.png";
    return format.arg(z).arg(x).arg(y);
}

QString QOCMStandardProvider::providerName()
{
    return tr("OpenCycleMap");
}

QString QOCMStandardProvider::providerID()
{
    return "OCMSTD";
}

int QOCMStandardProvider::minZoomLevel()
{
    return 0;
}

int QOCMStandardProvider::maxZoomLevel()
{
    return 18;
}
