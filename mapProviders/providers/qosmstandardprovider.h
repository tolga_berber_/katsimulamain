#ifndef QOSMSTANDARDPROVIDER_H
#define QOSMSTANDARDPROVIDER_H

#include "qtilemapprovider.h"

/**
 * @brief OpenStreetMap için kullanılacak harita sağlayıcı sınıf.
 *
 */
class QOSMStandardProvider : public QTileMapProvider
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici,Qt uyumluluğu için kullanılmaktadır.
     *
     */
    explicit QOSMStandardProvider(QObject *parent = 0);

    /**
     * @brief @see MapProvider::urlForTile(int,int,int)
     *
     */
    virtual QString urlStringForTile(int x, int y, int z);

    /**
     * @brief @see MapProvider::providerName()
     *
     */
    virtual QString providerName();

    /**
     * @brief @see MapProvider::providerID()
     *
     */
    virtual QString providerID();

    /**
     * @brief @see MapProvider::minZoomLevel()
     *
     */
    virtual int maxZoomLevel();

    /**
     * @brief @see MapProvider::maxZoomLevel()
     *
     */
    virtual int minZoomLevel();
signals:

public slots:

};

#endif // QOSMSTANDARDPROVIDER_H
