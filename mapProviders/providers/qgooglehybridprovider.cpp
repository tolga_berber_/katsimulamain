#include "qgooglehybridprovider.h"

QGoogleHybridProvider::QGoogleHybridProvider(QObject *parent) :
    QTileMapProvider(parent)
{
}

QString QGoogleHybridProvider::urlStringForTile(int x, int y, int z)
{
    QString format ="http://mt1.google.com/vt/lyrs=y&x=%2&y=%3&z=%1";
    return format.arg(z).arg(x).arg(y);
}

QString QGoogleHybridProvider::providerName()
{
    return tr("Google Hibrid");
}

QString QGoogleHybridProvider::providerID()
{
    return "GOGLHYB";
}

int QGoogleHybridProvider::minZoomLevel()
{
    return 0;
}

int QGoogleHybridProvider::maxZoomLevel()
{
    return 18;
}
