#ifndef MAPPROVIDER_H
#define MAPPROVIDER_H

#include <QObject>
#include <QString>
#include <QUrl>
#include <QRect>
#include <QRectF>
#include <QPoint>

class KsTileSource;

/**
 * @brief Harita sağlayıcıların gerçeklemesi gereken arabirim. Bu arabirim dünyayı kareler halinde sunan sağlayıcılar için kullanılmaktadır.
 *
 */
class MapProvider {
public:
    /**
     * @brief Belirtilen koordinattaki karenin fotoğrafını içeren URL metnini döndüren fonksiyon.
     *
     * @param x Karenin X koordinatı
     * @param y Karenin Y koordinatı
     * @param z Karenin yaklaşım oranı
     * @return QString Kare için gerekli URL metni
     */
    virtual QString urlStringForTile(int x, int y, int z)=0;

    virtual KsTileSource *tileSource()=0;

    virtual bool isProviderReady()=0;

    virtual void setNetworkReady(bool ready)=0;

    virtual void setDBReady(bool ready)=0;

    /**
     * @brief Belirtilen koordinattaki karenin fotoğrafını içeren URL'yi döndüren fonksiyon.
     *
     * @param x Karenin X koordinatı
     * @param y Karenin Y koordinatı
     * @param z Karenin yaklaşım oranı
     * @return QString Kare için gerekli URL
     */
    virtual QUrl urlForTile(int x, int y, int z)=0;

    /**
     * @brief Sağlayıcı Adını döndüren Fonksiyon.
     *
     * @return QString Sağlayıcı Adı
     */
    virtual QString providerName()=0;

    /**
     * @brief Sağlayıcı ID'yi döndüren fonksiyon.
     *
     * @return QString Sağlayıcı ID
     */
    virtual QString providerID()=0;

    /**
     * @brief Belirtilen dünya koordinatlarını içerecek kare koordinatlarını döndüren fonksiyon (Örn, (-180,-85)x(360,170) Z=1 => (0,0)x(2,2)
     *
     * @param rect Dünya koordinatları
     * @param z Yaklaşma seviyesi
     * @return QRect Kare koordinatları
     */
    virtual QRect tileRectForWorldRect(QRectF rect, int z)=0;

    /**
     * @brief Belirtilen karenin dünya üzerindeki koordinatlarını döndüren fonksiyon.
     *
     * @param x Karenin X koordinatı
     * @param y Karenin Y koordinatı
     * @param z Yakınlaşma seviyesi
     * @return QRectF Karenin dünya koordinatları
     */
    virtual QRectF tileWorldRect(int x,int y,int z)=0;

    /**
     * @brief Belirtilen karenin dünya üzerindeki koordinatlarını döndüren fonksiyon.
     *
     * @param p Karenin konumu
     * @param z Yakınlaşma seviyesi
     * @return QRectF Karenin dünya koordinatları
     */
    virtual QRectF tileWorldRect(QPoint p, int z)=0;

    /**
     * @brief Belirtilen yakınlaşma seviyesinin geçerli olduğunu öğrenmek için kullanılan fonksiyon.
     *
     * @param zoomLevel Test edilecek yakınlaşma değeri.
     * @return bool Yakınlaşma değeri geçerliyse true değilse false.
     */
    virtual bool isValidZoomLevel(int zoomLevel)=0;

    /**
     * @brief En düşük yakınlaşma değerini döndüren fonksiyon.
     *
     * @return int En küçük yakınlaşma değeri.
     */
    virtual int minZoomLevel()=0;

    /**
     * @brief En büyük yakınlaşma değerini döndüren fonksiyon.
     *
     * @return int En büyük yakınlaşma değeri.
     */
    virtual int maxZoomLevel()=0;
};

Q_DECLARE_INTERFACE(MapProvider, "Katsimula.MapProvider/1.0")

#endif // MAPPROVIDER_H
