#ifndef QMAPPROVIDERSTORE_H
#define QMAPPROVIDERSTORE_H

#include <QObject>
#include <QList>
#include <QStringList>
#include "providers/mapprovider.h"

#include <QActionGroup>
#include <QMap>

typedef QMap<MapProvider*, QAction*> Provider2ActionMap;

/**
 * @brief Harita sağlayıcılarını saklayacak veri yapısı.
 *
 */
typedef QList<MapProvider*> MapProviders;


/**
 * @brief Program dahilinde kullanılabilecek bütün harita sağlayıcılarını yönetecek olan nesne.
 *
 */
class QMapProviderStore : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Bu sınıf singleton tasarım şablonunu kullandığından sınıftan sadece 1 nesne üretilebilir. Üretilen nesneyede bu statik fonksiyon aracılığı ile erişilmelidir.
     * Bu fonksiyon çok kanallı programlama da çağırılmak içi güvenlidir.
     *
     * @return QMapProviderStore Paylaşılan nesne.
     */
    static QMapProviderStore *sharedInstance();
    
    /**
     * @brief Nesnenin kullandığı kaynakları temizler.
     *
     */
    static void close();

    /**
     * @brief Aktif harita sağlayıcıların sayısını döndüren fonksiyon.
     *
     * @return int Harita sağlayıcıların sayısı.
     */
    int providerCount();
    
    /**
     * @brief Aktif harita sağlayıcı nesnesini döndüren fonksiyon.
     *
     * @return MapProvider Aktif harita sağlayıcı nesnesi.
     */
    MapProvider *activeProvider();
    
    /**
     * @brief Kullanılabilecek sağlayıcılarının isimlerini döndüren fonksiyon.
     *
     * @return QStringList Harita sağlayıcılarının listesi.
     */
    QStringList providerNames();

    /**
     * @brief Aktif harita sağlayıcıların desteklediği en yüksek yaklaşma değeri.
     *
     * @return int En yüksek harita yaklaşma değeri.
     */
    int maxZoomLevel();

    QActionGroup *mapActionGroup();

signals:
    /**
     * @brief Aktif harita sağlayıcı değiştiğinde tetiklenen sinyal.
     *
     */
    void activeProviderChanged();

public slots:
    /**
     * @brief Aktif hartia sağlayıcıyı değiştirmek için kullanılan fonksiyon.
     *
     * @param name Yeni sağlayıcı adı.
     */
    void setActiveProvider(QString name);

    void setActiveProvider(MapProvider *provider);

private:
    /**
     * @brief İlklendirici, singleton tasarım şablonu kullanıldığı için nesnenin public olarak ilklendirilmesi gerekmektedir. Hazır olan harita sağlayıcılar sisteme
     * bu esnada eklenir.
     *
     * @param parent
     */
    explicit QMapProviderStore(QObject *parent = 0);
    static QMapProviderStore *s_pcStore; /**< Tüm sınıflarda paylaşılan nesneyi tutan değişken. */

    MapProviders m_cMapProviders; /**< Harita sağlayıcılarının listesi. */
    MapProvider *m_pcActiveMapProvider; /**< Aktif harita sağlayıcı */
    QActionGroup *m_pcActions;
    Provider2ActionMap m_cActionMap;
};

#endif // QMAPPROVIDERSTORE_H
