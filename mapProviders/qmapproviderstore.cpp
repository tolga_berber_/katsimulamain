#include "qmapproviderstore.h"
#include <QMutex>
#include "providers/qocmstandardprovider.h"
#include "providers/qosmstandardprovider.h"
#include "providers/qgooglehybridprovider.h"
#include <QApplication>

#include "providers/ksmapprovideraction.h"

//YAPILACAK Harita sağlayıcıları bir sınıf içinde toplayıp o sınıfın threadleri olması sağlansa daha iyi olacak gibi...
//YAPILACAK Burada harita geçiş eylemleri yapılsa daha iyi olacak gibi Örneğin KsMapSwitch Action diyebir sınıf yapıp onunla geçişleri sağlamak daha iyi olacak sanki....

QMapProviderStore *QMapProviderStore::s_pcStore = NULL;

QMapProviderStore *QMapProviderStore::sharedInstance()
{
    static QMutex mutex;
    if(!s_pcStore) {
        mutex.lock();
        if(!s_pcStore)
            s_pcStore = new QMapProviderStore();
        mutex.unlock();
    }
    return s_pcStore;
}

void QMapProviderStore::close()
{
    static QMutex mutex;
    mutex.lock();
    delete s_pcStore;
    s_pcStore = NULL;
    mutex.unlock();
}

int QMapProviderStore::providerCount()
{
    return m_cMapProviders.size();
}

MapProvider *QMapProviderStore::activeProvider()
{
    return m_pcActiveMapProvider;
}

QStringList QMapProviderStore::providerNames()
{
    QStringList result;
    foreach (MapProvider *provider, m_cMapProviders) {
        result<<provider->providerName();
    }
    return result;
}

int QMapProviderStore::maxZoomLevel()
{
    int result = 0;
    foreach (MapProvider *provider, m_cMapProviders) {
        result = qMax(provider->maxZoomLevel(), result);
    }
    return result;
}

QActionGroup *QMapProviderStore::mapActionGroup()
{
    return m_pcActions;
}

void QMapProviderStore::setActiveProvider(QString name)
{
    foreach (MapProvider *provider, m_cMapProviders) {
        if(provider->providerID()==name) {
            setActiveProvider(provider);
            return;
        }
    }
}

void QMapProviderStore::setActiveProvider(MapProvider *provider)
{
    if(provider != m_pcActiveMapProvider) {
        m_pcActiveMapProvider = provider;
        m_cActionMap[provider]->setChecked(true);
        emit activeProviderChanged();
    }
}

QMapProviderStore::QMapProviderStore(QObject *parent) :
    QObject(parent)
{
    m_cMapProviders.append(new QOSMStandardProvider());
    m_cMapProviders.append(new QOCMStandardProvider());
    m_cMapProviders.append(new QGoogleHybridProvider());

    while(true) {
        bool value = true;
        foreach (MapProvider *provider, m_cMapProviders) {
            value = value & provider->isProviderReady();
        }
        if(value)
            break;
        qApp->processEvents();
        QThread::usleep(2000);
    }

    m_pcActions = new QActionGroup(this);

    foreach (MapProvider *provider, m_cMapProviders) {
        m_cActionMap[provider] = new KsMapProviderAction(provider, m_pcActions);
        m_pcActions->addAction(m_cActionMap[provider]);
    }
    m_pcActions->setExclusive(true);

    setActiveProvider(m_cMapProviders[0]);
    // m_pcActiveMapProvider = m_cMapProviders[0];
}
