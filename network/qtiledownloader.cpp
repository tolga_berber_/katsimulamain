#include "qtiledownloader.h"

#include "../mapProviders/qmapproviderstore.h"

QTileDownloader::QTileDownloader(MapProvider *provider, QObject *parent) :
    QObject(parent),
    m_pcProvider(provider)
{
    m_pcNAM = new QNetworkAccessManager();

    connect(m_pcNAM, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkRequestCompleted(QNetworkReply*)));

    m_pcProvider->setNetworkReady(true);
}

QTileDownloader::~QTileDownloader()
{
    delete m_pcNAM;
}

void QTileDownloader::networkRequestCompleted(QNetworkReply *reply)
{
    QByteArray arr = reply->readAll();

    int x = reply->request().attribute((QNetworkRequest::Attribute)(QNetworkRequest::User)).toInt();
    int y = reply->request().attribute((QNetworkRequest::Attribute)(QNetworkRequest::User+1)).toInt();
    int z = reply->request().attribute((QNetworkRequest::Attribute)(QNetworkRequest::User+2)).toInt();

    emit dataReady(x,y,z,arr);

    reply->deleteLater();
}

void QTileDownloader::downloadTile(int x, int y, int z)
{
    QNetworkRequest *pNetRequest = new QNetworkRequest(m_pcProvider->urlForTile(x,y,z));
    pNetRequest->setHeader(QNetworkRequest::UserAgentHeader, QVariant::fromValue<QString>("Katsimula.dev/0.1"));
    pNetRequest->setAttribute((QNetworkRequest::Attribute)(QNetworkRequest::User), x);
    pNetRequest->setAttribute((QNetworkRequest::Attribute)(QNetworkRequest::User+1), y);
    pNetRequest->setAttribute((QNetworkRequest::Attribute)(QNetworkRequest::User+2), z);

    m_pcNAM->get(*pNetRequest);
}

//QString QTileDownloader::quadKey(int x, int y, int z)
//{
//    QString str = "";
//    for(int i=z;i>0;i--) {
//        char digit='0';
//        int mask=1<<(i-1);
//        if((x&mask)!=0) {
//            digit++;
//        }
//        if((y&mask)!=0) {
//            digit++;
//            digit++;
//        }
//        str += digit;
//    }
//    return str;
//}
