#ifndef QTILEDOWNLOADER_H
#define QTILEDOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include "mapProviders/providers/mapprovider.h"

/**
 * @brief
 *
 */
class QTileDownloader : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit QTileDownloader(MapProvider *provider, QObject *parent = 0);
    /**
     * @brief
     *
     */
    ~QTileDownloader();

signals:
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     * @param array
     */
    void dataReady(int x, int y, int z, const QByteArray &array);

    void ready(bool ready);

public slots:
    /**
     * @brief
     *
     * @param reply
     */
    void networkRequestCompleted(QNetworkReply *reply);
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     */
    void downloadTile(int x, int y, int z);

private:
    QNetworkAccessManager *m_pcNAM; /**< TODO */
    MapProvider *m_pcProvider;
};

#endif // QTILEDOWNLOADER_H
