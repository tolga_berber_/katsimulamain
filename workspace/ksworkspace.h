#ifndef KSWORKSPACE_H
#define KSWORKSPACE_H

#include <QObject>

#include "mainwindow.h"
#include "graphicObjects/qgraphicsmapview.h"
#include "graphicObjects/qgraphicsmapscene.h"
#include "widgets/kslayerwidget.h"
#include "mapProviders/qmapproviderstore.h"
#include "utilities/ksgeometryhelper.h"

#include "interactions/ksinteractionstore.h"

#include "propertyEditors/kspropertyeditorstore.h"

#include "storage/ksproject.h"

#include "db/ksstaticobjectstore.h"

/**
 * @brief Programın çalışma ortamı sınıfı.
 *
 * Program içindeki bütün görsellik ve dosyalama nesneleri bu sınıf üzerinden yönetilecektir. Bu sınıf direkt olarak oluşturulamaz onun yerine
 * @see KsWorkspace::SharedInstance() fonksiyonu kullanılmalıdır.
 *
 */
class KsWorkspace : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Bütün uygulamada paylaşılacak olan nesneyi döndürür. Eğer nesne daha önce oluşturulmamışsa bu fonksiyon ilk kez çağırıldığında oluşacaktır.
     *
     * NOT: Bu sınıf başka threadlerden güvenli olarak çağırılabilir.
     *
     * @return KsWorkspace Bütün uygulama için kullanılacak çalışma alanı nesnesi.
     */
    static KsWorkspace *SharedInstance();

    /**
     * @brief Çalışma alanına ait ana ekran nesnesini döndürür.
     *
     * @return MainWindow Çalışma alanına ait ana ekran nesnesi.
     */
    MainWindow *mainWindow();

    /**
     * @brief Katmanları yöneten dock widget'ı döndüren fonksiyon.
     *
     * @return KsLayerWidget Katman Yönetim Nesnesi
     */
    KsLayerWidget *layerManager();

    /**
     * @brief Ana ekranda görünen harita görünümünü döndürür.
     *
     * @return QGraphicsMapView Ana ekranda görünen harita görünümü nesnesi.
     */
    QGraphicsMapView *mapView();

    /**
     * @brief Harita sahnesini öğrenmek için kullanılır.
     *
     * @return QGraphicsMapScene Aktif harita sahnesi.
     */
    QGraphicsMapScene *mapScene();

    /**
     * @brief Çalışma alanını geri döndürmek için kullanılır.
     *
     */
    void restoreWorkspace();

    void saveWorkspace();

    QMapProviderStore *mapProviders();

    KsGeometryHelper *geometryHelper();

    KsInteractionStore *interactionStore();

    KsPropertyEditorStore *propertyEditorStore();

    KsProject *currentProject();

    KsStaticObjectStore *staticDatabase();

    bool newProject();

    bool openProject(QString projectPath);

    QString getDockStyleSheet();

    QString getTabBarStyleSheet();

private:
    static KsWorkspace *s_pcWorkspace; /**< Statik sınıf örneği */
    MainWindow *m_pcMainWindow; /**< Çalışma Alanındaki ana ekran nesnesi */
    QGraphicsMapView *m_pcView; /**< Ana ekranda görüntülenecek olan harita görünüm nesnesi */
    KsLayerWidget *m_pcLayerWidget; /**< Katman yönetim panelini tutan nesne */
    QMapProviderStore *m_pcMapProviders;
    KsGeometryHelper *m_pcGeometryHelper;
    KsInteractionStore *m_pcInteractionStore;
    KsPropertyEditorStore *m_pcPropertyEditorStore;
    KsProject *m_pcProject;
    KsStaticObjectStore *m_pcStaticDB;

    /**
     * @brief Sınıf ilklendiricisi. Bu sınıf direkt olarak ilklendirilemez, onun yerine @see SharedInstance() fonksiyonu kullanılmalıdır.
     *
     * @param parent Üst sınıfa ait nesne, NULL olarak başlayacaktır.
     */
    explicit KsWorkspace(QObject *parent = 0);

    QString getStyleSheetFile(QString fileName);

};

#endif // KSWORKSPACE_H
