#ifndef KSDIRECTORYMANAGER_H
#define KSDIRECTORYMANAGER_H

#include <QObject>
#include <QHash>
#include <QDir>
#include <QSqlDatabase>

/**
 * @brief
 *
 */
typedef QHash<QString, QString> DirectoryMap;

/**
 * @brief
 *
 */
class KsDirectoryManager : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @return KsDirectoryManager
     */
    static KsDirectoryManager *SharedInstance();

    /**
     * @brief
     *
     * @param dirAlias
     * @param relPath
     */
    void registerDirectory(QString dirAlias, QString relPath);
    /**
     * @brief
     *
     * @param dirAlias
     * @param file
     * @return QString
     */
    QString getPathForFile(QString dirAlias, QString file);

    QString getPathForRelPath(QString dirAlias, QString relDir);
    /**
     * @brief
     *
     * @param dirAlias
     * @param file
     * @param dbName
     * @return QSqlDatabase
     */
    QSqlDatabase openSqliteDatabase(QString dirAlias, QString file, QString dbName="");

signals:

public slots:

private:
    static KsDirectoryManager *s_pcInstance; /**< TODO */
    QString m_strBaseDirectory; /**< TODO */
    DirectoryMap m_cDirMap; /**< TODO */
    QDir m_cBaseDir; /**< TODO */

    /**
     * @brief
     *
     * @param parent
     */
    explicit KsDirectoryManager(QObject *parent = 0);
    /**
     * @brief
     *
     * @param alias
     */
    void ensureAlias(QString alias);
};

#endif // KSDIRECTORYMANAGER_H
