#include "ksworkspace.h"
#include <QMutex>

#include "Actions/qactionstore.h"

#include "storage/kssettingsstorage.h"

#include "ksdirectorymanager.h"

#include <QMessageBox>

#include <QTextStream>

KsWorkspace *KsWorkspace::s_pcWorkspace = NULL;

KsWorkspace *KsWorkspace::SharedInstance()
{
    static QMutex mutex;

    if(!s_pcWorkspace) {
        mutex.lock();

        if(!s_pcWorkspace) {
            s_pcWorkspace = new KsWorkspace();
        }

        mutex.unlock();
    }

    return s_pcWorkspace;
}

MainWindow *KsWorkspace::mainWindow()
{
    return this->m_pcMainWindow;
}

KsLayerWidget *KsWorkspace::layerManager()
{
    if(!m_pcLayerWidget) {
        m_pcLayerWidget = new KsLayerWidget(m_pcMainWindow);
    }
    return this->m_pcLayerWidget;
}

QGraphicsMapView *KsWorkspace::mapView()
{
    return this->m_pcView;
}

QGraphicsMapScene *KsWorkspace::mapScene()
{
    return qobject_cast<QGraphicsMapScene*>(this->m_pcView->scene());
}

void KsWorkspace::restoreWorkspace()
{
    m_pcView = new QGraphicsMapView(m_pcMainWindow);
    m_pcMainWindow->setCentralWidget(m_pcView);
    connect(mapScene(), SIGNAL(visiblePortionChanged(QRectF)), m_pcMainWindow, SLOT(setWorldRect(QRectF)));
    newProject();
    KsSettingsStorage::sharedInstance();
    // m_pcGraph = new KsTrafficGraph(this);
    //YAPILACAK Burada nesnelerin geri yükleme işi yapılmalı, şimdilik bütün dock widgetler görüntüleniyor...
    m_pcMainWindow->setMenuWidget(QActionStore::SharedInstanceForMainWindow(m_pcMainWindow)->getTabWidget(m_pcMainWindow));
    m_pcMainWindow->addDockWidget(Qt::LeftDockWidgetArea, layerManager());
    m_pcMainWindow->addDockWidget(Qt::LeftDockWidgetArea, m_pcPropertyEditorStore->getUI());
    connect(mapScene(), SIGNAL(layerItemSelected(QGraphicsMapLayer*,KsGraphicObject*)),
            m_pcPropertyEditorStore, SLOT(layerObjectSelected(QGraphicsMapLayer*,KsGraphicObject*)));
    connect(mapScene(), SIGNAL(layerItemDeSelected(QGraphicsMapLayer*,KsGraphicObject*)),
            m_pcPropertyEditorStore, SLOT(layerObjectDeselected(QGraphicsMapLayer*,KsGraphicObject*)));
    mapScene()->setupLayers();
    mapScene()->setActiveInteractionObject(m_pcInteractionStore->interaction(KsInteractionStore::ITDefaultInteraction));
    QVariant scene = KsSettingsStorage::sharedInstance()->readSetting("scene.lastRect");
    if(scene.isValid()) {
        QRectF sceneRect = scene.toRectF();
        int zoomLevel = KsSettingsStorage::sharedInstance()->readSetting("scene.lastZoomLevel").toInt();
        int scaleLevel = KsSettingsStorage::sharedInstance()->readSetting("scene.lastScaleLevel").toInt();
        this->mapScene()->setVisiblePortion(sceneRect, zoomLevel, scaleLevel, 0);
    }
}

void KsWorkspace::saveWorkspace()
{
    QVariant lastRect = mapScene()->visiblePortion(0);
    QVariant zoomLevel = mapScene()->worldItem()->zoomLevel();
    QVariant scaleLevel = mapScene()->worldItem()->scaleLevel();
    KsSettingsStorage::sharedInstance()->saveSetting("scene.lastRect", lastRect);
    KsSettingsStorage::sharedInstance()->saveSetting("scene.lastZoomLevel", zoomLevel);
    KsSettingsStorage::sharedInstance()->saveSetting("scene.lastScaleLevel", scaleLevel);
}

QMapProviderStore *KsWorkspace::mapProviders()
{
    return m_pcMapProviders;
}

KsGeometryHelper *KsWorkspace::geometryHelper()
{
    return m_pcGeometryHelper;
}

KsInteractionStore *KsWorkspace::interactionStore()
{
    return m_pcInteractionStore;
}

KsPropertyEditorStore *KsWorkspace::propertyEditorStore()
{
    return m_pcPropertyEditorStore;
}

KsProject *KsWorkspace::currentProject()
{
    return m_pcProject;
}

KsStaticObjectStore *KsWorkspace::staticDatabase()
{
    return m_pcStaticDB;
}

bool KsWorkspace::newProject()
{
    if(m_pcProject!=NULL) {
        if(m_pcProject->needSave()) {
            QMessageBox box(m_pcMainWindow);
            box.setWindowTitle(tr("Uyarı"));
            box.setText(tr("Projede kaydedilmemiş değişikler var!"));
            box.setInformativeText(tr("Bu değişiklikler kaydedilsin mi?"));
            box.setIcon(QMessageBox::Question);
            box.setStandardButtons(QMessageBox::Save|QMessageBox::Cancel|QMessageBox::Discard);
            box.setButtonText(QMessageBox::Save, tr("Kaydet"));
            box.setButtonText(QMessageBox::Discard, tr("Kaydetme"));
            box.setButtonText(QMessageBox::Cancel, tr("İptal"));

            int answer = box.exec();

            if(answer==QMessageBox::Save) {
                m_pcProject->save();
            } else if(answer==QMessageBox::Cancel) {
                return false;
            }
        }
        delete m_pcProject;
    }

    m_pcProject = new KsProject(this);
    m_pcProject->createProject();

    return true;
}

bool KsWorkspace::openProject(QString projectPath)
{
    if(m_pcProject!=NULL) {
        if(m_pcProject->needSave()) {
            QMessageBox box(m_pcMainWindow);
            box.setWindowTitle(tr("Uyarı"));
            box.setText(tr("Projede kaydedilmemiş değişikler var!"));
            box.setInformativeText(tr("Bu değişiklikler kaydedilsin mi?"));
            box.setIcon(QMessageBox::Question);
            box.setStandardButtons(QMessageBox::Save|QMessageBox::Cancel|QMessageBox::Discard);
            box.setButtonText(QMessageBox::Save, tr("Kaydet"));
            box.setButtonText(QMessageBox::Discard, tr("Kaydetme"));
            box.setButtonText(QMessageBox::Cancel, tr("İptal"));

            int answer = box.exec();

            if(answer==QMessageBox::Save) {
                m_pcProject->save();
            } else if(answer==QMessageBox::Cancel) {
                return false;
            }
        }
        delete m_pcProject;
    }

    m_pcProject = new KsProject(this);
    QStringList pathElements = projectPath.split('/');
    QString projectName = pathElements.last();
    m_pcProject->setProjectName(projectName);
    m_pcProject->setProjectPath(projectPath);
    m_pcProject->openProject();

    return true;
}

QString KsWorkspace::getDockStyleSheet()
{
    return getStyleSheetFile(":/stylesheet/dock.qss");
}

QString KsWorkspace::getTabBarStyleSheet()
{
    return getStyleSheetFile(":/stylesheet/docktab.qss");
}

KsWorkspace::KsWorkspace(QObject *parent) :
    QObject(parent),
    m_pcLayerWidget(NULL),
    m_pcProject(NULL)
{
    m_pcMapProviders = QMapProviderStore::sharedInstance();
    m_pcGeometryHelper = KsGeometryHelper::sharedInstance();
    m_pcInteractionStore = KsInteractionStore::sharedInstance();
    m_pcPropertyEditorStore = KsPropertyEditorStore::sharedInstance();
    m_pcMainWindow = new MainWindow();
    KsDirectoryManager::SharedInstance()->registerDirectory("staticDb","staticData");
    m_pcStaticDB = new KsStaticObjectStore("staticDb",
                                           KsDirectoryManager::SharedInstance()->getPathForFile("staticDb", "staticdata.ksdf"));
    m_pcStaticDB->open();
}

QString KsWorkspace::getStyleSheetFile(QString fileName)
{
    QString result;
    QFile stylesheet(fileName);

    if(stylesheet.open(QIODevice::ReadOnly)) {
        QTextStream str(&stylesheet);
        result = str.readAll();
        stylesheet.close();
    }

    return result;
}
