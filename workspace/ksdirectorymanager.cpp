#include "ksdirectorymanager.h"
#include <QMutex>
#include <QStandardPaths>
#include "utilities/ksdbutilities.h"

KsDirectoryManager *KsDirectoryManager::s_pcInstance=NULL;

KsDirectoryManager *KsDirectoryManager::SharedInstance()
{
    static QMutex mutex;

    if(s_pcInstance==NULL) {
        mutex.lock();

        if(s_pcInstance==NULL) {
            s_pcInstance = new KsDirectoryManager();
        }

        mutex.unlock();
    }

    return s_pcInstance;
}

void KsDirectoryManager::registerDirectory(QString dirAlias, QString relPath)
{
    m_cDirMap[dirAlias] = relPath;
    QDir tmp(m_strBaseDirectory+"/"+relPath);
    if(!tmp.exists()) {
        tmp.mkpath(m_strBaseDirectory+"/"+relPath);
    }
}

QString KsDirectoryManager::getPathForFile(QString dirAlias, QString file)
{
    ensureAlias(dirAlias);
    QString dPath = m_strBaseDirectory+"/"+m_cDirMap[dirAlias]+"/"+file;
    return dPath;
}

QString KsDirectoryManager::getPathForRelPath(QString dirAlias, QString relDir)
{
    ensureAlias(dirAlias);
    QString dPath = m_strBaseDirectory+"/"+m_cDirMap[dirAlias]+"/"+relDir;
    QDir d;
    d.mkpath(dPath);
    return dPath;
}

QSqlDatabase KsDirectoryManager::openSqliteDatabase(QString dirAlias, QString file, QString dbName)
{
    // QString fP = getPathForFile(dirAlias, file);
    return KsDbUtilities::openSqliteDatabase(m_strBaseDirectory+"/"+m_cDirMap[dirAlias], file, dbName);
    //QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", dbName);
    //db.setDatabaseName(fP);
    //if(!db.open()) {
    //}
    //return db;
}

KsDirectoryManager::KsDirectoryManager(QObject *parent) :
    QObject(parent)
{
    m_strBaseDirectory = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    m_cBaseDir.setPath(m_strBaseDirectory);
    if(!m_cBaseDir.exists()) {
        m_cBaseDir.mkpath(m_strBaseDirectory);
    }
    m_cDirMap["CACHE"] = m_strBaseDirectory;
}

void KsDirectoryManager::ensureAlias(QString alias)
{
    if(!m_cDirMap.contains(alias)) {
        registerDirectory(alias, alias);
    }
}
