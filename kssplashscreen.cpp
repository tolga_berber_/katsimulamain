#include "kssplashscreen.h"
//#include "mapProviders/qmapproviderstore.h"
#include <QApplication>
#include "workspace/ksworkspace.h"
//#include "utilities/ksgeometryhelper.h"
#include <QThread>

KsSplashScreen::KsSplashScreen(QWidget *parent) :
    QSplashScreen(parent)
{
    QPixmap pxMap(":/img/splash.png");
    setPixmap(pxMap);
    qApp->setApplicationName(tr("KaTSimula"));
    qApp->setApplicationVersion(tr("0.1"));
    qApp->setApplicationDisplayName(tr("KaTSimula: Karayolu Trafik Simülasyonu"));
}

void KsSplashScreen::startLoading()
{
    showMessage(tr("Harita Sağlayıcılar Hazırlanıyor..."));
    //qApp->processEvents();
    //QMapProviderStore::SharedInstance();
    //KsGeometryHelper::SharedInstance();
    qApp->processEvents();
    KsWorkspace::SharedInstance()->mainWindow()->showMaximized();
    KsWorkspace::SharedInstance()->restoreWorkspace();
    finish(KsWorkspace::SharedInstance()->mainWindow());
}
