#ifndef QTILEDATABASE_H
#define QTILEDATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QImage>
#include <QMutex>

#include "mapProviders/providers/mapprovider.h"

/**
 * @brief
 *
 */
class QTileDatabase : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit QTileDatabase(MapProvider *provider, QObject *parent = 0);
    /**
     * @brief
     *
     */
    virtual ~QTileDatabase();
    /**
     * @brief
     *
     * @return bool
     */
    bool ready() {
        QMutexLocker locker(&m_cMutex);
        return m_bReady;
    }

    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     * @return bool
     */
    bool isImageReady(int x, int y, int z);

signals:
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     * @param img
     */
    void cacheImage(int x, int y, int z, const QImage &img);
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     */
    void imageDoesNotExists(int x, int y, int z);


public slots:
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     * @param arr
     */
    void saveTile(int x, int y, int z, const QByteArray &arr);
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     */
    void getTile(int x, int y, int z);

    void clearCache();

private:
    QMutex m_cMutex; /**< TODO */
    QSqlDatabase m_cDB; /**< TODO */
    volatile bool m_bReady; /**< TODO */
    MapProvider *m_pcProvider;

    /**
     * @brief
     *
     */
    void checkDB();
};

#endif // QTILEDATABASE_H
