#include "qtiledatabase.h"

#include <QDir>
#include <QStandardPaths>
#include <QSqlQuery>
#include <QSqlError>
#include <QVariant>

#include "workspace/ksdirectorymanager.h"

QTileDatabase::QTileDatabase(MapProvider *provider, QObject *parent) :
    QObject(parent),
    m_pcProvider(provider)
{
    m_pcProvider->setDBReady(false);

    KsDirectoryManager::SharedInstance()->registerDirectory(provider->providerID()+"_TDB", tr("cache/%1").arg(m_pcProvider->providerID()));

    m_cDB = KsDirectoryManager::SharedInstance()->openSqliteDatabase(provider->providerID()+"_TDB", provider->providerID()+".mbtiles", provider->providerID());
    checkDB();
}

QTileDatabase::~QTileDatabase()
{
    m_cDB.close();
}

bool QTileDatabase::isImageReady(int x, int y, int z)
{
    QString str = tr("select count(tile_data) as rs from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3").arg(z).arg(x).arg(y);
    QSqlQuery q(str, m_cDB);

    if(!q.exec()) {
        return false;
    }

    while(q.next()) {
        if(!q.isValid()) {
            return false;
        }
        int count = q.value("rs").toInt();
        if(count)
            return true;
    }



    return false;
}

void QTileDatabase::saveTile(int x, int y, int z, const QByteArray &arr)
{
    if(isImageReady(x,y,z)) {
       return;
    }
    QSqlQuery q("insert into tiles (zoom_level, tile_column, tile_row, tile_data) values (?, ?, ?, ?)", m_cDB);

    q.addBindValue(z);
    q.addBindValue(x);
    q.addBindValue(y);
    q.addBindValue(arr);

    if(!q.exec()) {
    }

}

void QTileDatabase::getTile(int x, int y, int z)
{
    if(!isImageReady(x,y,z)) {
        emit imageDoesNotExists(x,y,z);
        return;
    }

    QString str = tr("select tile_data from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3").arg(z).arg(x).arg(y);
    QSqlQuery q(str, m_cDB);
    q.next();

    QByteArray arr = q.value("tile_data").toByteArray();

    QImage image;
    image.loadFromData(arr);
    emit cacheImage(x,y,z,image);
}

void QTileDatabase::clearCache()
{
    QString str = "delete from tiles";
    QSqlQuery q(str, m_cDB);
    q.exec();
}

void QTileDatabase::checkDB()
{
    if(!m_cDB.tables().contains("metadata")) {
        QSqlQuery q(m_cDB);

        if(q.exec("CREATE TABLE metadata (name text, value text);")) {
            QSqlQuery metaQ(m_cDB);
            metaQ.prepare("insert into metadata (name, value) values (?, ?)");
            QVariantList names;
            names << "name"<<"type"<<"version"<<"description"<<"format";
            metaQ.addBindValue(names);

            QVariantList values;
            values<<"cacheTiles"<<"overlay"<<"1.0.0"<<"Cache Tiles For The Katsimula Application"<<"jpg";
            metaQ.addBindValue(values);

            if(!metaQ.execBatch()) {
                return;
            }
        } else {
            return;
        }
    }

    if(!m_cDB.tables().contains("tiles")) {
        QSqlQuery q(m_cDB);

        if(!q.exec("CREATE TABLE tiles (zoom_level integer, tile_column integer, tile_row integer, tile_data blob);")) {
            return;
        }
    }
    m_pcProvider->setDBReady(true);
}
