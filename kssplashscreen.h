#ifndef KSSPLASHSCREEN_H
#define KSSPLASHSCREEN_H

#include <QSplashScreen>
#include <QMainWindow>

/**
 * @brief Uygulama açılış ekranı sınıfı. Bu sınıf uygulama açılırken ekranda resim görüntülenmesini ve ilklendirme işlemlerini gerçekleştirir.
 *
 */
class KsSplashScreen : public QSplashScreen
{
    Q_OBJECT
public:
    /**
     * @brief Sınıf ilklendiricisi. Uygulamanın adı, şirket adı gibi değişkenleri ayarlar.
     *
     * @param parent Göz ardı edilir, Qt uyumluluğu için konulmuştur.
     */
    explicit KsSplashScreen(QWidget *parent = 0);

signals:

public slots:

    /**
     * @brief Yükleme işlemini başlatan fonksiyondur.
     *
     *        Önbellek thread'ini başlatır ve bitimini @see cacheReady() fonksiyonuna bağlar.
     *
     */
    void startLoading();

};

#endif // KSSPLASHSCREEN_H
