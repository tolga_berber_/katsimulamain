#ifndef KSGRAPHICOBJECT_H
#define KSGRAPHICOBJECT_H

#include <QGraphicsObject>
#include "qgraphicsmaplayer.h"
#include <QMenu>

class QGraphicsMapLayer;

/**
 * @brief Katmanların içerisine yerleştirilebilecek grafik nesnelerinin ata sınıfı.
 *
 */
class KsGraphicObject : public QGraphicsObject
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici, herhangi bir işlem yapmamaktadır.
     *
     * @param parent Qt uyumluluğu için konulmuştur.
     */
    explicit KsGraphicObject(QGraphicsItem *parent = 0);

    /**
     * @brief Grafik nesnesinin ait olduğu katman.
     *
     * @return const QGraphicsMapLayer Nesnenin ait olduğu grafik katmanı.
     */
    QGraphicsMapLayer *layer();

signals:
    void itemSelectedChanged(KsGraphicObject *obj, bool selected);
    void itemPositionChanged(KsGraphicObject *obj, QPointF newLocation);

public slots:
    /**
     * @brief Nesnenin ait olduğu grafik katmanını değiştirmek için kullanılan fonksiyon.
     *
     * @param layer Yeni katman nesnesi.
     */
    void setLayer(QGraphicsMapLayer *layer);


private:
    QGraphicsMapLayer *m_pcLayer; /**< Ait olunan katmanı tutan değişken */

private:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

protected:
    virtual bool hasContextMenu();
    virtual QMenu *getPopupMenu();

    virtual void beforeItemSelect(uint &newValue);
    virtual void afterItemSelected(uint newValue);

    virtual void beforePositionChange(QPointF &newLocation);
    virtual void afterPositionChanged(QPointF newLocation);
    virtual void afterScenePositionChanged(QPointF newLocation);

    virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *evt);

};

#endif // KSGRAPHICOBJECT_H
