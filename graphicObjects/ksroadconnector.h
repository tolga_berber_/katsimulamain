#ifndef KSROADCONNECTOR_H
#define KSROADCONNECTOR_H

#include "ksgraphicobject.h"

// #include "graph/kstrafficvertex.h"

class KsRoadConnector : public KsGraphicObject
{
    Q_OBJECT
public:
    explicit KsRoadConnector(QGraphicsItem *parent = 0);

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

signals:
    // void vertexChanged(KsTrafficVertex *newVertex);

public slots:
    //void setVertex(KsTrafficVertex *newVertex);

private:
    //KsTrafficVertex *m_ptrVertex;

};

#endif // KSROADCONNECTOR_H
