#include "ksgraphicnode.h"

#include "db/model/project/ksnodeobject.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>

KsGraphicNode::KsGraphicNode(QPointF pnt, QGraphicsItem *parent) :
    KsGraphicObject(parent)
{
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
    m_cOrigin.rx() = pnt.x()-0.0001;
    m_cOrigin.ry() = pnt.y()-0.0001;

    connect(this,SIGNAL(itemPositionChanged(KsGraphicObject*,QPointF)), this,SLOT(updateModelPos(KsGraphicObject*,QPointF)));
}

KsGraphicNode::KsGraphicNode(KsNodeObject *node, QGraphicsItem *parent) :
    KsGraphicObject(parent), m_ptrModel(NULL)
{
    setFlag(QGraphicsItem::ItemSendsScenePositionChanges);
    setModel(node);
    connect(this,SIGNAL(itemPositionChanged(KsGraphicObject*,QPointF)), this,SLOT(updateModelPos(KsGraphicObject*,QPointF)));
}

KsGraphicNode::~KsGraphicNode()
{
}

KsNodeObject *KsGraphicNode::model()
{
    return m_ptrModel;
}

QPointF KsGraphicNode::location()
{
    return m_ptrModel->location();
}

QRectF KsGraphicNode::boundingRect() const
{

    return QRectF(m_cOrigin,QSizeF(0.0002,0.0002));
}

void KsGraphicNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(m_ptrModel->nodeType()==KsNodeObject::NTSource) {
        QImage img(":/img/action/draw/ADDNODE.png");
        painter->drawImage(boundingRect(),img);
    } else {
        QPen p(Qt::black);
        QBrush b(Qt::gray);
        painter->save();
        painter->setPen(p);
        painter->setBrush(b);
        painter->drawEllipse(boundingRect());
        painter->restore();
    }
}

void KsGraphicNode::setModel(KsNodeObject *model)
{
    if(m_ptrModel!=model) {
        m_ptrModel = model;
        m_cOrigin = m_ptrModel->location()-QPointF(0.0001, 0.0001);
        setObjectName(model->name());
        connect(model, SIGNAL(nameChanged(QString)), this, SLOT(updateObjectName(QString)));
        setParent(m_ptrModel);
        emit modelChanged(m_ptrModel);
    }
}

void KsGraphicNode::setLocation(QPointF pos)
{
    m_ptrModel->setLocation(pos+QPointF(0.0001, 0.0001));
}

void KsGraphicNode::updateModelPos(KsGraphicObject *obj, QPointF pnt)
{
    Q_UNUSED(obj);
    m_ptrModel->setLocation(m_cOrigin+pnt+QPointF(0.0001, 0.0001));
}

void KsGraphicNode::updateObjectName(QString str)
{
    setObjectName(str);
}
