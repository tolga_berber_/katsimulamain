#ifndef QGRAPHICSMAPVIEW_H
#define QGRAPHICSMAPVIEW_H

#include <QGraphicsView>
#include "qgraphicsmapscene.h"

/**
 * @brief
 *
 */
class QGraphicsMapView : public QGraphicsView
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param parent
     */
    explicit QGraphicsMapView(QWidget *parent = 0);

    void scaleXY(qreal sx, qreal sy);

signals:
    /**
     * @brief
     *
     */
    void contentScrolled();

    void contentScaled();

public slots:

    void zoomIn();
    void zoomOut();

protected:
    /**
     * @brief
     *
     * @param dx
     * @param dy
     */
    virtual void scrollContentsBy(int dx, int dy);


private:
    QGraphicsMapScene *m_pcMapScene; /**< TODO */

};

#endif // QGRAPHICSMAPVIEW_H
