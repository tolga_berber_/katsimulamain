#ifndef QMAPLAYERMODEL_H
#define QMAPLAYERMODEL_H

#include <QAbstractItemModel>
#include "qgraphicsmaplayer.h"

/**
 * @brief Layer nesnelerini treeviewda göstermek için kullanılacak sınıf.
 *
 * Bu sınıf sahnedeki grafik katmanlarını ve nesnelerini hiyerarşik bir yapıda göstermek amacıyla geliştirilmiştir.
 *
 */
class QMapLayerModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici, temel olarak hiçbir şey yapmamaktadır. Sadece Qt uyumluluğu için konulmuştur.
     *
     * @param parent Göz ardı edilebilir, Qt uyumluluğu için kullanılmaktadır.
     */
    explicit QMapLayerModel(QObject *parent = 0);

    QModelIndex getModelIndex(QGraphicsMapLayer *layer, KsGraphicObject *obj);

signals:

public slots:
    void objectAddedToLayer(QGraphicsMapLayer *layer, KsGraphicObject *obj);
    void objectRemovedFromLayer(QGraphicsMapLayer *layer, int idx);

private:
    /**
     * @brief Belirtilen üst index için belirtilen satır ve sütundaki indexi öğrenmek için kullanılacak fonksiyon. Ayrıntılı bilgi için Qt'nin Model/View framework'üne bakılabilir.
     *
     * @param row Satır numarası
     * @param column Sütun Numarası
     * @param parent Üst index
     * @return QModelIndex Oluşturulması istenilen index
     */
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const;

    /**
     * @brief Bir çocuk indexin ait olduğu üst indexi öğrenmek amacıyla kullanılan fonksiyondur.
     *
     * @param child Üst indexi öğrenilmek istenen grafik nesnesi.
     * @return QModelIndex Üst index
     */
    virtual QModelIndex parent(const QModelIndex &child) const;

    /**
     * @brief Belirtilen indexteki veriyi (görsel veya metinsel) öğrenmek için kullanılacak fonksiyon.
     *
     * @param index Verisi öğrenilmek istenilen index.
     * @param role Veri türü
     * @return QVariant Gösterilecek veri
     */
    virtual QVariant data(const QModelIndex &index, int role) const;

    /**
     * @brief Modelin bağlanacağı görünüm nesnelerinin başlık bilgileri için kullanılacak fonksiyon.
     *
     * @param section Sütun numarası
     * @param orientation Yönlendirme
     * @param role Veri türü
     * @return QVariant Başlık verisi.
     */
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    /**
     * @brief Üst indexin içerdiği satır sayısını öğrenmek için kullanılacak fonksiyon.
     *
     * @param parent Üst index
     * @return int Satır sayısı
     */
    virtual int rowCount(const QModelIndex &parent) const;

    /**
     * @brief Üst indexin göstereceği verinin sütun sayısını öğrenmek için kullanılacak fonksiyon.
     *
     * @param parent Üst index
     * @return int Sütun sayısı
     */
    virtual int columnCount(const QModelIndex &parent) const;
};

#endif // QMAPLAYERMODEL_H
