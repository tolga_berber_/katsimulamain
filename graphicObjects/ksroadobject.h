#ifndef KSROADOBJECT_H
#define KSROADOBJECT_H

#include "ksgraphicobject.h"

class KsRoadModelObject;

class KsRoadObject : public KsGraphicObject
{
    Q_OBJECT
public:
    explicit KsRoadObject(QGraphicsItem *parent = 0);
    explicit KsRoadObject(KsRoadModelObject *model, QGraphicsItem *parent=0);

    QPolygonF roadNormal() const;

    virtual QRectF boundingRect() const;
    QPainterPath shape() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);
    int numberOfControlPoints();

    void updatePath();

    KsRoadModelObject *model();

    QPointF startPoint();
    QPointF endPoint();

//    virtual QtTreePropertyBrowser *getBrowser();
//    virtual void connectBrowser(QtTreePropertyBrowser *browser);
//    virtual void disconnectBrowser(QtTreePropertyBrowser *browser);

signals:
    void modelChanged(KsRoadModelObject *model);

public slots:
    void addPointToRoadNormal(QPointF normalPoint);
    void setNormalPoint(int index, QPointF point);
    void removeNormalPoint(int index);
    void updateRoadGeometry();
    void setInitialMode(bool mode);

    void setModel(KsRoadModelObject *model);
    void updateRoadNormal(QPolygonF pol);

    // Popup events
    void handlePopupActions(QAction *act);

private:
    QMenu *m_ptrMenu;
    bool m_bInitialMode;
    QPolygonF *m_arrRoadLines;
    QPainterPath m_pathBoundary;

    QPolygonF m_tempRoadNormal;
    QPolygonF m_tempBezier;

    KsRoadModelObject *m_ptrModel;

    QColor m_colDensity;
    int numOfLanes();

protected:
    virtual bool hasContextMenu();
    virtual QMenu *getPopupMenu();
    virtual void afterItemSelected(uint newValue);
};

#endif // KSROADOBJECT_H
