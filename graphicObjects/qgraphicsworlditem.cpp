#include "qgraphicsworlditem.h"
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <QStyleOptionGraphicsItem>
#include <QPainter>

#include "qgraphicsmapscene.h"
#include "workspace/ksworkspace.h"


#define W_LEFT    (-180)
#define W_WIDTH   (360)
#define W_TOP     (-85.0511287798)
#define W_HEIGHT  (170.1022575596)
#define W_MAXZOOMLEVEL  18
#define W_MINZOOMLEVEL  0

#define W_MINSCALELEVEL 0

QGraphicsWorldItem::QGraphicsWorldItem(QGraphicsItem *parent) :
    KsGraphicObject(parent),
    m_iZoom(0),
    m_iScale(0),
    m_fLatitude(0),
    m_fLongitude(0),
    m_bRetilingEnabled(true)
{
    m_afScales[0] = 1;
    m_aiZoom[0] = 0;
    for(int i=1;i<W_MAXSCALELEVEL+1;i++) {
        m_afScales[i] = m_afScales[i-1]*2;
        m_aiZoom[i] = qMin(KsWorkspace::SharedInstance()->mapProviders()->maxZoomLevel(),i);
    }

    for(int i=0;i<=KsWorkspace::SharedInstance()->mapProviders()->maxZoomLevel();i++) {
        m_cZoomLevels.append(ZoomLevelTileHash());
    }

    initWorld();

    connect(this, SIGNAL(xChanged()), this, SLOT(posChanged()));
    connect(this, SIGNAL(xChanged()), this, SLOT(posChanged()));
    connect(KsWorkspace::SharedInstance()->mapProviders(), SIGNAL(activeProviderChanged()), this, SLOT(providerSwitch()));

    setObjectName(tr("Dünya Haritası"));

}

QRectF QGraphicsWorldItem::boundingRect() const
{
    return QRectF(W_LEFT, W_TOP, W_WIDTH, W_HEIGHT);
}

void QGraphicsWorldItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF QGraphicsWorldItem::visibleWorldPart(int idx)
{
    QGraphicsMapScene *mScene = (QGraphicsMapScene*)scene();
    return mapFromScene(mScene->visiblePortion(idx)).boundingRect().intersected(boundingRect());
}

/*
QtTreePropertyBrowser *QGraphicsWorldItem::getBrowser()
{
    QRectF rect = visibleWorldPart(0);
    QtPointFPropertyManager *pointFManager = new QtPointFPropertyManager;
    QtProperty *pointFProperty = pointFManager->addProperty(tr("Sol Üst Köşe Koordinatı"));
    pointFProperty->setToolTip(tr("Haritanın Sol Üst Koordinatı"));
    pointFProperty->setEnabled(false);
    pointFManager->setValue(pointFProperty, rect.topLeft());

    QtSizeFPropertyManager *sizeFManager = new QtSizeFPropertyManager;
    QtProperty *coordinates = sizeFManager->addProperty(tr("Görünen Bölüm"));
    coordinates->setToolTip(tr("Görünen Bölge Büyüklüğü"));
    coordinates->setEnabled(false);
    sizeFManager->setValue(coordinates, rect.size());

    QtDoubleSpinBoxFactory *spinFactory = new QtDoubleSpinBoxFactory;

    QtTreePropertyBrowser *browser = new QtTreePropertyBrowser;

    browser->setFactoryForManager(pointFManager->subDoublePropertyManager(), spinFactory);
    browser->setFactoryForManager(sizeFManager->subDoublePropertyManager(), spinFactory);

    browser->addProperty(pointFProperty);
    browser->addProperty(coordinates);

    return browser;
}

void QGraphicsWorldItem::connectBrowser(QtTreePropertyBrowser *browser)
{
    Q_UNUSED(browser);
    if(m_ptrPropertyBrowser!=browser) {
        m_ptrPropertyBrowser = browser;
    }
}

void QGraphicsWorldItem::disconnectBrowser(QtTreePropertyBrowser *browser)
{
    Q_UNUSED(browser)
    m_ptrPropertyBrowser = NULL;
}
*/

void QGraphicsWorldItem::setZoomLevel(int newZoomLevel)
{
    if(m_iZoom!=newZoomLevel) {
        if(!KsWorkspace::SharedInstance()->mapProviders()->activeProvider()->isValidZoomLevel(newZoomLevel)) {
        } else {
            m_iZoom = newZoomLevel;
            reTile();
            emit zoomLevelChanged(m_iZoom);
        }
    }
}

void QGraphicsWorldItem::setScaleLevel(int newScaleLevel)
{
    if(m_iScale!=newScaleLevel) {
        if(newScaleLevel>W_MAXSCALELEVEL || newScaleLevel<W_MINSCALELEVEL) {
        } else {
            m_iScale = newScaleLevel;
            setZoomLevel(m_aiZoom[m_iScale]);
            emit scaleLevelChanged(m_iScale);
        }
    }
}

void QGraphicsWorldItem::setLatitude(qreal newLatitude)
{
    if(m_fLatitude!=newLatitude) {
        if(newLatitude<W_TOP || newLatitude>(W_TOP+W_HEIGHT)) {
        } else {
            m_fLatitude = newLatitude;
            emit latitudeChanged(m_fLatitude);
        }
    }
}

void QGraphicsWorldItem::setLongitude(qreal newLongitude)
{
    if(m_fLongitude!=newLongitude) {
        if(newLongitude<W_LEFT || newLongitude>(W_LEFT+W_WIDTH)) {
        } else {
            m_fLongitude = newLongitude;
            emit latitudeChanged(m_fLongitude);
        }
    }
}

void QGraphicsWorldItem::zoomIn()
{
    // QGraphicsMapScene *oScene = (QGraphicsMapScene*)scene();
    // setTransformOriginPoint(mapFromScene(oScene->zoomCenter()));
    setScaleLevel(m_iScale+1);
    scene()->setSceneRect(mapToScene(boundingRect()).boundingRect());
}

void QGraphicsWorldItem::zoomOut()
{
    setScaleLevel(m_iScale-1);
    scene()->setSceneRect(mapToScene(boundingRect()).boundingRect());
}

void QGraphicsWorldItem::posChanged()
{
}

void QGraphicsWorldItem::initWorld()
{
    m_cZoomLevels[0][MAKE_TILE_COORDINATE(0,0)] = new QGraphicsTileItem(this, boundingRect(),0,0,0,this);
}

void QGraphicsWorldItem::reTile()
{
    if(!m_bRetilingEnabled) {
        return;
    }

    QRectF visiblePart = visibleWorldPart(0);
    QRect r = worldToTile(visiblePart);
    for(int x=r.left();x<=r.right();x++) {
        for(int y=r.top();y<=r.bottom();y++) {
            if(!m_cZoomLevels[m_iZoom].contains(MAKE_TILE_COORDINATE(x,y))) {
                m_cZoomLevels[m_iZoom].insert(MAKE_TILE_COORDINATE(x,y), new QGraphicsTileItem(this, tileRect(QPoint(x,y)),x,y,m_iZoom,this) );
            }
        }
    }
}

void QGraphicsWorldItem::setRetilingEnabled(bool newValue)
{
    if(m_bRetilingEnabled!=newValue) {
        m_bRetilingEnabled = newValue;
    }
}

void QGraphicsWorldItem::providerSwitch()
{
    foreach(ZoomLevelTileHash hash, m_cZoomLevels) {
        foreach (QGraphicsItem *tile, hash.values()) {
            QGraphicsTileItem *pTile = (QGraphicsTileItem*)tile;
            pTile->clearImage();
        }
    }
}


QRect QGraphicsWorldItem::worldToTile(const QRectF rect)
{
    MapProvider *provider = KsWorkspace::SharedInstance()->mapProviders()->activeProvider();
    return provider->tileRectForWorldRect(rect, m_iZoom);
}

QRectF QGraphicsWorldItem::tileRect(const QPoint pnt)
{
    MapProvider *provider = KsWorkspace::SharedInstance()->mapProviders()->activeProvider();
    return provider->tileWorldRect(pnt, m_iZoom);
}

//void QGraphicsWorldItem::updatePropertyWindow()
//{
//    if(m_ptrPropertyBrowser) {
//        QList<QtProperty*> properties = m_ptrPropertyBrowser->properties();

//        QtPointFPropertyManager *pointMan = qobject_cast<QtPointFPropertyManager*>(properties[0]->propertyManager());
//        QtSizeFPropertyManager *sizeMan = qobject_cast<QtSizeFPropertyManager*>(properties[1]->propertyManager());

//        QRectF worldRect = visibleWorldPart(0);

//        pointMan->setValue(properties[0], worldRect.topLeft());
//        sizeMan->setValue(properties[1], worldRect.size());
//    }
//}


