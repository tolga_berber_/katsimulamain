#ifndef QGRAPHICSWORLDITEM_H
#define QGRAPHICSWORLDITEM_H

#include <QGraphicsObject>
#include <QGraphicsPixmapItem>
#include "tilestorage.h"

#include "ksgraphicobject.h"


#define W_MAXSCALELEVEL      20

/**
 * @brief Programda kullanılacak dünya nesnesi.
 *
 * Bu nesne sanal olarak dünya görüntüsü oluşturmak için kullanılmaktadır. Bu nesne parçalar halinde dünya görüntülerini göstermektedir.
 *
 */
class QGraphicsWorldItem : public KsGraphicObject
{
    Q_OBJECT
public:
    /**
     * @brief Sınıf ilklendiricisi.
     *
     * Mevcut harita sağlayıcıların yeteneklerini öğrenip gerekli yakınlaştırma parametrelerini ayarlar.
     *
     * @param parent Göz ardı edilebilir, Qt uyumluluğu için konulmuştur.
     */
    explicit QGraphicsWorldItem(QGraphicsItem *parent = 0);

    /**
     * @brief Dünya koordinatlarını verir.
     *
     * @return QRectF Qt koordinat sisteminde dünya koordinatları.
     */
    virtual QRectF boundingRect() const;

    /**
     * @brief Çizim işlemlerinin gerçekleştirildiği fonksiyon. Temel olara çizim işlemleri harita karelerinde gerçekleştirilir.
     *
     * @param painter Çizim nesnesi
     * @param option Çizim seçenekleri
     * @param widget Çizim yapılan görsel nesne
     */
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

    /**
     * @brief Aktif harita yakınlaşma seviyesini öğrenmek için kullanılacak fonksiyon.
     *
     * @return int Zoom seviyesi.
     */
    int zoomLevel() {
        return m_iZoom;
    }

    /**
     * @brief Görsel yakınlaşma seviyesini öğrenmek için kullanılacak fonksiyon. Gerçek ölçek 2^x olarak hesaplanmalıdır.
     *
     * @return int Görsel yakınlaştırma seviyesi
     */
    int scaleLevel() {
        return m_iScale;
    }

    /**
     * @brief Görünen kısmın orta noktasının latitude değerini öğrenmek için kullanılan fonksiyon.
     *
     * @return qreal Latitude değeri.
     */
    qreal latitude() {
        return m_fLatitude;
    }

    /**
     * @brief Görünen kısmın orta noktasının longitude değerini öğrenmek için kullanılan fonksiyon.
     *
     * @return qreal Longitude değeri.
     */
    qreal longitude() {
        return m_fLongitude;
    }

    /**
     * @brief Nesnenin belirtilen görünümdeki kısmının koordinatlarını öğrenmek için kullanılacak fonksiyon.
     *
     * @param idx Görüntü indexi.
     * @return QRectF Dünyanın görünen kısmı.
     */
    QRectF visibleWorldPart(int idx);

    bool retilingEnabled() {
        return m_bRetilingEnabled;
    }

    /*
    virtual QtTreePropertyBrowser *getBrowser();
    virtual void connectBrowser(QtTreePropertyBrowser *browser);
    virtual void disconnectBrowser(QtTreePropertyBrowser *browser);*/

signals:
    /**
     * @brief Harita yakınlaşma seviyesi değiştiğinde tetiklenen sinyal.
     *
     * @param zoomLevel Yeni harita yakınlaşma seviyesi.
     */
    void zoomLevelChanged(int zoomLevel);

    /**
     * @brief Harita latitude değeri değiştiğinde tetiklenen sinyal.
     *
     * @param zoomLevel Yeni latitude değeri.
     */
    void latitudeChanged(qreal latitude);

    /**
     * @brief Harita longitude değeri değiştiğinde tetiklenen sinyal.
     *
     * @param zoomLevel Yeni longitude değeri.
     */
    void longitudeChanged(qreal longitude);

    /**
     * @brief Görsel yakınlaşma seviyesi değiştiğinde tetiklenen sinyal.
     *
     * @param zoomLevel Yeni görsel yakınlaşma seviyesi.
     */
    void scaleLevelChanged(int scaleLevel);

public slots:
    /**
     * @brief Harita yakınlaşma değerini değiştirmek için kullanılacak fonksiyon. Eğer sağlayıcı yakınlaşma değerini desteklemiyorsa geçiş gerçekleşmeyecektir.
     *
     * @param newZoomLevel Yeni harita yakınlaşma değeri.
     */
    void setZoomLevel(int newZoomLevel);

    /**
     * @brief Görsel yakınlaşma değerini değiştirmek için kullanılacak fonksiyon.
     *
     * @param newZoomLevel Yeni görsel yakınlaşma değeri.
     */
    void setScaleLevel(int newScaleLevel);

    /**
     * @brief Haritanın latitude değerini değiştirmek için kullanılan fonksiyon.
     *
     * @param newLatitude Yeni latitude değeri.
     */
    void setLatitude(qreal newLatitude);

    /**
     * @brief Haritanın longitude değerini değiştirmek için kullanılan fonksiyon.
     *
     * @param newLatitude Yeni longitude değeri.
     */
    void setLongitude(qreal newLongitude);

    /**
     * @brief Bir sonraki harita yakınlaştırma değerine geçmeyi sağlayan fonksiyon.
     *
     */
    void zoomIn();

    /**
     * @brief Bir önceki harita yakınlaştırma değerine geçmeyi sağlayan fonksiyon.
     *
     */
    void zoomOut();

    /**
     * @brief Harita konumu değiştiğinde çalışacak fonksityon.
     *
     */
    void posChanged();

    /**
     * @brief Harita bilgilerini yeniden karelendiren fonksiyon.
     *
     */
    void reTile();

    void setRetilingEnabled(bool newValue);

private slots:

    /**
     * @brief Aktif harita sağlayıcı değiştiğinde çalışacak fonksiyon.
     *
     */
    void providerSwitch();

protected:


private:
    int m_iZoom; /**< Aktif harita yakınlaştırma değeri */
    int m_iScale; /**< Aktif görsel yakınlaştırma değeri */

    qreal m_fLatitude; /**< Latitude değeri */
    qreal m_fLongitude; /**< Longitude değeri */

    qreal m_afScales[W_MAXSCALELEVEL+1]; /**< Kullanılacak görsel yakınlaştırma değerleri */
    int m_aiZoom[W_MAXSCALELEVEL+1]; /**< Kullanılacak harita yakınlaştırma değerleri */

    ZoomLevels m_cZoomLevels; /**< Her bir yakınlaştırma değerinde kullanılan kareleri tutan veri yapısı */

    bool m_bRetilingEnabled;

    /**
     * @brief İlk dünya görünümünü ayarlayan fonksiyon
     *
     */
    void initWorld();

    /**
     * @brief Dünya koordinatlarını karelerin koordinatlarına döndüren fonksiyon.
     *
     * @param rect Dünya koordinatları
     * @return QRect Kare koordinatları
     */
    QRect worldToTile(const QRectF rect);

    /**
     * @brief Karele koordinatlarını dünya koordinatlarına döndüren fonksiyon.
     *
     * @param rect Kare koordinatları
     * @return QRect Dünya koordinatları
     */
    QRectF tileRect(const QPoint pnt);

//    void updatePropertyWindow();
};

#endif // QGRAPHICSWORLDITEM_H
