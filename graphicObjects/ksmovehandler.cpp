#include "ksmovehandler.h"
#include "workspace/ksworkspace.h"
#include "ksroadobject.h"

#define HANDLE_SIZE         2.5
#define HALF_HANDLE_SIZE    1.25
#define LINE_SIZE           0.25

QHash<int, KsMoveHandler*> KsMoveHandler::s_hashMoveHandlers;

KsMoveHandler *KsMoveHandler::getHandleForIndex(int index, KsRoadObject *roadObj)
{
    Q_UNUSED(index);

    if(s_hashMoveHandlers.contains(index)) {
        s_hashMoveHandlers[index]->setRoadObject(roadObj);
        s_hashMoveHandlers[index]->m_bIsInMove = false;
        s_hashMoveHandlers[index]->m_pntStartPnt = roadObj->roadNormal().at(index);
        s_hashMoveHandlers[index]->setPos(0,0);
        s_hashMoveHandlers[index]->m_iIndex = index;
        s_hashMoveHandlers[index]->setZValue(2000);
        s_hashMoveHandlers[index]->setVisible(true);
        s_hashMoveHandlers[index]->prepareGeometryChange();
        return s_hashMoveHandlers[index];
    } else {
        KsMoveHandler *moveHandle = new KsMoveHandler();
        s_hashMoveHandlers[index] = moveHandle;
        moveHandle->setRoadObject(roadObj);
        moveHandle->m_bIsInMove = false;
        moveHandle->m_pntStartPnt = roadObj->roadNormal().at(index);
        moveHandle->m_iIndex = index;
        moveHandle->setVisible(true);
        moveHandle->setZValue(2000);
        KsWorkspace::SharedInstance()->mapScene()->addItem(moveHandle);
        moveHandle->prepareGeometryChange();
        return moveHandle;
    }
}

QRectF KsMoveHandler::boundingRect() const
{

    return QRectF(m_pntStartPnt.x()-HALF_HANDLE_SIZE*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees(),
                  m_pntStartPnt.y()-HALF_HANDLE_SIZE*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees(),
                  HANDLE_SIZE*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees(),
                  HANDLE_SIZE*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees());
}

void KsMoveHandler::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen(Qt::black, LINE_SIZE*KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees());
    QBrush brush(Qt::green);

    painter->setPen(pen);
    painter->setBrush(brush);

    painter->drawRect(boundingRect());
}

void KsMoveHandler::setRoadObject(KsRoadObject *roadObject)
{
    m_ptrRoadObject = roadObject;
}

void KsMoveHandler::afterPositionChanged(QPointF newLocation)
{
    m_ptrRoadObject->setNormalPoint(m_iIndex, m_pntStartPnt+newLocation);
}

KsMoveHandler::KsMoveHandler(QGraphicsItem *parent) :
    KsGraphicObject(parent),
    m_ptrRoadObject(NULL)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsScenePositionChanges);
}

void KsMoveHandler::hideAllMoveHandlers()
{
    foreach (int key, s_hashMoveHandlers.keys()) {
        s_hashMoveHandlers[key]->setVisible(false);
        s_hashMoveHandlers[key]->m_ptrRoadObject = NULL;
    }
}

void KsMoveHandler::showMoveHandlersForObject(KsRoadObject *road)
{
    for(int i=1;i<road->roadNormal().count()-1;i++) {
        getHandleForIndex(i, road);
    }
}
