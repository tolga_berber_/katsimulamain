#ifndef QGRAPHICSMAPSCENE_H
#define QGRAPHICSMAPSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneWheelEvent>
#include <QGraphicsView>
#include "qgraphicsworlditem.h"
#include "qgraphicsmaplayer.h"
#include "interactions/ksabstractinteraction.h"

/**
 * @brief Uygulamada kullanılacak sahne nesnesi.
 *
 * Katmanlar halinde nesneler içerecek bir sahne nesnesidir. En alt katman daima harita katmanı olacaktır. Qt Y ekseni olarak azalan bir yapıyı desteklemediği için, latitude değerleri
 * (85, -85) aralığı yerine (-85, 85) aralığına çekilmiştir. Bu sebeple dünyadaki konuma erişebilmek için lat değerleri -1 ile çarpılmalıdır.
 *
 */
class QGraphicsMapScene : public QGraphicsScene
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici fonksiyon.
     *
     * En alttaki harita nesnesini oluşturur ve harita katmanına ekler. Görünen bölge değiştiğinde harita nesnesinin güncellenmesini sağlayak sinyal slot eşlemesini yapar.
     *
     * @param parent Göz ardı edilebilir, Qt uyumluluğu için kullanılacaktır.
     */
    explicit QGraphicsMapScene(QObject *parent = 0);

    /**
     * @brief Bu sahnenin belirtilen indexteki view'daki görünen kısmının dünya koordinatlarını döndüren fonksiyon.
     *
     * @param idx view indexi.
     * @return QRectF Görünen bölgenin alanı.
     */
    QRectF visiblePortion(int idx);

    /**
     * @brief Katmanları tutmak için kullanılacak veri yapısı
     *
     */
    typedef QList<QGraphicsMapLayer*> LayerArray;

    /**
     * @brief Sahnedeki katmanların listesini döndüren fonksiyon.
     *
     * @return const LayerArray Katmanların listesi.
     */
    const LayerArray layers();

    QGraphicsWorldItem *worldItem();

    void setupLayers();

    QGraphicsMapLayer *mapLayer();

    QGraphicsMapLayer *currentTrafficObjectLayer();

    QGraphicsMapLayer *currentLinkLayer();

    float oneMeterInDegrees();

signals:
    /**
     * @brief Görünen bölge değiştiğinde tetiklenen sinyal.
     *
     */
    void visiblePortionChanged();

    void visiblePortionChanged(const QRectF &rect);

    void layerAdded(QGraphicsMapLayer *layer);

    void layerItemSelected(QGraphicsMapLayer *layer, KsGraphicObject *object);

    void layerItemDeSelected(QGraphicsMapLayer *layer, KsGraphicObject *object);

private slots:
    void internalVisibilityHandler();

public slots:
    void setActiveInteractionObject(KsAbstractInteraction *interaction);

    void addLayer(QGraphicsMapLayer *layer);

    void setVisiblePortion(QRectF rect, int zoomLevel, int scaleLevel, int idx);

protected:
    /**
     * @brief Fare tekeri döndürüldüğünde çalışan eylem.
     *
     * NOT: Bu eylemin değiştirilmesi gerekebilir. Etkileşim nesneleri bu eylemi değiştirebilir.
     *
     * @param event Qt olay parametreleri.
     */
    virtual void wheelEvent(QGraphicsSceneWheelEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void keyPressEvent(QKeyEvent *keyEvent);
    virtual void keyReleaseEvent(QKeyEvent *keyEvent);

private:
    QGraphicsWorldItem *m_pItem; /**< Dünya nesnesi */
    LayerArray m_cLayers; /**< Bu sahnedeki katmanların listesi */
    KsAbstractInteraction *m_pcActiveInteractionObject;
    QGraphicsMapLayer *m_pcActiveTrafficObjectLayer;
    QGraphicsMapLayer *m_pcActiveLinkLayer;
};

#endif // QGRAPHICSMAPSCENE_H
