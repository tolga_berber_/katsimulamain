#include "qgraphicsmapscene.h"
#include <QGraphicsView>

#include "interactions/ksinteractionstore.h"

#include <cmath>

#define WORLD_LAYER_INDEX   0

QGraphicsMapScene::QGraphicsMapScene(QObject *parent) :
    QGraphicsScene(parent),
    m_cLayers(),
    m_pcActiveInteractionObject(NULL),
    m_pcActiveTrafficObjectLayer(NULL),
    m_pcActiveLinkLayer(NULL)
{
    m_pItem = new QGraphicsWorldItem();
    connect(this, SIGNAL(visiblePortionChanged()), m_pItem, SLOT(reTile()));
    connect(this, SIGNAL(visiblePortionChanged()), this, SLOT(internalVisibilityHandler()));

    //setActiveInteractionObject(KsInteractionStore::SharedInstance()->interaction(KsInteractionStore::DefaultInteraction));
}

QRectF QGraphicsMapScene::visiblePortion(int idx)
{
    QGraphicsView *view = views().at(idx);
    return view->mapToScene(view->viewport()->rect()).boundingRect();
}

const QGraphicsMapScene::LayerArray QGraphicsMapScene::layers()
{
    return this->m_cLayers;
}

QGraphicsWorldItem *QGraphicsMapScene::worldItem()
{
    return this->m_pItem;
}

void QGraphicsMapScene::setupLayers()
{
    QGraphicsMapLayer *worldLayer = new QGraphicsMapLayer(this);
    addItem(m_pItem);
    worldLayer->addObject(m_pItem);
    worldLayer->setLayerType(QGraphicsMapLayer::LT_World);
    addLayer(worldLayer);

    QGraphicsMapLayer *objectLayer = new QGraphicsMapLayer(this);
    objectLayer->setLayerType(QGraphicsMapLayer::LT_TrafficObjects);
    addLayer(objectLayer);
    m_pcActiveTrafficObjectLayer = objectLayer;

    QGraphicsMapLayer *linkLayer = new QGraphicsMapLayer(this);
    linkLayer->setLayerType(QGraphicsMapLayer::LT_Links);
    addLayer(linkLayer);
    m_pcActiveLinkLayer = linkLayer;

}

QGraphicsMapLayer *QGraphicsMapScene::mapLayer()
{
    return m_cLayers.at(WORLD_LAYER_INDEX);
}

QGraphicsMapLayer *QGraphicsMapScene::currentTrafficObjectLayer()
{
    return m_pcActiveTrafficObjectLayer;
}

QGraphicsMapLayer *QGraphicsMapScene::currentLinkLayer()
{
    return m_pcActiveLinkLayer;
}

float QGraphicsMapScene::oneMeterInDegrees()
{
    float result = 0.0;

    QRectF visibleWorldPart = visiblePortion(0);

    float length = 111319.9*cos(-visibleWorldPart.center().y()*M_PI/180.0);

    result = 1/length;

    //result /= pow(2.0,m_pItem->zoomLevel());

    return result;
}

void QGraphicsMapScene::internalVisibilityHandler()
{
    emit visiblePortionChanged(m_pItem->visibleWorldPart(0));
}

void QGraphicsMapScene::setActiveInteractionObject(KsAbstractInteraction *interaction)
{
    if(interaction && m_pcActiveInteractionObject==interaction) {
        return;
    }
    bool stopRequested = false;
    if(m_pcActiveInteractionObject)
        m_pcActiveInteractionObject->beforeDeactivate(stopRequested);
    if(stopRequested) {
        return;
    }
    interaction->beforeActivate(stopRequested);
    if(stopRequested) {
        return;
    }
    KsAbstractInteraction *tmp = m_pcActiveInteractionObject;
    m_pcActiveInteractionObject = interaction;
    if(tmp)
        tmp->afterDeactivate();
    m_pcActiveInteractionObject->afterActivate();
}

void QGraphicsMapScene::addLayer(QGraphicsMapLayer *layer)
{
    if(m_cLayers.indexOf(layer)==-1) {
        m_cLayers.append(layer);
        connect(layer, SIGNAL(objectOfLayerSelected(QGraphicsMapLayer*,KsGraphicObject*)),
                this, SIGNAL(layerItemSelected(QGraphicsMapLayer*,KsGraphicObject*)));
        connect(layer, SIGNAL(objectOfLayerDeSelected(QGraphicsMapLayer*,KsGraphicObject*)),
                this, SIGNAL(layerItemDeSelected(QGraphicsMapLayer*,KsGraphicObject*)));
        emit layerAdded(layer);
    }
}

void QGraphicsMapScene::setVisiblePortion(QRectF rect, int zoomLevel, int scaleLevel, int idx)
{
    m_pItem->setRetilingEnabled(false);
    m_pItem->setZoomLevel(zoomLevel);
    m_pItem->setScaleLevel(scaleLevel);
    QGraphicsView *view = views().at(idx);
    view->setTransform(QTransform());
    view->scale(pow(2.0,zoomLevel), pow(2.0,zoomLevel));
    view->centerOn(rect.center());
    m_pItem->setRetilingEnabled(true);
    m_pItem->reTile();
}

void QGraphicsMapScene::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    QGraphicsScene::wheelEvent(event);
    m_pcActiveInteractionObject->wheelEvent(event);
}

void QGraphicsMapScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mouseMoveEvent(mouseEvent);
    m_pcActiveInteractionObject->mouseMoveEvent(mouseEvent);
}

void QGraphicsMapScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mousePressEvent(mouseEvent);
    m_pcActiveInteractionObject->mousePressEvent(mouseEvent);
}

void QGraphicsMapScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
    m_pcActiveInteractionObject->mouseReleaseEvent(mouseEvent);
}

void QGraphicsMapScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
    m_pcActiveInteractionObject->mouseDoubleClickEvent(mouseEvent);
}

void QGraphicsMapScene::keyPressEvent(QKeyEvent *keyEvent)
{
    QGraphicsScene::keyPressEvent(keyEvent);
    m_pcActiveInteractionObject->keyPressEvent(keyEvent);
}

void QGraphicsMapScene::keyReleaseEvent(QKeyEvent *keyEvent)
{
    QGraphicsScene::keyReleaseEvent(keyEvent);
    m_pcActiveInteractionObject->keyReleaseEvent(keyEvent);
}
