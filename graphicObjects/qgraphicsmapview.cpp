#include "qgraphicsmapview.h"
#include <QGLWidget>

QGraphicsMapView::QGraphicsMapView(QWidget *parent) :
    QGraphicsView(parent)
{
    m_pcMapScene = new QGraphicsMapScene(this);
    setScene(m_pcMapScene);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setRenderHint(QPainter::Antialiasing);
    setInteractive(true);
    setMouseTracking(true);

    connect(this, SIGNAL(contentScrolled()), m_pcMapScene, SIGNAL(visiblePortionChanged()));

}


void QGraphicsMapView::scaleXY(qreal sx, qreal sy)
{
    scale(sx,sy);
    emit contentScaled();
}

void QGraphicsMapView::zoomIn()
{
    int oldScaleLevel = m_pcMapScene->worldItem()->scaleLevel();
    m_pcMapScene->worldItem()->zoomIn();
    int newScaleLevel = m_pcMapScene->worldItem()->scaleLevel();
    if(oldScaleLevel==newScaleLevel)
        return;
    m_pcMapScene->worldItem()->setRetilingEnabled(false);
    scale(2.0,2.0);
    m_pcMapScene->worldItem()->setRetilingEnabled(true);
}

void QGraphicsMapView::zoomOut()
{
    int oldScaleLevel = m_pcMapScene->worldItem()->scaleLevel();
    m_pcMapScene->worldItem()->zoomOut();
    int newScaleLevel = m_pcMapScene->worldItem()->scaleLevel();
    if(oldScaleLevel==newScaleLevel)
        return;
    m_pcMapScene->worldItem()->setRetilingEnabled(false);
    scale(0.5,0.5);
    m_pcMapScene->worldItem()->setRetilingEnabled(true);
}

void QGraphicsMapView::scrollContentsBy(int dx, int dy)
{
    QGraphicsView::scrollContentsBy(dx,dy);
    emit contentScrolled();
}
