#include "ksgraphicobject.h"

#include <QGraphicsSceneContextMenuEvent>

KsGraphicObject::KsGraphicObject(QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_pcLayer(NULL)
{
}

QGraphicsMapLayer *KsGraphicObject::layer()
{
    return m_pcLayer;
}

void KsGraphicObject::setLayer(QGraphicsMapLayer *layer)
{
    if(m_pcLayer!=layer) {
        this->m_pcLayer = layer;
    }
}

QVariant KsGraphicObject::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    // bool val;
    uint uiVal;
    QPointF position;
    switch(change) {
    case QGraphicsItem::ItemSelectedChange:
        uiVal = value.toBool();
        beforeItemSelect(uiVal);
        return uiVal;
        break;
    case QGraphicsItem::ItemSelectedHasChanged:
        uiVal = value.toBool();
        afterItemSelected(uiVal);
        return uiVal;
        break;
    case QGraphicsItem::ItemPositionChange:
        position = value.toPointF();
        beforePositionChange(position);
        return position;
        break;
    case QGraphicsItem::ItemPositionHasChanged:
        position = value.toPointF();
        afterPositionChanged(position);
        return position;
        break;
    case QGraphicsItem::ItemScenePositionHasChanged:
        position = value.toPointF();
        afterScenePositionChanged(position);
        return position;
        break;
    default:
        return QGraphicsObject::itemChange(change, value);
    }
}

bool KsGraphicObject::hasContextMenu()
{
    return false;
}

QMenu *KsGraphicObject::getPopupMenu()
{
    return NULL;
}

void KsGraphicObject::beforeItemSelect(uint &newValue)
{
    Q_UNUSED(newValue);
}

void KsGraphicObject::afterItemSelected(uint newValue)
{
    emit itemSelectedChanged(this, newValue);
    Q_UNUSED(newValue);
}

void KsGraphicObject::beforePositionChange(QPointF &newLocation)
{
    Q_UNUSED(newLocation);
}

void KsGraphicObject::afterPositionChanged(QPointF newLocation)
{
    emit itemPositionChanged(this, newLocation);
    Q_UNUSED(newLocation);
}

void KsGraphicObject::afterScenePositionChanged(QPointF newLocation)
{
    Q_UNUSED(newLocation);
}

void KsGraphicObject::contextMenuEvent(QGraphicsSceneContextMenuEvent *evt)
{
    /*if(hasContextMenu()) {
        QMenu *popup = getPopupMenu();
        popup->popup(evt->screenPos());
    }*/
}
