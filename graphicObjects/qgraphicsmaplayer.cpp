#include "qgraphicsmaplayer.h"

#include "workspace/ksworkspace.h"

#include "graphicObjects/ksroadobject.h"

QGraphicsMapLayer::QGraphicsMapLayer(QObject *parent) :
    QObject(parent),
    m_cLayerObjects(),
    m_fZValue(1000),
    m_bVisible(true),
    m_eType(LT_TrafficObjects)
{
    setObjectName(tr("Trafik Kaynakları/Hedefleri"));
}

const GraphicObjectList QGraphicsMapLayer::layerObjects()
{
    return m_cLayerObjects;
}

QGraphicsMapLayer::LayerType QGraphicsMapLayer::layerType()
{
    return m_eType;
}

bool QGraphicsMapLayer::layerVisibility()
{
    return m_bVisible;
}

qreal QGraphicsMapLayer::layerZValue()
{
    return m_fZValue;
}

void QGraphicsMapLayer::setVisible(bool visible)
{
    if(m_bVisible == visible) {
        return;
    }
    m_bVisible = visible;
    foreach (QGraphicsObject *obj, m_cLayerObjects) {
        obj->setVisible(m_bVisible);
    }

    emit visibleChanged(m_bVisible);
}

void QGraphicsMapLayer::setZ(qreal z)
{
    if(m_fZValue==z || (z<=0 && m_eType!=LT_World) || (z>=0 && m_eType==LT_World)) {
        return;
    }
    m_fZValue=z;
    foreach(QGraphicsObject *obj, m_cLayerObjects) {
        obj->setZValue(m_fZValue);
    }
    emit zChanged(m_fZValue);
}

void QGraphicsMapLayer::setLayerType(QGraphicsMapLayer::LayerType type)
{
    if(m_eType!=type) {
        m_eType = type;
        switch (m_eType) {
        case LT_World:
            setZ(-1);
            setObjectName(tr("Dünya Haritası Katmanı"));
            break;
        case LT_TrafficObjects:
            setZ(1000);
            setObjectName(tr("Trafik Kaynakları/Hedefleri"));
            break;
        default:
            setZ(1);
            setObjectName(tr("Yollar"));
            break;
        }
    }
}

void QGraphicsMapLayer::lockObjects()
{
    if(m_eType==LT_World)
        return;
    foreach (KsGraphicObject *obj, m_cLayerObjects) {
        obj->setFlag(QGraphicsItem::ItemIsMovable, false);
        obj->setFlag(QGraphicsItem::ItemIsSelectable, false);
    }
}

void QGraphicsMapLayer::unlockObjects()
{
    if(m_eType==LT_World)
        return;
    foreach (KsGraphicObject *obj, m_cLayerObjects) {
        if(m_eType!=LT_Links)
            obj->setFlag(QGraphicsItem::ItemIsMovable);
        obj->setFlag(QGraphicsItem::ItemIsSelectable);
    }
}

void QGraphicsMapLayer::handleItemSelection(KsGraphicObject *obj, bool newValue)
{
    if(newValue) {
        KsRoadObject *rObj = qobject_cast<KsRoadObject*>(obj);
        emit objectOfLayerSelected(this, obj);
    } else {
        emit objectOfLayerDeSelected(this, obj);
        if(m_eType!=LT_World) {
            QGraphicsMapScene *scn = KsWorkspace::SharedInstance()->mapScene();
            QGraphicsMapLayer *worldLayer = scn->mapLayer();
            emit worldLayer->objectOfLayerSelected(worldLayer, scn->worldItem());
        }
    }
}

void QGraphicsMapLayer::addObject(KsGraphicObject *obj)
{
    if(m_cLayerObjects.indexOf(obj)==-1) {
        m_cLayerObjects.append(obj);
        obj->setZValue(m_fZValue);
        obj->setVisible(m_bVisible);
        obj->setLayer(this);
        emit newObjectAdded(this, obj);
        connect(obj, SIGNAL(itemSelectedChanged(KsGraphicObject*,bool)), this, SLOT(handleItemSelection(KsGraphicObject*,bool)));
        connect(obj, SIGNAL(destroyed()), this, SLOT(handleObjectDestroy()));
    } else {
    }
}

void QGraphicsMapLayer::removeObject(KsGraphicObject *obj, bool deleteIt)
{
    if(m_cLayerObjects.indexOf(obj)!=-1) {
        emit objectRemoved(this, obj);
        int idx = m_cLayerObjects.indexOf(obj);
        emit objectAtIndexRemoved(this, idx);
        disconnect(obj, SIGNAL(itemSelectedChanged(KsGraphicObject*,bool)), this, SLOT(handleItemSelection(KsGraphicObject*,bool)));
        m_cLayerObjects.removeAll(obj);
        if(deleteIt)
            delete obj;
    }
}

void QGraphicsMapLayer::handleObjectDestroy()
{
    QObject *obj = sender();
    removeObject(reinterpret_cast<KsGraphicObject*>(obj), false);
}
