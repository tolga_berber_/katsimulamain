#ifndef TILESTORAGE_H
#define TILESTORAGE_H

#include <QPair>
#include <QHash>
#include <QList>
#include "qgraphicstileitem.h"

class QGraphicsTileItem;

/**
 * @brief
 *
 */
typedef QPair<int,int> TileCoordinate;
/**
 * @brief
 *
 */
typedef QHash<TileCoordinate, QGraphicsItem*> ZoomLevelTileHash;
/**
 * @brief
 *
 */
typedef QList<ZoomLevelTileHash> ZoomLevels;

#define MAKE_TILE_COORDINATE(x,y)   qMakePair((x),(y))
#define TILE_X(coord)               (coord).first
#define TILE_Y(coord)               (coord).second

#endif // TILESTORAGE_H
