#include "qgraphicstileitem.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include "qgraphicsmapscene.h"

#include "workspace/ksworkspace.h"
#include "tileSource/kstilesource.h"

QGraphicsTileItem::QGraphicsTileItem(QGraphicsWorldItem *world, QRectF bRect, int x, int y, int z, QGraphicsItem *parent) :
    QGraphicsObject(parent),
    m_iX(x),
    m_iY(y),
    m_iZ(z),
    m_cRect(bRect),
    m_cImage(),
    m_pcWorld(world)
{
    setZValue(m_iZ);
    setFlag(QGraphicsItem::ItemIsMovable,false);
    setFlag(QGraphicsItem::ItemIsSelectable,false);
}

QRectF QGraphicsTileItem::boundingRect() const
{
    return m_cRect;
}

void QGraphicsTileItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    int vLevel = m_pcWorld->zoomLevel()-m_iZ;
    if(vLevel>1 || vLevel<0 || !isInVisiblePortion(0)) {
        return;
    }
    if(m_cImage.isNull()) {
        MapProvider *provider = KsWorkspace::SharedInstance()->mapProviders()->activeProvider();
        connect(provider->tileSource(), SIGNAL(tileImage(int,int,int,const QImage&)), this, SLOT(tileImage(int,int,int,const QImage&)));
        /*connect(QTileCache::SharedInstance(), SIGNAL(tileImage(int,int,int,const QImage&)), this, SLOT(tileImage(int,int,int,const QImage&)));
        connect(QTileCache::SharedInstance(), SIGNAL(tileImageNotFound(int,int,int)), this, SLOT(tileImageNotFound(int,int,int)));*/
        // QTileCache::SharedInstance()->getTileImage(m_iX, m_iY, m_iZ);
        provider->tileSource()->requestTile(m_iX, m_iY, m_iZ);
    } else {
        painter->drawImage(m_cRect, m_cImage);
    }
}

void QGraphicsTileItem::clearImage()
{
    m_cImage = QImage();
    update();
}

void QGraphicsTileItem::tileImage(int x, int y, int z, const QImage &img)
{
    if(x==m_iX && y==m_iY && z==m_iZ) {
        MapProvider *provider = KsWorkspace::SharedInstance()->mapProviders()->activeProvider();
        disconnect(provider->tileSource(), SIGNAL(tileImage(int,int,int,const QImage&)), this, SLOT(tileImage(int,int,int,const QImage&)));
        //disconnect(QTileCache::SharedInstance(), SIGNAL(tileImage(int,int,int,const QImage&)), this, SLOT(tileImage(int,int,int,const QImage&)));
        //disconnect(QTileCache::SharedInstance(), SIGNAL(tileImageNotFound(int,int,int)), this, SLOT(tileImageNotFound(int,int,int)));
        m_cImage = img;
        update();
    }
}

//void QGraphicsTileItem::tileImageNotFound(int x, int y, int z)
//{
//    if(m_iX==x && m_iY==y && m_iZ==z) {
//        disconnect(QTileCache::SharedInstance(), SIGNAL(tileImage(int,int,int,const QImage&)), this, SLOT(tileImage(int,int,int,const QImage&)));
//        disconnect(QTileCache::SharedInstance(), SIGNAL(tileImageNotFound(int,int,int)), this, SLOT(tileImageNotFound(int,int,int)));
//        connect(QMapNetworkHandler::SharedInstance(), SIGNAL(tileDataReady(int,int,int,const QByteArray&)), this, SLOT(tileDataReady(int,int,int,const QByteArray&)));
//        QMapNetworkHandler::SharedInstance()->downloadTile(x,y,z);
//    }
//}

//void QGraphicsTileItem::tileDataReady(int x, int y, int z, const QByteArray &arr)
//{
//    if(x==m_iX && y==m_iY && z==m_iZ) {
//        m_cImage = QImage();
//        m_cImage.loadFromData(arr);
//        disconnect(QMapNetworkHandler::SharedInstance(), SIGNAL(tileDataReady(int,int,int,const QByteArray&)), this, SLOT(tileDataReady(int,int,int,const QByteArray&)));
//        update();
//    }
//}

bool QGraphicsTileItem::isInVisiblePortion(int idx)
{
    return m_pcWorld->visibleWorldPart(idx).intersects(boundingRect());
}

QRectF QGraphicsTileItem::visiblePortion(int idx)
{
    return m_pcWorld->visibleWorldPart(idx);
}
