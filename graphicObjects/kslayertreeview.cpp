#include "kslayertreeview.h"
#include "qmaplayermodel.h"
#include <QHeaderView>
#include "workspace/ksworkspace.h"

KsLayerTreeView::KsLayerTreeView(QWidget *parent) :
    QTreeView(parent),
    m_bHandleVisibility(true)
{
    setModel(new QMapLayerModel(this));
    header()->setStretchLastSection(false);
    header()->setSectionResizeMode(0, QHeaderView::Stretch);
    setColumnWidth(1,50);
    setExpandsOnDoubleClick(false);

    connect(this, SIGNAL(clicked(QModelIndex)), this, SLOT(layerDoubleClicked(QModelIndex)));
    connect(KsWorkspace::SharedInstance()->mapScene(), SIGNAL(layerAdded(QGraphicsMapLayer*)), this, SLOT(layerAdded(QGraphicsMapLayer*)));
}

void KsLayerTreeView::layerDoubleClicked(const QModelIndex &index)
{
    if(!index.isValid())
        return;
    if(index.column()==1) {
        QVector<int> changedRoles;
        changedRoles<<Qt::DecorationRole;
        if(m_bHandleVisibility) {
            QGraphicsMapLayer *layer = qobject_cast<QGraphicsMapLayer*>((QObject*)index.internalPointer());
            if(layer) {
                layer->setVisible(!layer->layerVisibility());
                emit visibilityOfLayerChanged(layer);
                const QModelIndex begin = index.child(0,1);
                const QModelIndex end = index.child(layer->layerObjects().size()-1,1);
                dataChanged(begin, end, changedRoles);
            } else {
                KsGraphicObject *obj = qobject_cast<KsGraphicObject*>((QObject*)index.internalPointer());
                obj->setVisible(!obj->isVisible());
                emit visibilityOfObjectChanged(obj);
            }
            dataChanged(index,index,changedRoles);
        } else {
            QGraphicsMapLayer *layer = qobject_cast<QGraphicsMapLayer*>((QObject*)index.internalPointer());
            if(layer) {
                emit toggleVisibilityOfLayerRequested(layer);
                const QModelIndex begin = index.child(0,1);
                const QModelIndex end = index.child(layer->layerObjects().size()-1,1);
                dataChanged(begin, end, changedRoles);
            } else {
                KsGraphicObject *obj = qobject_cast<KsGraphicObject*>((QObject*)index.internalPointer());
                emit toggleVisibilityOfObjectRequested(obj);
            }
            dataChanged(index,index,changedRoles);
        }
    }
}

void KsLayerTreeView::layerAdded(QGraphicsMapLayer *layer)
{
    QMapLayerModel *modelObj = qobject_cast<QMapLayerModel*>(model());
    connect(layer, SIGNAL(newObjectAdded(QGraphicsMapLayer*,KsGraphicObject*)), modelObj, SLOT(objectAddedToLayer(QGraphicsMapLayer*,KsGraphicObject*)));
    connect(layer, SIGNAL(objectAtIndexRemoved(QGraphicsMapLayer*,int)), modelObj, SLOT(objectRemovedFromLayer(QGraphicsMapLayer*,int)));
    connect(layer, SIGNAL(objectOfLayerSelected(QGraphicsMapLayer*,KsGraphicObject*)), this, SLOT(objectSelected(QGraphicsMapLayer*,KsGraphicObject*)));
}

void KsLayerTreeView::objectSelected(QGraphicsMapLayer *layer, KsGraphicObject *obj)
{
    QMapLayerModel *modelObj = qobject_cast<QMapLayerModel*>(model());
    QModelIndex idx = modelObj->getModelIndex(layer, obj);


    setCurrentIndex(idx);
}
