#ifndef QGRAPHICSTILEITEM_H
#define QGRAPHICSTILEITEM_H

#include <QGraphicsObject>
#include <QImage>
#include "qgraphicsworlditem.h"

class QGraphicsWorldItem;

/**
 * @brief
 *
 */
class QGraphicsTileItem : public QGraphicsObject
{
    Q_OBJECT
public:
    /**
     * @brief
     *
     * @param world
     * @param bRect
     * @param x
     * @param y
     * @param z
     * @param parent
     */
    explicit QGraphicsTileItem(QGraphicsWorldItem *world, QRectF bRect=QRectF(), int x=0, int y=0, int z=0, QGraphicsItem *parent = 0);

    /**
     * @brief
     *
     * @return int
     */
    int tileX() {
        return m_iX;
    }

    /**
     * @brief
     *
     * @return int
     */
    int tileY() {
        return m_iY;
    }

    /**
     * @brief
     *
     * @return int
     */
    int zoomLevel() {
        return m_iZ;
    }

signals:

public:
    /**
     * @brief
     *
     * @return QRectF
     */
    virtual QRectF boundingRect() const;
    /**
     * @brief
     *
     * @param painter
     * @param option
     * @param widget
     */
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

public slots:
    // World SLOTS
    /**
     * @brief
     *
     */
    void clearImage();

    // cache slots
    /**
     * @brief
     *
     * @param x
     * @param y
     * @param z
     * @param img
     */
    void tileImage(int x, int y, int z, const QImage &img);

private:
    int m_iX; /**< TODO */
    int m_iY; /**< TODO */
    int m_iZ; /**< TODO */
    QRectF m_cRect; /**< TODO */
    QImage m_cImage; /**< TODO */
    QGraphicsWorldItem *m_pcWorld; /**< TODO */

    /**
     * @brief
     *
     * @param idx
     * @return QRectF
     */
    QRectF visiblePortion(int idx);
    /**
     * @brief
     *
     * @param idx
     * @return bool
     */
    bool isInVisiblePortion(int idx);
};

#endif // QGRAPHICSTILEITEM_H
