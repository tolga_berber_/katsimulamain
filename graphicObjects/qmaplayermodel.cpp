#include "qmaplayermodel.h"
#include "../workspace/ksworkspace.h"
#include "qgraphicsmapscene.h"
#include "qgraphicsmaplayer.h"
#include "assert.h"

QMapLayerModel::QMapLayerModel(QObject *parent) :
    QAbstractItemModel(parent)
{
}

QModelIndex QMapLayerModel::getModelIndex(QGraphicsMapLayer *layer, KsGraphicObject *obj)
{
    int rowIndex = layer->layerObjects().indexOf(obj);

    return createIndex(rowIndex, 0, (void*)obj);
}

void QMapLayerModel::objectAddedToLayer(QGraphicsMapLayer *layer, KsGraphicObject *obj)
{
    Q_UNUSED(obj);
    int row = KsWorkspace::SharedInstance()->mapScene()->layers().indexOf(layer);
    QModelIndex idx = index(row,0,QModelIndex());
    beginInsertRows(idx, layer->layerObjects().size(), layer->layerObjects().size());
    endInsertRows();
}

void QMapLayerModel::objectRemovedFromLayer(QGraphicsMapLayer *layer, int idx)
{
    int row = KsWorkspace::SharedInstance()->mapScene()->layers().indexOf(layer);
    QModelIndex pindex = index(row,0,QModelIndex());
    beginRemoveRows(pindex, idx, idx);
    endRemoveRows();
}

QModelIndex QMapLayerModel::index(int row, int column, const QModelIndex &parent) const
{
    if(!hasIndex(row,column,parent))
        return QModelIndex();

    if(!parent.isValid()) {
        QGraphicsMapScene *scn = KsWorkspace::SharedInstance()->mapScene();
        int layerSize = scn->layers().size();
        if(row>=layerSize)
            return QModelIndex();
        else
            return createIndex(row, column, scn->layers().at(row));
    } else {
        QGraphicsMapLayer *layer = qobject_cast<QGraphicsMapLayer*>((QObject*)parent.internalPointer());
        assert(layer);
        if(row>=layer->layerObjects().size()) {
            return QModelIndex();
        } else {
            return createIndex(row,column, layer->layerObjects().at(row));
        }
    }
}

QModelIndex QMapLayerModel::parent(const QModelIndex &child) const
{
    if(!child.isValid())
        return QModelIndex();

    QGraphicsMapLayer *layerObj = qobject_cast<QGraphicsMapLayer*>((QObject*)child.internalPointer());
    if(layerObj) {
        return QModelIndex();
    } else {
        QGraphicsMapScene *scene = KsWorkspace::SharedInstance()->mapScene();
        KsGraphicObject *ptr = qobject_cast<KsGraphicObject*>((QObject*)child.internalPointer());
        QGraphicsMapLayer *layerObj = (QGraphicsMapLayer*)ptr->layer();
        return createIndex(scene->layers().indexOf(layerObj), 0, layerObj);
    }
}

QVariant QMapLayerModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if(index.column()==1) {
        QGraphicsMapLayer *layerObj = qobject_cast<QGraphicsMapLayer*>((QObject*)index.internalPointer());
        QPixmap visiblePixmap(":/img/widgets/layerwidget/layer_visible.png");
        QPixmap hiddenPixmap(":/img/widgets/layerwidget/layer_hide.png");
        if(layerObj) {
            if(role==Qt::DecorationRole) {
                if(layerObj->layerVisibility()) {
                    return visiblePixmap;
                } else {
                    return hiddenPixmap;
                }
            }
        } else {
            KsGraphicObject *obj = qobject_cast<KsGraphicObject*>((QObject*)index.internalPointer());
            if(role==Qt::DecorationRole) {
                if(obj->isVisible()) {
                    return visiblePixmap;
                } else {
                    return hiddenPixmap;
                }
            }
        }
        if(role==Qt::SizeHintRole) {
            return visiblePixmap.size();
        }

        return QVariant();
    }

    if(role!=Qt::DisplayRole)
        return QVariant();

    QGraphicsMapLayer *layerObj = qobject_cast<QGraphicsMapLayer*>((QObject*)index.internalPointer());
    if(layerObj) {
        if(index.column()==0) {
            return layerObj->objectName();
        } else {
            return layerObj->layerVisibility()?tr("Görünür"):tr("Gizli");
        }
    } else {
        KsGraphicObject *obj = qobject_cast<KsGraphicObject*>((QObject*)index.internalPointer());
        if(index.column()==0) {
            return obj->objectName();
        } else {
            return obj->isVisible()?tr("Görünür"):tr("Gizli");
        }
    }
}

QVariant QMapLayerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);
    if(role!=Qt::DisplayRole)
        return QVariant();
    switch (section) {
    case 0:
        return tr("Katman Adı");
        break;
    default:
        return tr("Görünür");
        break;
    }
}

int QMapLayerModel::rowCount(const QModelIndex &parent) const
{
    if(!parent.isValid())
        return KsWorkspace::SharedInstance()->mapScene()->layers().size();
    else {
        QGraphicsMapLayer *ptr = qobject_cast<QGraphicsMapLayer*>((QObject*)parent.internalPointer());
        if(ptr)
            return ptr->layerObjects().size();
        else
            return 0;
    }
}

int QMapLayerModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}
