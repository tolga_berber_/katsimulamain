#ifndef KSLAYERTREEVIEW_H
#define KSLAYERTREEVIEW_H

#include <QTreeView>
#include "qgraphicsmaplayer.h"
#include "ksgraphicobject.h"

/**
 * @brief Katmanları ve katmandaki nesneleri gösteren treeview.
 *
 */
class KsLayerTreeView : public QTreeView
{
    Q_OBJECT
public:
    /**
     * @brief İlklendirici, Treeview ile ilgili ayarlamaları yapmaktadır.
     *
     */
    explicit KsLayerTreeView(QWidget *parent = 0);

    /**
     * @brief Katman ve nesne görünürlüklerinin otomatik olarak yapılıp yapılmayacağını döndüren fonksiyon.
     *
     * @return true katmanların ve nesnelerin görünürlük özellikleri otomatik olarak güncellenir ilgili sinyal tetiklenir.
     *         false ise katmanların ve nesnelerin görünürlük özellikleri otomatik olarak güncellenmez ilgili sinyaller aracılığı ile bu işlem gerçekleştirilir.
     */
    bool handleVisibility();

signals:
    /**
     * @brief Bir katmanın görünürlüğü değiştirilmek istendiğinde tetiklenecek sinyal (otomatik mod kapalı iken)
     *
     * @param layer Görünürlüğü değişecek katman nesnesi
     */
    void toggleVisibilityOfLayerRequested(QGraphicsMapLayer *layer);

    /**
     * @brief Bir nesnenin görünürlüğü değiştirilmek istendiğinde tetiklenecek sinyal (otomatik mod kapalı iken)
     *
     * @param object Görünürlüğü değişecek nesne
     */
    void toggleVisibilityOfObjectRequested(KsGraphicObject *object);

    /**
     * @brief Bir katmanın görünürlüğü değiştirildiğinde tetiklenecek sinyal (otomatik mod açık iken)
     *
     * @param layer Görünürlüğü değişecek katman nesnesi
     */
    void visibilityOfLayerChanged(QGraphicsMapLayer *layer);

    /**
     * @brief Bir nesnenin görünürlüğü değiştirildiğinde tetiklenecek sinyal (otomatik mod açık iken)
     *
     * @param object Görünürlüğü değişecek nesne
     */
    void visibilityOfObjectChanged(KsGraphicObject *object);

private slots:
    /**
     * @brief Herhangi bir katman'a çift tıklandığında çalışaca eylem.
     *
     * @param index Çift tıklatılan katman.
     */
    void layerDoubleClicked(const QModelIndex &index);

    void layerAdded(QGraphicsMapLayer *layer);

    void objectSelected(QGraphicsMapLayer *layer, KsGraphicObject *obj);

private:
    bool m_bHandleVisibility; /**< Görünürlüğün otomatik yapılıp yapılmayacağını tutan değişken */

};

#endif // KSLAYERTREEVIEW_H
