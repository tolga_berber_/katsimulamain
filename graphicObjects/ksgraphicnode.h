#ifndef KSGRAPHICNODE_H
#define KSGRAPHICNODE_H

#include "ksgraphicobject.h"

class KsNodeObject;

class KsGraphicNode : public KsGraphicObject
{
    Q_OBJECT
public:
    explicit KsGraphicNode(QPointF pnt, QGraphicsItem *parent = 0);
    explicit KsGraphicNode(KsNodeObject *node, QGraphicsItem *parent=0);
    ~KsGraphicNode();


    KsNodeObject *model();
    QPointF location();

    virtual QRectF boundingRect() const;

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

signals:
    void modelChanged(KsNodeObject *model);
    void locationChanged(QPointF loc);

public slots:
    void setModel(KsNodeObject *model);
    void setLocation(QPointF pos);

private slots:
    void updateModelPos(KsGraphicObject *obj, QPointF pnt);
    void updateObjectName(QString str);

private:
    KsNodeObject *m_ptrModel;
    QPointF m_cOrigin;

};


#endif // KSGRAPHICNODE_H
