#ifndef KSMOVEHANDLER_H
#define KSMOVEHANDLER_H

#include "ksgraphicobject.h"
#include <QHash>

class KsRoadObject;

class KsMoveHandler : public KsGraphicObject
{
    Q_OBJECT
public:

    static KsMoveHandler *getHandleForIndex(int index, KsRoadObject *roadObj);
    static void hideAllMoveHandlers();
    static void showMoveHandlersForObject(KsRoadObject *road);

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

signals:

public slots:
    void setRoadObject(KsRoadObject *roadObject);

protected:
    virtual void afterPositionChanged(QPointF newLocation);

private:
    bool m_bIsInMove;
    int m_iIndex;
    explicit KsMoveHandler(QGraphicsItem *parent = 0);
    static QHash<int, KsMoveHandler*> s_hashMoveHandlers;

    KsRoadObject *m_ptrRoadObject;

    QPointF m_pntStartPnt;
};

#endif // KSMOVEHANDLER_H
