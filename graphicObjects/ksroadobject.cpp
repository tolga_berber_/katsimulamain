#include "ksroadobject.h"

#include "graphicObjects/qgraphicsmapscene.h"

#include "db/model/project/ksroadmodelobject.h"
#include "db/model/project/kslaneobject.h"

#include "workspace/ksworkspace.h"

#include "ksmovehandler.h"

#include "utilities/polygonhandlers.h"

#include <cmath>

#include <QMenu>

#include <QtMath>

#include "widgets/ksroadpropertieswidget.h"


#define LINE_LENGTH 0.25
#define DASH_LENGTH 4.5
#define DASH_SPACE_LENGTH 7.5

KsRoadObject::KsRoadObject(QGraphicsItem *parent) :
    KsGraphicObject(parent),
    m_arrRoadLines(NULL),
    m_ptrModel(NULL),
    m_ptrMenu(NULL)
{
    m_colDensity = qRgb(127,255,0);
}

KsRoadObject::KsRoadObject(KsRoadModelObject *model, QGraphicsItem *parent):
    KsGraphicObject(parent),
    m_bInitialMode(false),
    m_arrRoadLines(NULL),
    m_ptrMenu(NULL)
{
    setModel(model);
}

QPolygonF KsRoadObject::roadNormal() const
{
    return m_tempRoadNormal;
}

QRectF KsRoadObject::boundingRect() const
{
    return m_pathBoundary.boundingRect();
}

QPainterPath KsRoadObject::shape() const
{
    return m_pathBoundary;
}

void KsRoadObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);

    float lineWidth = KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*LINE_LENGTH;

    QVector<qreal> dashes;
    qreal space = KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*DASH_SPACE_LENGTH*pow(2.01,KsWorkspace::SharedInstance()->mapScene()->worldItem()->zoomLevel());
    qreal size =  KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*DASH_LENGTH*pow(2.01,KsWorkspace::SharedInstance()->mapScene()->worldItem()->zoomLevel());
    dashes<<size<<space;

    QBrush normalBrush(qRgb(196,196,196));
    QBrush densityBrush(m_colDensity);
    QBrush selectedBrush(qRgb(196,196,196));

    QPen normalBoundaryPen(Qt::black);
    QPen normalLanePen(Qt::white);
    QPen selectedBoundaryPen(Qt::black);
    QPen selectedLanePen(Qt::black);

    normalBoundaryPen.setWidthF(lineWidth);
    normalLanePen.setWidthF(lineWidth);
    normalLanePen.setDashPattern(dashes);

    selectedBoundaryPen.setWidthF(lineWidth);
    selectedLanePen.setWidthF(lineWidth);
    selectedLanePen.setDashPattern(dashes);

    if(isSelected()) {
        painter->setBrush(selectedBrush);
    } else {
        painter->setBrush(normalBrush);
    }

    painter->fillPath(m_pathBoundary, painter->brush());
    painter->setPen(isSelected()?selectedBoundaryPen:normalBoundaryPen);
    painter->drawPolyline(m_arrRoadLines[0]);

    painter->setPen(isSelected()?selectedLanePen:normalLanePen);
    for(int i=1;i<numOfLanes();i++) {
        painter->drawPolyline(m_arrRoadLines[i]);
    }
    painter->setPen(isSelected()?selectedBoundaryPen:normalBoundaryPen);
    painter->drawPolyline(m_arrRoadLines[numOfLanes()]);
}

int KsRoadObject::numberOfControlPoints()
{
    return m_tempRoadNormal.length();
}

void KsRoadObject::addPointToRoadNormal(QPointF normalPoint)
{
    m_tempRoadNormal.append(normalPoint);
    if(!m_bInitialMode)
        m_ptrModel->setRoadNormal(m_tempRoadNormal);
    updateRoadGeometry();
    //TODO Başlangıç ve bitiş noktaları için edge güncellemek lazım.
}

void KsRoadObject::setNormalPoint(int index, QPointF point)
{
    if(m_tempRoadNormal[index]!=point) {
        m_tempRoadNormal[index] = point;
        updateRoadGeometry();
        if(!m_bInitialMode)
            m_ptrModel->setRoadNormal(m_tempRoadNormal);
    }
}

void KsRoadObject::removeNormalPoint(int index)
{
    m_tempRoadNormal.removeAt(index);
    updateRoadGeometry();
    if(!m_bInitialMode)
        m_ptrModel->setRoadNormal(m_tempRoadNormal);
}

void KsRoadObject::updatePath()
{
    for(int i=0;i<m_arrRoadLines[0].length()-1;i++) {
        if(!i) {
            m_pathBoundary.moveTo(m_arrRoadLines[0][0]);
        }
        m_pathBoundary.lineTo(m_arrRoadLines[0][i+1]);
    }

    for(int i=m_arrRoadLines[numOfLanes()].length()-1;i>=0;i--) {
        m_pathBoundary.lineTo(m_arrRoadLines[numOfLanes()][i]);
        if(!i) {
            m_pathBoundary.lineTo(m_arrRoadLines[0][0]);
        }
    }
}

KsRoadModelObject *KsRoadObject::model()
{
    return m_ptrModel;
}

QPointF KsRoadObject::startPoint()
{
    return m_tempRoadNormal.first();
}

QPointF KsRoadObject::endPoint()
{
    return m_tempRoadNormal.last();
}

void KsRoadObject::updateRoadGeometry()
{
    prepareGeometryChange();
    if(m_ptrModel && m_ptrModel->isCurve()) {
        m_tempBezier = PolygonHandlers::calculateBezier(m_tempRoadNormal);

        if(m_arrRoadLines) {
            delete[] m_arrRoadLines;
        }

        m_pathBoundary = QPainterPath();
        m_arrRoadLines = new QPolygonF[numOfLanes()+1];

        float roadWidth = KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*(m_ptrModel?m_ptrModel->roadWidth():5);

        if(m_tempRoadNormal.length()>=2) {
            float width = -roadWidth/2;
            m_arrRoadLines[0] = PolygonHandlers::calculateParalelPolygon(m_tempBezier, width);
            for(int i=0;i<numOfLanes();i++) {
                width += KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*(m_ptrModel?(*m_ptrModel)[i]->laneWidth():2.5);
                m_arrRoadLines[i+1] = PolygonHandlers::calculateParalelPolygon(m_tempBezier, width);
            }
            updatePath();
        } else {
            m_pathBoundary.moveTo(m_tempRoadNormal[0]);
        }

    } else {
        if(m_arrRoadLines) {
            delete[] m_arrRoadLines;
        }

        m_pathBoundary = QPainterPath();
        m_arrRoadLines = new QPolygonF[numOfLanes()+1];

        float roadWidth = KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*(m_ptrModel?m_ptrModel->roadWidth():5);

        if(m_tempRoadNormal.length()>=2) {
            float width = -roadWidth/2;
            m_arrRoadLines[0] = PolygonHandlers::calculateParalelPolygon(m_tempRoadNormal, width);
            for(int i=0;i<numOfLanes();i++) {
                width += KsWorkspace::SharedInstance()->mapScene()->oneMeterInDegrees()*(m_ptrModel?(*m_ptrModel)[i]->laneWidth():2.5);
                m_arrRoadLines[i+1] = PolygonHandlers::calculateParalelPolygon(m_tempRoadNormal, width);
            }
            updatePath();
        } else {
            m_pathBoundary.moveTo(m_tempRoadNormal[0]);
        }
    }
}

void KsRoadObject::setInitialMode(bool mode)
{
    if(m_bInitialMode!=mode) {
        m_bInitialMode = mode;

        if(!m_bInitialMode)
            m_ptrModel->setRoadNormal(m_tempRoadNormal);
    }
}

void KsRoadObject::setModel(KsRoadModelObject *model)
{
    if(m_ptrModel!=model) {
        m_ptrModel = model;
        if(m_bInitialMode) {
            model->setRoadNormal(m_tempRoadNormal);
        } else {
            m_tempRoadNormal = model->roadNormal();
        }
        updateRoadGeometry();
        connect(m_ptrModel, SIGNAL(curveChanged(bool)), this, SLOT(updateRoadGeometry()));
        connect(m_ptrModel, SIGNAL(laneCountChanged(qint32)), this, SLOT(updateRoadGeometry()));
        connect(m_ptrModel, SIGNAL(roadNormalChanged(QPolygonF)), this, SLOT(updateRoadNormal(QPolygonF)));

        setObjectName(m_ptrModel->name());

        setParent(model);

        emit modelChanged(m_ptrModel);
    }
}

void KsRoadObject::updateRoadNormal(QPolygonF pol)
{
    if(m_tempRoadNormal != pol) {
        m_tempRoadNormal = pol;

        updateRoadGeometry();
    }
}

void KsRoadObject::handlePopupActions(QAction *act)
{
    if(act->text()==tr("Yol Özellikleri")) {
        KsRoadPropertiesWidget *wgt = new KsRoadPropertiesWidget(m_ptrModel);

        if(wgt->exec()==QDialog::Accepted) {
        } else {
        }
    }
}


int KsRoadObject::numOfLanes()
{
    return m_ptrModel?m_ptrModel->laneCount():2;
}

bool KsRoadObject::hasContextMenu()
{
    return true;
}

QMenu *KsRoadObject::getPopupMenu()
{
    if(m_ptrMenu)
        return m_ptrMenu;

    m_ptrMenu = new QMenu;
    m_ptrMenu->addAction(tr("Yol Özellikleri"));

    connect(m_ptrMenu,SIGNAL(triggered(QAction*)),
            this, SLOT(handlePopupActions(QAction*)));
    return m_ptrMenu;
}

void KsRoadObject::afterItemSelected(uint newValue)
{
    if(newValue) {
        KsMoveHandler::showMoveHandlersForObject(this);
    } else {
        KsMoveHandler::hideAllMoveHandlers();
    }

    KsGraphicObject::afterItemSelected(newValue);
}


