#ifndef QGRAPHICSMAPLAYER_H
#define QGRAPHICSMAPLAYER_H

#include <QObject>
#include <QGraphicsObject>
#include <QList>
#include "ksgraphicobject.h"

class KsGraphicObject;

/**
 * @brief Grafik nesnelere ait listeyi tutmak için kullanılacak veri yapısı.
 *
 */
typedef QList<KsGraphicObject*> GraphicObjectList;

/**
 * @brief Sahne üzerindeki bir katmanı göstermek için kullanılacak katman nesnesi.
 *
 */
class QGraphicsMapLayer : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief İlklendirici fonksiyon.
     *
     *        Burada katmanın saklayacağı grafik nesnelerine ait liste oluşturulur.
     *
     * @param parent Göz ardı edilebilir, Qt uyumluluğu için kullanılmaktadır.
     */
    explicit QGraphicsMapLayer(QObject *parent = 0);

    /**
     * @brief Katmandaki nesnelerin listesini öğrenmek amacıyla kullanılır.
     *
     * @return const GraphicObjectList Grafik nesne listesi.
     */
    const GraphicObjectList layerObjects();

    /**
     * @brief Katman türü için kullanılacak numaralandırma.
     *
     *        Uygulamada (şimdilik) 2 adet katman yeralmaktadır. Bunlardan birincisi harita katmanı ikincisi de nesne katmanıdır.
     * Bu numaralandırma sayesinde katman türleri belirlenmektedir.
     *
     */
    typedef enum {
        LT_World=0, /**< Dünya Katmanı */
        LT_TrafficObjects, /**< Nesne Katmanı */
        LT_Links
    } LayerType;

    /**
     * @brief Katmanın türün öğrenmek amacıyla kullanılan fonksiyondur.
     *
     * @return LayerType Katman türü @see LayerType
     */
    LayerType layerType();

    /**
     * @brief Katmanın görünürlüğünü döndüren fonksiyon.
     *
     * @return bool Katman görünürlüğü.
     */
    bool layerVisibility();

    /**
     * @brief Katmanın Z değeri.
     *
     * @return qreal Katmanın Z değeri
     */
    qreal layerZValue();

signals:

    /**
     * @brief Katmanın Z değeri @see setZ(qreal) fonksiyonu ile değiştirildiğinde ateşlenecek sinyal.
     *
     * @param newZ Yeni Z değeri.
     */
    void zChanged(qreal newZ);

    /**
     * @brief Katmanın görünürlüğü @see setVisible(bool) fonksiyonu ile değiştirildiğinde ateşlenecek sinyal.
     *
     * @param newVisibility Yeni Katman görünürlüğü.
     */
    void visibleChanged(bool newVisibility);

    /**
     * @brief Katmanın türü @see setLayerType(LayerType) fonksiyonu ile değiştirildiğinde ateşlenecek sinyal.
     *
     * @param newType Yeni Katman türü.
     */
    void typeChanged(LayerType newType);

    void newObjectAdded(QGraphicsMapLayer *layer, KsGraphicObject *object);

    void objectRemoved(QGraphicsMapLayer *layer, KsGraphicObject *object);

    void objectAtIndexRemoved(QGraphicsMapLayer *layer, int idx);

    void objectOfLayerSelected(QGraphicsMapLayer *layer, KsGraphicObject* object);
    void objectOfLayerDeSelected(QGraphicsMapLayer *layer, KsGraphicObject* object);


public slots:

    /**
     * @brief Katmanı gizleyip göstermek için kullanılan fonksiyondur.
     *
     * @param visible Katmanın yeni durumunu belirtir.
     */
    void setVisible(bool visible);

    /**
     * @brief Katmanı öne veya arkaya getirmek için kullanılacak fonksiyondur.
     *
     * @param z Yeni Z değeri (Nesne katmanları için 0 dan büyük, harita katmanı için 0'dan küçük olmalıdır.)
     */
    void setZ(qreal z);

    /**
     * @brief Katman türünü belirlemek amacıyla kullanılır. Harita katmanlarının Z indexi <b>pozitif</b> olamaz.
     *
     * @param type Yeni katman türü.
     */
    void setLayerType(LayerType type);

    void lockObjects();

    void unlockObjects();

private slots:
    void handleItemSelection(KsGraphicObject *obj, bool newValue);

public slots:
    /**
     * @brief Katmana grafik nesnesi ekler.
     *
     *        Katmana grafik nesnesi eklemek için kullanılan fonksiyondur.
     *
     * @param obj Eklenecek Grafik Nesnesi.
     */
    void addObject(KsGraphicObject *obj);

    /**
     * @brief Katmandan grafik nesnesi siler.
     *
     *        Katmandan grafik nesnesi silmek için kullanılan fonksiyondur.
     *
     * @param obj Silinecek Grafik Nesnesi.
     */
    void removeObject(KsGraphicObject *obj, bool deleteIt=true);

    void handleObjectDestroy();

private:
    GraphicObjectList m_cLayerObjects; /**< Katmandaki nesnelerin listesi */
    qreal m_fZValue; /**< Katmanın Z Indexi */
    bool m_bVisible; /**< Katmanın görünür olup olmadığını tutan değişken */
    LayerType m_eType; /**< Katman türü için kullanılır. */
};

#endif // QGRAPHICSMAPLAYER_H
