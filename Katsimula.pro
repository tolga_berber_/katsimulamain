#-------------------------------------------------
#
# Project created by QtCreator 2014-06-20T18:32:52
#
#-------------------------------------------------

QT       += core gui sql network opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

RC_FILE = exeproperties.rc

macx-g++: include(/Users/tolgaberber/Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowser.pri)
macx-clang: include(/Users/tolgaberber/Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowser.pri)
win32: include(C:/Qt/5.4/src/qttools/src/shared/qtpropertybrowser/qtpropertybrowser.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    cache/qtiledatabase.cpp \
    graphicObjects/qgraphicsmapscene.cpp \
    graphicObjects/qgraphicsmapview.cpp \
    graphicObjects/qgraphicstileitem.cpp \
    graphicObjects/qgraphicsworlditem.cpp \
    network/qtiledownloader.cpp \
    kssplashscreen.cpp \
    mapProviders/qmapproviderstore.cpp \
    mapProviders/providers/qtilemapprovider.cpp \
    mapProviders/providers/qosmstandardprovider.cpp \
    mapProviders/providers/qocmstandardprovider.cpp \
    mapProviders/providers/qgooglehybridprovider.cpp \
    Actions/qactionstore.cpp \
    Actions/RoadNetActions/ksnewaction.cpp \
    Actions/fileActions/ksnewprojectaction.cpp \
    Actions/ksabstractaction.cpp \
    Actions/fileActions/ksopenprojectaction.cpp \
    Actions/RoadNetActions/kscheckroadnetwork.cpp \
    Actions/fileActions/ksscenarios.cpp \
    Actions/fileActions/ksaddscenario.cpp \
    Actions/fileActions/ksremovescenario.cpp \
    Actions/editActions/kscopy.cpp \
    Actions/editActions/kscut.cpp \
    Actions/editActions/kspaste.cpp \
    Actions/editActions/ksundo.cpp \
    Actions/editActions/ksredo.cpp \
    Actions/viewActions/ksfullscreen.cpp \
    Actions/viewActions/kszoomin.cpp \
    Actions/viewActions/kszoomout.cpp \
    workspace/ksworkspace.cpp \
    graphicObjects/qgraphicsmaplayer.cpp \
    graphicObjects/qmaplayermodel.cpp \
    graphicObjects/ksgraphicobject.cpp \
    widgets/kslayerwidget.cpp \
    graphicObjects/kslayertreeview.cpp \
    workspace/ksdirectorymanager.cpp \
    tileSource/kstilesource.cpp \
    utilities/ksgeometryhelper.cpp \
    interactions/ksinteractionstore.cpp \
    interactions/ksabstractinteraction.cpp \
    interactions/ksdefaultinteraction.cpp \
    mapProviders/providers/ksmapprovideraction.cpp \
    graphicObjects/ksgraphicnode.cpp \
    interactions/ksnodecreateinteraction.cpp \
    interactions/ksinteractionaction.cpp \
    storage/ksproject.cpp \
    interactions/kslinkcreateinteraction.cpp \
    storage/kssettingsstorage.cpp \
    graphicObjects/ksroadobject.cpp \
    graphicObjects/ksroadconnector.cpp \
    Actions/fileActions/ksclearcacheaction.cpp \
    graphicObjects/ksmovehandler.cpp \
    propertyEditors/kspropertyeditorstore.cpp \
    propertyEditors/propertyEditors/ksworldpropertyeditor.cpp \
    widgets/propertyeditorholder.cpp \
    utilities/polygonhandlers.cpp \
    widgets/carlist.cpp \
    Actions/dbActions/ksaraceditoraction.cpp \
    widgets/yeniaractanimlama.cpp \
    Actions/simulation/ksstartsimulation.cpp \
    Actions/simulation/kspausesimulation.cpp \
    Actions/simulation/ksstopsimulation.cpp \
    Actions/dbActions/kssimulationresultreport.cpp \
    utilities/ksdbutilities.cpp \
    db/ksobjectstore.cpp \
    db/ksstaticobjectstore.cpp \
    db/model/ksvehicle.cpp \
    db/ksprojectobjectstore.cpp \
    db/model/project/kslaneobject.cpp \
    db/model/project/ksroadmodelobject.cpp \
    db/model/project/ksnodeobject.cpp \
    graf/ksgraf.cpp \
    Actions/fileActions/kssaveaction.cpp \
    propertyEditors/propertyEditors/ksnodepropertyeditor.cpp \
    propertyEditors/propertyEditors/ksroadpropertyeditor.cpp \
    interactions/ksintersectioncreateinteraction.cpp \
    widgets/ksroadpropertieswidget.cpp \
    Simulation/kssimulationengine.cpp \
    db/model/simulation/kssimulationvehicle.cpp \
    db/model/simulation/kssimulationlocation.cpp \
    db/model/simulation/kssimulationspeed.cpp \
    db/model/simulation/kssimulationlane.cpp \
    db/model/simulation/kssimulationinfo.cpp \
    db/model/simulation/kssimulationcommondata.cpp \
    db/model/simulation/kssimulationcarbonemissions.cpp \
    db/ksspontaneousdb.cpp

HEADERS  += mainwindow.h \
    cache/qtiledatabase.h \
    graphicObjects/qgraphicsmapscene.h \
    graphicObjects/qgraphicsmapview.h \
    graphicObjects/qgraphicstileitem.h \
    graphicObjects/qgraphicsworlditem.h \
    graphicObjects/tilestorage.h \
    network/qtiledownloader.h \
    kssplashscreen.h \
    mapProviders/qmapproviderstore.h \
    mapProviders/providers/mapprovider.h \
    mapProviders/providers/qtilemapprovider.h \
    mapProviders/providers/qosmstandardprovider.h \
    mapProviders/providers/qocmstandardprovider.h \
    mapProviders/providers/qgooglehybridprovider.h \
    Actions/qactionstore.h \
    Actions/actionstore.h \
    Actions/RoadNetActions/ksnewaction.h \
    Actions/KsAction.h \
    Actions/fileActions/ksnewprojectaction.h \
    Actions/ksabstractaction.h \
    Actions/fileActions/ksopenprojectaction.h \
    Actions/RoadNetActions/kscheckroadnetwork.h \
    Actions/allactions.h \
    Actions/fileActions/ksscenarios.h \
    Actions/fileActions/ksaddscenario.h \
    Actions/fileActions/ksremovescenario.h \
    Actions/editActions/kscopy.h \
    Actions/editActions/kscut.h \
    Actions/editActions/kspaste.h \
    Actions/editActions/ksundo.h \
    Actions/editActions/ksredo.h \
    Actions/viewActions/ksfullscreen.h \
    Actions/viewActions/kszoomin.h \
    Actions/viewActions/kszoomout.h \
    workspace/ksworkspace.h \
    graphicObjects/qgraphicsmaplayer.h \
    graphicObjects/qmaplayermodel.h \
    graphicObjects/ksgraphicobject.h \
    widgets/kslayerwidget.h \
    graphicObjects/kslayertreeview.h \
    workspace/ksdirectorymanager.h \
    tileSource/kstilesource.h \
    utilities/ksgeometryhelper.h \
    utilities/BoostGeometryHelpers.h \
    interactions/ksinteractionstore.h \
    interactions/ksabstractinteraction.h \
    interactions/kskeyboardinteraction.h \
    interactions/ksmouseinteraction.h \
    interactions/ksdefaultinteraction.h \
    mapProviders/providers/ksmapprovideraction.h \
    graphicObjects/ksgraphicnode.h \
    interactions/ksnodecreateinteraction.h \
    interactions/ksinteractionaction.h \
    storage/ksproject.h \
    interactions/kslinkcreateinteraction.h \
    storage/kssettingsstorage.h \
    graphicObjects/ksroadobject.h \
    graphicObjects/ksroadconnector.h \
    Actions/fileActions/ksclearcacheaction.h \
    graphicObjects/ksmovehandler.h \
    propertyEditors/kspropertyeditorstore.h \
    propertyEditors/propertyEditors/ksworldpropertyeditor.h \
    propertyEditors/kspropertyeditorbridge.h \
    propertyEditors/propertyeditorconstants.h \
    widgets/propertyeditorholder.h \
    utilities/polygonhandlers.h \
    widgets/carlist.h \
    Actions/dbActions/ksaraceditoraction.h \
    widgets/yeniaractanimlama.h \
    Actions/simulation/ksstartsimulation.h \
    Actions/simulation/kspausesimulation.h \
    Actions/simulation/ksstopsimulation.h \
    Actions/dbActions/kssimulationresultreport.h \
    utilities/ksdbutilities.h \
    db/ksobjectstore.h \
    db/ksstaticobjectstore.h \
    db/model/ksvehicle.h \
    db/ksprojectobjectstore.h \
    db/model/project/kslaneobject.h \
    db/model/project/ksroadmodelobject.h \
    db/model/project/ksnodeobject.h \
    graf/ksgraf.h \
    Actions/fileActions/kssaveaction.h \
    propertyEditors/propertyEditors/ksnodepropertyeditor.h \
    propertyEditors/propertyEditors/ksroadpropertyeditor.h \
    interactions/ksintersectioncreateinteraction.h \
    widgets/ksroadpropertieswidget.h \
    Simulation/kssimulationengine.h \
    db/model/simulation/kssimulationvehicle.h \
    db/model/simulation/kssimulationlocation.h \
    db/model/simulation/kssimulationspeed.h \
    db/model/simulation/kssimulationlane.h \
    db/model/simulation/kssimulationinfo.h \
    db/model/simulation/kssimulationcommondata.h \
    db/model/simulation/kssimulationcarbonemissions.h \
    db/ksspontaneousdb.h

FORMS    += \
    widgets/kslayerwidget.ui \
    widgets/propertyeditorholder.ui \
    widgets/carlist.ui \
    widgets/yeniaractanimlama.ui \
    widgets/ksroadpropertieswidget.ui

RESOURCES += \
    gui_resource.qrc

INCLUDEPATH += .


win32:DEFINES += _USE_MATH_DEFINES

TRANSLATIONS = languages/GUI_tr.ts \
    languages/GUI_en.ts

CODECFORSRC = UTF-8

OTHER_FILES += \
    exeproperties.rc

# Kütüphane Kullanımı
win32-msvc2013 {
    QMAKE_CXXFLAGS += /openmp
    INCLUDEPATH += C:\Projects\boost
    DEFINES += BOOST_NO_CXX_DEFAULTED_FUNCTIONS BOOST_NO_CXX11_DEFAULTED_FUNCTIONS
}

win32-msvc2013_64 {
    QMAKE_CXXFLAGS += /openmp
    INCLUDEPATH += C:\Projects\boost
    DEFINES += BOOST_NO_CXX_DEFAULTED_FUNCTIONS BOOST_NO_CXX11_DEFAULTED_FUNCTIONS
}

win32-g++ {
    QMAKE_CXXFLAGS += -fopenmp -fpermissive
    QMAKE_LFLAGS += -fopenmp -fpermissive
    INCLUDEPATH += C:\Projects\boost
}

linux {
    QMAKE_CXXFLAGS += -fopenmp -isystem /Users/tolga/libs/boost/include/boost-1_55
    QMAKE_LFLAGS += -fopenmp
}

macx-clang {
    QMAKE_CXXFLAGS += -isystem /usr/local/include
    QMAKE_CXXFLAGS += -fpermissive
    QMAKE_LFLAGS += -fpermissive

}

macx-g++ {
    QMAKE_CXXFLAGS += -isystem /usr/local/include
    QMAKE_CXXFLAGS += -fpermissive
    QMAKE_LFLAGS += -fpermissive
}

android-g++ {
    QMAKE_CXXFLAGS += -fopenmp -isystem /usr/local/include
    QMAKE_LFLAGS += -fopenmp
}

DISTFILES +=
