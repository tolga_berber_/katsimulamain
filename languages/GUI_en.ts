<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CarList</name>
    <message>
        <location filename="../widgets/carlist.ui" line="14"/>
        <location filename="../widgets/carlist.ui" line="22"/>
        <source>Tanımlı Araçlar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.ui" line="67"/>
        <source>Yeni Araç Ekle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.ui" line="93"/>
        <source>Araç Sil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.ui" line="119"/>
        <source>Kapat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.cpp" line="84"/>
        <source>Seçili aracı silmek istediğinize emin misiniz?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.cpp" line="85"/>
        <source>Bu işlem geri alınamaz!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.cpp" line="87"/>
        <source>Evet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/carlist.cpp" line="88"/>
        <source>Hayır</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsAbstractAction</name>
    <message>
        <location filename="../Actions/ksabstractaction.cpp" line="37"/>
        <source>Henüz Hazır Değil!</source>
        <translation>Not Ready Yet!</translation>
    </message>
    <message>
        <location filename="../Actions/ksabstractaction.cpp" line="38"/>
        <source>Yazılımcılarımız bu fonksiyon üzerinde çalışmaktadırlar! Sabrınız için teşekkür ederiz.</source>
        <translation>Our developers are working hard to complete this function! Thanks for your patient.</translation>
    </message>
</context>
<context>
    <name>KsAbstractInteraction</name>
    <message>
        <location filename="../interactions/ksabstractinteraction.cpp" line="71"/>
        <source>Belirlenmedi</source>
        <translation>Not Specified</translation>
    </message>
    <message>
        <location filename="../interactions/ksabstractinteraction.cpp" line="76"/>
        <source>NONE</source>
        <translation>NONE</translation>
    </message>
</context>
<context>
    <name>KsAddScenario</name>
    <message>
        <location filename="../Actions/fileActions/ksaddscenario.cpp" line="6"/>
        <source>Senaryo İşlemleri</source>
        <translation>Scenario</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksaddscenario.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksaddscenario.cpp" line="9"/>
        <source>Senaryo
Ekle</source>
        <translation>Add
Scenario</translation>
    </message>
</context>
<context>
    <name>KsAracEditorAction</name>
    <message>
        <location filename="../Actions/dbActions/ksaraceditoraction.cpp" line="9"/>
        <source>Sabit Bilgiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/dbActions/ksaraceditoraction.cpp" line="10"/>
        <source>VERİTABANI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/dbActions/ksaraceditoraction.cpp" line="12"/>
        <source>Tanımlı
Araçlar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsCheckRoadNetwork</name>
    <message>
        <location filename="../Actions/RoadNetActions/kscheckroadnetwork.cpp" line="6"/>
        <source>Ağ İşlemleri</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../Actions/RoadNetActions/kscheckroadnetwork.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/RoadNetActions/kscheckroadnetwork.cpp" line="9"/>
        <source>Ağı
Kontrol Et</source>
        <translation>Check
Road Net</translation>
    </message>
</context>
<context>
    <name>KsClearCacheAction</name>
    <message>
        <location filename="../Actions/fileActions/ksclearcacheaction.cpp" line="9"/>
        <source>Diğer İşlemler</source>
        <translation>Other</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksclearcacheaction.cpp" line="10"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksclearcacheaction.cpp" line="12"/>
        <source>Önbelleği
Temizle</source>
        <translation>Clear
Cache</translation>
    </message>
</context>
<context>
    <name>KsCopy</name>
    <message>
        <location filename="../Actions/editActions/kscopy.cpp" line="6"/>
        <source>Pano</source>
        <translation>Clipboard</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kscopy.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kscopy.cpp" line="9"/>
        <source>Kopyala</source>
        <translation>Copy</translation>
    </message>
</context>
<context>
    <name>KsCut</name>
    <message>
        <location filename="../Actions/editActions/kscut.cpp" line="6"/>
        <source>Pano</source>
        <translation>Clipboard</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kscut.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kscut.cpp" line="9"/>
        <source>Kes</source>
        <translation>Cut</translation>
    </message>
</context>
<context>
    <name>KsDefaultInteraction</name>
    <message>
        <location filename="../interactions/ksdefaultinteraction.cpp" line="79"/>
        <source>Seçim Aracı</source>
        <translation>Select Tool</translation>
    </message>
    <message>
        <location filename="../interactions/ksdefaultinteraction.cpp" line="84"/>
        <source>DEFAULT</source>
        <translation>DEFAULT</translation>
    </message>
</context>
<context>
    <name>KsFullScreen</name>
    <message>
        <location filename="../Actions/viewActions/ksfullscreen.cpp" line="7"/>
        <source>Çalışma Alanı</source>
        <translation>Workspace</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/ksfullscreen.cpp" line="8"/>
        <source>GÖRÜNÜM</source>
        <translation>VIEW</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/ksfullscreen.cpp" line="10"/>
        <source>Tam Ekran</source>
        <translation>Full Screen</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/ksfullscreen.cpp" line="12"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
</context>
<context>
    <name>KsInteractionAction</name>
    <message>
        <location filename="../interactions/ksinteractionaction.cpp" line="9"/>
        <source>Araçlar</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="../interactions/ksinteractionaction.cpp" line="10"/>
        <source>ÇİZİM ARAÇLARI</source>
        <translation>DRAWING</translation>
    </message>
    <message>
        <location filename="../interactions/ksinteractionaction.cpp" line="11"/>
        <source>:/img/action/draw/%1.png</source>
        <translation>:/img/action/draw/%1.png</translation>
    </message>
</context>
<context>
    <name>KsIntersectionCreateInterAction</name>
    <message>
        <location filename="../interactions/ksintersectioncreateinteraction.cpp" line="16"/>
        <source>İsimsiz Kavşak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interactions/ksintersectioncreateinteraction.cpp" line="56"/>
        <source>INTERSECTION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interactions/ksintersectioncreateinteraction.cpp" line="61"/>
        <source>Kavşak
Ekleme</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsLayerWidget</name>
    <message>
        <location filename="../widgets/kslayerwidget.ui" line="17"/>
        <source>Katmanlar</source>
        <translation>Layers</translation>
    </message>
    <message>
        <location filename="../widgets/kslayerwidget.ui" line="106"/>
        <source>Katman Ekle</source>
        <translation>Add Layer</translation>
    </message>
    <message>
        <location filename="../widgets/kslayerwidget.ui" line="115"/>
        <source>katmanSil</source>
        <translation>Delete Layer</translation>
    </message>
    <message>
        <location filename="../widgets/kslayerwidget.ui" line="124"/>
        <source>katmanYenidenAdlandir</source>
        <translation>Rename Layer</translation>
    </message>
</context>
<context>
    <name>KsLinkCreateInteraction</name>
    <message>
        <source>Yol %1</source>
        <translation type="vanished">Road %1</translation>
    </message>
    <message>
        <location filename="../interactions/kslinkcreateinteraction.cpp" line="48"/>
        <source>İsimsiz Yol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interactions/kslinkcreateinteraction.cpp" line="118"/>
        <source>LINKDRAW</source>
        <translation>LINKDRAW</translation>
    </message>
    <message>
        <location filename="../interactions/kslinkcreateinteraction.cpp" line="123"/>
        <source>Yol Çiz</source>
        <translation>Draw Road</translation>
    </message>
</context>
<context>
    <name>KsMapProviderAction</name>
    <message>
        <location filename="../mapProviders/providers/ksmapprovideraction.cpp" line="8"/>
        <source>Harita</source>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../mapProviders/providers/ksmapprovideraction.cpp" line="9"/>
        <source>GÖRÜNÜM</source>
        <translation>VIEW</translation>
    </message>
    <message>
        <location filename="../mapProviders/providers/ksmapprovideraction.cpp" line="10"/>
        <source>:/img/action/view/%1.png</source>
        <translation>:/img/action/view/%1.png</translation>
    </message>
</context>
<context>
    <name>KsNewAction</name>
    <message>
        <location filename="../Actions/RoadNetActions/ksnewaction.cpp" line="6"/>
        <source>Ağ İşlemleri</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../Actions/RoadNetActions/ksnewaction.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/RoadNetActions/ksnewaction.cpp" line="9"/>
        <source>Yeni Yol Ağı</source>
        <translation>New Road Net</translation>
    </message>
</context>
<context>
    <name>KsNewProjectAction</name>
    <message>
        <location filename="../Actions/fileActions/ksnewprojectaction.cpp" line="7"/>
        <source>Proje İşlemleri</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksnewprojectaction.cpp" line="8"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksnewprojectaction.cpp" line="10"/>
        <source>Yeni Proje</source>
        <translation>New Project</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksnewprojectaction.cpp" line="11"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsNodeCreateInteraction</name>
    <message>
        <source>Trafik Kaynağı/Hedefi</source>
        <translation type="vanished">Traffic Source/Target</translation>
    </message>
    <message>
        <location filename="../interactions/ksnodecreateinteraction.cpp" line="16"/>
        <source>Trafik Kaynağı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../interactions/ksnodecreateinteraction.cpp" line="56"/>
        <source>ADDNODE</source>
        <translation>ADDNODE</translation>
    </message>
    <message>
        <location filename="../interactions/ksnodecreateinteraction.cpp" line="61"/>
        <source>Trafik Kaynağı
Ekleme</source>
        <translation>Add Traffic
Source</translation>
    </message>
</context>
<context>
    <name>KsNodePropertyEditor</name>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksnodepropertyeditor.cpp" line="84"/>
        <source>Kaynak Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksnodepropertyeditor.cpp" line="85"/>
        <source>Kaynak Adı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksnodepropertyeditor.cpp" line="88"/>
        <source>Kaynağın programdaki ID si.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsObjectStore</name>
    <message>
        <location filename="../db/ksobjectstore.cpp" line="91"/>
        <source>drop table %1;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../db/ksobjectstore.cpp" line="108"/>
        <source>PRAGMA table_info(%1);</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsOpenProjectAction</name>
    <message>
        <location filename="../Actions/fileActions/ksopenprojectaction.cpp" line="10"/>
        <source>Proje İşlemleri</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksopenprojectaction.cpp" line="11"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksopenprojectaction.cpp" line="13"/>
        <source>Proje Aç</source>
        <translation>Open Project</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksopenprojectaction.cpp" line="14"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksopenprojectaction.cpp" line="21"/>
        <source>Proje Klasörünü Seçin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsPaste</name>
    <message>
        <location filename="../Actions/editActions/kspaste.cpp" line="6"/>
        <source>Pano</source>
        <translation>Clipboard</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kspaste.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/kspaste.cpp" line="9"/>
        <source>Yapıştır</source>
        <translation>Paste</translation>
    </message>
</context>
<context>
    <name>KsPauseSimulation</name>
    <message>
        <location filename="../Actions/simulation/kspausesimulation.cpp" line="5"/>
        <source>Simülasyon Kontrolü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/kspausesimulation.cpp" line="6"/>
        <source>SİMÜLASYON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/kspausesimulation.cpp" line="8"/>
        <source>Duraklat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsProject</name>
    <message>
        <location filename="../storage/ksproject.cpp" line="58"/>
        <source>Yeni Proje Klasörü Seçin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../storage/ksproject.cpp" line="67"/>
        <location filename="../storage/ksproject.cpp" line="88"/>
        <location filename="../storage/ksproject.cpp" line="96"/>
        <location filename="../storage/ksproject.cpp" line="182"/>
        <location filename="../storage/ksproject.cpp" line="183"/>
        <source>%1/%2_design.ksdf</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsRedo</name>
    <message>
        <location filename="../Actions/editActions/ksredo.cpp" line="6"/>
        <source>Düzen</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/ksredo.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/ksredo.cpp" line="9"/>
        <source>Yinele</source>
        <translation>Redo</translation>
    </message>
</context>
<context>
    <name>KsRemoveScenario</name>
    <message>
        <location filename="../Actions/fileActions/ksremovescenario.cpp" line="6"/>
        <source>Senaryo İşlemleri</source>
        <translation>Scenario</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksremovescenario.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksremovescenario.cpp" line="9"/>
        <source>Senaryo
Sil</source>
        <translation>Delete
Scenario</translation>
    </message>
</context>
<context>
    <name>KsRoadObject</name>
    <message>
        <location filename="../graphicObjects/ksroadobject.cpp" line="268"/>
        <location filename="../graphicObjects/ksroadobject.cpp" line="294"/>
        <source>Yol Özellikleri</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsRoadPropertiesWidget</name>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="26"/>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="34"/>
        <source>Yol Özellikleri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="40"/>
        <source>Yol Uzunluğu : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="57"/>
        <source>Yol Adı :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="64"/>
        <source>Tasarım Hızı :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="71"/>
        <source>Eğim :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="95"/>
        <source>Şerit Sayısı :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="105"/>
        <source>Şeritler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="121"/>
        <source>Tamam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.ui" line="128"/>
        <source>İptal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/ksroadpropertieswidget.cpp" line="37"/>
        <source>%1 Yoluna Ait Özellikler</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsRoadPropertyEditor</name>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="136"/>
        <source>Kaynak Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="137"/>
        <source>Kaynak Adı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="138"/>
        <source>Şerit Sayısı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="139"/>
        <source>Eğrileştirme Aktif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="140"/>
        <source>Tasarım Hızı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="141"/>
        <source>Eğim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksroadpropertyeditor.cpp" line="144"/>
        <source>Kaynağın programdaki ID si.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsSaveAction</name>
    <message>
        <location filename="../Actions/fileActions/kssaveaction.cpp" line="9"/>
        <source>Proje İşlemleri</source>
        <translation type="unfinished">Project</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/kssaveaction.cpp" line="10"/>
        <source>GİRİŞ</source>
        <translation type="unfinished">BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/kssaveaction.cpp" line="12"/>
        <source>Kaydet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsScenarios</name>
    <message>
        <location filename="../Actions/fileActions/ksscenarios.cpp" line="6"/>
        <source>Senaryo İşlemleri</source>
        <translation>Scenario</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksscenarios.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/fileActions/ksscenarios.cpp" line="9"/>
        <source>Tanımlı
Senaryolar</source>
        <translation>Scenarios</translation>
    </message>
</context>
<context>
    <name>KsSimulationEngine</name>
    <message>
        <location filename="../Simulation/kssimulationengine.cpp" line="45"/>
        <source>%1_simrun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Simulation/kssimulationengine.cpp" line="46"/>
        <source>%1/%2_simrun.ksdf</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsSimulationResultReport</name>
    <message>
        <location filename="../Actions/dbActions/kssimulationresultreport.cpp" line="5"/>
        <source>Raporlar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/dbActions/kssimulationresultreport.cpp" line="6"/>
        <source>VERİTABANI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/dbActions/kssimulationresultreport.cpp" line="8"/>
        <source>Simülasyon
Sonuçları</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsSplashScreen</name>
    <message>
        <location filename="../kssplashscreen.cpp" line="13"/>
        <source>KaTSimula</source>
        <translation>KaTSimula</translation>
    </message>
    <message>
        <location filename="../kssplashscreen.cpp" line="14"/>
        <source>0.1</source>
        <translation>0.1</translation>
    </message>
    <message>
        <location filename="../kssplashscreen.cpp" line="15"/>
        <source>KaTSimula: Karayolu Trafik Simülasyonu</source>
        <translation>KaTSimula: Traffic Simulation Software</translation>
    </message>
    <message>
        <location filename="../kssplashscreen.cpp" line="20"/>
        <source>Harita Sağlayıcılar Hazırlanıyor...</source>
        <translation>Preparing Map Providers</translation>
    </message>
</context>
<context>
    <name>KsSpontaneousDB</name>
    <message>
        <location filename="../db/ksspontaneousdb.cpp" line="153"/>
        <source>drop table %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsStartSimulation</name>
    <message>
        <location filename="../Actions/simulation/ksstartsimulation.cpp" line="8"/>
        <source>Simülasyon Kontrolü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/ksstartsimulation.cpp" line="9"/>
        <source>SİMÜLASYON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/ksstartsimulation.cpp" line="11"/>
        <source>Başlat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsStopSimulation</name>
    <message>
        <location filename="../Actions/simulation/ksstopsimulation.cpp" line="5"/>
        <source>Simülasyon Kontrolü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/ksstopsimulation.cpp" line="6"/>
        <source>SİMÜLASYON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Actions/simulation/ksstopsimulation.cpp" line="8"/>
        <source>Durdur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsUndo</name>
    <message>
        <location filename="../Actions/editActions/ksundo.cpp" line="6"/>
        <source>Düzen</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/ksundo.cpp" line="7"/>
        <source>GİRİŞ</source>
        <translation>BASIC OPERATIONS</translation>
    </message>
    <message>
        <location filename="../Actions/editActions/ksundo.cpp" line="9"/>
        <source>Geri Al</source>
        <translation>Undo</translation>
    </message>
</context>
<context>
    <name>KsVehicle</name>
    <message>
        <location filename="../db/model/ksvehicle.cpp" line="7"/>
        <source>Kişisel Araç</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../db/model/ksvehicle.cpp" line="9"/>
        <source>Aile Aracı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../db/model/ksvehicle.cpp" line="11"/>
        <source>İş Aracı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../db/model/ksvehicle.cpp" line="41"/>
        <location filename="../db/model/ksvehicle.cpp" line="55"/>
        <source>b_%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsWorkspace</name>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="128"/>
        <location filename="../workspace/ksworkspace.cpp" line="159"/>
        <source>Uyarı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="129"/>
        <location filename="../workspace/ksworkspace.cpp" line="160"/>
        <source>Projede kaydedilmemiş değişikler var!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="130"/>
        <location filename="../workspace/ksworkspace.cpp" line="161"/>
        <source>Bu değişiklikler kaydedilsin mi?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="133"/>
        <location filename="../workspace/ksworkspace.cpp" line="164"/>
        <source>Kaydet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="134"/>
        <location filename="../workspace/ksworkspace.cpp" line="165"/>
        <source>Kaydetme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../workspace/ksworkspace.cpp" line="135"/>
        <location filename="../workspace/ksworkspace.cpp" line="166"/>
        <source>İptal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsWorldPropertyEditor</name>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksworldpropertyeditor.cpp" line="69"/>
        <source>Sol Üst Nokta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksworldpropertyeditor.cpp" line="70"/>
        <source>Görünen Alan Büyüklüğü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksworldpropertyeditor.cpp" line="73"/>
        <source>Ekranda Görünen Harita Bölümünün Sol Üst Köşe Koordinatı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../propertyEditors/propertyEditors/ksworldpropertyeditor.cpp" line="76"/>
        <source>Ekranda Görünen Harita Bölümünen Büyüklüğü</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KsZoomIn</name>
    <message>
        <location filename="../Actions/viewActions/kszoomin.cpp" line="8"/>
        <source>Çalışma Alanı</source>
        <translation>Workspace</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/kszoomin.cpp" line="9"/>
        <source>GÖRÜNÜM</source>
        <translation>VIEW</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/kszoomin.cpp" line="11"/>
        <source>Yakınlaş</source>
        <translation>Zoom In</translation>
    </message>
</context>
<context>
    <name>KsZoomOut</name>
    <message>
        <location filename="../Actions/viewActions/kszoomout.cpp" line="8"/>
        <source>Çalışma Alanı</source>
        <translation>Workspace</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/kszoomout.cpp" line="9"/>
        <source>GÖRÜNÜM</source>
        <translation>VIEW</translation>
    </message>
    <message>
        <location filename="../Actions/viewActions/kszoomout.cpp" line="11"/>
        <source>Uzaklaş</source>
        <translation>Zoom Out</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="61"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="66"/>
        <source>Görünen Bölge: (%1),(%2) Alan: %3</source>
        <translation>Visible Area: (%1),(%2) Area: %3</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="117"/>
        <source>%1 %2&lt;sup&gt;2&lt;/sup&gt;</source>
        <translation>%1 %2&lt;sup&gt;2&lt;/sup&gt;</translation>
    </message>
</context>
<context>
    <name>PropertyEditor</name>
    <message>
        <source>Özellikler</source>
        <translation type="vanished">Properties</translation>
    </message>
</context>
<context>
    <name>PropertyEditorHolder</name>
    <message>
        <location filename="../widgets/propertyeditorholder.ui" line="20"/>
        <source>Özellikler</source>
        <translation type="unfinished">Properties</translation>
    </message>
    <message>
        <location filename="../widgets/propertyeditorholder.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Bir Nesne Seçiniz.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGoogleHybridProvider</name>
    <message>
        <location filename="../mapProviders/providers/qgooglehybridprovider.cpp" line="16"/>
        <source>Google Hibrid</source>
        <translation>Google Hybrid</translation>
    </message>
</context>
<context>
    <name>QGraphicsMapLayer</name>
    <message>
        <location filename="../graphicObjects/qgraphicsmaplayer.cpp" line="15"/>
        <location filename="../graphicObjects/qgraphicsmaplayer.cpp" line="74"/>
        <source>Trafik Kaynakları/Hedefleri</source>
        <translation>Traffic Sources/Targets</translation>
    </message>
    <message>
        <location filename="../graphicObjects/qgraphicsmaplayer.cpp" line="70"/>
        <source>Dünya Haritası Katmanı</source>
        <translation>World Map</translation>
    </message>
    <message>
        <location filename="../graphicObjects/qgraphicsmaplayer.cpp" line="78"/>
        <source>Yollar</source>
        <translation>Roads</translation>
    </message>
</context>
<context>
    <name>QGraphicsWorldItem</name>
    <message>
        <location filename="../graphicObjects/qgraphicsworlditem.cpp" line="45"/>
        <source>Dünya Haritası</source>
        <translation>World Map</translation>
    </message>
    <message>
        <source>Sol Üst Köşe Koordinatı</source>
        <translation type="vanished">Top Left Coordinate</translation>
    </message>
    <message>
        <source>Haritanın Sol Üst Koordinatı</source>
        <translation type="vanished">Top Left Coordinate of Visible World Map</translation>
    </message>
    <message>
        <source>Görünen Bölüm</source>
        <translation type="vanished">Visible Area</translation>
    </message>
    <message>
        <source>Görünen Bölge Büyüklüğü</source>
        <translation type="vanished">Size of The Visible World Area</translation>
    </message>
</context>
<context>
    <name>QMapLayerModel</name>
    <message>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="119"/>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="126"/>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="141"/>
        <source>Görünür</source>
        <translation>Visible</translation>
    </message>
    <message>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="119"/>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="126"/>
        <source>Gizli</source>
        <translation>Hidden</translation>
    </message>
    <message>
        <location filename="../graphicObjects/qmaplayermodel.cpp" line="138"/>
        <source>Katman Adı</source>
        <translation>Layer Name</translation>
    </message>
</context>
<context>
    <name>QOCMStandardProvider</name>
    <message>
        <location filename="../mapProviders/providers/qocmstandardprovider.cpp" line="16"/>
        <source>OpenCycleMap</source>
        <translation>OpenCycleMap</translation>
    </message>
</context>
<context>
    <name>QOSMStandardProvider</name>
    <message>
        <location filename="../mapProviders/providers/qosmstandardprovider.cpp" line="16"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
</context>
<context>
    <name>QTileDatabase</name>
    <message>
        <location filename="../cache/qtiledatabase.cpp" line="17"/>
        <source>cache/%1</source>
        <translation>cache/%1</translation>
    </message>
    <message>
        <location filename="../cache/qtiledatabase.cpp" line="30"/>
        <source>select count(tile_data) as rs from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3</source>
        <translation>select count(tile_data) as rs from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3</translation>
    </message>
    <message>
        <location filename="../cache/qtiledatabase.cpp" line="75"/>
        <source>select tile_data from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3</source>
        <translation>select tile_data from tiles where zoom_level=%1 and tile_column=%2 and tile_row=%3</translation>
    </message>
</context>
<context>
    <name>QtBoolEdit</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="243"/>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="253"/>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="278"/>
        <source>True</source>
        <translation>True</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="253"/>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="278"/>
        <source>False</source>
        <translation>False</translation>
    </message>
</context>
<context>
    <name>QtBoolPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="1499"/>
        <source>True</source>
        <translation>True</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="1500"/>
        <source>False</source>
        <translation>False</translation>
    </message>
</context>
<context>
    <name>QtCharEdit</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="1577"/>
        <source>Clear Char</source>
        <translation>Clear Char</translation>
    </message>
</context>
<context>
    <name>QtColorEditWidget</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="2182"/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QtColorPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="6226"/>
        <source>Red</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="6234"/>
        <source>Green</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="6242"/>
        <source>Blue</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="6250"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
</context>
<context>
    <name>QtCursorDatabase</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="48"/>
        <source>Arrow</source>
        <translation>Arrow</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="50"/>
        <source>Up Arrow</source>
        <translation>Up Arrow</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="52"/>
        <source>Cross</source>
        <translation>Cross</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="54"/>
        <source>Wait</source>
        <translation>Wait</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="56"/>
        <source>IBeam</source>
        <translation>IBeam</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="58"/>
        <source>Size Vertical</source>
        <translation>Size Vertical</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="60"/>
        <source>Size Horizontal</source>
        <translation>Size Horizontal</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="62"/>
        <source>Size Backslash</source>
        <translation>Size Backslash</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="64"/>
        <source>Size Slash</source>
        <translation>Size Slash</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="66"/>
        <source>Size All</source>
        <translation>Size All</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="68"/>
        <source>Blank</source>
        <translation>Blank</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="70"/>
        <source>Split Vertical</source>
        <translation>Split Vertical</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="72"/>
        <source>Split Horizontal</source>
        <translation>Split Horizontal</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="74"/>
        <source>Pointing Hand</source>
        <translation>Pointing Hand</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="76"/>
        <source>Forbidden</source>
        <translation>Forbidden</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="78"/>
        <source>Open Hand</source>
        <translation>Open Hand</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="80"/>
        <source>Closed Hand</source>
        <translation>Closed Hand</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="82"/>
        <source>What&apos;s This</source>
        <translation>What&apos;s This</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="84"/>
        <source>Busy</source>
        <translation>Busy</translation>
    </message>
</context>
<context>
    <name>QtFontEditWidget</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="2380"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="2400"/>
        <source>Select Font</source>
        <translation>Select Font</translation>
    </message>
</context>
<context>
    <name>QtFontPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5899"/>
        <source>Family</source>
        <translation>Family</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5912"/>
        <source>Point Size</source>
        <translation>Point Size</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5920"/>
        <source>Bold</source>
        <translation>Bold</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5927"/>
        <source>Italic</source>
        <translation>Italic</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5934"/>
        <source>Underline</source>
        <translation>Underline</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5941"/>
        <source>Strikeout</source>
        <translation>Strikeout</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5948"/>
        <source>Kerning</source>
        <translation>Kerning</translation>
    </message>
</context>
<context>
    <name>QtLocalePropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2402"/>
        <source>&lt;Invalid&gt;</source>
        <translation>&lt;Invalid&gt;</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2410"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2460"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2468"/>
        <source>Country</source>
        <translation>Country</translation>
    </message>
</context>
<context>
    <name>QtPointFPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2879"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2950"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2958"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
</context>
<context>
    <name>QtPointPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2638"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2675"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="2682"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
</context>
<context>
    <name>QtPropertyBrowserUtils</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="177"/>
        <source>[%1, %2, %3] (%4)</source>
        <translation>[%1, %2, %3] (%4)</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="204"/>
        <source>[%1, %2]</source>
        <translation>[%1, %2]</translation>
    </message>
</context>
<context>
    <name>QtRectFPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4388"/>
        <source>[(%1, %2), %3 x %4]</source>
        <translation>[(%1, %2), %3 x %4]</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4544"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4552"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4560"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4569"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>QtRectPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3957"/>
        <source>[(%1, %2), %3 x %4]</source>
        <translation>[(%1, %2), %3 x %4]</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4077"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4084"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4091"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="4099"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>QtSizeFPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3564"/>
        <source>%1 x %2</source>
        <translation>%1 x %2</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3694"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3703"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>QtSizePolicyPropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5412"/>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5413"/>
        <source>&lt;Invalid&gt;</source>
        <translation>&lt;Invalid&gt;</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5414"/>
        <source>[%1, %2, %3, %4]</source>
        <translation>[%1, %2, %3, %4]</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5459"/>
        <source>Horizontal Policy</source>
        <translation>Horizontal Policy</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5468"/>
        <source>Vertical Policy</source>
        <translation>Vertical Policy</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5477"/>
        <source>Horizontal Stretch</source>
        <translation>Horizontal Stretch</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="5485"/>
        <source>Vertical Stretch</source>
        <translation>Vertical Stretch</translation>
    </message>
</context>
<context>
    <name>QtSizePropertyManager</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3199"/>
        <source>%1 x %2</source>
        <translation>%1 x %2</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3295"/>
        <source>Width</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="3303"/>
        <source>Height</source>
        <translation>Height</translation>
    </message>
</context>
<context>
    <name>QtTreePropertyBrowser</name>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qttreepropertybrowser.cpp" line="434"/>
        <source>Property</source>
        <translation>Property</translation>
    </message>
    <message>
        <location filename="../../../Qt/5.4/Src/qttools/src/shared/qtpropertybrowser/qttreepropertybrowser.cpp" line="435"/>
        <source>Value</source>
        <translation>Value</translation>
    </message>
</context>
<context>
    <name>YeniAracTanimlama</name>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="14"/>
        <source>Araç Tanımlama</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="24"/>
        <source>Genel Bilgiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="54"/>
        <source>Ad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="64"/>
        <source>Açıklama</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="74"/>
        <source>En Yüksek Hız</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="81"/>
        <source>Koltuk Sayısı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="88"/>
        <source>Araç Türü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="105"/>
        <source>Boyut Bilgileri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="132"/>
        <source>Genişlik</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="142"/>
        <source>Yükseklik</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="152"/>
        <source>Ağırlık</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="162"/>
        <source>Aks Yükü</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="172"/>
        <source>Karbon Emisyon Oranı</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="182"/>
        <source>Uzunluk </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.ui" line="198"/>
        <location filename="../widgets/yeniaractanimlama.ui" line="215"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../widgets/yeniaractanimlama.cpp" line="17"/>
        <source>:/img/vehicles/%1.png</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
