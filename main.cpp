#include "mainwindow.h"
#include <QApplication>
#include "kssplashscreen.h"

#include <QTranslator>

#include <QTime>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime::currentTime().msec());

    QTranslator trans;

    trans.load(":/translations/tr.qm");

    QCoreApplication::installTranslator(&trans);

    // MainWindow w;
    QImage img;
    img.load(":/img/icon.png");
    QPixmap pxMap = QPixmap::fromImage(img);
    qApp->setWindowIcon(QIcon(pxMap));

    KsSplashScreen sScreen;
    sScreen.show();
    qApp->processEvents();
    sScreen.startLoading();
    qApp->processEvents();

    return a.exec();
}
