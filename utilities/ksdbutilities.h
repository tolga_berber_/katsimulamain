#ifndef KSDBUTILITIES_H
#define KSDBUTILITIES_H

#include <QSqlDatabase>

class KsDbUtilities
{
public:
    static QSqlDatabase openSqliteDatabase(QString directory, QString file, QString dbName);
};

#endif // KSDBUTILITIES_H
