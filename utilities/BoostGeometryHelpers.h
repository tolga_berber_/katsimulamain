#ifndef BOOSTGEOMETRYHELPERS_H
#define BOOSTGEOMETRYHELPERS_H

#ifndef Q_MOC_RUN
#pragma GCC diagnostic push
#ifndef __MACH__
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif
#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/geometries/register/box.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/extensions/gis/latlong/latlong.hpp>
#include <boost/geometry/extensions/gis/geographic/strategies/area_huiller_earth.hpp>
#pragma GCC diagnostic pop
#endif

namespace bg = boost::geometry;

typedef bg::model::ll::point<bg::degree, qreal> Point;
typedef bg::model::polygon<Point> Polygon;
typedef bg::model::box<Point> Rect;
typedef bg::model::segment<Point> Line;
typedef bg::model::linestring<Point> LineString;

#endif // BOOSTGEOMETRYHELPERS_H
