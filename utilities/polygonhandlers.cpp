#include "polygonhandlers.h"

#include <QLineF>
#include <QtMath>

QPolygonF PolygonHandlers::calculateParalelPolygon(const QPolygonF &initialPolygon, float distance)
{
    QPolygonF result;

    if(initialPolygon.count()>2) {

        for(int i=0;i<initialPolygon.count()-2;i++) {
            QLineF segment1(initialPolygon[i], initialPolygon[i+1]);
            QLineF normalUnitVector1 = segment1.normalVector().unitVector();
            normalUnitVector1.setLength(distance);

            QLineF segment2(initialPolygon[i+1], initialPolygon[i+2]);
            QLineF normalUnitVector2 = segment2.normalVector().unitVector();
            normalUnitVector2.setLength(distance);

            if(!i) {
                QPointF initialPoint = initialPolygon[i];
                initialPoint.rx() += normalUnitVector1.dx();
                initialPoint.ry() += normalUnitVector1.dy();
                result.append(initialPoint);
            }

            segment1.translate(normalUnitVector1.dx(), normalUnitVector1.dy());
            segment2.translate(normalUnitVector2.dx(), normalUnitVector2.dy());

            QPointF intersectionPoint;

            if(segment1.intersect(segment2, &intersectionPoint)) {
                result.append(intersectionPoint);
            } else {
                QPointF initialPoint = initialPolygon[i+1];
                initialPoint.rx() += normalUnitVector2.dx();
                initialPoint.ry() += normalUnitVector2.dy();
                result.append(initialPoint);
            }

        }
    }

    QLineF finalSegment(initialPolygon.last(), initialPolygon[initialPolygon.count()-2]);
    QLineF normalUnitVectorFinal = finalSegment.normalVector().unitVector();
    normalUnitVectorFinal.setLength(-distance);

    QPointF finalPoint = initialPolygon.last();
    finalPoint.rx() += normalUnitVectorFinal.dx();
    finalPoint.ry() += normalUnitVectorFinal.dy();

    if(initialPolygon.count()==2) {
        QPointF initialPoint = initialPolygon.first();
        initialPoint.rx() += normalUnitVectorFinal.dx();
        initialPoint.ry() += normalUnitVectorFinal.dy();
        result.append(initialPoint);
    }
    result.append(finalPoint);

    return result;
}

QPolygonF PolygonHandlers::calculateBezier(const QPolygonF &initialPolygon)
{
    QPolygonF bezierPolygon;
    QPolygonF finalPolygon;
    bezierPolygon<<initialPolygon;
    bezierPolygon.prepend(initialPolygon.first());
    bezierPolygon.append(initialPolygon.last());
    for(int i=0;i<=bezierPolygon.length()-4;i++) {
        finalPolygon<<calculateBezierPoints(bezierPolygon[i],
                                            bezierPolygon[i+1],
                bezierPolygon[i+2],
                bezierPolygon[i+3]);

    }
    return finalPolygon;
}

bool PolygonHandlers::isPolygonClockWise(const QPolygonF &initialPoly)
{
    qreal edge = 0;

    qreal x1 = initialPoly.last().x();
    qreal y1 = initialPoly.last().y();

    for(int offset=0;offset<initialPoly.count();offset++) {
        qreal x2 = initialPoly[offset].x();
        qreal y2 = initialPoly[offset].y();

        edge += (x2-x1)*(y2+y1);

        x1=x2;
        y1=y2;
    }

    return edge>0;
}

QPolygonF PolygonHandlers::sortClockWise(const QPolygonF &initialPoly)
{
    QPolygonF result;
    QList<qreal> polarAngles;

    foreach (QPointF p, initialPoly) {
        result.append(p);
        qreal phi = qAtan2(p.y(), p.x());
        polarAngles.append(phi);
    }

    for(int i=0;i<polarAngles.count();i++) {
        polarAngles[i] -= polarAngles[0];
        if(polarAngles[i]<0) {
            polarAngles[i] = M_PI-polarAngles[i];
        }
    }

    for(int i=0;i<polarAngles.count();i++) {
        for(int j=i+1;j>0;j--) {
            if(polarAngles[j]<polarAngles[i]) {
                polarAngles.swap(i,j);
                QPointF &p = result[i];
                result[i] = result[j];
                result[j] = p;
            }
        }
    }

    return result;
}

QPolygonF PolygonHandlers::calculateBezierPoints(const QPointF &s1, const QPointF &s2, const QPointF &s3, const QPointF &s4, double step)
{
    BezierControlPoints S1,S2;

    findControlPoints(s1,s2,s3,S1);
    findControlPoints(s2,s3,s4,S2);

    int ctr = qFloor(((S1.l1==0?S1.l2:S1.l1)+(S2.l1==0?S2.l2:S2.l1))/step);

    QPointF p1 = s2;
    QPointF p2 = S1.c2;
    QPointF p3 = S2.c1;
    QPointF p4 = s3;

    double ss = 1.0 / (ctr+1.0), ss2 = ss*ss, ss3 = ss2*ss;

    double pre1 = 3.0*ss;
    double pre2 = 3.0*ss2;
    double pre4 = 6.0*ss2;
    double pre5 = 6.0*ss3;

    double tmp1x = p1.x()-p2.x()*2.0+p3.x();
    double tmp1y = p1.y()-p2.y()*2.0+p3.y();

    double tmp2x = (p2.x()-p3.x())*3.0-p1.x()+p4.x();
    double tmp2y = (p2.y()-p3.y())*3.0-p1.y()+p4.y();

    QPointF pf = p1;

    double dfx = (p2.x()-p1.x())*pre1+tmp1x*pre2+tmp2x*ss3;
    double dfy = (p2.y()-p1.y())*pre1+tmp1y*pre2+tmp2y*ss3;

    double ddfx = tmp1x*pre4+tmp2x*pre5;
    double ddfy = tmp1y*pre4+tmp2y*pre5;

    double dddfx = tmp2x*pre5;
    double dddfy = tmp2y*pre5;

    QPolygonF result;

    while(ctr--) {
        pf += QPointF(dfx, dfy);

        dfx += ddfx;
        dfy += ddfy;

        ddfx += dddfx;
        ddfy += dddfy;

        result<<pf;
    }

    result<<p4;


    return result;
}

void PolygonHandlers::findControlPoints(const QPointF &s1, const QPointF &s2, const QPointF &s3, BezierControlPoints &bcp)
{
    double dx1 = s1.x()-s2.x();
    double dy1 = s1.y()-s2.y();

    double dx2 = s2.x()-s3.x();
    double dy2 = s2.y()-s3.y();

    bcp.l1 = qSqrt(dx1*dx1+dy1*dy1);
    bcp.l2 = qSqrt(dx2*dx2+dy2*dy2);

    QPointF m1((s1.x()+s2.x())/2.0, (s1.y()+s2.y())/2.0);
    QPointF m2((s2.x()+s3.x())/2.0, (s2.y()+s3.y())/2.0);

    double dxm = m1.x()-m2.x();
    double dym = m1.y()-m2.y();

    double k = bcp.l2/(bcp.l1+bcp.l2);

    QPointF cm(m2.x()+dxm*k, m2.y()+dym*k);

    double tx = s2.x()-cm.x();
    double ty = s2.y()-cm.y();

    bcp.c1.rx() = m1.x()+tx;
    bcp.c1.ry() = m1.y()+ty;

    bcp.c2.rx() = m2.x()+tx;
    bcp.c2.ry() = m2.y()+ty;
}



