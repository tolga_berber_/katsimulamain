#include "ksgeometryhelper.h"
#include "BoostGeometryHelpers.h"
#include <QMutex>

#include <QtMath>


KsGeometryHelper *KsGeometryHelper::s_pcSharedInstance = NULL;

inline Point convertToBoost(const QPointF &src) {
    return Point(bg::longitude<qreal>(src.x()), bg::latitude<qreal>(src.y()));
}

inline Line convertToBoost(const QLineF &src) {
    return Line(convertToBoost(src.p1()), convertToBoost(src.p2()));
}

inline Polygon convertToBoost(const QPolygonF &src) {
    Polygon result;
    foreach (const QPointF &pnt, src) {
        bg::append(result, convertToBoost(pnt));
    }
    return result;
}

inline Rect convertToBoost(const QRectF& src) {
    return Rect(convertToBoost(src.topLeft()), convertToBoost(src.bottomRight()));
}

KsGeometryHelper::KsGeometryHelper()
{
}

KsGeometryHelper *KsGeometryHelper::sharedInstance()
{
    static QMutex mutex;

    if(!s_pcSharedInstance) {
        mutex.lock();
        if(!s_pcSharedInstance) {
            s_pcSharedInstance = new KsGeometryHelper();
        }
        mutex.unlock();
    }

    return s_pcSharedInstance;
}

qreal KsGeometryHelper::length(const QLineF &src)
{
    qreal R = 6371000;
    qreal phi1 = qDegreesToRadians(src.p1().y());
    qreal phi2 = qDegreesToRadians(src.p1().y());
    qreal deltaphi = qDegreesToRadians(src.p2().y()-src.p1().y())/2;
    qreal deltalamda = qDegreesToRadians(src.p2().x()-src.p1().x())/2;

    qreal a = qSin(deltaphi)*qSin(deltaphi)+qCos(phi1)*qCos(phi2)*qSin(deltalamda)*qSin(deltalamda);

    qreal c = 2*qAtan2(qSqrt(a), qSqrt(1-a));

    qreal d = R*c;

    return d;
}

qreal KsGeometryHelper::length(const QPolygonF &src)
{
    qreal result = 0;
    for(int i=0;i<src.count()-1;i++) {
        result += length(QLineF(src[i], src[i+1]));
    }

    return result;
}

qreal KsGeometryHelper::length(const QRectF &src)
{
    qreal result = 0;

    result += length(QLineF(src.topLeft(), src.topRight()));
    result += length(QLineF(src.topRight(), src.bottomRight()));
    result += length(QLineF(src.bottomRight(), src.bottomLeft()));
    result += length(QLineF(src.bottomLeft(), src.topLeft()));

    return result;
}

QPointF KsGeometryHelper::lengthToPolygonLocation(const QPolygonF &poly, qreal targetLength)
{

    QLineF tLine;

    for(int i=0;i<poly.count()-1;i++) {
        tLine.setP1(poly[i]);
        tLine.setP2(poly[i+1]);

        qreal finalLength = length(tLine);


        if(targetLength<finalLength) {
            qreal r = targetLength/finalLength;
            return tLine.pointAt(r);
        }

        targetLength -= finalLength;
    }

    return QPointF();
}

qreal KsGeometryHelper::area(const QPolygonF &src)
{
    Polygon p = convertToBoost(src);
    return bg::area(p);
}

qreal KsGeometryHelper::area(const QRectF &src)
{
    Rect r = convertToBoost(src);
    return bg::area(r);
}
