#ifndef POLYGONHANDLERS_H
#define POLYGONHANDLERS_H

#include <QPolygonF>

struct BezierControlPoints {
    QPointF c1;
    QPointF c2;
    double l1;
    double l2;
};

class PolygonHandlers
{
public:
    static QPolygonF calculateParalelPolygon(const QPolygonF &initialPolygon, float distance);

    static QPolygonF calculateBezier(const QPolygonF &initialPolygon);

    static bool isPolygonClockWise(const QPolygonF &initialPoly);

    static QPolygonF sortClockWise(const QPolygonF &initialPoly);

private:
    static QPolygonF calculateBezierPoints(const QPointF &s1, const QPointF &s2, const QPointF &s3,
                                    const QPointF &s4, double step=0.0001);

    static void findControlPoints(const QPointF &s1, const QPointF &s2, const QPointF &s3,
                           BezierControlPoints &bcp);
};

#endif // POLYGONHANDLERS_H
