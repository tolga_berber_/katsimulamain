#ifndef KSGEOMETRYHELPER_H
#define KSGEOMETRYHELPER_H

#include <QPointF>
#include <QLineF>
#include <QPolygonF>
#include <QRectF>

#include "BoostGeometryHelpers.h"

class KsGeometryHelper
{
private:
    explicit KsGeometryHelper();
    static KsGeometryHelper *s_pcSharedInstance;

public:

    static KsGeometryHelper *sharedInstance();

    inline QPointF convertFromQt(const QPointF &src) {
        return QPointF(src.x(),-1.0*src.y());
    }

    inline QLineF convertFromQt(const QLineF &src) {
        return QLineF(convertFromQt(src.p1()),convertFromQt(src.p2()));
    }

    inline QPolygonF convertFromQt(const QPolygonF &src) {
        QPolygonF result;
        foreach (const QPointF &p1, src) {
            result<<convertFromQt(p1);
        }
        return result;
    }

    inline QRectF convertFromQt(const QRectF &src) {
        return QRectF(convertFromQt(src.topLeft()), convertFromQt(src.bottomRight()));
    }

    qreal length(const QLineF &src);
    qreal length(const QPolygonF &src);
    qreal length(const QRectF &src);

    QPointF lengthToPolygonLocation(const QPolygonF &poly, qreal targetLength);

    qreal area(const QPolygonF &src);
    qreal area(const QRectF &src);
};

#endif // KSGEOMETRYHELPER_H
